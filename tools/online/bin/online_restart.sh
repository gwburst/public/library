#!/usr/bin/bash -l

conf=$1
log=$2
opt=$3

if [ ! -e $log ]; then
    touch $log
fi

echo $log $conf $opt
cwb_online_exec restart $conf $log $opt

