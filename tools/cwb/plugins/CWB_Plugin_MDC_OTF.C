/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


//!NOISE_MDC_SIMULATION
// Plugin to injected MDC 'on the fly'  

#define XIFO 4

#pragma GCC system_header

#include "cwb.hh"
#include "cwb2G.hh"
#include "config.hh"
#include "network.hh"
#include "wavearray.hh"
#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TRandom.h"
#include "TComplex.h"
#include "TMath.h"
#include "mdc.hh"
#include "xmdc.hh"
#include "TMD5.h"
#include <vector>

// ---------------------------------------------------------------------------------
// DEFINES
// ---------------------------------------------------------------------------------

#define XMDC_FNAME       "" 
#define XMDC_STEP       100
#define XMDC_JITTER     10
#define XMDC_OFFSET     50
#define XMDC_CONFIG      "" 
#define XMDC_READ        "random" 
#define XMDC_INJ_SIZE    1 
#define MDC_OTF_DUMP_LOG false
#define MDC_OTF_BATCH_LOG false
#define CCAST(PAR) const_cast<char*>(PAR)

// ---------------------------------------------------------------------------------
// USER PLUGIN OPTIONS
// ---------------------------------------------------------------------------------

struct uoptions {
  TString  xmdc_fname;
  double   xmdc_step;
  double   xmdc_jitter;
  double   xmdc_offset;
  TString  xmdc_config;
  TString  xmdc_read;
  int	   xmdc_inj_size;
  bool	   mdc_otf_dump_log;
  bool	   mdc_otf_batch_log;
};

// ---------------------------------------------------------------------------------
// Global Variables
// ---------------------------------------------------------------------------------

uoptions        gOPT;                           // global User Options

TString 	odir_xmdc="";			// temporary xmdc output directory

// ---------------------------------------------------------------------------------
// FUNCTIONS
// ---------------------------------------------------------------------------------

void ResetUserOptions();
void ReadUserOptions(TString options);
void PrintUserOptions();

void 
CWB_Plugin(TFile* jfile, CWB::config* cfg, network* net, WSeries<double>* x, TString ifo, int type)  {

  if(type==CWB_PLUGIN_CONFIG) {  
    cfg->mdcPlugin=true;		// disable read mdc from frames

    // --------------------------------------------------------------------
    // read plugin user options
    // --------------------------------------------------------------------

    ResetUserOptions();			// set default config options
    ReadUserOptions(cfg->parPlugin);	// user config options : read from parPlugin
    PrintUserOptions();

    if(gOPT.xmdc_inj_size<=0) {
      cout << endl << "CWB_Plugin_MDC_OTF.C: Error input plugin parameters xmdc_inj_size must be >0"<< endl << endl;
      gSystem->Exit(1);
    }
  }

  if(type==CWB_PLUGIN_IREADDATA) {  

    if(gOPT.xmdc_config=="") return;

    int gIFACTOR=-1; IMPORT(int,gIFACTOR)

    TString xmdc_config = gOPT.xmdc_config;
    cout << "CWB_Plugin_MDC_OTF.C: XMDC config file name -> " << xmdc_config << endl;

    TString cwb_scripts = TString(gSystem->Getenv("CWB_SCRIPTS"));
    unsigned int Pid = gSystem->GetPid();	// used to tag in a unique way the temporary files 
    TString tag = gSystem->BaseName(xmdc_config);
    tag.ReplaceAll(".C","_xmdc");
    tag = TString::Format("%s_%d",tag.Data(),Pid); 
    int seed = 100*(net->nRun)+gIFACTOR;	// WARNING : seed must be the same for each detector within the same job
    TString odir = cfg->nodedir;
    char command[1024];
    sprintf(command,"yes | %s/cwb_xmdc.csh %s '--ninj %d --seed %d --tag %s --odir %s --rtype none'",
                    cwb_scripts.Data(),xmdc_config.Data(),gOPT.xmdc_inj_size,seed,tag.Data(),odir.Data());
    cout << endl << "exec -> " << command << endl << endl;

    int ret=gSystem->Exec(command);
    if(ret) {
      cout << "CWB_Plugin_MDC_OTF.C: Error while executing command:\n\n" << command << endl << endl;
      gSystem->Exit(1);
    }

    odir_xmdc = TString::Format("%s/%s_ninj_%d_seed_%d",odir.Data(),tag.Data(),gOPT.xmdc_inj_size,seed);
    TString ofname = TString::Format("%s/%s/xmdc_%s_ninj_%d_seed_%d.root",
                                     odir_xmdc.Data(),XMDC_SUBDIR_DATA,tag.Data(),gOPT.xmdc_inj_size,seed);
    sprintf(cfg->parPlugin,"xmdc_fname=%s xmdc_read=%s xmdc_step=%f xmdc_jitter=%f xmdc_offset=%f",
                           ofname.Data(),gOPT.xmdc_read.Data(),gOPT.xmdc_step,gOPT.xmdc_jitter,gOPT.xmdc_offset);
  }

  if(type==CWB_PLUGIN_MDC) {  

    cout << "Execute CWB_Plugin_MDC_OTF.C : Inject On The Fly MDC ..." << endl;

    // ---------------------------------
    // Declare mdc class 
    // On The Fly MDC Injections
    // ---------------------------------

    CWB::mdc MDC(net);
    CWB_PLUGIN_EXPORT(MDC)

    // export global variables to the config plugin
    int gIFACTOR=-1; IMPORT(int,gIFACTOR)
    int xstart = (int)x->start();
    int xstop  = (int)x->stop();
    CWB_PLUGIN_EXPORT(net)
    CWB_PLUGIN_EXPORT(cfg)
    CWB_PLUGIN_EXPORT(xstart)
    CWB_PLUGIN_EXPORT(xstop)
    CWB_PLUGIN_EXPORT(gIFACTOR)
    int gIFOID=0; for(int n=0;n<cfg->nIFO;n++) if(ifo==net->getifo(n)->Name) {gIFOID=n;break;}
    CWB_PLUGIN_EXPORT(gIFOID)

    // ---------------------------------
    // read plugin config 
    // CWB_Plugin_MDC_OTF_Config.C
    // ---------------------------------

    int error=0;
    // execute config plugin
    cfg->configPlugin.Exec(NULL,&error);
    if(error) {
      cout << "Error executing macro : " << cfg->configPlugin.GetTitle() << endl;
      cfg->configPlugin.Print(); 
      gSystem->Exit(1); 
    }
    // print list waveforms declared in the config plugin 
    MDC.Print();

    // ---------------------------------
    // get mdc data
    // fill x array with MDC injections
    // ---------------------------------

    MDC.Get(*x,ifo);

    // ---------------------------------
    // set mdc list in the network class 
    // ---------------------------------

    if(ifo.CompareTo(net->ifoName[0])==0) {
      net->mdcList.clear();
      net->mdcType.clear();
      net->mdcTime.clear();
      net->mdcList=MDC.mdcList;
      net->mdcType=MDC.mdcType;
      net->mdcTime=MDC.mdcTime;
    }

    // ---------------------------------
    // write MDC log file  
    // if enabled then the txt log file is created under the output dir
    // the cwb_merge collect all log job files on a single file under the merge directory
    // ---------------------------------

    if(gOPT.mdc_otf_dump_log) {
      char logFile[512];
      int runID = net->nRun;
      int Tb=x->start()+cfg->segEdge;
      int dT=x->size()/x->rate()-2*cfg->segEdge;
      TString data_label = cfg->data_label;
      data_label.ReplaceAll("-","_");       // in frame file name '-' is a special character used to extract the gps time
      if(gOPT.mdc_otf_batch_log) 
        sprintf(logFile,"%s/log-%s_%d_job%d-%d-%d.txt",cfg->output_dir,data_label.Data(),gIFACTOR,runID,int(Tb),int(dT));
      else 
        sprintf(logFile,"%s/log-%s_%d_job%d-%d-%d.txt",cfg->data_dir,data_label.Data(),gIFACTOR,runID,int(Tb),int(dT));
      cout << "Dump : " << logFile << endl;
      if(ifo==cfg->ifo[0]) MDC.DumpLog(logFile);
    }

    // ---------------------------------
    // print MDC injections list 
    // ---------------------------------

    cout.precision(14);
    if(ifo.CompareTo(net->ifoName[0])==0) {
      for(int k=0;k<(int)net->mdcList.size();k++) cout << k << " mdcList " << MDC.mdcList[k] << endl;
      for(int k=0;k<(int)net->mdcTime.size();k++) cout << k << " mdcTime " << MDC.mdcTime[k] << endl;
      for(int k=0;k<(int)net->mdcType.size();k++) cout << k << " mdcType " << MDC.mdcType[k] << endl;
    }
  }

  if(type==CWB_PLUGIN_ICOHERENCE) {
    if(odir_xmdc!="") { 		// remove xmdc temporary file
      TString command = TString::Format("rm -r %s",odir_xmdc.Data());
      unsigned int Pid = gSystem->GetPid();
      if(TString(gSystem->BaseName(odir_xmdc)).Contains(TString::Format("%d",Pid))) {
        int ret=gSystem->Exec(command.Data());
        if(ret) {
          cout << "CWB_Plugin_MDC_OTF.C: Error while executing command:\n\n" << command << endl << endl;
          gSystem->Exit(1);
        }
      }
    }
  }

  if(type==CWB_PLUGIN_OLIKELIHOOD) {

    TString xmdc_fname = gOPT.xmdc_fname;
    //cout << "CWB_Plugin_MDC_OTF.C: XMDC injection file name -> " << xmdc_fname << endl;

    if(xmdc_fname=="") return;		// check if mdc uses the xmdc injection file

    // --------------------------------------------------------------------
    // add xmdc history to output history wave file
    // --------------------------------------------------------------------

    cwb2G* gCWB2G; IMPORT(cwb2G*,gCWB2G)
    CWB::History* gHISTORY = gCWB2G->history;
    if(gHISTORY==NULL) {
      cout << "CWB_Plugin_MDC_OTF.C : Error - gHYSTORY is null" << endl;
      gSystem->Exit(1);
    }

    // update history
    TString xmdc_stage_name = CWB::xmdc::GetStageName();
    gHISTORY->SetHistoryModify(true);
    if(!gHISTORY->StageAlreadyPresent(CCAST(xmdc_stage_name.Data())))   gHISTORY->AddStage(CCAST(xmdc_stage_name.Data()));
    if(!gHISTORY->TypeAllowed(CCAST("XMDC_FILE_NAME"))) gHISTORY->AddType(CCAST("XMDC_FILE_NAME"));
    if(!gHISTORY->TypeAllowed(CCAST("XMDC_FILE_MD5")))  gHISTORY->AddType(CCAST("XMDC_FILE_MD5"));

    // save xmdc injection file name
    if(!xmdc_fname.BeginsWith("/")) xmdc_fname = gSystem->ExpandPathName("$PWD/"+xmdc_fname);
    gHISTORY->AddHistory(const_cast<char*>(xmdc_stage_name.Data()), const_cast<char*>("XMDC_FILE_NAME"), const_cast<char*>(xmdc_fname.Data()));
    // save xmdc injection file md5 sum
    TMD5* md5 = TMD5::FileChecksum(xmdc_fname.Data());
    gHISTORY->AddHistory(const_cast<char*>(xmdc_stage_name.Data()), const_cast<char*>("XMDC_FILE_MD5"), const_cast<char*>(md5->AsString()));
    delete md5;
  }

  return;
}

void PrintUserOptions() {

    cout << "-----------------------------------------"     << endl;
    cout << "inj plugin config options                "     << endl;
    cout << "-----------------------------------------"     << endl << endl;

    cout << "XMDC_FNAME        " << gOPT.xmdc_fname << endl;
    cout << "XMDC_STEP         " << gOPT.xmdc_step << endl;
    cout << "XMDC_JITTER       " << gOPT.xmdc_jitter << endl;
    cout << "XMDC_OFFSET       " << gOPT.xmdc_offset << endl;
    cout << "XMDC_CONFIG       " << gOPT.xmdc_config << endl;
    cout << "XMDC_READ         " << gOPT.xmdc_read << endl;
    cout << "XMDC_INJ_SIZE     " << gOPT.xmdc_inj_size << endl;
    cout << "MDC_OTF_DUMP_LOG  " << gOPT.mdc_otf_dump_log << endl;
    cout << "MDC_OTF_BATCH_LOG " << gOPT.mdc_otf_batch_log << endl;

    cout << endl;
}

void ReadUserOptions(TString options) {

  if(options.CompareTo("")!=0) {
    cout << options << endl;
    if(!options.Contains("--")) {  // parameters are used only by cwb_inet

      TObjArray* token = TString(options).Tokenize(TString(' '));
        for(int j=0;j<token->GetEntries();j++){

        TObjString* tok = (TObjString*)token->At(j);
        TString stok = tok->GetString();

        if(stok.Contains("xmdc_fname=")) {
          TString xmdc_fname=stok;
          xmdc_fname.Remove(0,xmdc_fname.Last('=')+1);
          gOPT.xmdc_fname=xmdc_fname;
        }

        if(stok.Contains("xmdc_step=")) {
          TString xmdc_step=stok;
          xmdc_step.Remove(0,xmdc_step.Last('=')+1);
          if(xmdc_step.IsFloat()) gOPT.xmdc_step=xmdc_step.Atof();
        }

        if(stok.Contains("xmdc_jitter=")) {
          TString xmdc_jitter=stok;
          xmdc_jitter.Remove(0,xmdc_jitter.Last('=')+1);
          if(xmdc_jitter.IsFloat()) gOPT.xmdc_jitter=xmdc_jitter.Atof();
        }

        if(stok.Contains("xmdc_offset=")) {
          TString xmdc_offset=stok;
          xmdc_offset.Remove(0,xmdc_offset.Last('=')+1);
          if(xmdc_offset.IsFloat()) gOPT.xmdc_offset=xmdc_offset.Atof();
        }

        if(stok.Contains("xmdc_config=")) {
          TString xmdc_config=stok;
          xmdc_config.Remove(0,xmdc_config.Last('=')+1);
          gOPT.xmdc_config=xmdc_config;
        }

        if(stok.Contains("xmdc_read=")) {
          TString xmdc_read=stok;
          xmdc_read.Remove(0,xmdc_read.Last('=')+1);
          gOPT.xmdc_read=xmdc_read;
        }

        if(stok.Contains("xmdc_inj_size=")) {
          TString xmdc_inj_size=stok;
          xmdc_inj_size.Remove(0,xmdc_inj_size.Last('=')+1);
          if(xmdc_inj_size.IsDigit()) gOPT.xmdc_inj_size=xmdc_inj_size.Atoi();
        }

        if(stok.Contains("mdc_otf_dump_log=")) {
          TString mdc_otf_dump_log=stok;
          mdc_otf_dump_log.Remove(0,mdc_otf_dump_log.Last('=')+1);
          gOPT.mdc_otf_dump_log=(mdc_otf_dump_log=="true")?true:false;
        }

        if(stok.Contains("mdc_otf_batch_log=")) {
          TString mdc_otf_batch_log=stok;
          mdc_otf_batch_log.Remove(0,mdc_otf_batch_log.Last('=')+1);
          gOPT.mdc_otf_batch_log=(mdc_otf_batch_log=="true")?true:false;
        }
      }
    }
  }
}

void ResetUserOptions() {

  gOPT.xmdc_fname        = XMDC_FNAME;
  gOPT.xmdc_step   	 = XMDC_STEP;
  gOPT.xmdc_jitter 	 = XMDC_JITTER;
  gOPT.xmdc_offset 	 = XMDC_OFFSET;
  gOPT.xmdc_config       = XMDC_CONFIG;
  gOPT.xmdc_read         = XMDC_READ;
  gOPT.xmdc_inj_size     = XMDC_INJ_SIZE;
  gOPT.mdc_otf_dump_log  = MDC_OTF_DUMP_LOG;
  gOPT.mdc_otf_batch_log = MDC_OTF_BATCH_LOG;

}


