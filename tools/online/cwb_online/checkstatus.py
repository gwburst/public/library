#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago, Andrea Miani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess, os, sys, glob
import ligo.segments.utils as seg_ut
from cwb_online.detector import detector
from cwb_online.frame import FRAME
from cwb_online.run_utils import get_time, tconvert, sconvert, get_date_from_gps

def getratio(bkg_fin,bkg_st):
  if (len(bkg_st)>0):
    ratio=100.*len(bkg_fin)/len(bkg_st)
  else:
    ratio=0.
  return ratio

def send_mails(line,cWB_conf):
    msg="""Subject:  Check online status at %s %s

%s"""%(os.environ['SITE_CLUSTER'],cWB_conf.online_dir,line)
    #print msg
    com="echo \"%s\" | /usr/sbin/sendmail -F %sonline-%s %s"%(msg,cWB_conf.gracedb_analysis,cWB_conf.gracedb_search,",".join(cWB_conf.error_emails))
    subprocess.getstatusoutput(com)
def return_error(value,time):
  timelimit=5
  machine=os.environ["HOSTNAME"]
  if (value<0):
    return "ERROR","LOG FILE MISSING"
  elif (value>0):
    return "OK","RUNNING AT %s"%machine
  elif (time<timelimit):
    return "WARNING","NOT RUNNING AT %s, LAST UPDATE: %i s"%(machine,time)
  else:
    return "ERROR","NOT RUNNING AT %s, LAST UPDATE: %i s"%(machine,time)
def def_colors(w):
    if (w=="ERROR"):
      return "red"
    if (w=="WARNING"):
      return "yellow"
    if (w=="OK"):
      return "limegreen"
def findnotfin(started,pos_s,finished,pos_f):
  started.sort()
  started_time=[x.split("/")[pos_s] for x in started]
  st=segmentlist([])
  for s in started_time:
    st.append(segment(s.split("-")[0],s.split("-")[1]))
  st.coalesce()
  finished.sort()
  finished_time=[x.split("/")[pos_f] for x in finished]
  fin=segmentlist([])
  for f in finished_time:
    fin.append(segment(f.split("-")[0],f.split("-")[1]))
  fin.coalesce()
  st-=fin
  #print repr(st)
  logs=[]
  for s in st:
    start=float(s[0])
    end=start+moving_step
    while(end<=float(s[1])):
      dirs=[x for x in started if x.find("%i-%i"%(start,end))!=-1]
      if (len(dirs)>0):
        #print repr(dirs)
        logs.append(dirs[0])
        start+=moving_step
        end+=moving_step
      else:
        end+=moving_step
  return logs

def analyzedir(idir,iszero):
   subdir="finished"
   if (iszero==True):
     subdir="OUTPUT/%s"%subdir
   if (os.path.exists("%s/%s"%(idir,subdir))):
     return True
   else:
     return False

def analyzedirs(list_dates,iszero):
  started=[]
  finished=[]
  not_finished=[]
  for l in list_dates:
    dirs=glob.glob("%s/*"%l)
    for d in dirs:
      started.append(d)
      dtype=analyzedir(d,iszero)
      if (dtype==True):
        finished.append(d)
      else:
        not_finished.append(d)
  return started,not_finished,finished

def logrotate(date,cWB_conf):
    subprocess.getstatusoutput("mkdir %s/log/%s"%(cWB_conf.online_dir,date))
    f=open("%s/log/%s/logrotate.conf"%(cWB_conf.online_dir,date),"w")
    rotate_conf="""%s/log/*.log {
olddir %s/log/%s/
compress
delaycompress
sharedscripts
dateext
copytruncate
}"""%(cWB_conf.online_dir,cWB_conf.online_dir,date)
    print(rotate_conf,file=f)
    f.close()
    subprocess.getstatusoutput("/usr/bin/env logrotate %s/log/%s/logrotate.conf --force --state %s/log/%s/logrotate.state"%(cWB_conf.online_dir,date,cWB_conf.online_dir,date))
    subprocess.getstatusoutput("gzip %s/log/%s/*.log-*"%(cWB_conf.online_dir,date))

#---------- CLASS: TIME ----------#

class Time:
  def __init__(self,n,t=0):
    self.value=t
    self.name=n
  def __sub__(self,otherTime):
    return Time("(%s-%s)"%(self.name,otherTime.name),self.value-otherTime.value)
  def table_line(self):
        if self.value != 0:
            return "<tr><th align=left>%s</th><td>%d : %s</td></tr>"%(self.name,self.value,tconvert(self.value))
        else:
            return "<tr><th align=left>%s</th><td>%s</td></tr>"%(self.name,"detector is not operative")
  def last_analyzed_time(self,path):
    updirs=glob.glob("%s"%path)
    updirs.sort()
    if updirs:
        downdirs=glob.glob("%s/*"%updirs[len(updirs)-1])
        downdirs.sort()
        last_dir=downdirs[len(downdirs)-1]
        last_analyzed_time=int(last_dir.split("/")[len(last_dir.split("/"))-1].split("-")[1])
    else:
        last_analyzed_time=0
    self.value=last_analyzed_time
    return updirs
  def last_coincident_time(self,ifile,tot_time):
    f=open(ifile)
    combined_global=seg_ut.fromsegwizard(f)
    f.close()
    if combined_global:
        if ((int(combined_global[-1][1])-int(combined_global[-1][0]))>(tot_time)): # if instead of while
            last_coincident_time=int(combined_global[-1][1])
            print("lct = %i" %last_coincident_time)
        else:
            #####
            i=1
            while ((int(combined_global[-1-i][1])-int(combined_global[-1-i][0]))<(tot_time)):
                i+=1
                #####
                last_coincident_time=int(combined_global[-1-i][1])
            else:
                last_coincident_time=int(combined_global[-1-i][1])
    else:
        last_coincident_time=0
    self.value=last_coincident_time
  def getlastfromfile(self,idir,ifile):
    a=subprocess.getstatusoutput("grep  %s %s | grep gwf"%(idir,ifile))
    rotate_date=""
    if (len(a[1].split("\n"))>0):
      first=a[1].split("\n")[0]
      last=a[1].split("\n")[-1]
      if (len(first.split(" "))>0):
        first_read_file=first.split(" ")[1]
        last_read_file=last.split(" ")[1]
        #print(first_read_file,last_read_file);
        if (last_read_file.find(idir)==-1):
          print("Not found %s"%idir)
        else:
          f_last=FRAME(last_read_file)
          self.value=f_last.end
          f_first=FRAME(first_read_file)
          first_value=f_first.end
          if (get_date_from_gps(get_time())!=get_date_from_gps(first_value)):
             rotate_date=get_date_from_gps(first_value)
    return rotate_date
    #a=subprocess.getstatusoutput("grep  %s %s | grep gwf | tail -n 1"%(idir,ifile))
    #read_file=a[1].split(" ")[1]
    #if (read_file.find(idir)==-1):
    # print("Not found %s"%idir)
    #else:
    # f=FRAME(read_file)
    # self.value=f.end
  def getlastfromdir(self,idir):
    files=glob.glob("%s/*.gwf"%idir)
    getlastfromfiles(files)
  def getlastfromfiles(self,files):
    files.sort()
    last_file=files[len(files)-1]
    f=FRAME(last_file)
    self.value=f.end

#---------- CLASS: CONDITION ----------#

class Condition:
  def __init__(self,Time,Value,w,line):
    bad="%s is more than %s (%i>%i), %s"%(Time.name,Value.name,Time.value,Value.value,line)
    good="%s is less than %s (%i<%i)"%(Time.name,Value.name,Time.value,Value.value)
    if (Time.value > Value.value):
      self.table_line="<tr><th colspan=\"2\" bgcolor=\"%s\">%s: %s</th></tr>"%(def_colors(w),w,bad)
      if (def_colors(w).find("red")!=-1):
        self.tmail=bad
    else:
      self.table_line="<tr><th colspan=\"2\" bgcolor=\"%s\">%s: %s</th></tr>"%(def_colors("OK"),"OK",good)

#---------- CLASS: CHECK ----------#

class check:
  def __init__(self):
    self.times=[]
    self.conditions=[]
  def print_times(self):
    table="<center><table border=1 cellpadding=5>"
    for t in self.times:
      table+=t.table_line()
    for c in self.conditions:
      table+=c.table_line
    table+="</table></center>"
    return table
  def print_cond(self):
    res=""
    for c in self.conditions:
      try:
       res="%s\n%s"%(res,c.tmail)
      except:
       pass
    return res

#-------------------------------------#
#---------- STARTING SCRIPT ----------#
#-------------------------------------#

#if(__name__=="__main__"):
def check_status(cWB_conf):
 current_time=Time("Current time",get_time())
 detectors=[]
 for ifo,ifile in zip(cWB_conf.ifos,cWB_conf.ifo_files):
   detectors.append(detector(ifo,"%s/%s"%(cWB_conf.config_dir,ifile.split("/")[-1])))

 last_send_file="%s/log/sentstatus"%cWB_conf.online_dir
 if (hasattr(cWB_conf,"error_emails")):
   send_mail=True
 else:
   send_mail=False
 text_mail=""
 if (os.path.exists(last_send_file) and send_mail==True):
  a=subprocess.getstatusoutput("date -r %s"%(last_send_file))
  gps_time=sconvert(a[1])
  #last_send=float(a[1].split("\n")[len(a[1].split("\n"))-1])
  last_send=float(gps_time)
  #print current_time.value-last_send
  if ((current_time.value-last_send)<3600):
   send_mail=False

#---------- Here I check for FRAMES ----------#

# Be avare that if some frames are missing there could be problems.
 frames_C=check()
 frames_C.times.append(current_time)
 frames_time=Time("Delay Frame Time",30)
 for det in detectors:
   last=Time("Last Frame %s"%det.name)
   last.getlastfromfiles(det.previouslist)
   frames_C.times.append(last)
   frames_C.conditions.append(Condition(current_time-last,frames_time,"WARNING","check if frames are updated"))

#---------- Here I check for TIMES ----------#

 current_time=Time("Current time",get_time())
 read_C=check()
 read_C.times.append(current_time)
 read_time=Time("Delay Frame Time",30)
 for det in detectors:
   last=Time("Last Read %s"%det.name)
   idir="/".join(det.frame_name.split("/")[:-1])
   if hasattr(cWB_conf,"production_dir"):
      rotate_date=last.getlastfromfile(idir,"%s/log/run.log"%(cWB_conf.production_dir))
   else:
     rotate_date=last.getlastfromfile(idir,"%s/log/run.log"%(cWB_conf.online_dir))
   read_C.times.append(last)
   read_C.conditions.append(Condition(current_time-last,read_time,"ERROR","check if pipeline is reading frames"))

#---------- Here I check for ZERO LAG ANALYSIS ----------# 
 
 current_time=Time("Current time",get_time())
 zero_l_an=Time("Last Analyzed Time")
 zero_dates=zero_l_an.last_analyzed_time("%s/%s/????-??-??"%(cWB_conf.run_dir,cWB_conf.jobs_dir))
 zero_l_coinc=Time("Last Coincident Time")
 zero_l_coinc.last_coincident_time("%s/%s/seg_global.txt"%(cWB_conf.run_dir,cWB_conf.seg_dir),cWB_conf.seg_duration + 2*cWB_conf.job_offset)
 seg_dur=Time("Segment length",cWB_conf.seg_duration+2*cWB_conf.job_offset)
 mov_step=Time("Moving step",cWB_conf.moving_step+cWB_conf.job_offset)

 zero_C=check()
 zero_C.times.append(current_time)
 zero_C.times.append(zero_l_an)
 zero_C.times.append(zero_l_coinc)
 zero_C.conditions.append(Condition(zero_l_coinc-zero_l_an,seg_dur,"ERROR","check if pipeline is running"))
 zero_C.conditions.append(Condition(zero_l_coinc-zero_l_an,mov_step,"WARNING","check if data are good in the last period"))

#---------- Here I check for BGK ANALYSIS ----------#

 current_time=Time("Current time",get_time())
 bkg_l_an=Time("Last Analyzed Time")
 bkg_dates=bkg_l_an.last_analyzed_time("%s/%s/????-??-??"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir))
 bkg_l_coinc=Time("Last Coincident Time")
 bkg_l_coinc.last_coincident_time("%s/%s/considered.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir),cWB_conf.seg_duration + 2*cWB_conf.job_offset)
 bkg_seg_dur=Time("Segment length",cWB_conf.bkg_job_duration+2*cWB_conf.job_offset)

 bkg_C=check()
 bkg_C.times.append(current_time)
 bkg_C.times.append(bkg_l_an)
 bkg_C.times.append(bkg_l_coinc)
 bkg_C.conditions.append(Condition(bkg_l_coinc-bkg_l_an,bkg_seg_dur,"ERROR","check if jobs are submitted"))

 #zero_notfin=glob.glob("%s/%s/????-??-??/*/log"%(cWB_conf.run_dir,cWB_conf.jobs_dir))
 #zero_st=glob.glob("%s/%s/????-??-??/*/log.zip"%(cWB_conf.run_dir,cWB_conf.jobs_dir))
 #zero_st+=zero_notfin
 #zero_fin=glob.glob("%s/%s/????-??-??/*/OUTPUT/finished"%(cWB_conf.run_dir,cWB_conf.jobs_dir))
 zero_st,zero_notfin,zero_fin=analyzedirs(zero_dates,True)
 #zero_notfin=findnotfin(zero_st,-2,zero_fin,-3,cWB_conf.moving_step)
 bkg_st=[]
 #bkg_st.append(glob.glob("%s/%s/????-??-??/*/start"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir)))
 bkg_fin=[]
 #bkg_fin.append(glob.glob("%s/%s/????-??-??/*/finished"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir)))
 #for l in range(1,cWB_conf.bkg_njobs):
 # bkg_st.append(glob.glob("%s/%s_%i/????-??-??/*/start"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir,l)))
 # bkg_fin.append(glob.glob("%s/%s_%i/????-??-??/*/finished"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir,l)))
 tbkg_st,tbkg_notfin,tbkg_fin=analyzedirs(bkg_dates,False)
 bkg_st.append(tbkg_st)
 bkg_fin.append(tbkg_fin)
 for l in range(1,cWB_conf.bkg_njobs+1):
   newbkg_dates=list(map(lambda x: x.replace(cWB_conf.jobs_dir,"%s_%i"%(cWB_conf.jobs_dir,l)),bkg_dates))
   tbkg_st,tbkg_notfin,tbkg_fin=analyzedirs(newbkg_dates,False)
   bkg_st.append(tbkg_st)
   bkg_fin.append(tbkg_fin)
 

 #logs=["run","web_pages_run","web_pages_week","web_pages_day","web_pages_mid","web_pages_hour","run_ts"]
 if hasattr(cWB_conf, 'production_dir'):
   #logs=["run","web_pages_week","web_pages_day","web_pages_daily"]
   logs=["run","web_pages_week","web_pages_dailydaycheck"]
 else:
   #logs=["run","web_pages_week","web_pages_day","web_pages_daily","run_ts"]
   logs=["run","web_pages_week","web_pages_dailydaycheck","run_ts"]
 #logs=["run","web_pages_all","run_ts"]
 lens=[]
 gpss=[]
 for l in logs:
  if (os.path.exists("%s/log/%s.log"%(cWB_conf.online_dir,l))):
   a=subprocess.getstatusoutput("fuser %s/log/%s.log"%(cWB_conf.online_dir,l))
   lens.append(len(a[1]))
   a=subprocess.getstatusoutput("date -r %s/log/%s.log"%(cWB_conf.online_dir,l))
   gps_time=sconvert(a[1])
   gpss.append(float(get_time())-float(gps_time))
   #gpss.append(float(get_time())-float(a[1].split("\n")[len(a[1].split("\n"))-1]))
  else:
    text_mail="%s\n%s.log does not exist"%(text_mail,l)
    lens.append(-1)
    gpss.append(0)
  #print "%s %i"%(l,len(a[1]))

#---------- CREATION OF THE WEB PAGE ----------#

 current_time=Time("Current time",get_time())
 page_checkdir="%s/%s/check.html"%(cWB_conf.run_dir,cWB_conf.summaries_dir)
 if (hasattr(cWB_conf,"backup_dir")):
    page_checkdir="%s/%s/check.html"%(cWB_conf.backup_dir,cWB_conf.summaries_dir)
 ff=open("%s"%(page_checkdir),"w")

#---------- Header of the Page ----------#

 print("<html>", file=ff)
 print("<title>STATUS</title>", file=ff)
 print("<body>", file=ff)
 print("""<h1 align=center><font size=10 face="Courier" color="#DF013A">STATUS</font></h1>""", file=ff)
 print("<center><i>The page was generated by %s at %d : %s</i></center><p>"%(os.environ["HOSTNAME"],current_time.value,tconvert(current_time.value)), file=ff)
 print("<hr size=3 noshade color=\"blue\">", file=ff)

#---------- TABLE: Where and if scripts are running ----------#

 print("<h3 align=center><font size=6 face=\"Courier\"  color=\"#01DF01\">Running scripts</font></h3>", file=ff)
 print("<center><table border=1 cellpadding=5>", file=ff)
 for l,ll,tt in zip(logs,lens,gpss):
  s,ss=return_error(ll,tt)
  print("<tr><td>%s</td><td bgcolor=\"%s\">%s</td></tr>"%(l,def_colors(s),ss), file=ff)
  if (def_colors(s).find("red")!=-1):
    text_mail="%s\n%s %s"%(text_mail,l,ss)
 print("</table></center>", file=ff)

#---------- TABLE: Are the Frames Update? ----------#
 
 print("<center><table border=0 cellpadding=5>", file=ff)
 print("<tr><td>", file=ff)

 print("<h3 align=center><font size=6 face=\"Courier\"  color=\"#01DF01\">Frames update</font></h3>", file=ff)
 print(frames_C.print_times(), file=ff)
 text_mail="%s%s"%(text_mail,frames_C.print_cond())

#---------- TABLE: Are the Frames Read? ----------#

 print("</td><td>", file=ff)

 print("<h3 align=center><font size=6 face=\"Courier\"  color=\"#01DF01\">Frames reading</font></h3>", file=ff)
 print(read_C.print_times(), file=ff)
 text_mail="%s%s"%(text_mail,read_C.print_cond())

 print("</td></tr>", file=ff)
 print("</table>", file=ff)

#---------- TABLE: Zero Lag ----------#
 
 print("<center><table border=0 cellpadding=5>", file=ff)
 print("<tr><td>", file=ff)

 print("<h3 align=center><font size=6 face=\"Courier\"  color=\"#01DF01\">Zero lag</font></h3>", file=ff)
 print(zero_C.print_times(), file=ff)
 text_mail="%s%s"%(text_mail,zero_C.print_cond())

#---------- TABLE: Background ----------#
 
 print("</td><td>", file=ff)

 print("<h3 align=center><font size=6 face=\"Courier\"  color=\"#01DF01\">Background</font></h3>", file=ff)
 print(bkg_C.print_times(), file=ff)
 text_mail="%s%s"%(text_mail,bkg_C.print_cond())

 print("</td></tr>", file=ff)
 print("</table>", file=ff)

#---------- TABLE: Jobs Started & Finished ----------#
 #print("<tr><td>Analysis</td><td>Started</td><td>Finished</td><td>Percentage</td><td>Log of not finished</td></tr>", file=ff)

 print("<h3 align=center><font size=6 face=\"Courier\"  color=\"#01DF01\">Started and finished</font></h3>", file=ff)
 print("<center><table border=1 cellpadding=5>", file=ff)
 print("<tr><th colspan=2>Analysis</th><th>Started</th><th>Finished</th><th>Percentage</th></tr>", file=ff)
 notfin=""
 lines=1
 if (len(zero_st)>0):
     print("<tr><th colspan=2>Zero lag</th><td align=\"right\">%i</td><td align=\"right\">%i</td><td align=\"right\">%.1f</td></tr>"%(len(zero_st),len(zero_fin),100.*len(zero_fin)/len(zero_st)), file=ff)
     #for f in range(len(zero_notfin)):
     #  notfin+="<a href=\"%s/log\">%i</a>,"%(zero_notfin[f].replace(cWB_conf.run_dir,cWB_conf.web_link),f+1)
     #  if ((f+1)%25==0):
     #    notfin+="</td><tr><td>"
     #    lines+=1
     #print("<tr><td rowspan=\"%i\">Zero lag</td><td rowspan=\"%i\">%i</td><td rowspan=\"%i\">%i</td><td rowspan=\"%i\">%.1f</td>%s</td></tr>"%(lines,lines,len(zero_st),lines,len(zero_fin),lines,100.*len(zero_fin)/len(zero_st),notfin), file=ff)
 l=0
 print("<tr><th rowspan=%i>Bkg</th><th>Superlag %i</th><td align=\"right\">%i</td><td align=\"right\">%i</td><td align=\"right\">%.1f</td></tr>"%(cWB_conf.bkg_njobs+1,l,len(bkg_st[l]),len(bkg_fin[l]),getratio(bkg_fin[l],bkg_st[l])), file=ff)
 for l in range(1,cWB_conf.bkg_njobs+1):
  #if (len(bkg_st[l])>0):
  #  ratio=100.*len(bkg_fin[l])/len(bkg_st[l])
  #else:
  #  ratio=0.
  print("<tr><th>Superlag %i</th><td align=\"right\">%i</td><td align=\"right\">%i</td><td align=\"right\">%.1f</td></tr>"%(l,len(bkg_st[l]),len(bkg_fin[l]),getratio(bkg_fin[l],bkg_st[l])), file=ff)
 print("</table></center>", file=ff)
 #print("<h3 align=center><font size=6 face=\"Courier\"  color=\"#01DF01\">Log of not finished</font></h3>", file=ff)
 #print("<center><table border=1 cellpadding=5>", file=ff)
 #notfin=""
 #for f in range(len(zero_notfin)):
 #  notfin+="<a href=\"%s/log\">%i</a>,"%(zero_notfin[f].replace(cWB_conf.run_dir,cWB_conf.web_link),f+1)
 #  if ((f+1)%50==0):
 #     print("<tr><td>%s</td></tr>"%notfin,file=ff)
 #     notfin=""
 #print("</table></center>", file=ff)
 print("</body>", file=ff)
 print("</html>", file=ff)
 ff.close()
 print(text_mail)
 if (send_mail==True and len(text_mail)>0):
  send_mails(text_mail,cWB_conf)
  subprocess.getstatusoutput("rm %s;touch %s"%(last_send_file,last_send_file))

 if (rotate_date!=""):
     logrotate(rotate_date,cWB_conf)
     if (hasattr(cWB_conf,"backup_dir")):
         subprocess.getstatusoutput("rm -rf %s/%s/%s"%(cWB_conf.run_dir,cWB_conf.jobs_dir,rotate_date))
