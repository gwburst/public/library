
// ----------------------------------------------------------
// XGB parameters used for Tuning/Training/Prediction
// ----------------------------------------------------------

// This is an example which shows how to setup XGB in the user_pparameters.C file

{

  // tuning/training/prediction
  pp_xgb_search         = "blf";				// input search type (bbh/imbhb/blf/bhf/bld)

  // tuning
  pp_xgb_ofile          = "data/xgb_hyper_parameters.out";	// output XGB hyper-parameters file

  // tuning/training
  pp_xgb_nfile          = TString(work_dir)+"/nfile.py";	// input background file (wave_*.root/.py)
  pp_xgb_sfile          = TString(work_dir)+"/sfile.py";	// input simulation file (wave_*.root/.py)
  pp_xgb_nfrac          = 0.2;					// input fraction of events selected from bkg
  pp_xgb_sfrac          = 0.2;					// input fraction of events selected from sim
  pp_xgb_config         = "$CWB_SCRIPTS/cwb_xgboost_config.py"; // input XGB configuration 
								// if not provided then the default config cwb_xgboost_config.py is used
  // training/prediction
  pp_xgb_model          = "xgb/xgb_model_STD_LF.dat";		// output(training)/input(prediction) XGB model

  // prediction
  pp_xgb_rthr           = 3;					// rhor threshold for output root file
  pp_xgb_fgetrhor       = "";					// user getrhor function used to compute rhor
								// if not provided then the default fuction in cwb_xgboost.py is used
  // XGB hyper-parameters (tuning)
  pp_xgb_learning_rate    = { 0.1,	0.03 };
  pp_xgb_max_depth        = { 6,	9,	12   };
  pp_xgb_min_child_weight = { 5.,	10.  };
  pp_xgb_gamma            = { 2.0,	5.0,	10.0 };
  pp_xgb_colsample_bytree = { 0.8,	1.0  };
  pp_xgb_subsample        = { 0.6,	0.8  };

  // used as input by the "cwb_condor xgb" command
  // can be also defined in user_parameters.C file
  strcpy(condor_tag,"ligo.prod.o3.burst.ebbh.cwboffline");

}

