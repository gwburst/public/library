#  ---------------------------------------------------------------
#  CUSTOM CONDA ENVIRONMENT
#  -----------------------------------------------------------------

conda activate /home/waveburst/.conda/envs/cwb-env     # NEW  

#  -----------------------------------------------------------------
#  WAT env for CIT CLUSTER
#  -----------------------------------------------------------------


  export SITE_CLUSTER="CIT"
  export BATCH_SYSTEM="CONDOR"
  export _USE_CMAKE=1;
  
#  this section is specific for the CWB pipeline 
#  -----------------------------------------------------------------

  
  export CWB_ANALYSIS="2G"

  export CWB_CONFIG="/home/waveburst/SOFT/cWB/tags/config/cWB-OfflineO3-v9-O3b"
  #export CWB_CONFIG="${HOME}/git/cWB/config_o3"
  #export CWB_CONFIG="/home/francesco.salemi/git/cWB/config_o3"

  export HOME_WWW="~waveburst/waveburst/WWW"
  export HOME_CED_WWW="~waveburst/waveburst/WWW/ced"
  export HOME_CED_PATH="/home/waveburst/public_html/waveburst/WWW/ced"
  export CWB_DOC_URL="https://ldas-jobs.ligo.caltech.edu/~waveburst/waveburst/WWW/doc"
  export CWB_REP_URL="https://ldas-jobs.ligo.caltech.edu/~waveburst/waveburst/WWW/reports"
  export CWB_GIT_URL="https://git.ligo.org/cWB"

  export HOME_WAT_FILTERS="${CWB_CONFIG}/XTALKS"
  export HOME_BAUDLINE="${HOME_LIBS}/BAUDLINE/baudline_1.08_linux_x86_64"
  export HOME_ALADIN="${HOME_LIBS}/ALADIN/Aladin.v7.533"

  export HOME_SKYMAP_LIB="${HOME_LIBS}/SKYMAP/skymap_statistics"
  export CWB_USER_URL="https://ldas-jobs.ligo.caltech.edu/~${USER}/reports"

  
  export CWB_PEGASUS_WATENV="/cvmfs/virgo.infn.it/waveburst/SOFT/WAT/trunk/cnaf_watenv.sh"
  export CWB_PEGASUS_SITE="creamce_cnaf"

  
  
  #export LD_LIBRARY_PATH=${HOME_LIBS}/SYSLIBS:$LD_LIBRARY_PATH

#  -----------------------------------------------------------------
#  DO NOT MODIFY !!!
#
#  1) In this section the HOME_WAT env is automatically initialized 
#  2) Init Default cWB library 
#  3) Init Default cWB config  
#  -----------------------------------------------------------------

  
   MYSHELL=`readlink /proc/$$/exe`
  if [[  "$MYSHELL" =~ "tcsh"  ]]; then
    echo "\nEntering in TCSH section..."
     CWB_CALLED=($_)
    if [[=$#CWB_CALLED = 2 ]]; then # alias
      set CWB_CALLED=`alias=$CWB_CALLED`
    fi
    if [[  "$CWB_CALLED" != ""  ]]; then     
       WATENV_SCRIPT=`readlink -f $CWB_CALLED[2]`
    else                                
      echo "\nError: script must be executed with source command"
      return 0 1
    fi
     script_dir=`dirname $WATENV_SCRIPT`
  fi
  if [[  "$MYSHELL" =~ "bash"  ]]; then
    echo ""
    echo "Entering in BASH section..."
    script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) #_SKIP_CSH2SH_
  fi
  if [[  "$MYSHELL" =~ "zsh"  ]]; then
    echo "\nEntering in ZSH section..."
    script_dir=$( cd "$( dirname "${(%):-%N}" )" && pwd )        #_SKIP_CSH2SH 
 fi
  export HOME_WAT=$script_dir
  export HOME_WAT_INSTALL="${HOME_WAT}/tools/install"
  #export HOME_WAT_INSTALL="${HOME_WAT}/tools/install_test_fix-cwb_report_sim.C"


  #source $HOME_WAT/tools/config.sh     # NEW  
  source $CWB_CONFIG/setup.sh          

  
  export CWB_HTML_INDEX="${CWB_MACROS}/html_templates/html_index_template_modern.txt"

  if [[ -f  $HOME_WAT_INSTALL/etc/cwb/cwb-activate.sh && -f $HOME_WAT_INSTALL/etc/cwb/scripts/cwb_watenv.sh ]]; then
        source $HOME_WAT_INSTALL/etc/cwb/cwb-activate.sh
  else
        echo "${HOME_WAT_INSTALL} is currently missing the activation code"
  fi
  [[ ":$PATH:" =~ ":$HOME_WAT_INSTALL/bin:" ]] || PATH="$HOME_WAT_INSTALL/bin:$PATH"
  source $CWB_SCRIPTS/cwb_watenv.sh     
  
#TO BE REMOVED LATER      # NEW
  export CWB_NETS_FILE="${CWB_SCRIPTS}/cwb_net.sh"
  export CWB_MPARAMETERS_FILE="./merge/cwb_user_parameters.C" 
  export CONDOR_LOG_DIR="$PWD/condor_logs"
  export WWW_PUBLIC_DIR="${HOME}/public_html/reports"
  #export _USE_OSG=1        # comment if not OSG
  #export CWB_CONTAINER_IMAGE="docker://containers.ligo.org/francesco.salemi/cwb_docker:debug2"   # comment if not running with container
  #export CWB_CONTAINER_IMAGE="docker://containers.ligo.org/joshua.willis/cwb_docker:devel"
