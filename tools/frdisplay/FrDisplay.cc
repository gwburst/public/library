/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


/*---------------------------------------------------------------------------*/
/* File: FrDisplay.c                          Last update:    Jun 26, 2011   */
/*                                                                           */
/* Copyright (C) 2011, G.Vedovato                                            */
/* For the licensing terms see the LICENSE file.                             */
/* For the list of contributors see the history section of the documentation */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "FrameL.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <math.h> 
#include <sys/wait.h>
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdio.h>
#include <signal.h>
#include <sys/param.h>
#include <sys/user.h>
#include <sys/sysctl.h>
#include "Filter.hh"
#include "TSystem.h"
#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"

void Help()
{
 printf(" \n" 
     " This program reads frames from one or more input files, and print   \n"
    "  the requested information.                                          \n"
     "-------------------------------------------------------------------- \n"
     " Syntax is: FrDisplay -i <input file / ldf>                          \n"
     "                    -f <first frame: (run # frame #) or (GPS time)>  \n" 
     "                    -l <last  frame: (run # frame #) or (GPS time)>  \n" 
     "                        or length in second                          \n"
     "                    -t <list of tag channels>                        \n" 
     "                    -d <debug level>                                 \n"
     "                    -c -1  to leave the data uncompressed            \n"
     "                    -top <number of ADC in the hit-parade>           \n"
     "                    -x <baudline parameters list>                    \n" 
     "                    -o <ldf observatory parameter>                   \n" 
     "                    -s <ldf data type  parameter>                    \n" 
     "                    -k <filter parameters list>                      \n" 
     "                    -a <file access mode [s(sequential)/r(random)]>  \n" 
     "                    -h (to get this help)                            \n"
     "  If one of the next option is there, we do only a partial frame dump\n"
     "                    -adc   to dump only the FrAdcData information    \n"
     "                    -sms   to dump only the FrSerData information    \n"
     "                    -proc  to dump only the FrProcData information   \n"
     "                    -sim   to dump only the FrSimData information    \n"
     "                    -sum   to dump only the FrSummary information    \n"
     "                    -stat  to dump only the static information       \n"
     "                    -raw   to dump only the raw data information     \n"
     "                    -event to dump only the FrEvent and FrSimEvent   \n"
     "-------------------------------------------------------------------- \n"
     " Remarks:                                                            \n"
     "  -i : Argument can be one or more file names                        \n"
     "  -t : Tag is a channel list with wild cards like: ADC122 ADC5*      \n"
     "        If a name start by - it is interpreted as an anti-tag        \n"
     " -f -l:These options works only with the debug level > 2             \n"
     " -d 0: One line per file based on the TOC information                \n"
     "       First frame time, file length, time of the first/last event,  \n"
     " -d 1: A few lines per file based on the TOC information             \n"
     "       First/last frame time, number of frame in file, missing frame \n"
     "       Count the number of frame elements like ADC, Proc, Sim, Event \n"
     "       Gives the name of few of them and the typical event amplitude \n"
     " -d 2: More lines per file based on the TOC information              \n"
     "       Same as -d 1 plus the full list of channels, event            \n"
     " -d 3: Read the frame to print information                           \n"
     "       Print the list of all frame (time, length, quality word)      \n"
     " -d 4: Read the frame to print information with more info            \n"
     "       Same as -d 3 plus summary information for all channels        \n"
     "       Print frame history                                           \n"
     " -d 5: Read the frame to print information with more info            \n"
     "        Same as -d 4 plus a full dump of the channel content...      \n"
     "-------------------------------------------------------------------- \n"
     "\n");
}

struct Parameters{
  char *name;                   /* input file name                  */
  char *tag;                    /* output tag                       */
  char *baudline;               /* baudline parameters              */
  char *observatory;            /* ldf observatory par              */
  char *datatype;               /* ldf data type par                */
  char *filter;                 /* filter parameters                */
  char *access;                 /* file access mode                 */
  int frun;                     /* first run to copy                */
  int fframe;                   /* first frame to copy              */
  int lrun;                     /* last run to copy                 */
  int lframe;                   /* last frame to copy               */
  double ftime;                 /* time for the first frame         */
  double ltime;                 /* time for the last frame          */
  int dType;                    /* type of dump                     */
  int debug;                    /* debug level                      */
  int comp;                     /* compression level                */
  int nTop;                     /* number of ADC for the hit parade */
};

enum FileAccess {SEQUENTIAL,RANDOM};
enum Type {INPUT, OUTPUT, TAG, FIRST, LAST, DEBUG, COMPRESS, TOP, BAUDLINE,OBSERVATORY,DATATYPE,FILTER,ACCESS};
#define UNSET -999
#define ADC 0x1
#define SMS 0x2
#define SIM 0x4
#define STAT  0x8
#define EVENT 0x10
#define SUM   0x20
#define RAW   0x40
#define PROC  0x80

void    ReadParameters(int argc, char **argv);
void    StrCat(char **oldbuf, char *more);

double  FrVectMax(FrVect *vect, int chId);
FrVect* FrVectDumpBinary(FrVect *vect, int fp, int chId, int debugLvl); 

int     StartBaudline(int gpsSec, int gpsNSec, double scale, double sampleRate, char* chname, int nchannel, char* params);
void    GetBaudlineDateFormat(int gpsSec, int gpsNSec, char* sbdate);
int     GpsToUnixTime(int gpsSec);
int     GpsToGpsLeaps(int gpsSec);
int     System(const char *command);
void    INThandler(int);
pid_t   getProcessId(const char * csProcessName);
void    FrFileDumpBinary(FrFile* file, int fp, double* scale, int nchannel, double sampleRate);
void    getUniqueFileList(TString ifile, TString ofile);

struct Parameters Par;
int  baudline_rnID;
char baudline_wNAME[256];
int  baudline_nP;
char baudline_FFL[256];
CWB::Filter* filter[3]={NULL,NULL,NULL};
int iglobal[3]={0,0,0};
float uscaleby=1; 
TString workdir=".";
TString worksite="ATLAS";
int fAccess=RANDOM;
char chnames[3][64]={"","",""};

#define MIN_TIME_GAP 256

/*--------------------------------------------------------- Main ------------*/
int main(int argc, char **argv)
/*---------------------------------------------------------------------------*/
{FrFile *file;         
 FrameH *frame;
 FrAdcData *adc;
 FrSerData *sms;
 FrProcData *proc;
 FrSimEvent *simE;
 FrEvent *evt;
 FrSimData *sim;
 FrSummary *sum;
 double tStart, tEnd, gtime, start, len;
 double tCurrent=0; 
 double sampleRate = 0.;
 int fp=0;  

 double scale[3]={0.,0.,0.};
 int nchannel=0;
 baudline_rnID=0; 
 baudline_nP=0;
 signal(SIGINT, INThandler);

 struct timeval tv;
 gettimeofday(&tv, NULL);
 srand(tv.tv_usec);
 baudline_rnID = rand();   // random name ID

 TString OS = "";
 if(TString(gSystem->GetBuildArch()).Contains("linux")) OS="Linux";
 if(TString(gSystem->GetBuildArch()).Contains("macos")) OS="Darwin";
 if(OS=="") {cout << "Error : Operative System not supported!!!  " << endl;exit(1);}

 char cmd[256];
 if(OS=="Linux")  sprintf(cmd,"mkdir -p /dev/shm/%s",getlogin());
 if(OS=="Darwin") sprintf(cmd,"mkdir -p /tmp/%s",getlogin());  //MAC
 //printf("%s\n",cmd);
 System(cmd);

// workdir=TString(gSystem->Getenv("FRDISPLAY_WORKDIR"));
// worksite=TString(gSystem->Getenv("FRDISPLAY_WORKSITE"));
 char sworkdir[256];
 sprintf(sworkdir,"%s",getenv("FRDISPLAY_WORKDIR"));
 workdir=TString(sworkdir); 
 char sworksite[256];
 sprintf(sworksite,"%s",getenv("FRDISPLAY_WORKSITE"));
 worksite=TString(sworksite); 
 worksite.ToUpper();
 
 /*----------------- Read input arguments -------------------------*/

 ReadParameters(argc, argv);

 /*---------------------------- Open the input file(s) ------------*/

 FrLibSetLvl(Par.debug-3);
 file = FrFileINew(Par.name);
 if(file == NULL)
    {fprintf(stderr,"Cannot open input file %s\n %s",
                    Par.name, FrErrorGetHistory()); 
     return(0);}
 file->compress = Par.comp;
 file->chkSumFrFlag = FR_YES;

 /*------------- Table Of Content dump if debugLevel <2 ------------*/

 if(Par.debug < 3)
   {FrFileIDumpT(file, stdout, Par.debug+1, Par.tag, Par.ftime, Par.ltime );
    return(0);}
 Par.debug -= 2;

 /*-------------- set the input file(s) to the first frames -------*/

 if(Par.ftime != 0)
   {frame = FrameReadT(file, Par.ftime);
    if(frame == NULL) /* search for the next frame in the requested range */
      {tStart = FrFileITNextFrame(file, Par.ftime);
       if(Par.ltime > 100000000) tEnd  = Par.ltime;
       else           tEnd = Par.ftime + Par.ltime;
       if(tStart < tEnd) frame = FrameReadT(file, tStart);}}
 else if((Par.frun > 0) || (Par.fframe > 0))
   {frame = FrameReadN(file, Par.frun, Par.fframe);}
 else{frame = FrameRead(file);}
 if(frame == NULL) fprintf(stderr,
    "  Cannot read a frame\n %s", FrErrorGetHistory());

 /*----------------List Channels ----------------------------*/

 if(Par.debug>2) { 
   if(((Par.dType & ADC) != 0) && (frame->rawData != NULL)) {
     int nadc=1;
     for(adc = frame->rawData->firstAdc; adc != NULL; adc=adc->next) {
       if(adc->sampleRate>50) {
         if(Par.tag == NULL) { 
             fprintf(stdout," ADC-%4d: %32s - rate: %15.2f - size: %10lu\n", 
                     nadc,adc->name,adc->sampleRate,adc->data->nData);
         } else if (TString(Par.tag).IsDigit()) {
             if(TString(Par.tag).Atoi()==nadc) {
               free((char*)(Par.tag));
               Par.tag=0;
               StrCat(&(Par.tag),adc->name); 
             }
         }
         nadc++;
       }
     }
     if(Par.tag == NULL) exit(0);
   }
   if((Par.dType & PROC) != 0) {
     int nproc=1;
     for(proc = frame->procData; proc != NULL; proc = proc->next) {
       if(Par.tag == NULL) { 
         if(proc->data->dx[0]>0)
           fprintf(stdout," PROC-%4d: %32s - rate: %15.2f - size: %10lu\n", 
                   nproc,proc->name,1./proc->data->dx[0],proc->data->nData);
         } else if (TString(Par.tag).IsDigit()) {
             if(TString(Par.tag).Atoi()==nproc) {
               free((char*)(Par.tag));
               Par.tag=0;
               StrCat(&(Par.tag),proc->name); 
             }
       }
       nproc++;
     }
     if(Par.tag == NULL) exit(0);
   }
 }

 /*----------------Start the Main loop ----------------------------*/

 int cnt=0; 
 TString cfileName=""; 
 while(frame != NULL) {

    FrFileH *fileH=file->current;
      
 /*-------------- Check that we are in the limits -----------------*/

    if(frame->GTimeS+1.e-9*frame->GTimeN > Par.ltime) break;
    if(frame->run > Par.lrun) break;
    if((frame->run == Par.lrun ) && (frame->frame > Par.lframe)) break;

 /*--------------- Output frame with some tag ---------------------*/
 
     if(Par.tag  != NULL) FrameTag(frame, Par.tag);

 /*---------------- Zoom the vectors if needed ---------------------*/

     if(Par.debug > 2) 
       {gtime = frame->GTimeS+1.e-9*frame->GTimeN;
        if(Par.ftime > gtime || Par.ltime < gtime+frame->dt)
          {if(Par.ftime > gtime) start = Par.ftime - gtime;
           else                  start = 0;
           if(Par.ltime < gtime+frame->dt) len = Par.ltime - gtime - start;
           else                            len = frame->dt - start;
           if(frame->rawData != NULL) 
             {for(adc = frame->rawData->firstAdc; adc != NULL; adc=adc->next)
                {FrVectZoomIn(adc->data, start, len);}}
           for(proc = frame->procData; proc != NULL; proc = proc->next)
                {FrVectZoomIn(proc->data, start, len);}
           for(sim = frame->simData; sim != NULL; sim = sim->next)
                {FrVectZoomIn(sim->data, start, len);}}}

 /*---------------- Dump the data ----------------------------------*/
 
     if(Par.dType == 0)
       {FrameDump(frame, stdout, Par.debug);
        if(Par.comp != 0) FrameCompDump(frame, stdout, Par.debug);}
     else
       {printf("Frame %d/%d GTimeS=%d\n",frame->run,frame->frame,frame->GTimeS);
        if(((Par.dType & ADC) != 0) && (frame->rawData != NULL)) {
          if((fp==0) && (Par.debug > 2)) {
             int size=0;
             char chname[64]="";  
             FrAdcData *xadc;
             for(xadc = frame->rawData->firstAdc; xadc != NULL; xadc=xadc->next) {
               if(nchannel>2)
                 {printf("Multichannel with num channel gt 3 not allowed !!!\n");exit(1);}
               if(size==0) {
                 size = xadc->data->nData;
               } else {
                 if(size!=xadc->data->nData)
                   {printf("Multichannel with different size not allowed !!!\n");exit(1);}
               }
               if(sampleRate==0) {
                 sampleRate = xadc->sampleRate;
               } else {
                 if(sampleRate!=xadc->sampleRate)
                   {printf("Multichannel with different sampleRates not allowed !!!\n");exit(1);}
               }
               /*----------------- Init Filter         -------------------------*/
               if(Par.filter!=NULL && filter[nchannel]==NULL) {
                 char sratepar[32];
                 sprintf(sratepar," -s %f ",sampleRate);
                 StrCat(&(Par.filter),sratepar);
                 filter[nchannel] = new CWB::Filter(Par.filter);
               }
               scale[nchannel]=FrVectMax(xadc->data,nchannel);
               if(scale[nchannel]>0) scale[nchannel]=32768./scale[nchannel];
               for(int j=0;j<nchannel;j++) if(filter[j]!=NULL) filter[j]->Reset();
               if(nchannel==0)
                 sprintf(chname,"%s",xadc->name);
               else
                 sprintf(chname,"%s %s",chname,xadc->name);
               
               sprintf(chnames[nchannel],"%s",xadc->name);
               nchannel++;
             }
             if(nchannel==0)
               {printf("The request channel is not present in the frame !!!\n");exit(1);}
             double scaleby;
             if(nchannel==1) {
               scaleby=scale[0]/4.; // (1./4.) to avoid amplitude cut -> glitches
               scale[0]=1.;
             } else {
               for(int j=0;j<nchannel;j++) scale[j]=32768./scale[j];
               scaleby=32768./4.;
             }
             scaleby*=uscaleby;
             printf("sampleRate %f - scale %e - chname %s - nchannel %d\n",sampleRate,scaleby,chname,nchannel);
             fp=StartBaudline(frame->GTimeS, frame->GTimeN, scaleby, sampleRate, chname, nchannel, Par.baudline);
          }
          if(fAccess==RANDOM) goto END;
          int nVect=0;
          FrVect* outVect[3];
          for(adc = frame->rawData->firstAdc; adc != NULL; adc=adc->next) {
            outVect[nVect]=FrVectDumpBinary(adc->data, 0, nVect, Par.debug);
            nVect++; 
          }
          if(nchannel!=0 && nVect!=nchannel) 
            {printf("Multichannel : num of channels is changed !!!\n");exit(1);}
          FrVect* dumpVect = FrVectNew1D(const_cast<char*>("dumpVect"), FR_VECT_4R, nVect*outVect[0]->nData, 0, NULL, NULL);
          float* buffer = (float*)dumpVect->data;
          //for(int j=0;j<nVect;j++) cout << j << " scale : " << scale[j] << endl;
          for(int i=0;i<outVect[0]->nData;i++) 
             for(int j=0;j<nVect;j++) 
               buffer[nVect*i+j]=(((float*)outVect[j]->data)[i])/scale[j];
          for(int j=0;j<nVect;j++) FrVectFree(outVect[j]);
          write(fp, buffer, sizeof(float)*dumpVect->nData);
          FrVectFree(dumpVect);
        } 
        if(((Par.dType & SMS) != 0) && (frame->rawData != NULL))
          {for(sms = frame->rawData->firstSer; sms != NULL; sms = sms->next) 
	    {printf(" SMS: %s Data:%30s\n", sms->name, sms->data);}}
        if((Par.dType & STAT) != 0)
          {if(frame->detectProc != NULL)
              {printf("Detector used for reconstruction:\n");
               FrDetectorDump(frame->detectProc, stdout, Par.debug);}
           if(frame->detectSim != NULL)
              {printf("Detector used for simulation:\n");
               FrDetectorDump(frame->detectSim, stdout, Par.debug);}}
        if((Par.dType & SIM) != 0)
          {for(sim = frame->simData; sim != NULL; sim = sim->next) 
              {FrSimDataDump(sim, stdout, Par.debug);}}
        if((Par.dType & EVENT) != 0)
          {for(simE = frame->simEvent; simE != NULL; simE = simE->next)
              {FrSimEventDump(simE, stdout,Par.debug);}
           for(evt = frame->event; evt != NULL; evt = evt->next) 
              {FrEventDump(evt, stdout, Par.debug);}}
        if((Par.dType & PROC) != 0) {
           if((fp==0) && (Par.debug > 2)) {
             int size=0;
             char chname[64]="";  
             for(proc = frame->procData; proc != NULL; proc = proc->next) {
               printf("Reconstructed Data: %s\n", proc->name);
               if(nchannel>2)
                 {printf("Multichannel with num channel gt 3 not allowed !!!\n");exit(1);}
               if(size==0) {
                 size = proc->data->nData;
               } else {
                 if(size!=proc->data->nData)
                   {printf("Multichannel with different size not allowed !!!\n");exit(1);}
               }
               if(sampleRate==0) {
                 if(proc->data->dx[0] < 1) sampleRate = 1./proc->data->dx[0];
                 else {printf("sampleRate not allowed !!!\n");exit(1);}
               } else {
                 if(sampleRate!=1./proc->data->dx[0])
                   {printf("Multichannel with different sampleRates not allowed !!!\n");exit(1);}
               }
               /*----------------- Init Filter         -------------------------*/
               if(Par.filter!=NULL && filter[nchannel]==NULL) {
                 char sratepar[32];
                 sprintf(sratepar," -s %f ",sampleRate);
                 StrCat(&(Par.filter),sratepar);
                 filter[nchannel] = new CWB::Filter(Par.filter);
               }
               scale[nchannel]=FrVectMax(proc->data,nchannel);
               if(scale[nchannel]>0) scale[nchannel]=32768./scale[nchannel];
               for(int j=0;j<nchannel;j++) if(filter[j]!=NULL) filter[j]->Reset();
               if(nchannel==0)
                 sprintf(chname,"%s",proc->name);
               else
                 sprintf(chname,"%s %s",chname,proc->name);
               
               sprintf(chnames[nchannel],"%s",proc->name);
               nchannel++;
             }
             if(nchannel==0)
               {printf("The request channel is not present in the frame !!!\n");exit(1);}
             double scaleby;
             if(nchannel==1) {
               scaleby=scale[0]/4.; // (1./4.) to avoid amplitude cut -> glitches
               scale[0]=1.;
             } else {
               for(int j=0;j<nchannel;j++) scale[j]=32768./scale[j];
               scaleby=32768./4.;
             }
             scaleby*=uscaleby;
             printf("sampleRate %f - scale %e - chname %s - nchannel %d\n",sampleRate,scaleby,chname,nchannel);
             fp=StartBaudline(frame->GTimeS, frame->GTimeN, scaleby, sampleRate, chname, nchannel, Par.baudline);
          }
          if(fAccess==RANDOM) goto END;
          int nVect=0;
          FrVect* outVect[3];
          for(proc = frame->procData; proc != NULL; proc = proc->next) {
            outVect[nVect]=FrVectDumpBinary(proc->data, 0, nVect, Par.debug);
            nVect++; 
          }
          if(nchannel!=0 && nVect!=nchannel) 
            {printf("Multichannel : num of channels is changed !!!\n");exit(1);}
          FrVect* dumpVect = FrVectNew1D(const_cast<char*>("dumpVect"), FR_VECT_4R, nVect*outVect[0]->nData, 0, NULL, NULL);
          float* buffer = (float*)dumpVect->data;
          //for(int j=0;j<nVect;j++) cout << j << " scale : " << scale[j] << endl;
          for(int i=0;i<outVect[0]->nData;i++) 
             for(int j=0;j<nVect;j++) 
               buffer[nVect*i+j]=(((float*)outVect[j]->data)[i])/scale[j];
          for(int j=0;j<nVect;j++) FrVectFree(outVect[j]);
          write(fp, buffer, sizeof(float)*dumpVect->nData);
          FrVectFree(dumpVect);
        } 
        if((Par.dType & SUM) != 0)   
           {for(sum = frame->summaryData; sum != NULL; sum = sum->next)
               {printf("Summary: %s %s \n",sum->name, sum->comment); 
                FrVectDump( sum->moments, stdout, Par.debug);}}
        if((Par.dType & RAW) != 0)
           {FrRawDataDump(frame->rawData, stdout, Par.debug);}}

     if(Par.debug > 1) FrameStat(frame, stdout);
     
     if(Par.nTop != -2) FrameDumpTopADC(frame, stdout, Par.nTop, Par.comp);

     FrameFree(frame);

 /*---------------------- read the next frame --------------------*/

     frame = FrameRead(file);

 /*------------------End the Main loop ----------------------------*/
 }

END:
 if(fAccess==RANDOM) FrFileDumpBinary(file,fp,scale,nchannel,sampleRate); 

 FrFileIStat(file, stdout);

 if(Par.debug > 2) {
   printf("\n\nhit Ctrl-C\n\n");
   while (1) pause();
 }
 exit(0);
}

 /*------------------Read file in random mode  --------------------*/
void FrFileDumpBinary(FrFile* file, int fp, double* scale,int nchannel,double sampleRate) { 

 double tCurrent=0; 

 FrFileH *fileH=file->fileH;
 while(fileH!=NULL) {
   cout << fileH->fileName << endl;

   FrVect* outVect[3];
   for(int nVect=0;nVect<nchannel;nVect++) {
     FrFile* cfile=FrFileINew(fileH->fileName);
     FrTOCFFLBuild(cfile);
     FrFileH *cfileH=cfile->current;
     double cStart=cfileH->tStart;
     double cEnd=cfileH->tStart+cfileH->length;
     if(Par.ftime>cStart) cStart=Par.ftime;
     if(Par.ltime<cEnd) cEnd=Par.ltime;
     double clength=cEnd-cStart;

     if(tCurrent!=0 && nVect==0) {
       if(cStart!=tCurrent) {
         cout.precision(14);
         cout << "tCurrent " << tCurrent << endl;
         cout << "cStart   " << cStart << endl;
         cout << "cEnd     " << cEnd << endl;
         int nullSize=(cStart-tCurrent)*sampleRate;
         if(((cStart-tCurrent)<=MIN_TIME_GAP)&&((cStart-tCurrent)>0)) {
           FrVect* nullVect = FrVectNew1D(const_cast<char*>("NULL"), FR_VECT_4R, nullSize, 0, NULL, NULL);
           float* dF = (float*)nullVect->data;
           for(int i=0; i<nullSize; i++) dF[i]-0;;
           write(fp, dF, sizeof(float)*nullSize);
           FrVectFree(nullVect);
         } else {
           cout << "Error : data not contiguous !!!" << endl;
           exit(1);
         }
       }
     }
     tCurrent=cEnd;

     outVect[nVect]=FrFileIGetVectF(cfile, chnames[nVect],  cStart, clength);
     float* dF = (float *) outVect[nVect]->data;
     if(filter[nVect]!=NULL) {
       for(int i=0; i<outVect[nVect]->nData; i++) 
         dF[i]=filter[nVect]->Arma((double)dF[i]);
     }
     FrFileIClose(cfile);
   }

   FrVect* dumpVect = FrVectNew1D(const_cast<char*>("dumpVect"), FR_VECT_4R, nchannel*outVect[0]->nData, 0, NULL, NULL);
   float* buffer = (float*)dumpVect->data;
   //for(int j=0;j<nVect;j++) cout << j << " scale : " << scale[j] << endl;
   for(int i=0;i<outVect[0]->nData;i++) 
      for(int j=0;j<nchannel;j++) 
        buffer[nchannel*i+j]=(((float*)outVect[j]->data)[i])/scale[j];
   for(int j=0;j<nchannel;j++) FrVectFree(outVect[j]);
   write(fp, buffer, sizeof(float)*dumpVect->nData);
   FrVectFree(dumpVect);

   fileH=fileH->next;
 }
 
 return;
} 

/*------------------------------------------------------ SetParameters ------*/
void ReadParameters(int argc, char **argv)
/*---------------------------------------------------------------------------*/
{int type, i;
 char cmd[256]; 

 if (argc == 1)
    {Help();
     exit(0);}

 /*-------------------- Get Operative System Type -----------------*/

 TString OS = "";
 if(TString(gSystem->GetBuildArch()).Contains("linux")) OS="Linux";
 if(TString(gSystem->GetBuildArch()).Contains("macos")) OS="Darwin";
 if(OS=="") {cout << "Error : Operative System not supported!!!  " << endl;exit(1);}

 /*-------------------- Default values ----------------------------*/

 Par.name   = NULL;
 Par.tag    = NULL;
 Par.debug  = 1;
 Par.comp   = 0;
 Par.ftime  = UNSET;
 Par.ltime  = UNSET;
 Par.frun   = UNSET;
 Par.lrun   = UNSET;
 Par.fframe = UNSET;
 Par.lframe = UNSET;
 Par.nTop   = -2;

 /*------------------- Loop over the parameters -------------------*/

 type = UNSET;

 for (i=1; i<argc; i++)
    {if     (strcmp(argv[i],"-h") == 0) 
       {Help(); 
        exit(0);}
    else if (strcmp(argv[i],"-i") == 0) {type = INPUT;}
    else if (strcmp(argv[i],"-t") == 0) {type = TAG;}
    else if (strcmp(argv[i],"-f") == 0) {type = FIRST;}
    else if (strcmp(argv[i],"-l") == 0) {type = LAST;}
    else if (strcmp(argv[i],"-d") == 0) {type = DEBUG;}
    else if (strcmp(argv[i],"-c") == 0) {type = COMPRESS;}
    else if (strcmp(argv[i],"-x") == 0) {type = BAUDLINE;}
    else if (strcmp(argv[i],"-o") == 0) {type = OBSERVATORY;}
    else if (strcmp(argv[i],"-s") == 0) {type = DATATYPE;}
    else if (strcmp(argv[i],"-k") == 0) {type = FILTER;}
    else if (strcmp(argv[i],"-a") == 0) {type = ACCESS;}
    else if (strcmp(argv[i],"-top") == 0) {type = TOP;}
    else if (strcmp(argv[i],"-adc")   == 0) {Par.dType = Par.dType | ADC;}
    else if (strcmp(argv[i],"-sms")   == 0) {Par.dType = Par.dType | SMS;}
    else if (strcmp(argv[i],"-stat")  == 0) {Par.dType = Par.dType | STAT;}
    else if (strcmp(argv[i],"-sim")   == 0) {Par.dType = Par.dType | SIM;}
    else if (strcmp(argv[i],"-event") == 0) {Par.dType = Par.dType | EVENT;}
    else if (strcmp(argv[i],"-proc")  == 0) {Par.dType = Par.dType | PROC;}
    else if (strcmp(argv[i],"-sum")   == 0) {Par.dType = Par.dType | SUM;}
    else if (strcmp(argv[i],"-raw")   == 0) {Par.dType = Par.dType | RAW;}
    else
      {if(type == INPUT)       {StrCat(&(Par.name),argv[i]);} 
       if(type == TAG)         {StrCat(&(Par.tag),argv[i]);} 
       if(type == BAUDLINE)    {StrCat(&(Par.baudline),argv[i]);} 
       if(type == OBSERVATORY) {StrCat(&(Par.observatory),argv[i]);} 
       if(type == DATATYPE)    {StrCat(&(Par.datatype),argv[i]);} 
       if(type == FILTER)      {StrCat(&(Par.filter),argv[i]);} 
       if(type == ACCESS)      {StrCat(&(Par.access),argv[i]);} 
       if(type == FIRST)   
             {if(Par.ftime == UNSET) 
                   {Par.ftime = atof(argv[i]);}
              else {Par.frun = Par.ftime;
                    Par.fframe = atoi(argv[i]);}}
       if(type == LAST)   
             {if(Par.ltime == UNSET) 
                   {Par.ltime = atof(argv[i]);}
              else {Par.lrun = Par.ltime;
                    Par.lframe = atoi(argv[i]);}}
       if(type == DEBUG)     {Par.debug = atoi(argv[i]);} 
       if(type == COMPRESS)  {Par.comp  = atoi(argv[i]);} 
       if(type == TOP)       {Par.nTop  = atoi(argv[i]);} 
      }}

 /*------------------ Check start/stop parameters------------------*/

 if(Par.fframe != UNSET) Par.ftime = 0.;
 if(Par.lframe != UNSET) Par.ltime = 1.e+12;
 if(Par.frun   == UNSET) Par.frun   = 0;
 if(Par.lrun   == UNSET) Par.lrun   = INT_MAX;
 if(Par.fframe == UNSET) Par.fframe = 0;
 if(Par.lframe == UNSET) Par.lframe = INT_MAX;
 if(Par.ftime  == UNSET) Par.ftime  = 0;
 if(Par.ltime  == UNSET) Par.ltime  = INT_MAX;

 /*---------------------------- Check filter help  --------------*/

 if(Par.filter!=NULL) {
   if(strstr(Par.filter,"help")!=NULL) {
     printf("filter help !!!\n");
     CWBFilterHelp(); 
     exit(0); 
   }
 }

 /*---------------------------- Check baudline help  --------------*/

 if(Par.baudline!=NULL) {
   if(strstr(Par.baudline,"help")!=NULL) {
     printf("baudline help !!!\n");

     char baudline[256];
     if(getenv("BAUDLINE_CMD")!=NULL) {
       printf("BAUDLINE_CMD : %s\n",getenv("BAUDLINE_CMD"));
       sprintf(baudline,"%s",getenv("BAUDLINE_CMD"));}
     else
       {printf("Cannot get BAUDLINE_CMD env\n");exit(1);}

     sprintf(cmd,"%s -help",baudline);
     printf("%s\n",cmd);
     System(cmd);
     exit(0); 
   }
 }

 /*---------------------------- Set baudline_rc      --------------*/

 int downmix=0; 
 if(Par.baudline!=NULL) {
   TObjArray* token = TString(Par.baudline).Tokenize(TString(' '));
   for(int i=0;i<token->GetEntries();i++) {
     TObjString* tok = (TObjString*)token->At(i);
     TString stok = tok->GetString();
     if(stok.CompareTo("-downmix")==0) {
       if(i==token->GetEntries()-1) {cout << "error : downmix par without value" << endl;exit(1);}
       TObjString* vtok = (TObjString*)token->At(i+1);
       TString svtok = vtok->GetString();
       if(!svtok.IsFloat()) {cout << "error : downmix par not digit" << endl;exit(1);}
       downmix = svtok.Atoi(); 
     } 
     if(stok.CompareTo("-uscaleby")==0) {
       if(i==token->GetEntries()-1) {cout << "error : scaleby par without value" << endl;exit(1);}
       TObjString* vtok = (TObjString*)token->At(i+1);
       TString svtok = vtok->GetString();
       if(!svtok.IsFloat()) {cout << "error : scaleby par not digit" << endl;exit(1);}
       uscaleby = svtok.Atof(); 
       // remove scaleby from bauline input string
       sprintf(Par.baudline,"%s","");
       for(int j=0;j<token->GetEntries();j++) {
         TObjString* tok = (TObjString*)token->At(j);
         TString stok = tok->GetString();
         if(j!=i && j!=i+1) sprintf(Par.baudline,"%s %s",Par.baudline,stok.Data());
       }
     } 
   }
 }

 //UserGroup_t* uinfo = gSystem->GetUserInfo();
 //TString uname = uinfo->fUser;
 TString uhome=TString(gSystem->Getenv("HOME"));
 char baudline_rc_path[256];
 sprintf(baudline_rc_path,"%s/.baudline/baudline_rc",uhome.Data());
 char baudline_rc_tmp_path[256];
 sprintf(baudline_rc_tmp_path,"%s/.baudline/baudline_rc.tmp",uhome.Data());

 Long_t id,size,flags,mt;
 int estat = gSystem->GetPathInfo(baudline_rc_path,&id,&size,&flags,&mt);
 if (estat==0) {

    ifstream in;
    in.open(baudline_rc_path,ios::in);
    if (!in.good()) {cout << "Error Opening File : " << baudline_rc_path << endl;exit(1);}

    ofstream out;
    out.open(baudline_rc_tmp_path,ios::out);
    if (!out.good()) {cout << "Error Opening File : " << baudline_rc_tmp_path << endl;exit(1);}

    bool flag=false; 
    char line[256];
    int cnt=1;
    while (1) {
      in.getline(line,256);
      if (!in.good()) break;
      //cout << line << endl;
      TString sline(line);
      if(sline.Contains("downmix")) {out << "downmix " << downmix << endl;flag=true;}
      else out << sline.Data() << endl;
    }
    if(!flag) {out << "downmix " << downmix << endl;}
    in.close();
    out.close();

    char cmd[256];
    sprintf(cmd,"mv %s %s",baudline_rc_tmp_path,baudline_rc_path);
    gSystem->Exec(cmd);
 }

 /*---------------------------- Check access file mode ------------*/

 if((Par.access!=NULL)&&(strcmp(Par.access,"s"))==0) fAccess=SEQUENTIAL;
 if((Par.access!=NULL)&&(strcmp(Par.access,"r"))==0) fAccess=RANDOM;

 /*---------------------------- Check if input is an ffl file -----*/

 if((Par.name!=NULL)&&(TString(Par.name).Contains(".ffl"))==1) {

    TString ffl_file_path = gSystem->ExpandPathName(Par.name); 

    ifstream in;
    in.open(ffl_file_path.Data(),ios::in);
    if (!in.good()) {cout << "Error Opening File : " << ffl_file_path.Data() << endl;exit(1);}

    ofstream out;
    if(OS=="Linux")  sprintf(baudline_FFL,"/dev/shm/%s/%d.ffl",getlogin(),baudline_rnID);
    if(OS=="Darwin") sprintf(baudline_FFL,"/tmp/%s/%d.ffl",getlogin(),baudline_rnID); //MAC
    out.open(baudline_FFL,ios::out);
    if (!out.good()) {cout << "Error Opening File : " << baudline_FFL << endl;exit(1);}

    char file_path[1024];
    double start;
    double lenght;
    double dummy1,dummy2;
    while (1) {
      in >> file_path >> start >> lenght >> dummy1 >> dummy2;
      if (!in.good()) break;
      out.precision(14);
      cout.precision(14);
      if((start>=Par.ftime)&&(start<=Par.ltime)) {
        //cout << file_path << " " << start << " " <<  lenght << endl;
        out << file_path << " " << start << " " <<  lenght << " " << dummy1 << " " << dummy2 << endl;
      }
    }
    in.close();

    free((char*)(Par.name));
    Par.name=0;
    StrCat(&(Par.name),baudline_FFL); 
 }

 /*---------------------------- Check gw_data_find --------------*/

 if((Par.name!=NULL)&&(strcmp(Par.name,"ldf"))==0) {
   // check if gw_data_find is available
   int ret=gSystem->Exec("gw_data_find -v"); 
   if(ret!=0) {printf("gw_data_find is not available !!!\n");exit(1);}
   if(Par.datatype!=0) {
     if(strstr(Par.datatype,"help")!=NULL) {
       sprintf(cmd,"gw_data_find --show-types ");
       printf("%s\n",cmd);
       System(cmd);
       exit(0);
     }
   }

   if(Par.observatory==0) {printf("parameter observatory -o not defined !!!\n");exit(1);}
   if(Par.ftime==0) {printf("parameter GPS start -f not defined !!!\n");exit(1);}
   if(Par.ltime==0) {printf("parameter GPS stop -l not defined !!!\n");exit(1);}
   if(Par.datatype==0) {printf("parameter data type -s not defined !!!\n");exit(1);}

   if(OS=="Linux")  sprintf(baudline_FFL,"/dev/shm/%s/%d.ffl",getlogin(),baudline_rnID);
   if(OS=="Darwin") sprintf(baudline_FFL,"/tmp/%s/%d.ffl",getlogin(),baudline_rnID);  //MAC

   sprintf(cmd,"gw_data_find --observatory=%s --type=%s --gps-start-time=%d --gps-end-time=%d --url-type=file | sed -e \'s/file:\\/\\/localhost//g\' > %s",Par.observatory,Par.datatype,(int)Par.ftime,(int)Par.ltime,baudline_FFL);
   printf("%s\n",cmd);
   int pid=System(cmd);

   // check if output command file is empty 
   Long_t xid,xsize,xflags,xmt;
   int xestat = gSystem->GetPathInfo(baudline_FFL,&xid,&xsize,&xflags,&xmt);
   if (xestat!=0 || xsize==0) {	// if empty then remove temporary file
     sprintf(cmd,"/bin/rm %s",baudline_FFL);
     gSystem->Exec(cmd);
     gSystem->Exit(1);
   }

   // get rid of duplicate file names 
   getUniqueFileList(baudline_FFL,baudline_FFL);

   printf("pid %d\n",pid);

   free((char*)(Par.name));
   Par.name=0;
   StrCat(&(Par.name),baudline_FFL); 
 }

/*
 if((Par.tag == NULL)&&(Par.debug>4)) 
      {fprintf(stderr," Please provide at least one channel name\n");
       Help();
       exit(0);}
*/
 if(Par.name == NULL) 
      {fprintf(stderr," Please provide at least one input file\n");
       Help();
       exit(0);}

 /*-------------------------- Dump parameters ---------------------*/

 if(Par.ltime < Par.ftime) Par.ltime += Par.ftime -1.e-6;

 if (Par.debug>0)
   {printf("-----------Parameters used--------------\n");
    printf("  Input  Files: %s\n", Par.name);
    if(Par.tag != NULL) printf("  Tag          : %s\n",Par.tag);
    printf("  First frame  : %d %d (GPS=%.1f)\n",
                               Par.frun,Par.fframe,Par.ftime);
    if(Par.ltime < 100000000)
         printf("  Lenght       : %.1fs\n", Par.ltime);
    else printf("  Last  frame  : %d %d (GPS=%.1f)\n",
                              Par.lrun,Par.lframe,Par.ltime);
    printf("  Debug level  : %d\n", Par.debug);
    if((Par.dType & ADC)   != 0) printf(" Dump adc info\n");
    if((Par.dType & SMS)   != 0) printf(" Dump sms info\n");
    if((Par.dType & STAT)  != 0) printf(" Dump Stat info\n");
    if((Par.dType & SIM)   != 0) printf(" Dump sim info\n");
    if((Par.dType & EVENT) != 0) printf(" Dump event info\n");
    if((Par.dType & PROC)  != 0) printf(" Dump proc info\n");
    if((Par.dType & SUM)   != 0) printf(" Dump summary info\n");
    if((Par.dType & RAW)   != 0) printf(" Dump raw data info\n");
    if( Par.dType          == 0) printf(" Dump all Frame info\n");
    printf("----------------------------------------\n");}

 return;
}

/*----------------------------------------------------------------- StrCat --*/
void StrCat(char **oldbuf, char *more)
/*---------------------------------------------------------------------------*/
{int lenMore;
 char *newbuf;

 lenMore = strlen(more);

 if(*oldbuf == NULL)
   {*oldbuf =  (char*)malloc(strlen(more) + 1);
    strcpy(*oldbuf, more); 
    return;}

 newbuf = (char*)malloc( strlen(*oldbuf) + lenMore + 2);
 sprintf(newbuf,"%s %s",*oldbuf, more);
 free(*oldbuf);
 *oldbuf = newbuf;

 return;}

/*---------------------------------------------------------- StartBaudline --*/ 
int StartBaudline(int gpsSec, int gpsNSec, double scale, 
                  double sampleRate, char* chname, int nchannel, char* params) {
/*---------------------------------------------------------------------------*/
  char sbdate[256];
  GetBaudlineDateFormat(gpsSec, gpsNSec, sbdate);
  printf("date : %s - chname : %s\n",sbdate,chname);
  printf("sampleRate %f - scale %e\n",sampleRate,scale);

  char cmd[1024];
  int pid;

  char baudline[256];
  if(getenv("BAUDLINE_CMD")!=NULL) {
    printf("BAUDLINE_CMD : %s\n",getenv("BAUDLINE_CMD"));
    sprintf(baudline,"%s",getenv("BAUDLINE_CMD"));}
  else
    {printf("Cannot get BAUDLINE_CMD env\n");exit(1);}

  char palette[256];
  if(getenv("BAUDLINE_PALETTE")!=NULL) {
    printf("BAUDLINE_PALETTE : %s\n",getenv("BAUDLINE_PALETTE"));
    sprintf(palette,"%s",getenv("BAUDLINE_PALETTE"));}
  else
    {printf("Cannot get BAUDLINE_PALETTE env\n");exit(1);}

  TString OS = "";
  if(TString(gSystem->GetBuildArch()).Contains("linux")) OS="Linux";
  if(TString(gSystem->GetBuildArch()).Contains("macos")) OS="Darwin";
  if(OS=="") {cout << "Error : Operative System not supported!!!  " << endl;exit(1);}

  char pipe_name[256];
  if(OS=="Linux")  sprintf(pipe_name,"/dev/shm/%s/cwb-pipe-%d",getlogin(),baudline_rnID);
  if(OS=="Darwin") sprintf(pipe_name,"/tmp/%s/cwb-pipe-%d",getlogin(),baudline_rnID);  //MAC
  printf("%s\n",pipe_name);
  if(OS=="Linux")  sprintf(cmd,"mknod %s p",pipe_name);
  if(OS=="Darwin") sprintf(cmd,"mkfifo %s",pipe_name);
  printf("%s\n",cmd);
  pid = System(cmd);

  if(params==NULL) StrCat(&(params),const_cast<char*>(""));

  char tsession[256];
  sprintf(tsession,"\"%s - %s - ID%d\"",chname,FrStrUTC(gpsSec,0),baudline_rnID);

  sprintf(baudline_wNAME,"%s-%s-ID%d",chname,FrStrUTC(gpsSec,0),baudline_rnID);
  int i;
  for(i=0;i<strlen(baudline_wNAME);i++) if(baudline_wNAME[i]==' ') baudline_wNAME[i]='_';
  for(i=0;i<strlen(baudline_wNAME);i++) if(baudline_wNAME[i]==':') baudline_wNAME[i]='_';
  for(i=0;i<strlen(baudline_wNAME);i++) if(baudline_wNAME[i]=='-') baudline_wNAME[i]='_';
  printf("%s\n",baudline_wNAME);

  sprintf(cmd,"cat %s | %s -utc %s -spacebar recordpause -basefrequency 0 -decimateby 1 -psd -format le32f -stdin -overlap 100 -samplerate %f -scaleby %e -average.savepsd -record -memory 10 -reversetimeaxis -tsession %s -overlays 1 -channels %d -palette %s -fftsize 65536 %s&", pipe_name,baudline,sbdate,sampleRate,scale,tsession,nchannel,palette,params); 
  printf("%s\n",cmd);
  pid = System(cmd);
  printf("Baudline pid : %d\n",pid);

  sprintf(cmd,"ps -ef | grep baudline | grep ID%d | awk \'{print $2}\'",baudline_rnID);
  printf("%s\n",cmd);
  System(cmd);

  int fp = open(pipe_name, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU | O_NONBLOCK);
  if(fp <= 0)
    {fprintf(stderr,"Cannot open output pipe %s\n %s", pipe_name, FrErrorGetHistory()); 
    exit(1);}

  return fp;
}

void GetBaudlineDateFormat(int gpsSec, int gpsNSec, char* sbdate) {

  printf("gpsSec %d - gpsNSec %d\n",gpsSec,gpsNSec);
  printf("UTC : %s\n",FrStrUTC(gpsSec,0));

  time_t unix_date_sec = FrGPS2UTC(gpsSec, 0);
  int us = gpsNSec/1000;

  struct  tm* tp = gmtime(&unix_date_sec);

  int ss = tp->tm_sec;         // seconds 0:59
  int mm = tp->tm_min;         // minutes 0:59
  int hh = tp->tm_hour;        // hours 0:23
  int DD = tp->tm_mday;        // day of the month 1:31
  int MM = tp->tm_mon+1;       // month 0:11
  int YY = tp->tm_year;        // year since 1900

  char bdate[32]="00:00:00-00:00:00.000000";
  char tmp[32];int len=0;
  sprintf(tmp,"%d",YY);len=strlen(tmp);
  if(len==3) strncpy(bdate+2-len+1,tmp+1,len-1); else strncpy(bdate+2-len,tmp,len);
  sprintf(tmp,"%d",MM);len=strlen(tmp);strncpy(bdate+5-len,tmp,len);
  sprintf(tmp,"%d",DD);len=strlen(tmp);strncpy(bdate+8-len,tmp,len);
  sprintf(tmp,"%d",hh);len=strlen(tmp);strncpy(bdate+11-len,tmp,len);
  sprintf(tmp,"%d",mm);len=strlen(tmp);strncpy(bdate+14-len,tmp,len);
  sprintf(tmp,"%d",ss);len=strlen(tmp);strncpy(bdate+17-len,tmp,len);
  sprintf(tmp,"%d",us);len=strlen(tmp);strncpy(bdate+24-len,tmp,len);

  strcpy(sbdate,bdate);
  printf("Baudline Date Format : %s\n",sbdate);

  return;
}

int System(const char *command) {

  int pid, status;
  extern char **environ;

  if (command == NULL) {printf("System : NULL Command\n");exit(1);} 
  pid = fork();
  if (pid == -1) {printf("System : Fork Failed\n");exit(1);}
  if (pid == 0) {
      char *argv[4];
      argv[0] = const_cast<char*>("sh");
      argv[1] = const_cast<char*>("-c");
      argv[2] = (char*)command;
      argv[3] = 0;
      execve("/bin/sh", argv, environ);
      exit(127);
  }
  do {
      if (waitpid(pid, &status, 0) == -1) {
          if (errno != EINTR) {printf("System : waitpid error %d\n",errno);exit(1);}
      } else
          return pid;
  } while(1);
}

void  INThandler(int sig) {

  char  c;

  TString OS = "";
  if(TString(gSystem->GetBuildArch()).Contains("linux")) OS="Linux";
  if(TString(gSystem->GetBuildArch()).Contains("macos")) OS="Darwin";
  if(OS=="") {cout << "Error : Operative System not supported!!!  " << endl;exit(1);}

  signal(sig, SIG_IGN);
  printf("Do you really want to quit? [y/n] or save? [s] ");
  c = getchar();
  if (c == 'y' || c == 'Y') {
    char cmd[256];
    //sprintf(cmd,"ps -ef | grep baudline | grep ID%d | awk \'{print $2}\' | xargs kill",baudline_rnID);
    if(OS=="Linux")  sprintf(cmd,"ps | grep baudline | awk \'{print $1}\' | xargs kill");
    if(OS=="Darwin") sprintf(cmd,"ps | grep baudline | awk \'{print $1}\' | xargs kill");
    printf("%s\n",cmd);
    System(cmd);

    // remove temporary frame file list
    sprintf(cmd,"/bin/rm %s",Par.name); 
    //gSystem->Exec(cmd);

    exit(0);
  } else if (c == 's' || c == 'S') {
    printf("click window to be saved\n");
    char cmd[256];
    sprintf(cmd,"import -frame %s/%s_P%d.png",workdir.Data(),baudline_wNAME,baudline_nP++);
    printf("%s\n",cmd);
    System(cmd);
    fflush(stdout);
    signal(SIGINT, INThandler);
  } else {
    fflush(stdout);
    signal(SIGINT, INThandler);
  }
}


/*---------------------------------------------------------FrVectDumpBinary--*/
FrVect* FrVectDumpBinary(FrVect *vect,  
                         int ofp,
                         int chId,
                         int debugLvl)
/*---------------------------------------------------------------------------*/
{FRULONG i, nData, inValid;
 char *dC, **dSt;
 short  *dS;
 int    *dI;
 FRLONG *dL;
 float  *dF, ratio;
 double *dD;
 unsigned char   *dU;
 unsigned short  *dUS;
 unsigned int    *dUI;
 FRULONG *dUL;

// if(ofp  <= 0) return NULL;
 if(vect == NULL) return NULL;
 if(debugLvl < 1) return NULL;

 FILE* fp = stdin;

 nData = vect->nData;
/*
 if(vect->name == NULL)
      {fprintf(fp,"  Vector:- ndata=%"FRLLD,              nData);}
 else {fprintf(fp,"  Vector:%s ndata=%"FRLLD, vect->name, nData);}

 if(vect->GTime != 0) fprintf(fp,"  GTime=%.5f",vect->GTime);

 if(vect->unitY != NULL)
         {fprintf(fp," unitY=%s", vect->unitY);}

 if(vect->nDim == 1)
   {if(vect->unitX[0] != NULL) fprintf(fp," unitX=%s", vect->unitX[0]);
    fprintf(fp,"  startX=%g dx=%g\n", vect->startX[0], vect->dx[0]);}
 else
   {fprintf(fp," nDim=%d\n", vect->nDim);
    for(i=0; i<vect->nDim; i++)
      {fprintf(fp,"      Dimension=%"FRLLD" nx=%10"FRLLD" startX=%.2g dx=%g",
               i,vect->nx[i], vect->startX[i], vect->dx[i]);
       if(vect->unitX[i] != NULL) fprintf(fp," unit=%s\n", vect->unitX[i]);
       else fprintf(fp,"\n");}}
 fprintf(fp,"   Data");
*/

 /*-------------------------------------- data part ---------------*/
 FrVect* outVect=NULL;
 if(vect->compress == 0) {
    if(vect->type == FR_VECT_4R) { /*----------------- float-------------*/
       dF = (float *) vect->data;
       if(debugLvl >2) {
          outVect = FrVectNew1D(vect->name, FR_VECT_4R, nData, 0, NULL, NULL);
          float* buffer = (float*)outVect->data;
          if(filter[chId]!=NULL) 
            for(i=0; i<nData; i++) buffer[i]=filter[chId]->Arma((double)dF[i]);
          else
            for(i=0; i<nData; i++) buffer[i]=dF[i];
          if(ofp!=0) {
            write(ofp, buffer, sizeof(float)*nData);
            FrVectFree(outVect);outVect=NULL; 
          }
       }
    }
    else if(vect->type == FR_VECT_8R) { /*---------------- double ------------*/
       dD = (double *) vect->data;
       fprintf(fp,"(double) %s\n",FrVectStat(vect));
       if(debugLvl >2) {
          outVect = FrVectNew1D(vect->name, FR_VECT_4R, nData, 0, NULL, NULL);
          float* buffer = (float*)outVect->data;
          if(filter[chId]!=NULL) 
            for(i=0; i<nData; i++) buffer[i]=filter[chId]->Arma(dD[i]);
          else
            for(i=0; i<nData; i++) buffer[i]=dD[i];
          if(ofp!=0) {
            write(ofp, buffer, sizeof(float)*nData);
            FrVectFree(outVect);outVect=NULL; 
          } 
       }
    }
    else if(vect->type == FR_VECT_C)   /*---------------one byte integer-----*/
      {dC = vect->data;
       fprintf(fp,"(byte) %s\n",FrVectStat(vect));
       if(debugLvl > 2)
         {for(i=0; i<nData; i++)
            {if(i%20 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp,"%4d",dC[i]);}
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_2S)  /*-------------- short integer -------*/
      {dS = (short *) vect->data;
       fprintf(fp,"(short) %s\n",FrVectStat(vect));
       if(debugLvl > 2)
         {for(i=0; i<nData; i++)
            {if(i%10 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp," %6d",dS[i]);}
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_8S)  /*---------------long integer---------*/
      {dL = (FRLONG *) vect->data;
       fprintf(fp,"(8S) %s\n",FrVectStat(vect));
       if(debugLvl > 2)
         {for(i=0; i<nData; i++)
            {if(i%10 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp," %d\"FRLLD\"",dL[i]);}
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_4S)  /*------------- signed integer -------*/
      {dI = (int *) vect->data;
       fprintf(fp,"(int) %s\n",FrVectStat(vect));
       if(debugLvl > 2)
         {for(i=0; i<nData; i++)
            {if(i%10 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp," %8d",dI[i]);}
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_1U)    /*--------- unsigned  character----*/
      {dU = (unsigned char *)vect->dataU;
       fprintf(fp,"(1U) %s\n",FrVectStat(vect));
       if(debugLvl > 2)
         {for(i=0; i<nData; i++)
            {if(i%20 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp," %4d",dU[i]);}
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_2U)    /*-------------unsigned short -----*/
      {dUS = (unsigned short *) vect->data;
       fprintf(fp,"(2U) %s\n",FrVectStat(vect));
       if(debugLvl > 2)
         {for(i=0; i<nData; i++)
            {if(i%10 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp," %6d",dUS[i]);}
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_8U)
      {dUL = (FRULONG *) vect->data;
       fprintf(fp,"(8U) %s\n",FrVectStat(vect));
       if(debugLvl > 2)
         {for(i=0; i<nData; i++)
            {if(i%10 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp," %d\"FRLLD\"",dUL[i]);}
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_4U)
      {dUI = (unsigned int *) vect->data;
       fprintf(fp,"(4U) %s\n",FrVectStat(vect));
       if(debugLvl > 2)
         {for(i=0; i<nData; i++)
            {if(i%10 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp," %12d",dUI[i]);}
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_8C)    /*------------- complex float ------*/
      {dF = (float *) vect->data;
       fprintf(fp,"(8C) %s\n", FrVectStat(vect));
       if(debugLvl <3)
         {fprintf(fp,"   (%g,%g)", dF[0],dF[1]);
          if(nData > 1) fprintf(fp," (%g,%g) ...", dF[2],dF[3]);
          fprintf(fp,"\n");}
       else
         {for(i=0; i<nData; i++)
            {if(i%4 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp,"(%12g %12g)",dF[2*i],dF[2*i+1]);}
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_16C)  /*------------- complex double ------*/
      {dD = (double *) vect->data;
       fprintf(fp,"(16C) %s\n", FrVectStat(vect));
       if(debugLvl <3)
         {fprintf(fp,"   (%g,%g)",dD[0],dD[1]);
          if(nData > 1) fprintf(fp," (%g,%g) ...",dD[2],dD[3]);
          fprintf(fp,"\n");}
       else
         {for(i=0; i<nData; i++)
            {if(i%4 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp,"(%12g %12g)",dD[2*i],dD[2*i+1]);}
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_8H)     /*-------half complex float -------*/
      {dF = (float *) vect->data;
       fprintf(fp,"(8H) %s\n", FrVectStat(vect));
       if(debugLvl <3)
         {fprintf(fp,"   %g ", dF[0]);
          if(nData > 1) fprintf(fp,", (%g,%g) ...", dF[1],dF[nData-1]);
          fprintf(fp,"\n");}
       else
         {fprintf(fp,"%6d:(%12g %12g)", 0, dF[0], 0.);
          for(i=1; i<nData/2; i++)
            {if(i%4 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp,"(%12g %12g)",dF[i],dF[nData-i]);}
          if(nData%2 == 0) fprintf(fp,"(%12g 0.)",dF[nData/2]);
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_16H)      /*----half complex double -------*/
      {dD = (double *) vect->data;
       fprintf(fp,"(16H) %s\n", FrVectStat(vect));
       if(debugLvl <3)
         {fprintf(fp,"   %g",dD[0]);
          if(nData > 1) fprintf(fp,", (%g,%g) ...\n",dD[1],dD[nData-1]);
          fprintf(fp,"\n");}
       else
         {fprintf(fp,"%6d:(%12g %12g)", 0, dD[0], 0.);
          for(i=1; i<nData/2; i++)
            {if(i%4 == 0) fprintf(fp,"\n%d\"FRLLD\":",i);
             fprintf(fp,"(%12g %12g)",dD[i],dD[nData-i]);}
          if(nData%2 == 0) fprintf(fp,"(%12g 0.)",dD[nData/2]);
          fprintf(fp,"\n");}}

    else if(vect->type == FR_VECT_STRING)
      {dSt = (char **) vect->data;
       fprintf(fp,"(STRING)");
       if(dSt[0] != NULL) fprintf(fp," \"%s\"", dSt[0]);
       if(dSt[1] != NULL) fprintf(fp," \"%s\"", dSt[1]);
       fprintf(fp,"...\n");}
    else
      {fprintf(fp," unknown type: %d \n",vect->type );}}
  else
    {ratio = (nData*vect->wSize)/(float) vect->nBytes;
     fprintf(fp,"\n     the vector is %.2f compressed (%x) nBytes=%d\"FRLLD\" wSize=%d\n",
            ratio,vect->compress,vect->nBytes, vect->wSize);}

 if(vect->next != NULL)
   {fprintf(fp,"  Attached information:\n");
    FrVectDump(vect->next, fp, debugLvl) ;}

 return outVect;}


/*-------------------------------------------------------------FrVectMinMax--*/
double FrVectMax(FrVect *vect, int chId)
/*---------------------------------------------------------------------------*/
/* This function computes the min and max value of the input vector vect.    */
/* It returns 1 in case of failure or 0 in case of success.                  */
/*---------------------------------------------------------------------------*/
{int i;
 double value;

 if(vect == NULL)     return(1);
 if(vect->nData == 0) return(1);

 double min = 1.e+37;
 double max =-1.e+37;
 if(FrVectIsValid(vect) != 0) return(2);

 if(vect->type == FR_VECT_C)
    {for(i=0; i<vect->nData; i++) {value = vect->data[i];
                                   if(value > max) max = value;
                                   if(value < min) min = value;}}
 else if(vect->type == FR_VECT_2S)
    {for(i=0; i<vect->nData; i++) {value = vect->dataS[i];
                                   if(value > max) max = value;
                                   if(value < min) min = value;}}
 else if(vect->type == FR_VECT_8R)
    {for(i=0; i<vect->nData; i++) {
                                   if(filter[chId]!=NULL) {
                                     value=filter[chId]->Arma(vect->dataD[i]);
                                     if(i>vect->nData/2) {
                                       if(value > max) max = value;
                                       if(value < min) min = value;
                                     }
                                   } else {
                                     value = vect->dataD[i];
                                     if(value > max) max = value;
                                     if(value < min) min = value;
                                   }
                                   }}
 else if(vect->type == FR_VECT_4R)
    {for(i=0; i<vect->nData; i++) {
                                   if(filter[chId]!=NULL) {
                                     value=filter[chId]->Arma((double)vect->dataF[i]);
                                     if(i>vect->nData/2) {
                                       if(value > max) max = value;
                                       if(value < min) min = value;
                                     }
                                   } else {
                                     value = vect->dataF[i];
                                     if(value > max) max = value;
                                     if(value < min) min = value;
                                   }
                                   }}
 else if(vect->type == FR_VECT_4S)
    {for(i=0; i<vect->nData; i++) {value = vect->dataI[i];
                                   if(value > max) max = value;
                                   if(value < min) min = value;}}
 else if(vect->type == FR_VECT_8S)
    {for(i=0; i<vect->nData; i++) {value = vect->dataL[i];
                                   if(value > max) max = value;
                                   if(value < min) min = value;}}
 else if(vect->type == FR_VECT_8C)
    {return(0);}
 else if(vect->type == FR_VECT_16C)
    {return(0);}
 else if(vect->type == FR_VECT_2U)
    {for(i=0; i<vect->nData; i++) {value = vect->dataUS[i];
                                   if(value > max) max = value;
                                   if(value < min) min = value;}}
 else if(vect->type == FR_VECT_4U)
    {for(i=0; i<vect->nData; i++) {value = vect->dataUI[i];
                                   if(value > max) max = value;
                                   if(value < min) min = value;}}
 else if(vect->type == FR_VECT_8U)
    {for(i=0; i<vect->nData; i++) {value = vect->dataUL[i];
                                   if(value > max) max = value;
                                   if(value < min) min = value;}}
 else if(vect->type == FR_VECT_1U)
    {for(i=0; i<vect->nData; i++) {value = vect->dataU[i];
                                   if(value > max) max = value;
                                   if(value < min) min = value;}}

 min = fabs(min);
 max = fabs(max);
 if(min > max) max = min;
 return(max);}


void getUniqueFileList(TString ifile, TString ofile) {

  vector<std::string> ifileList;
  vector<std::string> ipathList;
  vector<std::string> ofileList;
  vector<std::string> opathList;

  ifstream in;
  in.open(ifile.Data());
  if(!in.good()) {
    cout << "FrDisplay - getUniqueFileList : Error Opening Input File : " << ifile.Data() << endl;
    gSystem->Exit(1);
  }

  // read file list
  char istring[1024];
  while(1) {
    in.getline(istring,1024);
    if (!in.good()) break;
    TObjArray* token = TString(istring).Tokenize(TString('/'));
    // extract last entry -> file name
    TObjString* stoken =(TObjString*)token->At(token->GetEntries()-1);
    TString fName = stoken->GetString();
    //cout << fName.Data() << endl;
    ipathList.push_back(istring);
    ifileList.push_back(fName.Data());
  }
  in.close();

  // extract unique file list
  for(int i=0;i<(int)ifileList.size();i++) {
    bool check=false;
    for(int j=0;j<(int)ofileList.size();j++) {
      if(TString(ofileList[j].c_str())==TString(ifileList[i].c_str())) {check=true;break;}
    }
    if(!check) {
      ofileList.push_back(ifileList[i]);
      opathList.push_back(ipathList[i]);
    }
    //cout << i << " " << ipathList[i] << endl;
    //cout << i << " " << ifileList[i] << endl;
  }

  // write unique file list
  ofstream out;
  out.open(ofile.Data(),ios::out);
  if(!out.good()) {
    cout << "FrDisplay - getUniqueFileList : Error Opening Output File : " << ofile.Data() << endl;
    gSystem->Exit(1);
  }

  for(int i=0;i<(int)ofileList.size();i++) {
    out << opathList[i] << endl;
    //cout << i << " " << opathList[i] << endl;
    //cout << i << " " << ofileList[i] << endl;
  }

  out.close();

  return;
}


