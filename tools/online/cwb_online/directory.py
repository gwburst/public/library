#!/usr/bin/env python3
    
# Copyright (C) 2019 Marco Drago, Serena Vinciguerra
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess, os, sys, glob
import uproot
import numpy as np
import cwb_online.run_utils as run_utils
import cwb_online.detector as detector
import cwb_online.xgbonline as xgbonline
import cwb_online.root_python as root_python
import healpy.pixelfunc as pixelfunc

class cwb_create:
  def __init__(self):
    self.add_prod_plugin=False
    self.add_cuts=""
    self.web_pages=["week","daily,day,check"]
    self.name_pages=["last_week","last_day"]
    try:
     self.prod=cWB_conf.production_dir    
     self.copy=True
    except:
     self.copy=False

  def make_checks(self,cWB_conf,mkdir):    
    # check if overwriting directories
    if (mkdir):
     if os.path.isdir(cWB_conf.web_dir):
       print("Error - web-directory %s already exists, change \"web_dir\" in cWB_conf.py"%(cWB_conf.web_dir))
       exit()
     if os.path.isdir(cWB_conf.online_dir):
       print("Error - online-directory %s already exists, change \"online_dir\" in cWB_conf.py"%(cWB_conf.online_dir))
       exit()
    
    if (cWB_conf.version_wat!=os.environ['CWB_ANALYSIS']):
        print("Error - cWB version requested",cWB_conf.version_wat,"is different from one used:",os.environ['CWB_ANALYSIS'])
        exit()

    if (cWB_conf.version_wat=="2G"):
      #check that scracth is high enough
      TDSize = root_python.getpar("TDSize",cWB_conf.cwb_par)
      SAMP_RATE = root_python.getpar("inRate",cWB_conf.cwb_par)
      levelR = root_python.getpar("levelR",cWB_conf.cwb_par)
      l_high = root_python.getpar("l_high",cWB_conf.cwb_par)
    
      if cWB_conf.job_offset<=1.5*TDSize/(SAMP_RATE>>(levelR+l_high)):
            print("Error - segEdge must be > 1.5x the length for time delay amplitudes!!! Increase job_offset or decrease levelR + l_high")
            print("job_offset: ",cWB_conf.job_offset," levelR ",levelR," l_high: ",l_high)
            exit()
    
    # check existing file plugin 
    if hasattr(cWB_conf, 'prod_plugins'):
      for prod_file in cWB_conf.prod_plugins:
        if not os.path.exists(prod_file):
                print("Error - Plugin not found: change \"prod_plugins\" %s entry in cWB_conf.py"%(prod_file))
                sys.exit(1)
    else:
      print("No plugin specified")
    
    # check existing lag
    if hasattr(cWB_conf, 'bkg_laglist'):
      if not os.path.exists(cWB_conf.bkg_laglist):
        print("Error - Bkg lag list %s not found: change \"bkg_laglist\" in cWB_conf.py"%(cWB_conf.bkg_laglist))
        sys.exit(1)
    else:
      print("No Lags specified")

    # check existing superlag
    if hasattr(cWB_conf, 'bkg_superlaglist'):
      if not os.path.exists(cWB_conf.bkg_superlaglist):
        print("Error - Bkg superlag list %s not found: change \"bkg_superlaglist\" in cWB_conf.py"%(cWB_conf.bkg_superlaglist))
        sys.exit(1)
    else:
      print("No Superlags specified")

    # check file Cut and xgb not together
    if (hasattr(cWB_conf, 'Cuts_file') and hasattr(cWB_conf, 'xgboost')):
        print("Error - Cut file and xgboost both defined, please select one of the two\nCut file: %s\nxgboost: %s"%(cWB_conf.Cuts_file,cWB_conf.xgboost))
        sys.exit(1)

    # check existing file Cut
    if hasattr(cWB_conf, 'Cuts_file'):
      if not os.path.exists(cWB_conf.Cuts_file):
        print("Error - Cut file %s not found: change \"Cuts_file\" in cWB_conf.py"%(cWB_conf.Cuts_file))
        sys.exit(1)
    else:
      print("No Cuts specified")

    if (not hasattr(cWB_conf, 'xgboost')):
      if (not hasattr(cWB_conf,"id_rho")):
          print("Error, id_rho not specified")
          exit()

    # check xgb cuts file exists
    if hasattr(cWB_conf,'time_boundaries'):
       lim=run_utils.getlimits(cWB_conf.time_boundaries)
       if (lim==-1):
           print("Error on trigger time limits, should be or a number or a list of two elements")
           sys.exit(1)

    # check if certificates exist
    if hasattr(cWB_conf, 'gracedb_group'):
      try:
        X509_USER_CERT=os.environ['X509_USER_CERT']
        if (len(X509_USER_CERT)==0):
          print("Error - X509_USER_CERT defined void")
          exit()
        else:
          if not os.path.exists(X509_USER_CERT):
            print("Error - X509_USER_CERT file %s not found"%X509_USER_CERT)
            exit()
      except:
        print("Error - X509_USER_CERT not defined")
        exit()
      try:
        X509_USER_KEY=os.environ['X509_USER_KEY']
        if (len(X509_USER_KEY)==0):
          print("Error - X509_USER_KEY defined void")
          sys.exit(1)
        else:
          if not os.path.exists(X509_USER_KEY):
            print("Error - X509_USER_KEY file %s not found"%X509_USER_KEY)
            sys.exit(1)
      except:
        print("Error - X509_USER_KEY not defined")
        sys.exit(1)
      
  def make_det_checks(self,cWB_conf,mkdir):
    detectors=[]
    for ifo,ifile in zip(cWB_conf.ifos,cWB_conf.ifo_files):
         if (not os.path.exists(ifile)):
           print("ERROR file",ifile,"for detector",ifo,"not existing")
           exit()
         tdet = detector.detector(ifo,ifile)
         detectors.append(tdet)
         channels_to_check=[tdet.channel_name[0]]
         for cat in tdet.DQ:
           channels_to_check+=getattr(tdet,"%s_channel"%cat)
         print(ifo,channels_to_check)
         if (len(tdet.previouslist)==0):
           print(tdet.path_zero[0]," does not contain any frames file, check dir path")
           exit(1)
         channels=subprocess.getstatusoutput("/usr/bin/env FrChannels %s"%tdet.previouslist[0])[1].split("\n")
         for chan in channels_to_check:
            sel=list(filter(lambda x: x.split(" ")[0]==chan, channels))
            if (len(sel)==0):
              print("Channel: ",chan, "not found in file: ",tdet.previouslist[0], "please check channel name")
              exit()
         count_qm=tdet.path_bkg[0].count("?")
         if (count_qm==0):
           ifiles=glob.glob("%s*.gwf"%tdet.path_bkg[0])
         else:
           paths=tdet.path_bkg[0].split("/")
           idirs=glob.glob("%s"%"/".join(paths[:-1]))
           ifiles=glob.glob("%s/*.gwf"%idirs[0])
    return detectors

  def make_xgboost_checks(self,cWB_conf):
    # check xgb cuts file exists
      xgb=xgbonline.xgbinfo(cWB_conf.xgboost)
      ok,error=xgb.check()
      if (ok==False):
        print("Error on xgboost definition, please check")
        print(error)
        sys.exit(1)
      for m in xgb.models:
        if not os.path.exists(m):
          print("Error - xgboost model %s not found"%m)
          exit(1)
      if (len(xgb.config)>0 and not os.path.exists(xgb.config)):
          print("Error - xgboost config %s not found"%xgb.config)
          exit(1)
      if (len(xgb.getrhor)>0 and not os.path.exists(xgb.getrhor)):
          print("Error - xgboost config %s not found"%xgb.getrhor)
          exit(1)
      for m in xgb.addfiles:
        if not os.path.exists(m):
          print("Error - xgboost addfile %s not found"%m)
          exit(1)
      if (hasattr(cWB_conf,"id_rho")):
          if (cWB_conf.id_rho!=1):
              print("Error, id_rho not compatible with xgboost, put id_rho=1 or delete it")
              exit()
    
  def make_dirs(self,cWB_conf,cwb_conf_path):    
    subprocess.getstatusoutput("mkdir -p %s"%(cWB_conf.run_dir))
    subprocess.getstatusoutput("mkdir -p %s/%s"%(cWB_conf.run_dir,cWB_conf.summaries_dir))
    subprocess.getstatusoutput("mkdir -p %s"%(cWB_conf.bkg_run_dir))
    subprocess.getstatusoutput("mkdir -p %s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.postprod_dir))
    subprocess.getstatusoutput("mkdir -p %s"%(cWB_conf.web_dir))
    if hasattr(cWB_conf, 'xgboost'):
       subprocess.getstatusoutput("mkdir -p %s/MODELS"%(cWB_conf.web_dir))
    subprocess.getstatusoutput("mkdir -p %s"%(cWB_conf.config_dir))
    subprocess.getstatusoutput("mkdir -p %s/%s"%(cWB_conf.run_dir,cWB_conf.jobs_dir))
    if (os.environ['SITE_CLUSTER']=="CASCINA"):
       run_utils.add_rootrc(cWB_conf.online_dir)

    if (self.copy):
      subprocess.getstatusoutput("ln -s %s/%s %s/%s"%(cWB_conf.run_dir.replace(cWB_conf.online_dir,cWB_conf.production_dir),cWB_conf.seg_dir,cWB_conf.run_dir,cWB_conf.seg_dir))
      subprocess.getstatusoutput("ln -s %s/%s %s/%s"%(cWB_conf.bkg_run_dir.replace(cWB_conf.online_dir,cWB_conf.production_dir),cWB_conf.seg_dir,cWB_conf.bkg_run_dir,cWB_conf.seg_dir))
      subprocess.getstatusoutput("ln -s %s/%s %s/%s"%(cWB_conf.bkg_run_dir.replace(cWB_conf.online_dir,cWB_conf.production_dir),cWB_conf.jobs_dir,cWB_conf.bkg_run_dir,cWB_conf.jobs_dir))
      for n in range(1,cWB_conf.bkg_njobs):
        subprocess.getstatusoutput("ln -s %s/%s_%i %s/%s_%i"%(cWB_conf.bkg_run_dir.replace(cWB_conf.online_dir,cWB_conf.production_dir),cWB_conf.jobs_dir,n,cWB_conf.bkg_run_dir,cWB_conf.jobs_dir,n))
    else:
      subprocess.getstatusoutput("mkdir -p %s/%s"%(cWB_conf.run_dir,cWB_conf.seg_dir))
      subprocess.getstatusoutput("mkdir -p %s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir))
      subprocess.getstatusoutput("mkdir -p %s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir))
      if (hasattr(cWB_conf,"backup_dir")):
         subprocess.getstatusoutput("mkdir -p %s/%s"%(cWB_conf.backup_dir,cWB_conf.jobs_dir))
         subprocess.getstatusoutput("mkdir -p %s/%s"%(cWB_conf.backup_dir,cWB_conf.summaries_dir))
      
    subprocess.getstatusoutput("cp %s %s/cWB_conf.py"%(cwb_conf_path,cWB_conf.online_dir))
    for ifile in cWB_conf.ifo_files:
      subprocess.getstatusoutput("cp %s %s/."%(ifile,cWB_conf.config_dir))
    subprocess.getstatusoutput("cp %s/tools/online/html/* %s/."%(os.environ['HOME_WAT'],cWB_conf.web_dir))

    for name in ["considered","processed","running","missing","run","jobs"]:
      subprocess.getstatusoutput("echo > %s/%s/%s.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir,name))
    
    if (hasattr(cWB_conf,"copy_web")):
       subprocess.getstatusoutput("mkdir %s/%s"%(cWB_conf.web_dir,cWB_conf.jobs_dir,))
       subprocess.getstatusoutput("mkdir %s/%s"%(cWB_conf.web_dir,cWB_conf.summaries_dir))
       subprocess.getstatusoutput("mkdir %s/%s"%(cWB_conf.web_dir,cWB_conf.postprod_dir))
    else:
     if (hasattr(cWB_conf,"backup_dir")):
       subprocess.getstatusoutput("ln -s %s/%s %s/."%(cWB_conf.backup_dir,cWB_conf.jobs_dir,cWB_conf.web_dir))
       subprocess.getstatusoutput("ln -s %s/%s %s/."%(cWB_conf.backup_dir,cWB_conf.summaries_dir,cWB_conf.web_dir))
     else:
       subprocess.getstatusoutput("ln -s %s/%s %s/."%(cWB_conf.run_dir,cWB_conf.jobs_dir,cWB_conf.web_dir))
       subprocess.getstatusoutput("ln -s %s/%s %s/."%(cWB_conf.run_dir,cWB_conf.summaries_dir,cWB_conf.web_dir))
     subprocess.getstatusoutput("ln -s %s/%s %s/."%(cWB_conf.bkg_run_dir,cWB_conf.postprod_dir,cWB_conf.web_dir))
    
  def cuts_file(self,cWB_conf):
    try:
      Cuts_lines=open("%s"%(cWB_conf.Cuts_file)).readlines()
      Cuts_file="%s/Cuts.hh"%(cWB_conf.config_dir)
      f=open("%s"%(Cuts_file),"w")
      for line in Cuts_lines:
        print("%s"%(line.replace("\n","")), file=f)
      if (len(cWB_conf.Cuts_list)>1):
        print("""\nTCut OR_cut      = TCut("OR_cut",(%s).GetTitle());"""%("||".join(cWB_conf.Cuts_list)), file=f)
      f.close()
      com="%s/bin/cwb_mkhtml %s"%(os.environ['HOME_WAT_INSTALL'],Cuts_file)
      print(com)
      subprocess.getstatusoutput(com)
      com="mv %s/Cuts/Cuts.hh.html %s/.;rm -rf %s/Cuts"%(cWB_conf.config_dir,cWB_conf.web_dir,cWB_conf.config_dir)
      subprocess.getstatusoutput(com)
      self.add_cuts="""#include "%s"
  
  """%(Cuts_file)
    except:
      pass

  def xgb_files(self,cWB_conf):
    xgb=xgbonline.xgbinfo(cWB_conf.xgboost)
    xgb_dir="%s/MODELS/%s"%(cWB_conf.config_dir,run_utils.get_date_from_gps(run_utils.get_time()))
    if (os.path.exists(xgb_dir)):
        print("Error, %s dir already exists"%xgb_dir)
        exit()
    subprocess.getstatusoutput("mkdir -p %s"%(xgb_dir))
    for l,m in zip(xgb.labels,xgb.models):
        subprocess.getstatusoutput("cp %s %s/xgb_model_%s.json"%(m,xgb_dir,l))
    if (len(xgb.config)==0):
       xgb.config="%s/cwb_xgboost_config.py"%os.environ['CWB_SCRIPTS']
    subprocess.getstatusoutput("cp %s %s/xgb_config.py"%(xgb.config,xgb_dir))
    if (len(xgb.getrhor)>0):
        subprocess.getstatusoutput("cp %s %s/xgb_getrhor.py"%(xgb.getrhor,xgb_dir))
    for a in xgb.addfiles:
        subprocess.getstatusoutput("cp %s %s/."%(a,xgb_dir))
    subprocess.getstatusoutput("rm %s/MODELS/preferred;ln -s %s %s/MODELS/preferred"%(cWB_conf.config_dir,xgb_dir,cWB_conf.config_dir))
    subprocess.getstatusoutput("cp -r %s %s/MODELS/."%(xgb_dir,cWB_conf.web_dir))
    if (len(xgb.link)>0):
        subprocess.getstatusoutput("echo %s > %s/LINK"%(xgb.link,xgb_dir))
        string_to_html="""<!DOCTYPE html>
        <html>
        <head>
        <title>Old Page</title>
        <meta charset="UTF-8" />
        <meta http-equiv="refresh" content="3; URL=%s" />
        </head>
        <body>
        <p>Placeholder. If you are not redirected within 3 seconds, click <a href="%s">here</a>.</p>
        </body>
        </html>"""%(xgb.link,xgb.link)
        out=open("%s/MODELS/%s/LINK.html"%(cWB_conf.web_dir,xgb_dir.split("/")[-1]),"w")
        print(string_to_html,file=out)
        out.close()
    return xgb_dir.split("/")[-1]

  def pastro_sim(self,cWB_conf):
      fsim = cWB_conf.pastro
      eventsim = uproot.open("%s:waveburst"%fsim)   #open waveburst tree
      df = eventsim.arrays(["rho","time"],library="pd")
      sim = df[(df["rho[1]"]>0) &  ((df["time[0]"] - df["time[2]"]) < 0.2 ) ]
      sim = sim.loc[:,["rho[1]"]]  # extract
      np.savetxt( '%s/pastro_data_sim.txt'%cWB_conf.config_dir,  sim.values, fmt='%f')

  def plugin_string(self,plugin_name):
    return """   plugin = TMacro("%s");        // Macro source
  plugin.SetTitle("%s");"""%(plugin_name,plugin_name.replace(".C","_C.so"))

  def compile_plugin(self,plugins,out_name):
    nplugins=len(plugins)
    if (nplugins==1):
      com="cp %s %s"%(plugins[0],out_name)
    else:
      com="yes | %s/bin/cwb_mplugin %s %s"%(os.environ['HOME_WAT_INSTALL'],out_name," ".join(plugins))
    subprocess.getstatusoutput(com)
    com="%s;%s/bin/cwb_compile %s"%(com,os.environ['HOME_WAT_INSTALL'],out_name)
    print("Compiling plugin: %s"%com)
    subprocess.getstatusoutput(com)

  def plugins(self,cWB_conf):
    plugs=[]
    if (cWB_conf.version_wat=="XP"):
      plugs.append("%s/plugins/CWB_Plugin_xLowLatency_Skymap.C"%os.environ['HOME_CWB'])
    else:
      plugs.append("%s/plugins/CWB_Plugin_LowLatency_Skymap.C"%os.environ['HOME_CWB'])
    if hasattr(cWB_conf, 'prod_plugins'):
      self.add_prod_plugin=True
      self.bkg_plugin="%s/CWB_Plugin_Ced.C"%(cWB_conf.config_dir)
      self.compile_plugin(cWB_conf.prod_plugins,self.bkg_plugin)
      plugs+=cWB_conf.prod_plugins
    self.run_plugin="%s/CWB_Plugin_Run.C"%(cWB_conf.config_dir)
    self.compile_plugin(plugs,self.run_plugin)
   
  def user_parameters(self,cWB_conf, dets,ced=False):
    if (hasattr(cWB_conf,"cwb_par")):
        lev=root_python.getpar("healpix",cWB_conf.cwb_par)
    else:
        lev=root_python.getpar("healpix")
    nSky=pixelfunc.nside2npix(pixelfunc.order2nside(lev))
    ofile="%s/user_parameters.C"%(cWB_conf.config_dir)
    if (ced==True):
      ofile=ofile.replace(".C","_ced.C")
    f=open(ofile,"w")
    
    print("""{""", file=f)
    for i in range(len(cWB_conf.ifos)):
        print("""  strcpy(ifo[%i],"%s");"""%(i,cWB_conf.ifos[i]), file=f)
    print("""  strcpy(refIFO,"%s");"""%(cWB_conf.ifos[0]), file=f)
    print("""
  //lags
  lagSize = 1;
  lagOff = 0;
  lagMax = 0;

  //jobs
  segLen = %i;
  segMLS = %i;
  segEdge = %i;
  segTHR = 0;
  
  nIFO = %i;

  nSky=%i;

  //simulation
  nfactor = 1;
  simulation = 0;"""%(cWB_conf.seg_duration,cWB_conf.seg_duration,cWB_conf.job_offset,len(cWB_conf.ifos),nSky), file=f)
    
    if (hasattr(cWB_conf,"cwb_par")):
      print("\n%s"%(cWB_conf.cwb_par), file=f)
    for i in range(len(dets)):
        print("""  strcpy(channelNamesRaw[%i],"%s");"""%(i,dets[i].channel_name[0]), file=f)
    for i in range(len(cWB_conf.ifos)):
        print("""  strcpy(frFiles[%i],"input/%s.frames");"""%(i,cWB_conf.ifos[i]), file=f)
   
    if (ced==True): 
      if (self.add_prod_plugin==True):
        print(self.plugin_string(self.bkg_plugin), file=f)
    else:
        print(self.plugin_string(self.run_plugin), file=f)
    
    print("""
  nDQF=%i;
  dqfile dqf[%i]={"""%(2*len(cWB_conf.ifos),2*len(cWB_conf.ifos)), file=f)
    for i in range(len(cWB_conf.ifos)):
        print("""                     {"%s" ,"input/burst.in",           CWB_CAT1, 0., false, false},"""%(cWB_conf.ifos[i]), file=f)
    for i in range(len(cWB_conf.ifos)):
        print("""                     {"%s" ,"input/%s_cat2.in",           CWB_CAT2, 0., false, false},"""%(cWB_conf.ifos[i],cWB_conf.ifos[i]), file=f)
    print("""                   };
  for(int i=0;i<nDQF;i++) DQF[i]=dqf[i];

  strcpy(data_dir,"OUTPUT");
  """,file=f)

    if (ced==False):
       print("""
  online = true;
  frRetryTime=0;""", file=f)
    print("}", file=f)
    f.close()
    
  def pe_parameters(self,cWB_conf):    
    try:
      pe_plugin="%s/%s"%(cWB_conf.config_dir,cWB_conf.pe_plugin.split("/")[len(cWB_conf.pe_plugin.split("/"))-1])
      add_pe_plugin=True
      com="cp %s %s"%(cWB_conf.pe_plugin,pe_plugin)
      subprocess.getstatusoutput(com)
      com="root -b -l %s+"%(pe_plugin)
      print("Please compile plugin: %s"%com)
    except:
      add_pe_plugin=False
    
    try:
        f=open("%s"%(cWB_conf.pe_par),"w")
    
        print("""{""", file=f)
    
        for i in range(len(cWB_conf.ifos)):
            print("""  strcpy(ifo[%i],"%s");"""%(i,cWB_conf.ifos[i]), file=f)
        print("""  strcpy(refIFO,"%s");"""%(cWB_conf.ifos[0]), file=f)
        print("""
  //lags
  lagSize = 1;
  lagOff = 0;
  lagMax = 0;

  //jobs
  segLen = %i;
  segMLS = %i;
  segEdge = %i;
  segTHR = 0;
  
  nIFO = %i;

  nSky=196608;

  //simulation
  nfactor = 1;
  simulation = 0;"""%(cWB_conf.seg_duration,cWB_conf.seg_duration,cWB_conf.job_offset,len(cWB_conf.ifos)), file=f)
    
        if (hasattr(cWB_conf,"cwb_par")): 
          print("\n%s"%(cWB_conf.cwb_par), file=f)
        for i in range(len(cWB_conf.ifos)):
            print("""  strcpy(channelNamesRaw[%i],"%s");"""%(i,cWB_conf.channelname[cWB_conf.ifos[i]]), file=f)
        for i in range(len(cWB_conf.ifos)):
            print("""  strcpy(frFiles[%i],"input/%s_scratch.frames");"""%(i,cWB_conf.ifos[i]), file=f)
    
        if (add_pe_plugin==True):
            print("""   plugin = TMacro("%s");        // Macro source
  plugin.SetTitle("%s");"""%(pe_plugin,pe_plugin.replace(".C","_C.so")), file=f)
    
        print("""
  nDQF=%i;
  dqfile dqf[%i]={"""%(2*len(cWB_conf.ifos),2*len(cWB_conf.ifos)), file=f)
        for i in range(len(cWB_conf.ifos)):
            print("""                      {"%s" ,"input/burst.in",           CWB_CAT1, 0., false, false},"""%(cWB_conf.ifos[i]), file=f)
        for i in range(len(cWB_conf.ifos)):
            print("""                      {"%s" ,"input/%s_cat2.in",           CWB_CAT2, 0., false, false},"""%(cWB_conf.ifos[i],cWB_conf.ifos[i]), file=f)
        print("""                   };
  for(int i=0;i<nDQF;i++) DQF[i]=dqf[i];

  strcpy(data_dir,"OUTPUT_PE");
  strcpy(tmp_dir,"tmp_pe");
  cedRHO = 1.000000;

  online = true;
  frRetryTime=0;
  dump = false;

}""", file=f)
        f.close()
    except:
        print("no parameter estimation")
    
  def bkg_parameters(self,cWB_conf,dets):
    for l in range(0,3): 
        superlag_string="""
  //super lags
  slagSize   = %i;
  slagMin    = 0;
  slagMax    = %i;
  slagOff    = 0;
"""%(cWB_conf.bkg_njobs+1,cWB_conf.bkg_njobs)
    
        tmpfile="%s/user_parameters_bkg.C"%(cWB_conf.config_dir)
        lagsize="%i"%cWB_conf.bkg_nlags
        if (hasattr(cWB_conf,"bkg_lagstep")):
            lagstep=cWB_conf.bkg_lagstep
        else:
            lagstep=1
        lagoff="1";
        if (cWB_conf.bkg_njobs>1 and l==0):
           superlag_string="""%s

  slagFile = new char[1024];
  strcpy(slagFile,"%s");
             """%(superlag_string,"%s/superlaglist.txt"%(cWB_conf.config_dir))
        else:
           superlag_string="%s"%(superlag_string)
        if (l==1):
           tmpfile=tmpfile.replace(".C","_split.C")
           superlag_string=""
        if (l==2):
           tmpfile="%s/tmp_bkg.C"%cWB_conf.config_dir
    
        f=open("%s"%(tmpfile),"w")
    
        print("""{""", file=f)
        for i in range(len(cWB_conf.ifos)):
            print("""  strcpy(ifo[%i],"%s");"""%(i,cWB_conf.ifos[i]), file=f)
        print("""  strcpy(refIFO,"%s");"""%(cWB_conf.ifos[0]), file=f)
        print("""  //lags
  lagSize    = %s;
  lagStep    = %s;
  lagOff     = %s;
  lagMax     = 0;
  %s
  %s
  //jobs
  segLen = %i;
  segMLS = %i;
  segEdge = %i;
  segTHR = 0;
  
   nIFO = %i;

  //simulation
  nfactor = 1;
  simulation = 0;"""%(lagsize,lagstep,lagoff,self.lag_string,superlag_string,cWB_conf.bkg_job_duration,cWB_conf.bkg_job_minimum,cWB_conf.job_offset,len(cWB_conf.ifos)), file=f)
    
        if (l==1):
            print("""  
  sprintf(data_label,"%s_%i",data_label,(int)dataShift[1]);
  TString data_Shift=TString(gSystem->Getenv(\"Slag_datashift\"));
  TObjArray*  bitoken   = data_Shift.Tokenize(TString(','));""", file=f)
            print("""
  TObjString* itok[%i];
  TString sitok[%i];"""%(len(cWB_conf.ifos),len(cWB_conf.ifos)), file=f)
            for i in range(len(cWB_conf.ifos)):
               print("""
  itok[%i] = (TObjString*)bitoken->At(%i);
  sitok[%i] = itok[%i]->GetString();
  dataShift[%i] = sitok[%i].Atoi();"""%(i,i,i,i,i,i), file=f)
        if (hasattr(cWB_conf,"cwb_par")): 
          print("\n%s"%(cWB_conf.cwb_par), file=f)
        for i in range(len(dets)):
            print("""  strcpy(channelNamesRaw[%i],"%s");"""%(i,dets[i].channel_name[0]), file=f)
        for i in range(len(cWB_conf.ifos)):
            print("""  strcpy(frFiles[%i],"input/%s.frames");"""%(i,cWB_conf.ifos[i]), file=f)
        if (self.add_prod_plugin==True and l<2):
            print(self.plugin_string(self.bkg_plugin), file=f)
        print("""
  nDQF=%i;
  dqfile dqf[%i]={"""%(len(cWB_conf.ifos),len(cWB_conf.ifos)), file=f)
        for i in range(len(cWB_conf.ifos)):
            print("""                     {"%s" ,"input/%s_burst.in",           CWB_CAT1, dataShift[%i], false, false},"""%(cWB_conf.ifos[i],cWB_conf.ifos[i],i), file=f)
        if (os.environ['SITE_CLUSTER']=="CASCINA"):
          nodedir_sub="sprintf(nodedir,\"%s/tmp\",gSystem->WorkingDirectory());"
        else:
          nodedir_sub=""
        print("""                   };
  for(int i=0;i<nDQF;i++) DQF[i]=dqf[i];

  online = true;
  %s
}"""%(nodedir_sub), file=f)
        f.close()
    
  def user_pparameters(self,cWB_conf):    
    pp_rho_max = 10;
    pp_rho_min = 5;
    if hasattr(cWB_conf, 'xgboost'):
      pp_rho_max = 30;
      pp_rho_min = 0;
    f=open("%s/user_pparameters.C"%(cWB_conf.config_dir),"w")
    print("""#define RUN_LABEL "%s"

//PUT VETO DEFINE HERE
 
{
  %s
  T_cor      = 0.0;       // cc cut
  T_cut      = 0.0;        // rho high frequency cut
  T_out      = 0.0;        // rho high frequency cut

  hours      = 1;         // bin size in hours for rate vs time plot

  pp_irho = %i;
  pp_inetcc =  0;
  pp_rho_max = %g;
  pp_rho_min = %g;

  pp_batch = true;

  pp_jet_benckmark = -1;
  pp_mem_benckmark = -1;

//PUT VETO FILES HERE

}"""%(cWB_conf.title,self.add_cuts,cWB_conf.id_rho,pp_rho_max,pp_rho_min), file=f)
    f.close()
    
  def lag_file(self,cWB_conf):    
    if hasattr(cWB_conf, 'bkg_laglist'):
      lag_file="%s/laglist.txt"%(cWB_conf.config_dir)
      subprocess.getstatusoutput("cp %s %s/laglist.txt"%(cWB_conf.bkg_laglist,cWB_conf.config_dir))
      print("cp %s %s/laglist.txt"%(cWB_conf.bkg_laglist,cWB_conf.config_dir))
      self.lag_string="""lagFile = new char[1024];
  strcpy(lagFile,"%s");"""%(lag_file)
    else:
      self.lag_string=""
    
  def superlag_file(self,cWB_conf):    
    if hasattr(cWB_conf, 'bkg_superlaglist'):
      superlag_file="%s/superlaglist.txt"%(cWB_conf.config_dir)
      subprocess.getstatusoutput("cp %s %s/superlaglist.txt"%(cWB_conf.bkg_superlaglist,cWB_conf.config_dir))
      print("cp %s %s/superlaglist.txt"%(cWB_conf.bkg_superlaglist,cWB_conf.config_dir))
      subprocess.getstatusoutput("rm -rf %s/tmp_bkg.C"%cWB_conf.config_dir)
    else:
      olddir=os.getcwd()
      subprocess.getstatusoutput("mkdir %s/tmp_ONLINE"%cWB_conf.online_dir)
      os.chdir("%s/tmp_ONLINE"%cWB_conf.online_dir)
      subprocess.getstatusoutput("mkdir -p config input report/dump")
      subprocess.getstatusoutput("mv %s/tmp_bkg.C config/user_parameters.C"%cWB_conf.config_dir)
      for i in range(len(cWB_conf.ifos)):
         f=open("input/%s_burst.in"%(cWB_conf.ifos[i]),"w")
         print("0 %i"%(cWB_conf.bkg_job_duration*(cWB_conf.bkg_njobs+2)), file=f)
         f.close() 
         f=open("input/%s_cat2.in"%(cWB_conf.ifos[i]),"w")
         print("0 %i"%(cWB_conf.bkg_job_duration*(cWB_conf.bkg_njobs+2)), file=f)
         f.close() 
      subprocess.getstatusoutput("%s/bin/cwb_dump slag"%(os.environ['HOME_WAT_INSTALL']))
      subprocess.getstatusoutput("cp report/dump/tmp_ONLINE.slag %s/superlaglist.txt"%(cWB_conf.config_dir))
      os.chdir(olddir)
      subprocess.getstatusoutput("rm -rf %s/tmp_ONLINE"%cWB_conf.online_dir)
    
  def create_crontab(self,cWB_conf):    
    dir_for_logfiles="%s/log"%(cWB_conf.online_dir)
    subprocess.getstatusoutput("mkdir -p %s"%(dir_for_logfiles))
    #try:
    #  dir_for_logfiles="%s/log"%(cWB_conf.online_dir)
    #  subprocess.getstatusoutput("mkdir -p %s"%(dir_for_logfiles))
    #  subprocess.getstatusoutput("ln -s %s %s/log"%(dir_for_logfiles,cWB_conf.online_dir))
    #except:
    #  dir_for_logfiles="%s/log"%(cWB_conf.online_dir)
    #  subprocess.getstatusoutput("mkdir %s/log"%(cWB_conf.online_dir))

    subprocess.getstatusoutput("mkdir %s/crontab"%(cWB_conf.online_dir))
    command="""* * * * * %s/bin/online_restart %s/cWB_conf.py %s/run.log zero >> /tmp/%s_restart_run.log 2>&1
    """%(os.environ['HOME_WAT_INSTALL'],cWB_conf.online_dir,dir_for_logfiles,cWB_conf.user)
    file="%s/crontab/run.crontab"%(cWB_conf.online_dir)
    f=open(file,"w")
    print(command, file=f)
    f.close()
    
    file="%s/crontab/web.crontab"%(cWB_conf.online_dir)
    ff=open(file,"w")
    #command="""* * * * * %s/bin/online_restart %s/cWB_conf.py %s/web_pages_%s.log loop-%s >> /tmp/%s_restart_web_pages.log 2>&1"""%(os.environ['HOME_WAT_INSTALL'],cWB_conf.online_dir,dir_for_logfiles,"daily","daily",cWB_conf.user)
    #print(command, file=ff)
    for w in self.web_pages:
      command="""*/20 * * * * %s/bin/online_restart %s/cWB_conf.py %s/web_pages_%s.log loop-%s >> /tmp/%s_restart_web_pages.log 2>&1"""%(os.environ['HOME_WAT_INSTALL'],cWB_conf.online_dir,dir_for_logfiles,w.replace(",",""),w,cWB_conf.user)
      print(command, file=ff)
    #command="""* * * * * %s/bin/online_restart %s/cWB_conf.py %s/web_pages_%s.log loop-%s >> /tmp/%s_restart_web_pages.log 2>&1"""%(os.environ['HOME_WAT_INSTALL'],cWB_conf.online_dir,dir_for_logfiles,"check","check",cWB_conf.user)
    #print(command, file=ff)
    ff.close()
    
    com="cat %s/crontab/run.crontab %s/crontab/web.crontab > %s/crontab/run_and_web.crontab"%(cWB_conf.online_dir,cWB_conf.online_dir,cWB_conf.online_dir)
    subprocess.getstatusoutput(com)
    
    if (not self.copy):
      command="""
    */%i * * * * %s/bin/online_restart %s/cWB_conf.py %s/run_ts.log bkg >> /tmp/%s_restart_run_ts.log 2>&1
    """%(int(cWB_conf.bkg_job_duration/60.),os.environ['HOME_WAT_INSTALL'],cWB_conf.online_dir,dir_for_logfiles,cWB_conf.user)
      file="%s/crontab/bkg.crontab"%(cWB_conf.online_dir)
      f=open(file,"w")
      print(command, file=f)
      f.close()
    
      com="cat %s/crontab/web.crontab %s/crontab/bkg.crontab > %s/crontab/web_andbkg.crontab"%(cWB_conf.online_dir,cWB_conf.online_dir,cWB_conf.online_dir)
      subprocess.getstatusoutput(com) 
    
      com="cat %s/crontab/run.crontab %s/crontab/web.crontab %s/crontab/bkg.crontab > %s/crontab/run_and_web_andbkg.crontab"%(cWB_conf.online_dir,cWB_conf.online_dir,cWB_conf.online_dir,cWB_conf.online_dir)
      subprocess.getstatusoutput(com) 
    
  def buildheader(self,cWB_conf,ifile,ofile):
    rows=run_utils.getrows(ifile,cWB_conf)
    fo=open(ofile,"w")
    print("<html>",file=fo)
    header=open(os.environ['CWB_HTML_INDEX']).readlines()
    print("\n".join(list(map(lambda x: x.strip(),header))),file=fo)
    print("<body>",file=fo)
    print("<center>",file=fo)
    print("<table border=1 cellpadding=5>",file=fo)
    print("<tr><th>Name</th><th>Explanation</th></tr>",file=fo)
    for r in rows:
      print("<tr><th>%s</th><td>%s</td></tr>"%(",".join(r.name),r.expl.replace("\n","<br>")),file=fo)
    print("</table>",file=fo)
    print("</center>",file=fo)
    print("</body>",file=fo)
    print("</html>",file=fo)
    fo.close()

  def create_web(self,cWB_conf):    
    file="%s/index.html"%(cWB_conf.web_dir)
    ffindex=open(file,"w")

    command="".join(open(os.environ['CWB_HTML_INDEX']).readlines())
    command+="""
</head>
<title>%s</title>
"""%cWB_conf.title
    
    command+="""
<body>
<html>
<h1 align=center>%s</h1>
<br>
<div class="tabber">
"""%cWB_conf.title
    
    page_length=3500
    
    for n in self.name_pages:
      title=n.replace("_"," ")
      command+="""
<div class="tabbertab">
  <h2>%s</h2>
  <iframe src="%s/%s/%s.html" width="100%%"  height="%ipx" frameborder="0"></iframe>                                                                                                                
</div>"""%(title,cWB_conf.summaries_dir,n,n,page_length)
    
    command+="""
<div class="tabbertab">
  <h2>Calendar</h2>
  <iframe src="main.html" width="100%%"  height="%ipx" frameborder="0"></iframe>
</div>"""%(page_length)
    
    command+="""
<div class="tabbertab">
  <h2>Status</h2>
  <iframe src="%s/check.html" width="100%%"  height="%ipx" frameborder="0"></iframe>
</div>
    
</div>
</html>
    """%(cWB_conf.summaries_dir,page_length)
    print(command, file=ffindex)
    ffindex.close()
    
  def clean_bkg(self,cWB_conf):    
    for name in ["considered","processed","running","missing","run","jobs"]:
      subprocess.getstatusoutput("echo > %s/%s/%s.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir,name))
    subprocess.getstatusoutput("rm -rf %s/%s*"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir))
    subprocess.getstatusoutput("mkdir -p %s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir))

def directory(cWB_conf,conf_file,clean,mkdir=False,xgb_up=False):    
    cwb=cwb_create() 
    if (xgb_up==True):
      if hasattr(cWB_conf, 'xgboost'):
        cwb.make_xgboost_checks(cWB_conf)
        odate=cwb.xgb_files(cWB_conf)
        print("Updated xgboost model with date",odate)
        exit()
    if (clean==False):
      cwb.make_checks(cWB_conf,mkdir)
      if hasattr(cWB_conf,'xgboost'):
         cwb.make_xgboost_checks(cWB_conf)
      dets=cwb.make_det_checks(cWB_conf,mkdir)
      print("check done, everything ok")
      if (mkdir):
        cwb.make_dirs(cWB_conf,conf_file)    
      if hasattr(cWB_conf, 'Cuts_file'):
        cwb.cuts_file(cWB_conf)
      if (hasattr(cWB_conf, 'xgboost') and mkdir):
        cwb.xgb_files(cWB_conf)
      if hasattr(cWB_conf, 'pastro'):
        cwb.pastro_sim(cWB_conf)
      cwb.plugins(cWB_conf)
      cwb.lag_file(cWB_conf)    
      cwb.user_parameters(cWB_conf,dets)
      cwb.user_parameters(cWB_conf,dets,True)
      cwb.bkg_parameters(cWB_conf,dets)
      cwb.user_pparameters(cWB_conf)    
      if (cWB_conf.bkg_njobs>1):
         cwb.superlag_file(cWB_conf)    
      cwb.create_crontab(cWB_conf)
      if (mkdir):
         cwb.create_web(cWB_conf)
         subprocess.getstatusoutput("cp %s/tools/online/html/header.txt %s/."%(os.environ['HOME_WAT'],cWB_conf.config_dir))
      cwb.buildheader(cWB_conf,"%s/header.txt"%cWB_conf.config_dir,"%s/header_caption.html"%cWB_conf.web_dir)
      print("%s/header.txt %s/header_caption.html"%(cWB_conf.config_dir,cWB_conf.web_dir))
    else:
      cwb.clean_bkg(cWB_conf)

    if (mkdir):
      print("Analysis dir: %s"%(cWB_conf.online_dir))
