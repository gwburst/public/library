/*
# Copyright (C) 2024 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*****************************************************************
 * Package:      xmdc Class Library (eXtended MDC)
 * File name:    xmdc.cc
 * Author:       Gabriele Vedovato (vedovato@lnl.infn.it)
 *****************************************************************/

// This class is used to generate injections distributed uniformely in volume

#include "xmdc.hh"
#include "TStopwatch.h"
#include "gnetwork.hh"
#include "TMD5.h"
#include "TEllipse.h"
#include "Math/IntegratorOptions.h"
#include "constants.hh"
#include "watversion.hh"

//______________________________________________________________________________
CWB::xmdc::xmdc() {InitConfig();gTREE=NULL;gMDC=NULL;gNET=NULL;gJFNAME="";} 	// Default Constructor

//______________________________________________________________________________
CWB::xmdc::xmdc(config ucfg) {
//
// Constructor
//
// init xmdc using the input XMDC user configuration
//
// Input	ucfg		XMDC user configuration
//

  CheckConfig(ucfg);
  this->cfg=ucfg;gTREE=NULL;gMDC=NULL;gNET=NULL;gJFNAME="";
}

//______________________________________________________________________________
CWB::xmdc::xmdc(TString jfname) {
//
// Constructor
//
// init xmdc using the informations contained in the input XMDC root file
//
// Input	jfname		XMDC path file name
//

  gTREE=NULL;gMDC=NULL;gNET=NULL;gJFNAME="";

  // save jfname in the global variable gJFNAME
  gJFNAME = jfname;
  if(!gJFNAME.BeginsWith("/")) gJFNAME = gSystem->ExpandPathName("$PWD/"+gJFNAME);

  // get config from XMDC root jfname
  cfg = GetConfig(jfname);

  // open injections XMDC root file name
  TFile* jroot = TFile::Open(jfname);
  if((jroot==NULL) || (jroot!=NULL && !jroot->IsOpen())) {
    cout << "CWB::xmdc::xmdc: Error opening root file " << jfname.Data() << endl;
    exit(1);
  }

  // get injections tree object from XMDC root jfname
  TTree* itree_inj = (TTree *)jroot->Get(XMDC_TREE_INJ_NAME);
  if(itree_inj==NULL) {cout << "CWB::xmdc::xmdc: Error: injections object not present in the injection in file -> " << jfname << endl;exit(1);}
  itree_inj->SetEstimate(itree_inj->GetEntries());

  // get xmdc object
  CWB::xmdc* XMDC = (CWB::xmdc*)jroot->Get(XMDC_OBJECT_NAME);
  if(XMDC==NULL) {cout << "CWB::xmdc::xmdc: Error - xmdc object not present in the injection in file -> " << jfname << endl;exit(1);}

  // get summary data from XMDC root jfname
  sdata = XMDC->GetSummary(); 
} 

//______________________________________________________________________________
CWB::xmdc::~xmdc() {
//
// Destructor
//

  if(gTREE!=NULL) delete gTREE;
  if(gMDC!=NULL)  delete gMDC;
  if(gNET!=NULL)  delete gNET;
}

//______________________________________________________________________________
void 
CWB::xmdc::ReportDistributions(TString plot_type, TString wfname, double ifar, rconfig rcfg, TString odir) {
//
// Plot distributions of cwb post-production events 
//  
// Input:	plot_type 	distance/snr
//		wfname		root wave file
//		ifar		select events (years)
//		rcfg		xmdc report config
//		odir		main output dir
//

  CheckReportConfig(rcfg);
  if(cfg.inj_distance<=0 && rcfg.hrss_at_1Mpc<=0) {
    cout << "CWB::xmdc::ReportDistributions: Error - if cfg.inj_distance<=0 then hrss_at_1Mpc must be > 0" << endl << endl;
    gSystem->Exit(1);
  }

  // -----------------------------------------------------
  // open wave and mdc root files
  // -----------------------------------------------------

  // open wave file
  TFile* ifile_wave = TFile::Open(wfname);
  if(ifile_wave==NULL) {cout<<"CWB::xmdc::ReportDistributions: Error opening file : "<<wfname<<endl;exit(1);}
  TTree* itree_wave = (TTree *) gROOT->FindObject("waveburst");
  if(itree_wave==NULL) {cout<<"CWB::xmdc::ReportDistributions: Error opening tree : "<<"waveburst"<<endl;exit(1);}
  int nsize_wave = itree_wave->GetEntries();
  itree_wave->SetEstimate(nsize_wave);
  if(nsize_wave==0) {
    cout << endl << "CWB::xmdc::ReportDistributions: Error - input wave file empty" << endl << endl;
    gSystem->Exit(1);
  }

  // open mdc file
  TString mdc_fname = wfname;
  mdc_fname.ReplaceAll("wave_","mdc_");
  TFile* ifile_mdc = TFile::Open(mdc_fname);
  if(ifile_mdc==NULL) {cout<<"CWB::xmdc::ReportDistributions: Error opening file : "<<mdc_fname<<endl;exit(1);}
  TTree* itree_mdc = (TTree *) gROOT->FindObject("mdc");
  if(itree_mdc==NULL) {cout<<"CWB::xmdc::ReportDistributions: Error opening tree : "<<"mdc"<<endl;exit(1);}
  int nsize_mdc = itree_mdc->GetEntries();
  itree_mdc->SetEstimate(nsize_mdc);
  if(nsize_mdc==0) {
    cout << endl << "CWB::xmdc::ReportDistributions: Error - input mdc file empty" << endl << endl;
    gSystem->Exit(1);
  }

  // -----------------------------------------------------
  // plot distributions
  // -----------------------------------------------------

  char sel[256]="";
  char tmp[256]="";
  TList* ifoList = itree_wave->GetUserInfo();
  int nifo = ifoList->GetSize();					// get number of detectors

  string* name = new string();
  itree_wave->SetBranchAddress("name", &name);

  int inj_nid = sdata.data.size();
  for(int inj_id=0;inj_id<inj_nid;inj_id++) {

    TString inj_name = sdata.data[inj_id].name;                         // waveform name

    // make plots
    TCanvas* canvas = new TCanvas("distribution", "distribution", 300,40, 600, 600);
    canvas->Range(-19.4801,-9.25,-17.4775,83.25);
    canvas->SetBorderSize(2);
    canvas->SetFrameFillColor(0);
    //canvas->SetGridx();
    //canvas->SetLogx();
    canvas->SetLogy();

    gStyle->SetOptStat(0);

    TString selection; 

    double distance_factor=1.0;
    if((cfg.sky_distribution=="uv")||(cfg.sky_distribution.Contains("poly")))
      distance_factor = (rcfg.distance_units=="Mpc") ? 1.0 : 1000.0;	// for uv   the fiducial_distance in summary is in Mpc
    else if(cfg.sky_distribution.Contains("gd")) distance_factor = 1;	// for mngd the fiducial_distance in summary is in Kpc 

    TString distance_mdc = (cfg.inj_distance>0) ? TString::Format("%f*distance",distance_factor) : TString::Format("(%e*%e/strain)",distance_factor,rcfg.hrss_at_1Mpc);

    if(plot_type=="distance") selection=TString::Format("%s>>h(100)",distance_mdc.Data());
    if(plot_type=="snr") {
      strcpy(sel,"");
      strcpy(tmp,"snr[0]*snr[0]");
      for(int i=1;i<nifo;i++) {sprintf(sel,"%s+snr[%d]*snr[%d]",tmp,i,i);strcpy(tmp,sel);canvas->SetLogx();}
      strcpy(tmp,sel);sprintf(sel,"sqrt(%s)>>h(1000,1,200)",tmp);
      selection=sel;
    }
    itree_mdc->Draw(selection,TString::Format("type[1]==%d && strain>0",inj_id+1));
    int nsize_mdc_sel = itree_mdc->GetSelectedRows();
    if(nsize_mdc_sel==0) {
      cout << endl << "CWB::xmdc::ReportDistributions: Error - empty selected mdc entries" << endl << endl;
      gSystem->Exit(1);
    }
    int size_mdc = itree_mdc->GetSelectedRows();
    TH1F *htemp1 = (TH1F*)gPad->GetPrimitive("h");

    TString distance_wave = (cfg.inj_distance>0) ? TString::Format("%f*range[1]",distance_factor) : TString::Format("(%e*%e/strain)",distance_factor,rcfg.hrss_at_1Mpc);

    if(plot_type=="distance") selection=distance_wave;
    if(plot_type=="snr") {
      strcpy(tmp,"iSNR[0]");
      for(int i=1;i<nifo;i++) {sprintf(sel,"%s+iSNR[%d]",tmp,i);strcpy(tmp,sel);}
      strcpy(tmp,sel);sprintf(sel,"sqrt(%s)",tmp);
      selection=sel;
    }
    itree_wave->Draw(selection,TString::Format("ifar/(24*3600*365)>%g && type[1]==%d && strain[1]>0",ifar,inj_id+1),"same");
    int size_wave = itree_wave->GetSelectedRows();
    if(size_wave==0) {
      cout << endl << "CWB::xmdc::ReportDistributions: Warinig - " << inj_name << " empty selected wave entries" << endl << endl;
      continue;
    }
    cout << "size_wave/size_mdc = " << size_wave << "/" << size_mdc << "\tfraction = " << size_wave/double(size_mdc) << endl;

    TH1F *htemp2 = (TH1F*)gPad->GetPrimitive("htemp");
    htemp1->SetTitle(TString::Format("%s (ID=%d) : det/inj = %d/%d",inj_name.Data(),inj_id,size_wave,size_mdc));
    htemp1->GetXaxis()->SetTitle(plot_type + ((plot_type=="distance") ? " ("+rcfg.distance_units+")" : ""));
    htemp1->GetYaxis()->SetTitle("counts");
    htemp1->GetXaxis()->SetTitleOffset(1.35);
    htemp1->GetYaxis()->SetTitleOffset(1.50);
    htemp1->GetXaxis()->CenterTitle(true);
    htemp1->GetYaxis()->CenterTitle(true);
    htemp1->GetXaxis()->SetLabelFont(42);
    htemp1->GetXaxis()->SetTitleFont(42);
    htemp1->GetYaxis()->SetLabelFont(42);
    htemp1->GetYaxis()->SetTitleFont(42);

    if(plot_type=="snr") {
      htemp1->GetYaxis()->SetRangeUser(0.1, pow(10.,TMath::Ceil(TMath::Log10(htemp1->GetMaximum()))));
    } else {
      htemp1->GetXaxis()->SetRangeUser(itree_mdc->GetMinimum(distance_wave),itree_mdc->GetMaximum(distance_wave));
        htemp1->SetMinimum(0.9);
    }

    //htemp1->GetXaxis()->SetMoreLogLabels();
    htemp2->SetFillColor(kRed);
    htemp2->SetLineColor(kRed);
    double hmin = htemp2->GetMinimum()>0 ? htemp2->GetMinimum()/2 : 0.5;
    double hmax = 2*htemp1->GetMaximum();
    htemp1->GetYaxis()->SetRangeUser(hmin,hmax);

    TString ofname = TString::Format("%s/ID_%02d_%s_%s.png",odir.Data(),inj_id,inj_name.Data(),plot_type.Data());
    cout << ofname << endl;

    canvas->Update();
    canvas->Print(ofname);
    delete canvas;
  }
}

//______________________________________________________________________________
void 
CWB::xmdc::ReportDistributions(TString wfname, vector<double> vifar, rconfig rcfg, TString odir) {
//
// Plot distributions of cwb post-production events for each ifar declared in the vifar
//  
// Input: 	wfname		root wave file
//		vifar		ifar vector used to select events (years)
//		rcfg		xmdc report config
//		odir		main output dir
//

  if(gJFNAME=="") {
    cout << "CWB::xmdc::ReportDistributions: Error - to use this method the constructor 'CWB::xmdc::xmdc(TString jfname, bool load_data)' must be used" << endl << endl;
    gSystem->Exit(1);
  }

  CheckMD5(wfname);

  CheckConfig();

  bool answer = CWB::Toolbox::question("please check the xmdc config: do you want to continue ? ");
  if(!answer) exit(1);

  gErrorIgnoreLevel = kWarning;		// Turn off in TCanvas::Print info message 

  if(rcfg.distance_units!="Kpc" && rcfg.distance_units!="Mpc") 
    {cout << "CWB::xmdc::ReportDistributions: Error - distance_units must be Kpc or Mpc" << endl;gSystem->Exit(1);}

  // -----------------------------------------------------
  // make output report directory
  // -----------------------------------------------------

  // read input_ninj and input seed from XMDC config
  int input_seed = cfg.seed;
  int input_ninj = cfg.inj_size;

  // get sub_dir
  TString sub_odir = gSystem->BaseName(wfname.Data());
  sub_odir.ReplaceAll("wave_","");
  sub_odir.ReplaceAll(".S_ifar","");
  sub_odir.ReplaceAll(".root","");
  sub_odir = sub_odir(sub_odir.Index('.')+1, sub_odir.Sizeof()-sub_odir.Index('.'));

  for(int i=0;i<vifar.size();i++) {

    double ifar = vifar[i];

    TString ifar_sdir = TString::Format("ifar%g",ifar); ifar_sdir.ReplaceAll('.',"d");

    TString odir_rep = TString::Format("%s/%s/snr_distance/%s", odir.Data(),sub_odir.Data(),ifar_sdir.Data());
    bool overwrite=CWB::Toolbox::checkFile(odir_rep,true);         // check if output report dir already exists
    if(!overwrite) gSystem->Exit(1);
    gSystem->Exec(TString("mkdir -p ")+odir_rep);                  // make output data directory

    // cleanup xmdc output directories
    gSystem->Exec(TString::Format("rm -f %s/*.png",odir_rep.Data()));
    gSystem->Exec(TString::Format("rm -f %s/*.txt",odir_rep.Data()));
    gSystem->Exec(TString::Format("rm -f %s/*.html",odir_rep.Data()));
    gSystem->Exec(TString::Format("rm -rf %s/png_html_index",odir_rep.Data()));

    // copy user_parameters.C file to xmdc odir_rep
    TString user_parameters_fname = TString(gSystem->Getenv("CWB_UPARAMETERS_FILE"));
    CWB::Toolbox::checkFile(user_parameters_fname);
    gSystem->Exec(TString::Format("cp %s %s/../",user_parameters_fname.Data(),odir_rep.Data()));
    // copy user_pparameters.C file to xmdc odir_rep
    TString user_pparameters_fname = TString(gSystem->Getenv("CWB_UPPARAMETERS_FILE"));
    CWB::Toolbox::checkFile(user_pparameters_fname);
    gSystem->Exec(TString::Format("cp %s %s/../",user_pparameters_fname.Data(),odir_rep.Data()));
    // save xmdc config to odir_rep
    PrintConfig(TString::Format("%s/../xmdc_config.txt",odir_rep.Data()));

    ReportDistributions("distance", wfname, ifar, rcfg, odir_rep);
    ReportDistributions("snr",      wfname, ifar, rcfg, odir_rep);

    // get work directory
    TString work_dir = gSystem->ExpandPathName("$PWD");

    // write report infos file
    TString report_infos =      TString::Format("work  directory     = %s",work_dir.Data());
            report_infos+= "\n"+TString::Format("merge label         = %s",sub_odir.Data());
    WriteReportInfos("postproduction",report_infos,odir_rep);

    // apply cwb_mkhtml to the output plot directory
    TString report_infos_html = "<a#href=\"\"../report_infos.html\"\">report#infos</a>";
    TString report_subtitle = TString::Format("(#%s#)<br><font#color=\"\"black\"\"><h5>%s</h5></font>",
                                               report_infos_html.Data(),work_dir.Data());
    TString cwb_scripts = TString(gSystem->Getenv("CWB_SCRIPTS"));
    char command[1024];
    sprintf(command,"%s/cwb_mkhtml.csh %s '--title #distance/snr#(IFAR>%gy) --subtitle %s --multi true'",
            cwb_scripts.Data(),odir_rep.Data(),ifar,report_subtitle.Data());
    Exec(command,false);
  }
}

//______________________________________________________________________________
void 
CWB::xmdc::ReportTypeDistributions(TString type, int ID, TString jname, TString ofname) {
//
// Plot the XMDC distributions (hrss, snr, psi, iota)
//  
// Input:  	type		name of the selected distribution (hrss, snr, psi, iota)
//		ID		waveform ID
//		jname		waveform name
//		ofname		output file path
//

  TCanvas* canvas = new TCanvas("distribution", "distribution", 300,40, 600, 600);
  canvas->Range(-19.4801,-9.25,-17.4775,83.25);
  canvas->SetBorderSize(2);
  canvas->SetFrameFillColor(0);
  //canvas->SetGridx();
  //canvas->SetLogx();
  canvas->SetLogy();

  gStyle->SetOptStat(0);

  TString selection;
  if(type=="hrss") {selection="abs(hrss)>>h(1000,5e-24,5e-22)";canvas->SetLogx();}
  if(type=="snr")  {selection="snr>>h(1000,0.1,200)";     canvas->SetLogx();}
  if(type=="psi")   selection="psi>>h(180,0,180)";
  if(type=="iota")  selection="iota>>h(180,0,180)";

  gTREE->Draw(selection,TString::Format("ID==%d",ID));
  int size_tested = gTREE->GetSelectedRows();
  TH1F *htemp1 = (TH1F*)gPad->GetPrimitive("h");
  selection = (type=="hrss") ? "abs(hrss)" : type;
  gTREE->Draw(selection,"selected==1 && "+TString::Format("ID==%d",ID),"same");
  int size_selected = gTREE->GetSelectedRows();
  double* value = gTREE->GetV1();
  double value_min=1.e20;
  for(int i=0;i<size_selected;i++) if(value[i]<value_min) value_min=value[i];
  //cout << type << "_min = " << value_min << endl;
  //cout << "size_selected/size_tested = " << size_selected << "/" << size_tested << "\tfraction = " << size_selected/double(size_tested) << endl;

  TH1F *htemp2 = (TH1F*)gPad->GetPrimitive("htemp");
  if(type=="hrss"||type=="snr") htemp1->SetTitle(TString::Format("%s (ID=%d) : sel/tst = %d/%d : %s_min = %0.2e",jname.Data(),ID,size_selected,size_tested,type.Data(),value_min));
  if(type=="psi"||type=="iota") htemp1->SetTitle(TString::Format("%s (ID=%d) : sel/tst = %d/%d",jname.Data(),ID,size_selected,size_tested));
  htemp1->GetXaxis()->SetTitle(type);
  htemp1->GetYaxis()->SetTitle("counts");
  htemp1->GetXaxis()->SetTitleOffset(1.35);
  htemp1->GetYaxis()->SetTitleOffset(1.50);
  htemp1->GetXaxis()->CenterTitle(true);
  htemp1->GetYaxis()->CenterTitle(true);
  htemp1->GetXaxis()->SetLabelFont(42);
  htemp1->GetXaxis()->SetTitleFont(42);
  htemp1->GetYaxis()->SetLabelFont(42);
  htemp1->GetYaxis()->SetTitleFont(42);
  //htemp1->GetXaxis()->SetMoreLogLabels();
  htemp2->SetFillColor(kRed);
  htemp2->SetLineColor(kRed);
  double hmin = htemp2->GetMinimum()>0 ? htemp2->GetMinimum()/2 : 0.5;
  double hmax = 2*htemp1->GetMaximum();
  htemp1->GetYaxis()->SetRangeUser(hmin,hmax);
 
  canvas->Update();
  canvas->Print(ofname);
  delete canvas;
}

//______________________________________________________________________________
void 
CWB::xmdc::ReportThetaPhiDistributions(TString type, int ID, TString jname, TString ofname) {
//
// Plot the theta vs phi distribution of the XMDC injections 
//  
// Input:  	type		name used to tag the output plot file 
//		ID		waveform ID
//		jname		waveform name
//		ofname		output file path
//

  // draw selected injections


  gnetwork* net = new gnetwork(gNET);
  // setup skymap options
  gskymap* gSM = net->GetGskymap();
  gSM->SetOptions("cartesian","Geographic",1);

  TH2D* h2 = (TH2D*)gSM->GetHistogram();
  h2->GetXaxis()->SetTitleSize(0.05);
  h2->GetXaxis()->SetLabelSize(0.05);
  h2->GetYaxis()->SetTitleSize(0.05);
  h2->GetYaxis()->SetLabelSize(0.05);
  h2->GetYaxis()->SetLabelFont(42);
  h2->GetYaxis()->SetLabelFont(42);
  h2->GetXaxis()->SetTitleFont(42);
  h2->GetYaxis()->SetTitleFont(42);
  h2->GetZaxis()->SetRangeUser(0,1.0);

  net->DrawAntennaPattern(3,0,true);

  gTREE->SetEstimate(gTREE->GetEntries());
  gTREE->Draw("theta:phi","selected==1 && "+TString::Format("ID==%d",ID),"goff");
  int isize_sel=gTREE->GetSelectedRows();

  double markerSize_sel = isize_sel>10000 ? 0.3 : 0.4;

  double* itheta_sel = gTREE->GetV1();
  double* iphi_sel   = gTREE->GetV2();

  double otheta_sel,ophi_sel;
  for(int i=0;i<isize_sel;i++) {
    CwbToGeographic(iphi_sel[i],itheta_sel[i],ophi_sel,otheta_sel);
    gSM->DrawMarker(ophi_sel,otheta_sel, 20, markerSize_sel, kBlack);  // Geographic
  }
  gSM->SetTitle(TString::Format("%s (ID=%d) : selected injections = %d",jname.Data(),ID,isize_sel));
  TString ofname_sel = ofname;
  ofname_sel.ReplaceAll(".png","_2_sel.png");
  cout << ofname_sel << endl;
  gSM->Print(ofname_sel);

  delete net;

  // draw tested injections

  gSM = new gskymap((int)4); 			// create gskymap with HEALPix order=4
  gSM->SetOptions("cartesian","celestial",1); 	// set gskymap options (WARNING: with hammer projection the GC and Galactic Disk are not displayed correctly)
  *gSM=0.;

  gTREE->SetEstimate(gTREE->GetEntries());
  gTREE->Draw("theta:phi:gps",TString::Format("ID==%d",ID),"goff");
  int isize_tst=gTREE->GetSelectedRows();

  double* itheta_tst = gTREE->GetV1();
  double* iphi_tst   = gTREE->GetV2();
  double* igps_tst   = gTREE->GetV3();

  double otheta_tst,ophi_tst;
  for(int i=0;i<isize_tst;i++) {
    CwbToCelestial(iphi_tst[i],itheta_tst[i],ophi_tst,otheta_tst,igps_tst[i]);
    otheta_tst=90-otheta_tst;
    ophi_tst=360-ophi_tst;
    int ind = gSM->getSkyIndex(otheta_tst,ophi_tst); 		// fill skymap
    double val = gSM->get(ind);
    gSM->set(ind,val+1);
  }

  // enable galactic disk
  gSM->SetGalacticDisk(0);
  gSM->SetGalacticDiskColor(kRed);

  gSM->Draw(0,"colz"); 				// draw skymap

  // draw galactic center
  double gc_dec = watconstants::GalacticCenterLatitude();
  double gc_ra  = watconstants::GalacticCenterLongitude();
  gc_ra=360-gc_ra;
  gSM->DrawMarker(gc_ra, gc_dec, (int)24, (double)7., kRed);
  gSM->DrawMarker(gc_ra, gc_dec, (int)20, (double)1., kRed);

  h2 = (TH2D*)gSM->GetHistogram();
  h2->GetXaxis()->SetNdivisions(-604);
  TCanvas* canvas = gSM->GetCanvas();
  canvas->Update();

  gSM->SetTitle(TString::Format("%s (ID=%d) : tested injections = %d",jname.Data(),ID,isize_tst));
  TString ofname_tst = ofname;
  ofname_tst.ReplaceAll(".png","_1_tst.png");
  cout << ofname_tst << endl;
  gSM->Print(ofname_tst);
}

//______________________________________________________________________________
void 
CWB::xmdc::ReportGDDistributions(TString wfname, TString view, int ID, TString jname, TString ofname) {
//
// Plot the Galactic Disk distribution of the XMDC injections (used by sky_distribution="mngd/mmgd")
//  
// Input:  	wfname		root wave file, if "" -> use gTREE else use tree in wfname (wave/mdc trees) 
//		view		gd view type, front/side view. front -> x,y plane, side -> x,z plane
//		ID		waveform ID
//		jname		waveform name
//		ofname		output file path
//

  double deg2rad = TMath::Pi()/180.;
  double rad2deg = 180./TMath::Pi();

  skymap sm;          					// used to convert phi -> ra
  CWB::mdc mdc;						// user to get MNGD/MMGD solar coordinates from gc

  TCanvas* canvas = new TCanvas("gd", "gd", 300,40, 600, 600);
  canvas->SetBorderSize(2);
  canvas->SetFrameFillColor(0);
  canvas->SetGridx();
  canvas->SetGridy();
  gStyle->SetPalette(kBird);

  double yzmax = (view=="front") ? 40 : 4;
  TH2D* h2 = new TH2D("gd","gd",100,-40,40,100,-yzmax,yzmax);
  h2->SetStats(0);
  h2->GetXaxis()->SetTitle("Kpc");
  h2->GetXaxis()->SetNdivisions(-204);
  h2->GetXaxis()->CenterTitle();
  h2->GetXaxis()->SetTitleSize(0.03);
  h2->GetXaxis()->SetTitleOffset(1.5);
  h2->GetXaxis()->SetLabelSize(0.03);
  h2->GetXaxis()->SetLabelFont(42);
  h2->GetXaxis()->SetTitleFont(42);
  h2->GetYaxis()->SetTitle("Kpc");
  h2->GetYaxis()->SetNdivisions(-204);
  h2->GetYaxis()->CenterTitle();
  h2->GetYaxis()->SetTitleSize(0.03);
  h2->GetYaxis()->SetLabelSize(0.03);
  h2->GetYaxis()->SetLabelFont(42);
  h2->GetYaxis()->SetTitleFont(42);

  // fiducial circle around the the earth position
  TEllipse circle(0.,0.,sdata.data[ID].fiducial_distance,sdata.data[ID].fiducial_distance);
  circle.SetLineColor(kRed);
  circle.SetFillStyle(0);
  gStyle->SetLineStyleString(11,"20 80");
  circle.SetLineStyle(11);

  // get galactic center coordinates
  XYZVector xyz;
  double xsun,ysun,zsun;
  if(cfg.sky_distribution=="mngd") 	mdc.GetMNGD(xsun,ysun,zsun);		// get sun coordinates from mngd model (galactic disk)
  else if(cfg.sky_distribution=="mmgd") mdc.GetMMGD(xsun,ysun,zsun);		// get sun coordinates from mmgd model (galactic disk)
  xyz.SetXYZ(xsun,ysun,zsun);
  double ilongitude = xyz.Phi()*rad2deg;
  double ilatitude = -(xyz.Theta()-TMath::Pi()/2.)*rad2deg;
  double olongitude,olatitude;
  GalacticToEquatorial(ilongitude, ilatitude, olongitude, olatitude);
  double gc_phi = olongitude;
  double gc_theta = olatitude;
  double gc_rho = sqrt(xyz.mag2());
  GeographicToCwb(gc_phi,gc_theta,gc_phi,gc_theta);
  TMarker *mP = new TMarker(xsun,ysun, 24);
  mP->SetMarkerSize(7);
  mP->SetMarkerColor(kBlack);

  TText *tP = new TText(xsun,ysun,"GC");
  tP->SetTextSize(0.02);
  tP->SetTextColor(kBlack);

  // ---------------------------------------------------------------
  // draw tested injections
  // ---------------------------------------------------------------

  int isize_tst;
  double *irho_tst, *itheta_tst, *iphi_tst, *igps_tst;
  if(wfname=="") {
    gTREE->SetEstimate(gTREE->GetEntries());
    gTREE->Draw("abs(rho):theta:phi:gps",TString::Format("ID==%d",ID),"goff");
    isize_tst=gTREE->GetSelectedRows();

    irho_tst   = gTREE->GetV1();
    itheta_tst = gTREE->GetV2();
    iphi_tst   = gTREE->GetV3();
    igps_tst   = gTREE->GetV4();
  } else {
    // get number of injected events: nsize_mdc
    TString mdc_fname = wfname;
    mdc_fname.ReplaceAll("wave_","mdc_");
    TFile* ifile_mdc = TFile::Open(mdc_fname);
    if(ifile_mdc==NULL) {cout<<"CWB::xmdc::ReportGDDistributions: Error opening file : "<<mdc_fname<<endl;exit(1);}
    TTree* itree_mdc = (TTree *) gROOT->FindObject("mdc");
    if(itree_mdc==NULL) {cout<<"CWB::xmdc::ReportGDDistributions: Error opening tree : "<<"mdc"<<endl;exit(1);}
    int nsize_mdc = itree_mdc->GetEntries();
    itree_mdc->SetEstimate(nsize_mdc);

    itree_mdc->SetEstimate(itree_mdc->GetEntries());
    itree_mdc->Draw("abs(distance):theta[0]:phi[0]:time[0]",TString::Format("type==%d",ID+1),"goff");
    isize_tst=itree_mdc->GetSelectedRows();

    irho_tst   = itree_mdc->GetV1();
    itheta_tst = itree_mdc->GetV2();
    iphi_tst   = itree_mdc->GetV3();
    igps_tst   = itree_mdc->GetV4();
  }

  h2->Reset();
  double otheta_tst, ophi_tst;
  for(int i=0;i<isize_tst;i++) {
    iphi_tst[i]=sm.phi2RA(iphi_tst[i],igps_tst[i]);  		// earth coordinates 2 celestial
    GeographicToCwb(iphi_tst[i],itheta_tst[i],iphi_tst[i],itheta_tst[i]);
    EquatorialToGalactic(iphi_tst[i],itheta_tst[i],ophi_tst,otheta_tst);
    //h2->Fill(ophi_tst,otheta_tst);
    double xgd = irho_tst[i]*cos(ophi_tst*deg2rad);
    double ygd = irho_tst[i]*sin(ophi_tst*deg2rad);
    double zgd = irho_tst[i]*sin(otheta_tst*deg2rad);
    if(view=="front") 	h2->Fill(xgd,ygd);			// galactic plane coordinates
    else		h2->Fill(xgd,zgd);			// galactic side view coordinates
  }
  h2->Draw("colz");
  TString title_tst_tag = (wfname=="") ? "tested injections" : "injected events";
  h2->SetTitle(TString::Format("%s (ID=%d) : %s = %d",jname.Data(),ID,title_tst_tag.Data(),isize_tst));
  mP->Draw(); 							// draw circle around the galactic center
  //tP->Draw();							// draw gd label in the galactic center
  if(sdata.data[ID].fiducial_distance<40) circle.Draw(); 	// draw fiducial circle around the the earth position
  // print plot into ofname
  TString ofname_tst = ofname;
  ofname_tst.ReplaceAll(".png","_1_tst.png");
  canvas->Print(ofname_tst);
  cout << ofname_tst << endl;

  // ---------------------------------------------------------------
  // draw selected injections
  // ---------------------------------------------------------------

  int isize_sel;
  double *irho_sel, *itheta_sel, *iphi_sel, *igps_sel;
  if(wfname=="") {
    gTREE->SetEstimate(gTREE->GetEntries());
    gTREE->Draw("abs(rho):theta:phi:gps","selected==1 && "+TString::Format("ID==%d",ID),"goff");
    isize_sel=gTREE->GetSelectedRows();

    irho_sel   = gTREE->GetV1();
    itheta_sel = gTREE->GetV2();
    iphi_sel   = gTREE->GetV3();
    igps_sel   = gTREE->GetV4();
  } else {
    // get number of detected events: nsize_wave
    TFile* ifile_wave = TFile::Open(wfname);
    if(ifile_wave==NULL) {cout<<"CWB::xmdc::ReportGDDistributions: Error opening file : "<<wfname<<endl;exit(1);}
    TTree* itree_wave = (TTree *) gROOT->FindObject("waveburst");
    if(itree_wave==NULL) {cout<<"CWB::xmdc::ReportGDDistributions: Error opening tree : "<<"waveburst"<<endl;exit(1);}
    int nsize_wave = itree_wave->GetEntries();
    itree_wave->SetEstimate(nsize_wave);

    TList* ifoList = itree_wave->GetUserInfo();
    int nifo = ifoList->GetSize();				// get number of detectors

    itree_wave->SetEstimate(itree_wave->GetEntries());
    itree_wave->Draw(TString::Format("abs(range[1]):theta[1]:phi[1]:time[%d]",nifo),TString::Format("type[1]==%d",ID+1),"goff");
    isize_sel=itree_wave->GetSelectedRows();

    irho_sel   = itree_wave->GetV1();
    itheta_sel = itree_wave->GetV2();
    iphi_sel   = itree_wave->GetV3();
    igps_sel   = itree_wave->GetV4();
  }

  h2->Reset();
  double otheta_sel, ophi_sel;
  for(int i=0;i<isize_sel;i++) {
    iphi_sel[i]=sm.phi2RA(iphi_sel[i],igps_sel[i]);  		// earth coordinates 2 celestial
    //h2->Fill(iphi_sel[i],itheta_sel[i]-90);
    GeographicToCwb(iphi_sel[i],itheta_sel[i],iphi_sel[i],itheta_sel[i]);
    EquatorialToGalactic(iphi_sel[i],itheta_sel[i],ophi_sel,otheta_sel);
    //h2->Fill(ophi_sel,otheta_sel);
    double xgd = irho_sel[i]*cos(ophi_sel*deg2rad);
    double ygd = irho_sel[i]*sin(ophi_sel*deg2rad);
    double zgd = irho_sel[i]*sin(otheta_sel*deg2rad);
    if(view=="front") 	h2->Fill(xgd,ygd);			// galactic plane coordinates
    else		h2->Fill(xgd,zgd);			// galactic side view coordinates
  }
  TString title_sel_tag = (wfname=="") ? "selected injections" : "detected events";
  h2->SetTitle(TString::Format("%s (ID=%d) : %s = %d",jname.Data(),ID,title_sel_tag.Data(),isize_sel));
  h2->Draw("colz");
  if(sdata.data[ID].fiducial_distance<40) circle.Draw(); 	// draw fiducial circle around the the earth position
  mP->Draw(); 							// draw circle around the galactic center
  //tP->Draw();							// draw gd label in the galactic center
  // print plot into ofname
  TString ofname_sel = ofname;
  TString ofname_sel_tag = (wfname=="") ? "_2_sel" : "_2_det";
  ofname_sel.ReplaceAll(".png",TString::Format("%s.png",ofname_sel_tag.Data()));
  canvas->Print(ofname_sel);
  cout << ofname_sel << endl;

  // delete objects
  delete tP;
  delete mP;
  delete h2;
  delete canvas;
}

//______________________________________________________________________________
void 
CWB::xmdc::ReportDistributions(TString ofname) {
//
// Plot the hrss ,snr , psi, iota, theta-phi distributions of XMDC injections 
//  
// Input:  	ofname		output file path
//

  // get number of waveforms
  vector<waveform> wfList = gMDC->GetWaveformList();
  int inj_nid = wfList.size();
  //for(int k=0;k<(int)wfList.size();k++) cout << k << " waveform name " << wfList[k].name << endl;

//  string* name = new string();
//  gTREE->SetBranchAddress("name", &name);

  // dump hrss,snr,... distributions plot
  vector<TString> vplot_odir;
  vector<TString> vplot_sdir;
  vector<TString> vtype = {"hrss","snr","psi","iota","theta_phi"};
  if(cfg.sky_distribution.Contains("gd")) {vtype.push_back("gd-front");vtype.push_back("gd-side");}
  cout << endl;
  for(int i=0;i<vtype.size();i++) {
    TString plot_sdir = "";
    if(vtype[i]=="hrss"  || vtype[i]=="snr")  plot_sdir="hrss_snr";
    if(vtype[i]=="iota"  || vtype[i]=="psi")  plot_sdir="iota_psi";
    if(vtype[i]=="theta_phi")                 plot_sdir="theta_phi";
    if(vtype[i]=="gd-front")                  plot_sdir="gd/front";
    if(vtype[i]=="gd-side")                   plot_sdir="gd/side";
    TString plot_odir = gSystem->DirName(ofname);
    plot_odir.ReplaceAll(XMDC_SUBDIR_DATA,TString::Format("%s/%s",XMDC_SUBDIR_REPORT,plot_sdir.Data()));
    bool exists=CWB::Toolbox::isFileExisting(plot_odir);		// check if output dir already exists
    if(!exists) gSystem->Exec(TString("mkdir -p ")+plot_odir); 		// make output plot directory
    exists=false;
    for(int i=0;i<(int)vplot_odir.size();i++) if(plot_odir==vplot_odir[i]) exists=true;
    if(!exists) {vplot_odir.push_back(plot_odir);vplot_sdir.push_back(plot_sdir);}
    for(int inj_id=0;inj_id<inj_nid;inj_id++) {
      TString inj_name = wfList[inj_id].name;				// waveform name
      TString plot_fname = plot_odir+"/"+gSystem->BaseName(ofname);
      plot_fname.ReplaceAll(".root",TString::Format("_ID_%02d_%s.png",inj_id,vtype[i].Data()));
      if(vtype[i]=="theta_phi") 	ReportThetaPhiDistributions(vtype[i], inj_id, inj_name, plot_fname);
      else if(vtype[i]=="gd-front") 	ReportGDDistributions("", "front", inj_id, inj_name, plot_fname);
      else if(vtype[i]=="gd-side")  	ReportGDDistributions("", "side", inj_id, inj_name, plot_fname);
      else                      	{ReportTypeDistributions(vtype[i], inj_id, inj_name, plot_fname);cout << plot_fname << endl;}
    }
  }

  // get work directory
  TString work_dir = gSystem->ExpandPathName("$PWD");

  // get xmd directory
  TString xmdc_dir = gSystem->DirName(gSystem->DirName(ofname));
  TString report_infos =      TString::Format("work directory      = %s",work_dir.Data());
          report_infos+= "\n"+TString::Format("xmdc directory      = %s",xmdc_dir.Data());

  // apply cwb_mkhtml to the output plot directory
  TString report_infos_html = "<a#href=\"\"../report_infos.html\"\">report#infos</a>";
  TString report_subtitle = TString::Format("(#%s#)<br><font#color=\"\"black\"\"><h5>%s</h5></font>",
                                             report_infos_html.Data(),work_dir.Data());
  for(int i=0;i<vplot_odir.size();i++) {
    // write report infos file
    WriteReportInfos("production",report_infos,vplot_odir[i],cfg.umacro_path_name);
    TString cwb_scripts = TString(gSystem->Getenv("CWB_SCRIPTS"));
    char command[1024];
    TString sky_dist_title="";
    if(cfg.sky_distribution=="uv") sky_dist_title="distributions#(uniform#in#volume)";
    else if(cfg.sky_distribution.Contains("poly")) {
      TString sky_distribution = cfg.sky_distribution;
      sky_distribution.ReplaceAll(" ","#");
      sky_dist_title="distributions#(polynomial#in#distance)#->#"+sky_distribution;
    }
    else if(cfg.sky_distribution.Contains("gd")) sky_dist_title="distributions#(galactic#disk)";
    sprintf(command,"%s/cwb_mkhtml.csh %s '--title %s#%s --subtitle %s --multi true'",
            cwb_scripts.Data(),vplot_odir[i].Data(),vplot_sdir[i].ReplaceAll("_","/").Data(),sky_dist_title.Data(),report_subtitle.Data());
    Exec(command,false);
  }
}

//______________________________________________________________________________
void 
CWB::xmdc::WriteHistory() {
//
// Write history to the XMDC root file 
//
// history contains auxiliary information about the libraries used and the source code of the user XMDC macro config & 
// the name of the user funtion used to initialize MDC
//  

  // --------------------------------------------------------
  // history
  // --------------------------------------------------------

  // declare stages
  const char* STAGE_NAMES[1] = {XMDC_STAGE};

  // declare types
  #define NTYPES	8
  char* TYPE_NAMES[NTYPES];
  for(int i=0;i<NTYPES;i++) TYPE_NAMES[i] = new char[32];

  sprintf(TYPE_NAMES[0], "WORKDIR"); 
  sprintf(TYPE_NAMES[1], "CMDLINE"); 
  sprintf(TYPE_NAMES[2], "WATVERSION"); 
  sprintf(TYPE_NAMES[3], "GITVERSION"); 
  sprintf(TYPE_NAMES[4], "GITBRANCH"); 
  sprintf(TYPE_NAMES[5], "ROOTVERSION"); 
  sprintf(TYPE_NAMES[6], "LALVERSION"); 
  sprintf(TYPE_NAMES[7], "XMDC_VERSION"); 

  // create history object
  CWB::History* history = new CWB::History(const_cast<char**>(STAGE_NAMES), 1, const_cast<char**>(TYPE_NAMES), NTYPES);
  history->SetSortOrder(InsertionOrder);

  // save work dir
  history->AddHistory(const_cast<char*>(XMDC_STAGE), const_cast<char*>("WORKDIR"), gSystem->ExpandPathName("$PWD"));

  // save cmd line
  char cmd_line[512]="";
  for(int i=0;i<gApplication->Argc();i++) sprintf(cmd_line,"%s %s",cmd_line,gApplication->Argv(i));
  //cout << "CMDLINE " << cmd_line << endl;
  history->AddHistory(const_cast<char*>(XMDC_STAGE), const_cast<char*>("CMDLINE"), cmd_line);

  // save library versions
  char framelib_version[32]; sprintf(framelib_version,"%f",FrLibVersion(NULL));
  history->AddHistory(const_cast<char*>(XMDC_STAGE), const_cast<char*>("WATVERSION"), watversion('s'));
  history->AddHistory(const_cast<char*>(XMDC_STAGE), const_cast<char*>("GITVERSION"), watversion('r'));
  history->AddHistory(const_cast<char*>(XMDC_STAGE), const_cast<char*>("GITBRANCH"), watversion('b'));
  history->AddHistory(const_cast<char*>(XMDC_STAGE), const_cast<char*>("ROOTVERSION"), const_cast<char*>(gROOT->GetVersion()));
  history->AddHistory(const_cast<char*>(XMDC_STAGE), const_cast<char*>("LALVERSION"), const_cast<char*>(GetLALVersion().Data()));

  // save XMDC version
  history->AddHistory(const_cast<char*>(XMDC_STAGE), const_cast<char*>("XMDC_VERSION"), const_cast<char*>(TString::Format("%d",GetVersion()).Data()));

  history->Write("history");
  delete history;
}

//______________________________________________________________________________
void 
CWB::xmdc::ReportWaveforms(vector<wavearray<double>> psd, TString odir) {
//
// Plot all waveforms hp,hx in time and frequency domain (frequency domain hp,hx are plotted together with PSD) 
//  
// Input:  	psd		PSD
//		ofname		output file path of the plot
//

  bool batch = gROOT->IsBatch();

  // ----------------------------------------------------
  // define output plot file name
  // ----------------------------------------------------

  TString odir_wf = TString::Format("%s/%s_ninj_%d_seed_%d/%s/time_freq",
                                    odir.Data(),cfg.tag.Data(),cfg.inj_size,cfg.seed,XMDC_SUBDIR_REPORT);
  gSystem->Exec(TString("mkdir -p ")+odir_wf);         			// make output wf directory

  // ----------------------------------------------------
  // Plot Waveforms
  // ----------------------------------------------------

  vector<waveform> wfList = gMDC->GetWaveformList();
  int inj_nid = wfList.size();
  for(int inj_id=0;inj_id<(int)wfList.size();inj_id++) {

    // define output plot file name
    TString ofname = TString::Format("%s/xmdc_%s_ninj_%d_seed_%d_ID_%02d_time.png",
                                     odir_wf.Data(),cfg.tag.Data(),cfg.inj_size,cfg.seed,inj_id);

    ReportWaveform(inj_id, psd, ofname); 
  }

  // ----------------------------------------------------
  // cwb_mkhtml
  // ----------------------------------------------------

  // get work directory
  TString work_dir = gSystem->ExpandPathName("$PWD");

  // get xmd directory
  TString xmdc_dir = TString::Format("%s/%s_ninj_%d_seed_%d",odir.Data(),cfg.tag.Data(),cfg.inj_size,cfg.seed);

  // write report infos file
  TString report_infos =      TString::Format("work directory      = %s",work_dir.Data());
          report_infos+= "\n"+TString::Format("xmdc directory      = %s",xmdc_dir.Data());
  WriteReportInfos("production",report_infos,odir_wf,cfg.umacro_path_name);

  // apply cwb_mkhtml to the output plot directory
  TString report_infos_html = "<a#href=\"\"../report_infos.html\"\">report#infos</a>";
  TString report_subtitle = TString::Format("(#%s#)<br><font#color=\"\"black\"\"><h5>%s</h5></font>",
                                             report_infos_html.Data(),work_dir.Data());
  TString cwb_scripts = TString(gSystem->Getenv("CWB_SCRIPTS"));
  char command[1024];
  sprintf(command,"%s/cwb_mkhtml.csh %s '--title frequency/time --subtitle %s --multi true'",
          cwb_scripts.Data(),odir_wf.Data(),report_subtitle.Data());
  Exec(command,false);

  gROOT->SetBatch(batch);  						// restore batch status
}

//______________________________________________________________________________
void 
CWB::xmdc::ReportWaveform(int inj_id, vector<wavearray<double>> psd, TString ofname) {
//
// Plot waveform hp,hx with inj_id in time and frequency domain (frequency domain hp,hx are plotted together with PSD) 
//  
// Input:  	inj_id		MDC ID of the waveform
//		psd		PSD
//		ofname		output file path of the plot
//

  bool batch = gROOT->IsBatch();
  gROOT->SetBatch(true);
  gStyle->SetTitleFont(12,"D");
  gStyle->SetGridColor(16);

  Color_t DeepSkyBlue4 = CWB::Toolbox::getTableau10BlindColor("DeepSkyBlue4");
  Color_t DarkOrange1 = CWB::Toolbox::getTableau10BlindColor("DarkOrange1");

  // ----------------------------------------------------
  // get waveform(inj_id) from MDC object
  // ----------------------------------------------------

  vector<waveform> wfList = gMDC->GetWaveformList();
  TString sig_name     = wfList[inj_id].name;
  wavearray<double> hp = wfList[inj_id].hp;
  wavearray<double> hx = wfList[inj_id].hx;

  vector<wavearray<double>> sig(2);
  for(int i=0;i<sig.size();i++) {
    sig[i].resize(cfg.inj_len*cfg.inj_srate);
    sig[i].rate(cfg.inj_srate);
    sig[i].start(0);
    sig[i]=0.0;
  
    int hsize = (i==0) ? hp.size() : hx.size();

    // compute offset where to ad hp
    int oS = int((sig[i].size()-hsize)/2);
    if(oS<0) {
      cout << "CWB::xmdc::ReportWaveform: Error - waveform size > user define buffer" << endl;
      gSystem->Exit(1);
    }

    // add hp/hx waveform to sig[i]
    for(int k=0;k<hsize;k++) {
      int j=k+oS;
      if (j>=(int)sig[i].size()) break;
      if (j>=0) sig[i][j] = (i==0) ? hp[k] : hx[k];
    }
  }

  // ----------------------------------------------------
  // plot injection in time domain
  // ----------------------------------------------------

  watplot* pst = NULL;
  for(int i=0;i<sig.size();i++) {
    double hrss=0;
    for(int k=0;k<sig[i].size();k++) hrss+=pow(sig[i][k],2);
    hrss=sqrt(hrss);
    // plot injection in time domain
    Color_t color = (i==0) ? DeepSkyBlue4 : DarkOrange1;
    gMDC->SetZoom(0.999999999);
    TString goption = (i==0) ? "ZOOM" : "SAME";
    pst = gMDC->Draw(sig[i],MDC_TIME,goption);
    pst->getGraph(i)->SetTitle(sig_name+TString::Format("    (hrss=%.2e)",hrss));
    pst->getGraph(i)->SetMarkerColor(color);
    if(i==0) pst->getGraph(i)->SetLineColor(color);
    else     pst->getGraph(i)->SetLineColorAlpha(color,0.464);
    pst->getGraph(i)->SetLineWidth(1);
    pst->getGraph(i)->SetMarkerStyle(1);
  }

  // draw the legend
  TLegend hleg(0.1289398,0.8-0.04,0.265043,0.8776371,NULL,"brNDC");
  hleg.SetTextAlign(22);
  hleg.SetLineColor(kBlack);
  hleg.AddEntry(pst->getGraph(0),TString::Format("%s","hp"),"lp");
  hleg.AddEntry(pst->getGraph(1),TString::Format("%s","hx"),"lp");
  hleg.Draw();

  // save plot
  pst->canvas->SetCanvasSize(700,500);
  pst->canvas->Update();
  pst->canvas->Print(ofname);
  cout << ofname << endl;

  // ----------------------------------------------------
  // plot injection in frequency domain
  // ----------------------------------------------------

  // get number of ifos
  int nifo = gMDC->GetNetwork()->ifoListSize();

  // get detector names
  vector<TString> detector_name;
  for(int i=0;i<nifo;i++) detector_name.push_back(gMDC->GetNetwork()->ifoName[i]);

  #define HRSS_SIG_FOR_FREQ_PLOT	1e-19
  vector<wavearray<double>> a(sig.size()); 					// signal amplitude array
  wavearray<double> f; 								// define frequency array
  for(int i=0;i<sig.size();i++) {

    // normalize sig[i] hrss to HRSS_SIG_FOR_FREQ_PLOT
    double hrss=0;
    for(int k=0;k<sig[i].size();k++) hrss+=pow(sig[i][k],2);
    sig[i]*=HRSS_SIG_FOR_FREQ_PLOT/sqrt(hrss); 

    // resample signal
    sig[i].Resample(cfg.inj_resample);

    // sig FFT
    sig[i].FFT(1);
    double df=(double)sig[i].rate()/(double)sig[i].size();
    sig[i]*=1./df;								// double side spectrum

    if(i==0) {f=psd[0]; for(int k=0;k<f.size();k++) f[k]=k*df;}

    // define signal amplitude array
    a[i]=psd[0];
    for(int k=0;k<a[i].size();k++) a[i][k]=sqrt(pow(sig[i][2*k],2)+pow(sig[i][2*k+1],2));
  }

  // display PSD
  TCanvas canvas;
  canvas.SetLogx();
  canvas.SetLogy();
  canvas.SetGridx();
  canvas.SetGridy();

  vector<TGraph*> grSIG(sig.size());
  for(int i=0;i<sig.size();i++) {
    grSIG[i] = new TGraph(f.size(),f.data,a[i].data);
    Color_t color = (i==0) ? kBlack : kGray+2;
    grSIG[i]->SetMarkerColor(color);
    if(i==0) grSIG[i]->SetLineColor(color);
    else     grSIG[i]->SetLineColorAlpha(color,0.464);
    grSIG[i]->SetLineWidth(1);
    grSIG[i]->SetMarkerStyle(1);
    grSIG[i]->SetTitle(sig_name+TString::Format("    (hrss=%.2e)",HRSS_SIG_FOR_FREQ_PLOT));
    grSIG[i]->GetHistogram()->GetYaxis()->SetRangeUser(1e-24,1e-21);
    grSIG[i]->GetHistogram()->GetXaxis()->SetRangeUser(cfg.inj_flow+1,cfg.inj_fhigh-1);
    grSIG[i]->GetHistogram()->GetXaxis()->SetTitle("Frequency (Hz)");
    grSIG[i]->GetHistogram()->GetYaxis()->SetTitle("[strain / #sqrt{Hz}]   ");
    grSIG[i]->GetHistogram()->GetXaxis()->CenterTitle(true);
    grSIG[i]->GetHistogram()->GetYaxis()->SetTitleOffset(1.2);
    grSIG[i]->GetHistogram()->GetXaxis()->SetTitleOffset(1.30);;
    grSIG[i]->GetHistogram()->GetYaxis()->SetTitleOffset(1.30);;
    if(i==0) grSIG[i]->Draw("ALP");
    else     grSIG[i]->Draw("SAME");
  }
  vector<TGraph*> grPSD(nifo);
  Color_t color = kBlack;
  for(int n=0;n<nifo;n++) {
    grPSD[n] = new TGraph(psd[n].size(),f.data,psd[n].data);
    grPSD[n]->SetTitle(sig_name);
    grPSD[n]->SetLineWidth(2);
    grPSD[n]->SetMarkerStyle(1);
    if(TString(detector_name[n])=="L1") color=TColor::GetColor("#66ccff");
    if(TString(detector_name[n])=="H1") color=kRed;
    if(TString(detector_name[n])=="V1") color=TColor::GetColor("#9900cc");
    grPSD[n]->SetMarkerColor(color);
    grPSD[n]->SetLineColor(color);
    grPSD[n]->Draw("SAME");
  }

  // draw the legend
  TLegend leg(0.1289398,0.78-nifo*0.05,0.265043,0.8776371,NULL,"brNDC");
  leg.SetTextAlign(22);
  leg.SetLineColor(kBlack);
  for(int n=0;n<nifo;n++)       leg.AddEntry(grPSD[n],TString::Format("%s",detector_name[n].Data()),"lp");
  leg.AddEntry(grSIG[0],TString::Format("%s","hp"),"lp");
  leg.AddEntry(grSIG[1],TString::Format("%s","hx"),"lp");
  leg.Draw();

  // print waveform
  ofname.ReplaceAll("time.png","freq.png");
  canvas.Print(ofname);
  cout << ofname << endl;

  gROOT->SetBatch(batch);  // restore batch status
}

//______________________________________________________________________________
TTree* 
CWB::xmdc::GetInjections(vector< wavearray<double> > psd, bool oplot, TString odir) {
//
// Is the main method used to generate injections 
//  
// Input:  	psd		vector of PSD with dimension = number of detectors
//		oplot		if true the plots of the injection distributions are produced
// 		odir		main output dir where the XMDC injection root file and report plots are stored
//
// Output:    	return the XMDC injections tree
//

  // get number of waveforms
  vector<waveform> wfList = gMDC->GetWaveformList();
  int inj_nid = wfList.size();
  //for(int k=0;k<(int)wfList.size();k++) cout << k << " waveform name " << wfList[k].name << endl;

  // get number of ifos
  int nifo = gMDC->GetNetwork()->ifoListSize();

  // get detector names
  vector<TString> detector_name;
  for(int i=0;i<nifo;i++) detector_name.push_back(gMDC->GetNetwork()->ifoName[i]);

  // --------------------------------------------------------
  // define tree injections list
  // --------------------------------------------------------

  TTree* jtree = new TTree(XMDC_TREE_INJ_NAME,XMDC_TREE_INJ_NAME);

  int		selected;
  double	gps;
  string*	name = new string();
  float		theta;
  float		phi;
  float		psi;
  double	rho;
  float		iota;
  double	hrss;
  int 		ID;
  int 		id;
  float		snr;
  float		Qa;
  float		Qp;

  jtree->Branch("selected",	&selected,	"selected/I");
  jtree->Branch("gps",		&gps,		"gps/D");
  jtree->Branch("name",		name);
  jtree->Branch("theta",	&theta,		"theta/F");
  jtree->Branch("phi",		&phi,		"phi/F");
  jtree->Branch("psi",		&psi,		"psi/F");
  jtree->Branch("rho",		&rho,		"rho/D");
  jtree->Branch("iota",		&iota,		"iota/F");
  jtree->Branch("hrss",		&hrss,		"hrss/D");
  jtree->Branch("ID",		&ID,		"ID/I");
  jtree->Branch("id",		&id,		"id/I");
  jtree->Branch("snr",		&snr,		"snr/F");
  if(cfg.qveto_enable) {
    jtree->Branch("Qa",		&Qa,		"Qa/F");
    jtree->Branch("Qp",		&Qp,		"Qp/F");
  }

  // define auxiliary tree used by the ReportDistributions
  if(gTREE!=NULL) delete gTREE;
  gTREE = jtree->CloneTree();

  // --------------------------------------------------------
  // define inj data buffers
  // --------------------------------------------------------

  wavearray<double> x[NIFO_MAX];
  for(int n=0;n<nifo;n++) {
    x[n].resize(cfg.inj_len*cfg.inj_srate);
    x[n].rate(cfg.inj_srate);
    x[n].start(0);
  }

  // --------------------------------------------------------
  // generation of the event list
  // --------------------------------------------------------

  double SNR[NIFO_MAX];

  TStopwatch watchTime;
  watchTime.Start();

  double SNR_min=1000000;
  double SNR_max=0;

  int ninj_gt_snr_thr[inj_nid];				// progressive number of events with snr>snr_min
  int ninj_gt_fiducial_hrss[inj_nid];

  // define the distance distribution 
  TF1* dist = NULL;
  if(cfg.sky_distribution=="uv") {			// define uniform volume distribution -> dist^2 [0,1]
    dist = new TF1("dist","pow(x,2)",0,1);
  } else if(cfg.sky_distribution.Contains("poly")) {	// define volume distribution -> p0 + p1*dist^1 + p2*dist^2 [0,1]
    cout << endl << "WARNING: this feature is not finalized" << endl; 
    cout <<         "         must be used only to generate WNB training sets" << endl << endl; 
    dist = InitPOLY(cfg.sky_distribution);
  }

  cout << endl;
  vector<double> fiducial_distance(inj_nid);

  // start generation of injections
  cout << endl << "Start generation of injections ..." << endl << endl; 
  char sout_header[1024];
  sprintf(sout_header,"#%*s %*s %*s %*s %*s %*s",
               4, "ID", 30, "name", 10, "nsel", 10, "ntst", 10, "fraction", 20, "Elapsed Time (sec)");
  cout << sout_header << endl << endl;

  int seed_trials=0;
  for(int inj_id=0;inj_id<inj_nid;inj_id++) {
    int xseed = cfg.seed+100*inj_id;
    gRandom->SetSeed(xseed);
    int trials=0;
    double xgps=cfg.inj_offset+cfg.inj_step+gRandom->Uniform(-cfg.inj_jitter, cfg.inj_jitter);
    ninj_gt_snr_thr[inj_id]=0;
    // get waveform averaged hrss over the inj_subid input waveforms
    double wf_hrss_avr=0;
    for(int k=0;k<(int)wfList[inj_id].par.size();k++) if(wfList[inj_id].par[k].name=="hrss") wf_hrss_avr+=wfList[inj_id].par[k].value;
    for(int inj_subid=0;inj_subid<wfList[inj_id].list.size();inj_subid++) {
      for(int k=0;k<(int)wfList[inj_id].par.size();k++) {
        if(wfList[inj_id].list[inj_subid].par[k].name=="hrss") wf_hrss_avr+=wfList[inj_id].list[inj_subid].par[k].value;
      }
    }
    wf_hrss_avr/=(1+wfList[inj_id].list.size());

    // compute fiducial distance 
    if(cfg.inj_distance>0) {
      fiducial_distance[inj_id] = cfg.inj_distance*wf_hrss_avr/cfg.inj[inj_id].fiducial_hrss;	// Mpc
      if(cfg.sky_distribution.Contains("gd")) {
        fiducial_distance[inj_id]*=1000.;							// Mpc -> Kpc
        if(cfg.sky_distribution=="mngd") gMDC->InitMNGD(fiducial_distance[inj_id]);
        if(cfg.sky_distribution=="mmgd") gMDC->InitMMGD(fiducial_distance[inj_id]);
      }
    }

    while(true) {

      trials++;
      seed_trials++;

      // select randomly the waveform sub id 
      int inj_subid = int(gRandom->Uniform(0,(int)(1+wfList[inj_id].list.size())));
      // get waveform hrss stored in the mdc class
      double wf_hrss=0;
      if(inj_subid==0) {
        for(int k=0;k<(int)wfList[inj_id].par.size();k++) 
          if(wfList[inj_id].par[k].name=="hrss") wf_hrss=wfList[inj_id].par[k].value;
      } else {
        for(int k=0;k<(int)wfList[inj_id].par.size();k++) 
          if(wfList[inj_id].list[inj_subid-1].par[k].name=="hrss") wf_hrss=wfList[inj_id].list[inj_subid-1].par[k].value;
      }
      double inj_dist = 0.;
      double inj_hrss = 0.;
      if((cfg.sky_distribution=="uv")||(cfg.sky_distribution.Contains("poly"))) {
        inj_dist = dist->GetRandom();				// get random distance -> range [0,1]
        inj_hrss = cfg.inj[inj_id].fiducial_hrss/inj_dist;
        // calculate fiducial distance 
        if(cfg.inj_distance>0) {				// calculate actual injection hrss based on wf_hrss
          inj_dist = fiducial_distance[inj_id]*inj_dist;	// calculate actual distance based on wf_hrss (hrss -> Mpc)
          inj_hrss = wf_hrss*(cfg.inj_distance/inj_dist); 	// injection hrss @ inj_dist
        }
        gMDC->SetInjHrss(inj_hrss);
      } else if (cfg.sky_distribution.Contains("gd")) {
        gMDC->SetInjHrss(0);					// 0 -> hrss is computed using the hrss of the wf read from files
      }
      gMDC->SetSourceListSeed(xseed+seed_trials+111);

      // --------------------------------------------------------
      // define sky distribution
      // --------------------------------------------------------
      vector<mdcpar> par;
      par.resize(6);
      par[0].name="entries"; par[0].value=1;
      par[1].name="rho_min"; par[1].value=0;
      par[2].name="rho_max"; par[2].value=0;
      par[3].name="iota";    par[3].value=cfg.inj_iota;
      par[4].name="ID";      par[4].value=inj_id;     		// ID
      par[5].name="id";      par[5].value=inj_subid;  		// id
      if((cfg.sky_distribution=="uv")||(cfg.sky_distribution.Contains("poly"))) {
        gMDC->SetSkyDistribution(MDC_RANDOM,par,xseed+seed_trials);
      } else if (cfg.sky_distribution=="mngd") {
        gMDC->SetSkyDistribution(MDC_MNGD,par,xseed+seed_trials);
      } else if (cfg.sky_distribution=="mmgd") {
        gMDC->SetSkyDistribution(MDC_MMGD,par,xseed+seed_trials);
      }

      float Qveto[2];Qveto[0]=Qveto[1]=1.e20;
      float qveto, qfactor; 
      double SNRnet=0.;
      bool is_ge_max_trials = ((cfg.inj_max_trials)&&(trials>=cfg.inj_max_trials));	// if it is true then inj is selected regardless of its snr
      for(int n=0;n<nifo;n++) {					// loop over detectors

        x[n].resize(cfg.inj_len*cfg.inj_srate);
        x[n].rate(cfg.inj_srate);
        if((cfg.sky_distribution=="uv")||(cfg.sky_distribution.Contains("poly"))) {
	  x[n].start(0);
        } else if(cfg.sky_distribution.Contains("gd")) {
	  x[n].start(int(xgps)-int(xgps)%cfg.inj_len);		// start must be an integer multiple of cfg.inj_len
	}

        gMDC->Get(x[n],detector_name[n].Data());		// get injection
        if(gMDC->srcList.size()==0) {
	  cout << endl << "CWB::xmdc::GetInjections: Error - source code must be debugged !!!" << endl << endl;
	  gSystem->Exit(1);
	}

        //gMDC->Draw(x[n],gMDC->FFT);return;        		// Draw Waveform in frequency domain
        //gMDC->Draw(x[n]);return;                		// Draw Waveform in time domain

        x[n].Resample(cfg.inj_resample);

        x[n].FFT(1);
        double df=(double)x[n].rate()/(double)x[n].size();
        x[n]*=1./df;						// double side spectrum

        // compute SNR
        SNR[n]=0;
        for(int i=0;i<psd[n].size();i++) {
          double freq=i*df;
          if(freq<cfg.inj_flow || freq>cfg.inj_fhigh) continue;
          SNR[n]+=(x[n][2*i]*x[n][2*i]+x[n][2*i+1]*x[n][2*i+1])/pow(psd[n][i],2)*df;
        }
        SNR[n]*=2;       					// add negative side integration
        SNR[n]=sqrt(SNR[n]);
        SNRnet+=SNR[n]*SNR[n];

	if(cfg.qveto_enable) { 					// compute Qveto[0],Qveto[1]
          for(int i=0;i<psd[n].size();i++) {			// whitening
            double freq=i*df;
            if(freq<cfg.inj_flow || freq>cfg.inj_fhigh) {x[n][2*i]=0.; x[n][2*i+1]=0.;}
            else {x[n][2*i]/=psd[n][i]; x[n][2*i+1]/=psd[n][i];}
          }
          x[n].FFT(-1);
          GetQveto(&x[n], qveto, qfactor);
          if(qveto<Qveto[0])   Qveto[0]=qveto;
          if(qfactor<Qveto[1]) Qveto[1]=qfactor;
        }
      }
      SNRnet=sqrt(SNRnet);
      if(SNRnet<SNR_min) SNR_min=SNRnet;
      if(SNRnet>SNR_max) SNR_max=SNRnet;

      // write event's parameters to event list file
      if((SNRnet>cfg.inj[inj_id].snr_min) && !is_ge_max_trials) ninj_gt_snr_thr[inj_id]++;

      selected = ((SNRnet>cfg.inj[inj_id].snr_min) && !is_ge_max_trials) ? 1 : 0;
      if((cfg.sky_distribution=="uv")||(cfg.sky_distribution.Contains("poly"))) {
        gps=0;
      } else if(cfg.sky_distribution.Contains("gd")) {
	gps=xgps;			// NOTE: we use xgps instead of gMDC->srcList[0].gps because the last one is in integer
      }
      *name    = gMDC->mdcType[gMDC->srcList[0].ID].c_str();
      theta    = gMDC->srcList[0].theta;
      phi      = gMDC->srcList[0].phi;
      psi      = gMDC->srcList[0].psi;
      iota     = gMDC->srcList[0].iota;
      if((cfg.sky_distribution=="uv")||(cfg.sky_distribution.Contains("poly"))) {
        hrss   = inj_hrss;
        rho    = -inj_dist;					// must be negative -> is not used by mdc class, it is only saved in the event log string
      								//                     and it is written on the range output wavebust tree
      } else if (cfg.sky_distribution.Contains("gd")) {
        hrss   = wf_hrss*10/gMDC->srcList[0].rho;		// rho is the distance in Kpc and wf_hrss is the wf hrss at 10 Kpc	
	rho    = -gMDC->srcList[0].rho;				// must be negative -> is not used by mdc class, it is only saved in the event log string
                                                                //                     and it is written on the range output wavebust tree
      }
      ID       = gMDC->srcList[0].ID;
      id       = gMDC->srcList[0].id;
      snr      = SNRnet;
      if(cfg.qveto_enable) {
        Qa     = sqrt(Qveto[0]);
        Qp     = Qveto[1]/(2*sqrt(log10(min((float)200.,snr*snr))));
      }

      gTREE->Fill();
      if((SNRnet>cfg.inj[inj_id].snr_min) && !is_ge_max_trials) {
	jtree->Fill();
        if(cfg.sky_distribution.Contains("gd")) {		// increment gps time
	  xgps=cfg.inj_offset+(ninj_gt_snr_thr[inj_id]+1)*cfg.inj_step+gRandom->Uniform(-cfg.inj_jitter, cfg.inj_jitter);
	}
      }

      if((ninj_gt_snr_thr[inj_id]>=cfg.inj_size) || is_ge_max_trials) break;
    }
    ninj_gt_fiducial_hrss[inj_id]=trials;			// number of events tested
    watchTime.Stop();
    int nsel = ninj_gt_snr_thr[inj_id];
    int ntst = ninj_gt_fiducial_hrss[inj_id];
    double fraction = (double)nsel/(double)ntst;
    char sout_values[1024];
    sprintf(sout_values," %*d %*s %*d %*d %*.4f %*d",
                 4, inj_id, 30,  wfList[inj_id].name.Data(), 10, nsel, 10, ntst, 10, fraction, 20, int(watchTime.RealTime()));
    cout << sout_values << endl;
    watchTime.Continue();
  }
  cout << endl;

  // fill the summary sdata structure with the final summary data
  gTREE->SetEstimate(gTREE->GetEntries());
  for(int inj_id=0;inj_id<inj_nid;inj_id++) {
    gTREE->Draw("selected",TString::Format("ID==%d",inj_id),"goff");
    int ninj_gt_fiducial_hrss = gTREE->GetSelectedRows();
    gTREE->Draw("selected","selected==1 && "+TString::Format("ID==%d",inj_id),"goff");
    int ninj_gt_snr_thr = gTREE->GetSelectedRows();
    jsummary data = {inj_id, wfList[inj_id].name, ninj_gt_snr_thr, ninj_gt_fiducial_hrss,fiducial_distance[inj_id]}; 
    sdata.data.push_back(data); 
  }

  if(dist!=NULL) delete dist;

  return jtree;
}

//______________________________________________________________________________
void 
CWB::xmdc::InitNetwork() {
//
// Initialize the gNET network object using the detector names declared in the user XMDC config 
//  

  // --------------------------------------------------------
  // define network
  // --------------------------------------------------------

  gNET = new network();
  int nifo = cfg.detector_name.size();                                      	// number of detectors
  detector* pD[NIFO_MAX];                                               	// pointers to detectors
  for(int n=0;n<nifo;n++) {
    pD[n] = new detector(const_cast<char*>(cfg.detector_name[n].Data()));
    gNET->add(pD[n]);                                                    	// add detectors to network object
  }
}

//______________________________________________________________________________
vector< wavearray<double> > 
CWB::xmdc::GetPSD() {  
//
// Read PSD from the input files names declared in the user XMDC config 
//  
// Output:  	PSD vector arrays, the vector dimension is number of detectors declared in the user XMDC config  
//

  int nifo = cfg.psd_fname.size();                                      	// number of detectors

  // --------------------------------------------------------
  // read PSD
  // --------------------------------------------------------

  double dFreq=double(cfg.inj_srate)/double(cfg.inj_len*cfg.inj_srate); 	// frequency resolution
  double fWidth = cfg.inj_resample/2.;                                  	// bandwidth
  cout << "fWidth = " << fWidth << " dFreq = " << dFreq << endl;

  vector< wavearray<double> > psd(nifo);
  for(int n=0;n<nifo;n++) {
    psd[n] = CWB::Toolbox::GetDetectorPSD(cfg.psd_fname[n],fWidth,dFreq);   	// psd is one side PSD
    psd[n]*=1./sqrt(2);                                                 	// single -> double side PSD
    cout << "PSD : " << " rate : " << psd[n].rate() << " size : " << psd[n].size() << endl;
  }

  // check if psd is defined in the frequency range [cfg.inj_flow,cfg.inj_fhigh]
  for(int n=0;n<nifo;n++) {
    for(int i=0;i<psd[0].size();i++) {
      double freq = i*dFreq;
      if((freq>=cfg.inj_flow && freq<=cfg.inj_fhigh)&&(psd[n].data[i]<=0)) {
        cout << endl << "CWB::xmdc::GetPSD: Error - file:" << endl << cfg.psd_fname[n] << endl;
	cout << "is zero at frequency " << freq << " (Hz)" << endl;
	cout << "must be >0 in the frequency range [" << cfg.inj_flow << "," << cfg.inj_fhigh << "] (Hz)" << endl << endl;
	gSystem->Exit(1);
      }
    }
  }

  return psd;
}

//______________________________________________________________________________
void 
CWB::xmdc::InitMDC(TFile* jroot) {
//
// Initialize MDC ;
// if cfg.umacro_func_name is empty MDC is initialized using the waveforms tree contained in jroot
// otherwise MDC is initialized using the user config macro stored in the history object contained in the jroot file 
//  
// Input:  	jroot		XMDC root file
//

  // --------------------------------------------------------
  // define gMDC->injections
  // --------------------------------------------------------

  CWB::mdc MDC(gNET);

  MDC.SetInjRate(1./(cfg.inj_len/2.));
  MDC.SetInjJitter(0.0);
  MDC.SetInjOffset(0.);

  CWB_PLUGIN_EXPORT(MDC)
  int seed = cfg.seed;
  CWB_PLUGIN_EXPORT(seed)
  CWB_PLUGIN_EXPORT(jroot)

  unsigned int Pid = gSystem->GetPid();  // used to tag in a unique way the temporary files 
  TString tmp_fname = TString::Format("tmp/xmdc_%s_%d.txt",cfg.tag.Data(),Pid);

  if(cfg.umacro_inj_type=="external") {		// MDC is initializate with the waveforms stored in the wavefors wtree

    InitMDC(jroot, tmp_fname);

  } else {					// MDC is initalize with umacro_func_name  function define in the user macro

    // read user macro from history
    CWB::History* jhistory = GetHistory(jroot);

    TString umacro_source_code  = cfg.umacro_source_code;
    TString umacro_func_name = cfg.umacro_func_name;

    // write user macro into temporary file
    TString tmp_macro_fname = tmp_fname;
    tmp_macro_fname.ReplaceAll(".txt",".C");
    cout << "output file name : " << tmp_macro_fname << endl;

    ofstream out;
    out.open(tmp_macro_fname.Data(),ios::out);
    if(!out.good()) {cout << "CWB::xmdc::InitMDC: Error Opening File : " << tmp_macro_fname << endl;exit(1);}
    out << umacro_source_code;
    out.close();

    TMacro macro_config = TMacro(tmp_macro_fname);
    macro_config.SetName(umacro_func_name);
    macro_config.SetTitle(umacro_func_name);

    // execute config plugin
    int error=0;
    macro_config.Exec(NULL,&error);
    if(error) {
      cout << "CWB::xmdc::InitMDC: Error executing macro : " << macro_config.GetTitle() << endl;
      macro_config.Print();
      gSystem->Exit(1);
    }

    // delete temporary file tmp_macro_fname
    char command[1024];
    sprintf(command,"/bin/rm %s", tmp_macro_fname.Data());
    Exec(command,false);
  }

  gMDC = new CWB::mdc(MDC);
}

//______________________________________________________________________________
void 
CWB::xmdc::CheckVersion(TString jfname) {
//
// Check XMDC version: XMDC object version vs XMDC version in jfname 
//  
// Input:  	jfname		xmdc file name
//

  int xmdc_version = GetVersion();
  int xmdc_jfname_version = GetVersion(jfname);
  if(xmdc_jfname_version!=xmdc_version) {
    cout << endl << "CWB::xmdc::CheckVersion: Error - Object XMDC version " << xmdc_version << 
            " is different from XMDC version " << xmdc_jfname_version << " used in the the file " << jfname.Data() << endl << endl;
    exit(1);
  }
}

//______________________________________________________________________________
void 
CWB::xmdc::CheckConfig(config jcfg) {
//
// Check the input XMDC config jcfg
//  
// Input:  	jcfg		XMDC config
//

  if(jcfg.inj.size()<=0)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj list is empty" << endl << endl;gSystem->Exit(1);}
  for(int i=0;i<jcfg.inj.size();i++) {
    if(jcfg.inj[i].snr_min<0)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj.snr_min"+
	 TString::Format("[%.2f]",jcfg.inj[i].snr_min)+" must be >= 0" << endl << endl;gSystem->Exit(1);}
    if(jcfg.inj[i].fiducial_hrss<=0)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj.fiducial_hrss"+
	 TString::Format("[%.2f]",jcfg.inj[i].fiducial_hrss)+" must be > 0" << endl << endl;gSystem->Exit(1);}
  }

  if(jcfg.detector_name.size()<=0)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - detector_name is empty" << endl << endl;gSystem->Exit(1);}
  if(jcfg.psd_fname.size()!=jcfg.detector_name.size())
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - detector_name size and config.psd_fname size are different !!! " << endl << endl;gSystem->Exit(1);}
  if((jcfg.umacro_inj_type!="builtin")&&(jcfg.umacro_inj_type!="external")) 
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - umacro_inj_type be: (builtin/external)" << endl << endl;gSystem->Exit(1);}

  if((jcfg.sky_distribution!="uv")&&(jcfg.sky_distribution!="mngd")&&(jcfg.sky_distribution!="mmgd")&&(!jcfg.sky_distribution.Contains("poly"))) {
    PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - sky distribution (" << jcfg.sky_distribution << " ) not allowed, must be (uv/poly.../mngd/mmgd)" << endl << endl;
    gSystem->Exit(1);
  }

  if((jcfg.inj_len<0)||(jcfg.inj_len%4!=0))
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj_len must be a positive integer multiple of 4" << endl << endl;gSystem->Exit(1);}
  if(jcfg.inj_flow<0)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj_flow must be > 0" << endl << endl;gSystem->Exit(1);}
  if(jcfg.inj_fhigh<=jcfg.inj_flow)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj_fhigh must be > inj_flow" << endl << endl;gSystem->Exit(1);}
  if(jcfg.inj_distance<0)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj_distance must be >= 0" << endl << endl;gSystem->Exit(1);}
  if(jcfg.inj_size<=0)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj_size must be > 0" << endl << endl;gSystem->Exit(1);}
  if(jcfg.inj_max_trials<0)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj_max_trials must be >= 0" << endl << endl;gSystem->Exit(1);}

  int x=jcfg.inj_srate;		// must be power of 2
  if(!((x != 0) && ((x & (~x + 1)) == x)) || jcfg.inj_srate<=0)
	{PrintConfig(jcfg);cout<<"CWB::xmdc::CheckConfig: Error - inj_srate parameter non valid : must be power of 2 : "<<jcfg.inj_srate<<endl << endl;gSystem->Exit(1);}

  x=jcfg.inj_resample;         	// must be power of 2
  if(!((x != 0) && ((x & (~x + 1)) == x)) || jcfg.inj_resample<=0)
	{PrintConfig(jcfg);cout<<"CWB::xmdc::CheckConfig: Error - inj_resample parameter non valid : must be power of 2 : "<<jcfg.inj_resample<<endl << endl;gSystem->Exit(1);}

  if((jcfg.inj_fiducial_fraction<0)||(jcfg.inj_fiducial_fraction>1))
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj_fiducial_fraction must be in the range [0,1]" << endl << endl;gSystem->Exit(1);}

  if(jcfg.inj_step<jcfg.inj_len)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj_step must be > inj_len" << endl << endl;gSystem->Exit(1);}
  if(jcfg.inj_jitter>(jcfg.inj_step-jcfg.inj_len) || jcfg.inj_jitter<0)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj_jitter must be >= 0 & >= inj_step-inj_len" << endl << endl;gSystem->Exit(1);}
  if(jcfg.inj_offset<0)
	{PrintConfig(jcfg);cout << "CWB::xmdc::CheckConfig: Error - injection inj_offset must be >= 0" << endl << endl;gSystem->Exit(1);}

}

void CWB::xmdc::CheckReportConfig(rconfig rcfg) {

  bool error = false;

  cout << endl;
  cout << "----------------------------------------------------------------" << endl;
  cout << " check xmdc parameters defined in the user_pparameters.C file   " << endl;
  cout << "----------------------------------------------------------------" << endl << endl;

  if(rcfg.gid_name.size()==0) 				{cout << "CWB::xmdc::CheckReportConfig: Error - gid_name is empty" << endl << endl;error=true;}
  if(rcfg.gid.size()==0) 				{cout << "CWB::xmdc::CheckReportConfig: Error - gid is empty" << endl << endl;error=true;}
  for(int i=0;i<rcfg.gid.size();i++) {
    if(rcfg.gid[i].size()==0) 				{cout << "CWB::xmdc::CheckReportConfig: Error - gid"+
							 TString::Format("[%d]",i)+" vector size must be > 0" << endl << endl;error=true;}
  }

  if(rcfg.visibleV_min<0) 				{cout << "CWB::xmdc::CheckReportConfig: Error - visibleV_min must be > 0" << endl << endl;error=true;}
  if(rcfg.visibleV_max<=rcfg.visibleV_min) 		{cout << "CWB::xmdc::CheckReportConfig: Error - visibleV_max must be > visibleV_min" << endl << endl;error=true;}
  if(rcfg.visibleV_ifar.size()==0) 			{cout << "CWB::xmdc::CheckReportConfig: Error - visibleV_ifar is empty" << endl << endl;error=true;}
  else if(rcfg.visibleV_ifar[0]<0) 			{cout << "CWB::xmdc::CheckReportConfig: Error - visibleV_ifar[0] must be > 0" << endl << endl;error=true;}
  for(int i=1;i<rcfg.visibleV_ifar.size();i++) {
    if(rcfg.visibleV_ifar[i]<=rcfg.visibleV_ifar[i-1]) 	{cout << "CWB::xmdc::CheckReportConfig: Error - visibleV_ifar"+
							 TString::Format("[%d]",i)+" must be > visibleV_ifar"+TString::Format("[%d]",i-1) << endl << endl;error=true;}
  }

  if((rcfg.distance_units!="Kpc" && rcfg.distance_units!="Mpc"))
				 			{cout << "CWB::xmdc::CheckReportConfig: Error - wrong distance_units must be (Kpc/Mpc))" << endl << endl;error=true;}
  if(rcfg.hrss_at_1Mpc<0) 				{cout << "CWB::xmdc::CheckReportConfig: Error - hrss_at_1Mpc must be >= 0" << endl << endl;error=true;}

  if(error) {
    cout << "CWB::xmdc::CheckReportConfig: check the xmdc parameters defined in the user_pparameters.C file" << endl << endl;
    gSystem->Exit(1);
  }
}

//______________________________________________________________________________
TString 
CWB::xmdc::MakeInjections(TString odir, bool oplot, TTree* GetWaveforms()) {
//
// is the main method used to generate injections 
// output injections are written on a tree named injections
//  
// Input:  	odir			main output dir where the XMDC injection root file and report plots are stored
//		oplot			if true the plots of the injection distributions are produced
//		GetWaveforms		return the waveform tree, if the tree is NULL the MDC is initialized using the function provided by the user
//
// Output:   	return the path of the output XMDC root file
//

  if(gTREE!=NULL) {
    cout << "CWB::xmdc::MakeInjections: Error - gTREE is not NULL, CWB::xmdc::MakeInjections already done or class created with jfname" << endl;
    exit(1);
  }

  TStopwatch watchTime;
  watchTime.Start();

  gErrorIgnoreLevel = kWarning;		// Turn off in TCanvas::Print info message 

  bool answer = CWB::Toolbox::question("please check the xmdc config: do you want to continue ? ");
  if(!answer) exit(1);

  // --------------------------------------------------------
  // open output root file
  // --------------------------------------------------------

  CWB::Toolbox::checkFile(odir);

  TString odir_xmdc = TString::Format("%s/%s_ninj_%d_seed_%d",odir.Data(),cfg.tag.Data(),cfg.inj_size,cfg.seed);

  TString ofname = TString::Format("%s/%s/xmdc_%s_ninj_%d_seed_%d.root",
                                   odir_xmdc.Data(),XMDC_SUBDIR_DATA,cfg.tag.Data(),cfg.inj_size,cfg.seed);

  bool overwrite=CWB::Toolbox::checkFile(ofname,true);			// check if output file already exists
  if(!overwrite) gSystem->Exit(1);

  // make xmdc output directories
  gSystem->Exec(TString::Format("mkdir -p %s/%s",odir_xmdc.Data(),XMDC_SUBDIR_DATA));         // make xmdc output data directory
  gSystem->Exec(TString::Format("mkdir -p %s/%s",odir_xmdc.Data(),XMDC_SUBDIR_CONFIG));       // make xmdc output config directory
  // cleanup xmdc output directories
  gSystem->Exec(TString::Format("rm -f %s/%s/*.root",odir_xmdc.Data(),XMDC_SUBDIR_DATA));
  gSystem->Exec(TString::Format("rm -f %s/%s/*.C",odir_xmdc.Data(),XMDC_SUBDIR_CONFIG));
  gSystem->Exec(TString::Format("rm -f %s/%s/*.txt",odir_xmdc.Data(),XMDC_SUBDIR_CONFIG));
  gSystem->Exec(TString::Format("rm -f %s/%s/*.html",odir_xmdc.Data(),XMDC_SUBDIR_CONFIG));
  gSystem->Exec(TString::Format("rm -f %s/%s/*/*.png",odir_xmdc.Data(),XMDC_SUBDIR_REPORT));
  gSystem->Exec(TString::Format("rm -f %s/%s/*/*/*.png",odir_xmdc.Data(),XMDC_SUBDIR_REPORT));
  gSystem->Exec(TString::Format("rm -rf %s/%s/*/png_html_index",odir_xmdc.Data(),XMDC_SUBDIR_REPORT));

  // copy input config to xmdc output config directory
  CWB::Toolbox::checkFile(cfg.umacro_path_name);
  gSystem->Exec(TString::Format("cp %s %s/%s",cfg.umacro_path_name.Data(),odir_xmdc.Data(),XMDC_SUBDIR_CONFIG));
  // save xmdc config to xmdc output config directory
  PrintConfig(TString::Format("%s/%s/xmdc_config.txt",odir_xmdc.Data(),XMDC_SUBDIR_CONFIG));

  // open output xmdc root file
  TFile* jroot = TFile::Open(ofname,"RECREATE", "", 101);
  if((jroot==NULL) || (jroot!=NULL && !jroot->IsOpen())) {
    cout << "CWB::xmdc::MakeInjections: Error opening root file " << ofname.Data() << endl;
    exit(1);
  }

  jroot->cd();

  // --------------------------------------------------------
  // write history
  // --------------------------------------------------------

  WriteHistory();

  // --------------------------------------------------------
  // get & save the injection waveforms to the output root file
  // --------------------------------------------------------

  if(GetWaveforms!=NULL) {
    TTree* wtree = GetWaveforms();
    jroot->cd();
    wtree->Write();
    delete wtree;
  }

  // --------------------------------------------------------
  // Init network
  // --------------------------------------------------------

  InitNetwork();

  // --------------------------------------------------------
  // read PSD
  // --------------------------------------------------------

  psd = GetPSD();

  // --------------------------------------------------------
  // Init gMDC
  // --------------------------------------------------------

  InitMDC(jroot);

  if(cfg.inj.size()!=gMDC->wfList.size()) {
    cout << "CWB::xmdc::MakeInjections: Error - size=" << cfg.inj.size() 
         << " of xmdc config inj param must be equals to the size="
         << gMDC->wfList.size() << " of injection types"<< endl << endl;
    gSystem->Exit(1);
  }

  if(cfg.inj_size>0) {		// skip injections production

    // --------------------------------------------------------
    // write injections & XMDC object
    // --------------------------------------------------------

    // get & write injections event list to the output root file
    TTree* jtree = GetInjections(psd, oplot, odir);
    jtree->Write();
    delete jtree;

    // --------------------------------------------------------
    // save xmdc summary to xmdc output summary directory
    // --------------------------------------------------------

    gSystem->Exec(TString::Format("mkdir -p %s/%s",odir_xmdc.Data(),XMDC_SUBDIR_SUMMARY));	// make xmdc output summary directory
    gSystem->Exec(TString::Format("rm -f %s/%s/*.txt",odir_xmdc.Data(),XMDC_SUBDIR_SUMMARY));	// cleanup xmdc output summary directory	
    gSystem->Exec(TString::Format("rm -f %s/%s/*.html",odir_xmdc.Data(),XMDC_SUBDIR_SUMMARY));	// cleanup xmdc output summary directory	
    PrintSummary(TString::Format("%s/%s/xmdc_summary.txt",odir_xmdc.Data(),XMDC_SUBDIR_SUMMARY));
    if(cfg.inj_fiducial_fraction>0) {
      PrintFiducialHrssAtFixedFiducialFraction();
      PrintFiducialHrssAtFixedFiducialFraction(TString::Format("%s/%s/xmdc_fiducial_hrss_at_fixed_fraction.txt",odir_xmdc.Data(),XMDC_SUBDIR_SUMMARY));
    }

    // --------------------------------------------------------
    // Plot distributions
    // --------------------------------------------------------

    if(oplot) ReportDistributions(ofname);
  }

  // --------------------------------------------------------
  // Plot Waveform & PSD in time and frequency
  // --------------------------------------------------------

  if(oplot) ReportWaveforms(psd, odir);

  // --------------------------------------------------------
  // write XMDC object to the output root file
  // --------------------------------------------------------

  // delete global object before to write the XMDC object
  if(gNET!=NULL)  {delete gNET;gNET=NULL;}
  if(gMDC!=NULL)  {delete gMDC;gMDC=NULL;}
  if(gTREE!=NULL) {delete gTREE;gTREE=NULL;}

  this->Write(XMDC_OBJECT_NAME);

  // --------------------------------------------------------
  // close output root file
  // --------------------------------------------------------

  jroot->Close();

  cout << endl << endl;
  cout << "ofile                 - " << ofname << endl;
  cout << endl;
  watchTime.Continue();
  watchTime.Stop();
  cout << "Elapsed Time          - " << watchTime.RealTime() << " (sec)" << endl;
  cout << endl;

  return ofname; 
}

//______________________________________________________________________________
void
CWB::xmdc::ReportGDDistributions(TString wfname, rconfig rcfg, TString odir) {
//
// Plot the Galactic Disk distribution of the detected vs injected events (used by sky_distribution="mngd/mmgd")
//
// Input:       wfname          root wave file
//              rcfg            xmdc report config
//              odir            main output dir
//

  if(!cfg.sky_distribution.Contains("gd")) {
    cout << "CWB::xmdc::ReportGDDistributions: Error - this method can be used only with data processed with sky_distribution='mngd/mmgd'" << endl << endl;
    gSystem->Exit(1);
  }

  if(gJFNAME=="") {
    cout << "CWB::xmdc::ReportGDDistributions: Error - to use this method the constructor 'CWB::xmdc::xmdc(TString jfname, bool load_data)' must be used" << endl << endl;
    gSystem->Exit(1);
  }

  CheckMD5(wfname);

  CheckConfig();

  CWB::Toolbox::checkFile(odir);

  gErrorIgnoreLevel = kWarning;         // Turn off in TCanvas::Print info message

  CheckReportConfig(rcfg);
  if(cfg.inj_distance<=0 && rcfg.hrss_at_1Mpc<=0) {
    cout << "CWB::xmdc::ReportGDDistributions: Error - if cfg.inj_distance<=0 then hrss_at_1Mpc must be > 0" << endl << endl;
    gSystem->Exit(1);
  }

  TString sub_odir = gSystem->BaseName(wfname);
  sub_odir.ReplaceAll("wave_","");
  sub_odir.ReplaceAll(".S_ifar","");
  sub_odir.ReplaceAll(".root","");
  sub_odir = sub_odir(sub_odir.Index('.')+1, sub_odir.Sizeof()-sub_odir.Index('.'));

  bool answer = CWB::Toolbox::question("please check the xmdc config: do you want to continue ? ");
  if(!answer) exit(1);

  TString odir_rep = TString::Format("%s/%s/gd", odir.Data(),sub_odir.Data());
  bool overwrite=CWB::Toolbox::checkFile(odir_rep,true);                // check if output report dir already exists
  if(!overwrite) gSystem->Exit(1);
  gSystem->Exec(TString("mkdir -p ")+odir_rep);                         // make output data directory

  // cleanup xmdc output directories
  gSystem->Exec(TString::Format("rm -f %s/*/*.png",odir_rep.Data()));
  gSystem->Exec(TString::Format("rm -f %s/*/*.txt",odir_rep.Data()));
  gSystem->Exec(TString::Format("rm -f %s/*/*.html",odir_rep.Data()));
  gSystem->Exec(TString::Format("rm -rf %s/*/png_html_index",odir_rep.Data()));

  vector<TString> vplot_odir;
  vector<TString> vplot_sdir;
  vector<TString> vtype = {"gd-front","gd-side"};
  cout << endl;
  int inj_nid = sdata.data.size();
  for(int i=0;i<vtype.size();i++) {
    TString plot_sdir = "";
    if(vtype[i]=="gd-front")	plot_sdir="front";
    if(vtype[i]=="gd-side")	plot_sdir="side";
    TString plot_odir = odir_rep+"/"+plot_sdir;
    bool exists=CWB::Toolbox::isFileExisting(plot_odir);		// check if output dir already exists
    if(!exists) gSystem->Exec(TString("mkdir -p ")+plot_odir); 		// make output plot directory
    exists=false;
    for(int i=0;i<(int)vplot_odir.size();i++) if(plot_odir==vplot_odir[i]) exists=true;
    if(!exists) {vplot_odir.push_back(plot_odir);vplot_sdir.push_back(plot_sdir);}
    for(int inj_id=0;inj_id<inj_nid;inj_id++) {
      TString inj_name = sdata.data[inj_id].name;			// waveform name
      TString plot_fname = TString::Format("%s/ID_%02d_%s_%s.png",plot_odir.Data(),inj_id,inj_name.Data(),vtype[i].Data());
      if(vtype[i]=="gd-front") 		ReportGDDistributions(wfname, "front", inj_id, inj_name, plot_fname);
      else if(vtype[i]=="gd-side")  	ReportGDDistributions(wfname, "side",  inj_id, inj_name, plot_fname);
    }
  }

  // get work directory
  TString work_dir = gSystem->ExpandPathName("$PWD");

  // write report infos file
  TString report_infos =      TString::Format("work  directory     = %s",work_dir.Data());
          report_infos+= "\n"+TString::Format("merge label         = %s",sub_odir.Data());
  WriteReportInfos("production",report_infos,odir_rep,cfg.umacro_path_name);

  // apply cwb_mkhtml to the output plot director,cfg.umacro_path_namey
  TString report_infos_html = "<a#href=\"\"../report_infos.html\"\">report#infos</a>";
  TString report_subtitle = TString::Format("(#%s#)<br><font#color=\"\"black\"\"><h5>%s</h5></font>",
                                             report_infos_html.Data(),work_dir.Data());
  for(int i=0;i<vplot_odir.size();i++) {
    TString cwb_scripts = TString(gSystem->Getenv("CWB_SCRIPTS"));
    char command[1024];
    sprintf(command,"%s/cwb_mkhtml.csh %s '--title #gd/%s#distributions#(galactic#disk) --subtitle %s --multi true'",
            cwb_scripts.Data(),vplot_odir[i].Data(),vplot_sdir[i].Data(),report_subtitle.Data());
    Exec(command,false);
  }
}

//______________________________________________________________________________
void 
CWB::xmdc::ReportVisibleVolume(TString wfname, rconfig rcfg, TString odir) {
//
// Plot visible volume vs ifar of cwb post-production events
// NOTE: the array of ifar values are defined in the xmdc report config
//  
// Input: 	wfname		root wave file
//		rcfg		xmdc report config
//		odir		main output dir
//

  if(gJFNAME=="") {
    cout << "CWB::xmdc::ReportVisibleVolume: Error - to use this method the constructor 'CWB::xmdc::xmdc(TString jfname, bool load_data)' must be used" << endl << endl;
    gSystem->Exit(1);
  }

  double deg2rad = TMath::Pi()/180.;
  double rad2deg = 180./TMath::Pi();

  skymap sm;          			// used to convert phi -> ra
  CWB::mdc mdc;				// used to get MNGD/MNGD mass density distributions

  CheckMD5(wfname);

  CheckConfig();

  CWB::Toolbox::checkFile(odir);

  gErrorIgnoreLevel = kWarning;		// Turn off in TCanvas::Print info message 

  int inj_nid = cfg.inj.size();
  int inj_ngid = rcfg.gid.size();
  int nifar = rcfg.visibleV_ifar.size();

  CheckReportConfig(rcfg);
  if(cfg.inj_distance<=0 && rcfg.hrss_at_1Mpc<=0) {
    cout << "CWB::xmdc::ReportVisibleVolume: Error - if cfg.inj_distance<=0 then hrss_at_1Mpc must be > 0" << endl << endl;
    gSystem->Exit(1);
  }

  // check if rcfg.gid[i][j] are in the range [0,inj_nid]
  for(int i=0;i<rcfg.gid.size();i++) {
    for(int j=0;j<rcfg.gid[i].size();j++) {
      if(rcfg.gid[i][j]<0 || rcfg.gid[i][j]>inj_nid-1) {
	cout << "CWB::xmdc::ReportVisibleVolume: Error - rconfig.gid"+TString::Format("[%d][%d]=%d",i,j,rcfg.gid[i][j])+
                " in the range "+TString::Format("[0,%d]",inj_nid-1) << endl << endl;
        gSystem->Exit(1);
      }
    }
  }

  TString sub_odir = gSystem->BaseName(wfname);
  sub_odir.ReplaceAll("wave_","");
  sub_odir.ReplaceAll(".S_ifar","");
  sub_odir.ReplaceAll(".root","");
  sub_odir = sub_odir(sub_odir.Index('.')+1, sub_odir.Sizeof()-sub_odir.Index('.'));

  // read fractions from XMDC summary
  vector<float> frac(sdata.data.size());
  for(int inj_id=0; inj_id<sdata.data.size();inj_id++) {
     int nsel = sdata.data[inj_id].nsel;
     int ntst = sdata.data[inj_id].ntst;
     frac[inj_id] = (double)nsel/(double)ntst;
     //cout << inj_id << "\tsel - " << nsel << "\tntst - " << ntst << "\tfraction - " << frac[inj_id] << endl;
  }

  bool answer = CWB::Toolbox::question("please check the xmdc config: do you want to continue ? ");
  if(!answer) exit(1);

  TString odir_rep = TString::Format("%s/%s/visible_volume/vs_ifar", odir.Data(),sub_odir.Data());
  bool overwrite=CWB::Toolbox::checkFile(odir_rep,true);		// check if output report dir already exists
  if(!overwrite) gSystem->Exit(1);
  gSystem->Exec(TString("mkdir -p ")+odir_rep);				// make output data directory

  // cleanup xmdc output directories
  gSystem->Exec(TString::Format("rm -f %s/../*.C",odir_rep.Data()));
  gSystem->Exec(TString::Format("rm -f %s/*.txt",odir_rep.Data()));
  gSystem->Exec(TString::Format("rm -f %s/*.html",odir_rep.Data()));
  gSystem->Exec(TString::Format("rm -f %s/*.png",odir_rep.Data()));
  gSystem->Exec(TString::Format("rm -rf %s/png_html_index",odir_rep.Data()));

  // copy user_parameters.C file to xmdc odir_rep
  TString user_parameters_fname = TString(gSystem->Getenv("CWB_UPARAMETERS_FILE"));
  CWB::Toolbox::checkFile(user_parameters_fname);
  gSystem->Exec(TString::Format("cp %s %s/../",user_parameters_fname.Data(),odir_rep.Data()));
  // copy user_pparameters.C file to xmdc odir_rep
  TString user_pparameters_fname = TString(gSystem->Getenv("CWB_UPPARAMETERS_FILE"));
  CWB::Toolbox::checkFile(user_pparameters_fname);
  gSystem->Exec(TString::Format("cp %s %s/../",user_pparameters_fname.Data(),odir_rep.Data()));
  // save xmdc config to odir_rep
  PrintConfig(TString::Format("%s/../xmdc_config.txt",odir_rep.Data()));

  // get number of detected events: nsize_wave
  TFile* ifile_wave = TFile::Open(wfname);
  if(ifile_wave==NULL) {cout<<"CWB::xmdc::ReportVisibleVolume: Error opening file : "<<wfname<<endl;exit(1);}
  TTree* itree_wave = (TTree *) gROOT->FindObject("waveburst");
  if(itree_wave==NULL) {cout<<"CWB::xmdc::ReportVisibleVolume: Error opening tree : "<<"waveburst"<<endl;exit(1);}
  int nsize_wave = itree_wave->GetEntries();
  itree_wave->SetEstimate(nsize_wave);

  // check if ifar is contained in itree_wave
  TBranch* branch;
  bool ifar_exists=false;
  TIter next(itree_wave->GetListOfBranches());
  while ((branch=(TBranch*)next())) {
    if(TString("ifar").CompareTo(branch->GetName())==0) ifar_exists=true;
  }
  if(!ifar_exists) {cout<<"CWB::xmdc::ReportVisibleVolume: Error - wave tree " << wfname << " not includes ifar"<<endl;exit(1);}

  // get number of injected events: nsize_mdc
  TString mdc_fname = wfname;
  mdc_fname.ReplaceAll("wave_","mdc_");
  TFile* ifile_mdc = TFile::Open(mdc_fname);
  if(ifile_mdc==NULL) {cout<<"CWB::xmdc::ReportVisibleVolume: Error opening file : "<<mdc_fname<<endl;exit(1);}
  TTree* itree_mdc = (TTree *) gROOT->FindObject("mdc");
  if(itree_mdc==NULL) {cout<<"CWB::xmdc::ReportVisibleVolume: Error opening tree : "<<"mdc"<<endl;exit(1);}
  int nsize_mdc = itree_mdc->GetEntries();
  itree_mdc->SetEstimate(nsize_mdc);

  // write header
  cout << endl;
  TString fiducialV_name = "fiducialV("+rcfg.distance_units+"^3)";
  TString visibleV_name  = "visibleV("+rcfg.distance_units+"^3)";
  TString evisibleV_name = "evisibleV("+rcfg.distance_units+"^3)";
  char sout_header[1024];
  sprintf(sout_header,"#%*s %*s %*s %*s %*s %*s %*s %*s %*s",
               4, "ID", 16, "name", 10, "ndet", 10, "ninj", 10, "ntst", 18, fiducialV_name.Data(),
               18, visibleV_name.Data(), 18, evisibleV_name.Data(), 14, "epercentage");
  cout << sout_header << endl << endl;

  vector<vector<double>> fiducialV(inj_nid, vector<double>(nifar, 1));
  vector<vector<double>>  visibleV(inj_nid, vector<double>(nifar, 1));
  vector<vector<double>> evisibleV(inj_nid, vector<double>(nifar, 1));
  for(int n=0;n<nifar;n++) {
    cout << endl << "---------------------> ifar = " << rcfg.visibleV_ifar[n] << " y" << endl << endl;
    TString ofname = TString::Format("%s/visible_volume_vs_ifar%gy.txt",odir_rep.Data(),rcfg.visibleV_ifar[n]);
    ofstream out;
    out.open(ofname.Data(),ios::out);
    if(!out.good()) {cout << "CWB::xmdc::ReportVisibleVolume: Error Opening File : " << ofname << endl;exit(1);}
    out << sout_header << endl; 
    for(int i=0;i<inj_nid;i++) {

      int ID = i;
  
      // get number of detected events above the ifar=rcfg.visibleV_ifar[n]: ndet
      TString sel_wave = TString::Format("ifar/(24*3600*365)>%g && type[1]==%d",rcfg.visibleV_ifar[n],ID+1);
      //cout << sel_wave << endl;
      TList* ifoList = itree_wave->GetUserInfo();
      int nifo = ifoList->GetSize();						// get number of detectors
      itree_wave->Draw(TString::Format("time[%d]:range[1]:theta[1]:phi[1]",nifo).Data(),sel_wave.Data(),"goff");
      int ndet=itree_wave->GetSelectedRows();
      double* gps_wave      = itree_wave->GetV1();
      double* distance_wave = itree_wave->GetV2();
      double* theta_wave    = itree_wave->GetV3();
      double* phi_wave      = itree_wave->GetV4();

      // get number of injected events above the ifar=rcfg.visibleV_ifar[n]: ninj
      TString sel_mdc = TString::Format("type[1]==%d",ID+1);
      //cout << sel_mdc << endl;
      itree_mdc->Draw("type[1]",sel_mdc.Data(),"goff");
      int ninj=itree_mdc->GetSelectedRows();

      int ntot = int(ninj/frac[ID]);						// total number of events inside the fiducial volume
										// we consider only the events inside the observational time  !!!
      double distance_factor=1.0;
      if((cfg.sky_distribution=="uv")||(cfg.sky_distribution.Contains("poly")))
        distance_factor = (rcfg.distance_units=="Mpc") ? 1.0 : 1000.0;		// for uv   the fiducial_distance in summary is in Mpc
      else if(cfg.sky_distribution.Contains("gd")) distance_factor = 1;		// for mngd the fiducial_distance in summary is in Kpc 
      if(cfg.inj_distance>0) {
        fiducialV[ID][n] = 4./3.*TMath::Pi()*pow(distance_factor*sdata.data[ID].fiducial_distance,3);		// Fiducial Volume distance_units^3
      } else {
        fiducialV[ID][n] = 4./3.*TMath::Pi()*pow(distance_factor*rcfg.hrss_at_1Mpc/cfg.inj[ID].fiducial_hrss,3);// Fiducial Volume distance_units^3
      }

      if(cfg.sky_distribution=="uv") {

        double dV = fiducialV[ID][n]/ntot;					// volume per event in distance_units^3
        visibleV[ID][n] = ndet*dV;						// Visible Volume
        evisibleV[ID][n] = sqrt(ndet*pow(dV,2));				// Visible Volume Error

      } else if(cfg.sky_distribution.Contains("poly")) {

	cout << endl;
	cout << "WARNING: this feature is not finalized" << endl; 
	cout << "distance distribution now is in the range [0,1], must be in the range [0,fiducial_volume]" << endl;
	cout << endl;
	exit(1);

        TF1* dist = InitPOLY(cfg.sky_distribution);
        double Wdist = dist->Integral(0,1);                              	// for "uv" Wdist is the fiducial volume
        visibleV[ID][n]=0;
        evisibleV[ID][n]=0;
        for(int j=0;j<ndet;j++) {
          double wdist = dist->Eval(distance_wave[i]);
          double dV = (wdist>0) ? (Wdist/wdist)/ntot : 0;
          visibleV[ID][n]  += dV;
          evisibleV[ID][n] += pow(dV,2);
        }
        evisibleV[ID][n] = sqrt(evisibleV[ID][n]);                              // Visible Volume Error
        if(dist!=NULL) delete dist;

      } else if(cfg.sky_distribution.Contains("gd")) {

	TF3* gd = NULL;
	double xsun,ysun,zsun;							// sun coordinates (galactic disk)
	if(cfg.sky_distribution=="mngd") {
	  mdc.InitMNGD(sdata.data[ID].fiducial_distance);
          gd = mdc.GetMNGD(xsun,ysun,zsun);					// get mngd mass density distributiob
	} else if(cfg.sky_distribution=="mmgd") {
	  mdc.InitMMGD(sdata.data[ID].fiducial_distance);
          gd = mdc.GetMMGD(xsun,ysun,zsun);					// get mmgd mass density distributiob
	}
        double Wgd = gd->Integral(-40,40,-40,40,-4,4);				// for "uv" Wgd is the fiducial volume
        visibleV[ID][n]=0;
        evisibleV[ID][n]=0;
        for(int j=0;j<ndet;j++) {
          phi_wave[j]=sm.phi2RA(phi_wave[j],gps_wave[j]);  			// earth coordinates 2 celestial
          GeographicToCwb(phi_wave[j],theta_wave[j],phi_wave[j],theta_wave[j]);
          EquatorialToGalactic(phi_wave[j],theta_wave[j],phi_wave[j],theta_wave[j]);
          double xgd = distance_wave[j]*cos(phi_wave[j]*deg2rad);
          double ygd = distance_wave[j]*sin(phi_wave[j]*deg2rad);
          double zgd = distance_wave[j]*sin(theta_wave[j]*deg2rad);
      	  double wgd = gd->Eval(xgd, ygd, zgd);
	  //if(j<20) cout << j << " " << xgd << " " << ygd << " " << zgd << " " << wgd << endl; 
	  double dV = (wgd>0) ? (Wgd/wgd)/ntot : 0;
	  visibleV[ID][n]  += dV;
	  evisibleV[ID][n] += pow(dV,2);
        }
        evisibleV[ID][n] = sqrt(evisibleV[ID][n]);				// Visible Volume Error
	if(gd!=NULL) delete gd;
      }

      // error pecentage
      double epercentage = (visibleV[ID][n]>0) ? 100*(evisibleV[ID][n]/visibleV[ID][n]) : 0;

      char sout_values[1024];
      sprintf(sout_values," %*d %*s %*d %*d %*d %*.2f %*.5f %*.5f %*.1f",
                 4, ID, 16, sdata.data[ID].name.Data(), 10, ndet, 10, ninj, 10, ntot, 18, fiducialV[ID][n], 18, (visibleV[ID][n]), 18, (evisibleV[ID][n]), 14, epercentage);
      cout << sout_values << endl;
      out << sout_values << endl; 
    }
    out.close();
  }
  cout << endl;

  // ------------------------------------------------------------------------
  // plot the visible volumes vs ifar
  // ------------------------------------------------------------------------

  // init blind colors 
  Color_t color[10];
  color[0] = CWB::Toolbox::getTableau10BlindColor("DeepSkyBlue4");
  color[1] = CWB::Toolbox::getTableau10BlindColor("DarkOrange1");
  color[2] = CWB::Toolbox::getTableau10BlindColor("DarkGray");
  color[3] = CWB::Toolbox::getTableau10BlindColor("DimGray");
  color[4] = CWB::Toolbox::getTableau10BlindColor("SkyBlue3");
  color[5] = CWB::Toolbox::getTableau10BlindColor("Chocolate3");
  color[6] = CWB::Toolbox::getTableau10BlindColor("Gray");
  color[7] = CWB::Toolbox::getTableau10BlindColor("SlateGray1");
  color[8] = CWB::Toolbox::getTableau10BlindColor("SandyBrown");
  color[9] = CWB::Toolbox::getTableau10BlindColor("LightGray");

  gStyle->SetTitleOffset(1.0,"X");
  gStyle->SetTitleOffset(1.2,"Y");
  gStyle->SetLabelFont(42,"X");
  gStyle->SetLabelFont(42,"Y");
  gStyle->SetTitleFont(42,"X");
  gStyle->SetTitleFont(42,"Y");
  gStyle->SetGridStyle(3);
  gStyle->SetGridWidth(1);
  gStyle->SetGridColor(16);

  gStyle->SetTitleH(0.050);
  gStyle->SetTitleW(0.95);
  //gStyle->SetTitleX(0.98);
  gStyle->SetTitleY(0.98);
  gStyle->SetTitleFont(12,"D");
  gStyle->SetTitleColor(kBlue,"D");
  gStyle->SetTextFont(12);
  gStyle->SetTitleFillColor(kWhite);
  gStyle->SetLineColor(kWhite);
  gStyle->SetNumberContours(256);
  gStyle->SetCanvasColor(kWhite);
  gStyle->SetStatBorderSize(1);
  gStyle->SetOptStat(kFALSE);

  // remove the red box around canvas
  gStyle->SetFrameBorderMode(0);
  gROOT->ForceStyle();

  int nset=inj_ngid;
  //int nset=1;

  vector<TCanvas*> 	canvas(inj_ngid);
  vector<TMultiGraph*>  mg(inj_ngid);
  vector<TLegend*>      legend(inj_ngid);
  vector< vector<TGraphErrors*> > gr(inj_ngid);
  int gid_max_size=0; for(int iset=0;iset<nset;iset++) if(rcfg.gid[iset].size()>gid_max_size) gid_max_size=rcfg.gid[iset].size();
  for(int i=0;i<inj_ngid;i++) gr[i] = vector<TGraphErrors*>(gid_max_size);

  TString hrss_at_1Mpc_info = (rcfg.hrss_at_1Mpc>0) ? TString::Format(" (hrss @ 1Mpc = %g)",rcfg.hrss_at_1Mpc) : "";

  for(int iset=0;iset<nset;iset++) {

    mg[iset]     = new TMultiGraph();
    legend[iset] = new TLegend(0.75,0.90-0.02*rcfg.gid[iset].size(),0.99,0.99,NULL,"brNDC");

    for(int k=0;k<rcfg.gid[iset].size();k++) {
      int ID = rcfg.gid[iset][k];
      double* xifar = rcfg.visibleV_ifar.data();
      double* xvisibleV = visibleV[ID].data();
      double* xevisibleV = evisibleV[ID].data();
      gr[iset][k] = new TGraphErrors(nifar,xifar,xvisibleV,0,xevisibleV);
      gr[iset][k]->SetLineColor(color[k]);
      gr[iset][k]->SetLineWidth(2);
      gr[iset][k]->SetMarkerColor(color[k]);
      gr[iset][k]->SetMarkerStyle(20);
      //gr[iset][k]->SetLineStyle(7);
      gr[iset][k]->SetLineStyle(1);

      //for(int i=0; i<nifar; i++) gr[iset][k]->SetPoint(i,rcfg.visibleV_ifar[i],visibleV[ID][i]);
      //for(int i=0; i<nifar; i++) gr[iset][k]->SetPointError(i,rcfg.visibleV_ifar[i],evisibleV[ID][i]);

      mg[iset]->Add(gr[iset][k]);
    }
    char namecanvas[8];
    sprintf(namecanvas,"c%i",iset);
    canvas[iset] = new TCanvas(namecanvas,namecanvas,125+iset*10,82,842,655);
    canvas[iset]->Clear();
    canvas[iset]->ToggleEventStatus();
    canvas[iset]->SetLogy();
    canvas[iset]->SetLogx(true);
    canvas[iset]->SetGridx();
    canvas[iset]->SetGridy();
    canvas[iset]->SetFillColor(kWhite);
    canvas[iset]->cd();

    mg[iset]->SetTitle(rcfg.gid_name[iset]);
    mg[iset]->Paint("alp");
    mg[iset]->GetHistogram()->GetXaxis()->SetTitle("IFAR (Y)");
    mg[iset]->GetHistogram()->GetXaxis()->CenterTitle(true);
    mg[iset]->GetHistogram()->GetXaxis()->SetLabelFont(42);
    mg[iset]->GetHistogram()->GetXaxis()->SetTitleFont(42);
    mg[iset]->GetHistogram()->GetYaxis()->SetLabelFont(42);
    mg[iset]->GetHistogram()->GetYaxis()->SetTitleFont(42);
    mg[iset]->GetHistogram()->GetYaxis()->CenterTitle(true);
    mg[iset]->GetHistogram()->GetYaxis()->SetTitle("Visible Volume ("+rcfg.distance_units+"^{3})"+hrss_at_1Mpc_info);
    mg[iset]->GetHistogram()->GetYaxis()->SetRangeUser(rcfg.visibleV_min,rcfg.visibleV_max);
    mg[iset]->GetHistogram()->GetXaxis()->SetTitleOffset(1.30);;

    mg[iset]->Draw("alp");

    legend[iset]->SetBorderSize(1);
    legend[iset]->SetTextAlign(22);
    legend[iset]->SetTextFont(12);
    legend[iset]->SetLineColor(1);
    legend[iset]->SetLineStyle(1);
    legend[iset]->SetLineWidth(1);
    legend[iset]->SetFillColor(0);
    legend[iset]->SetFillStyle(1001);
    legend[iset]->SetTextSize(0.025);
    legend[iset]->SetLineColor(kBlack);
    legend[iset]->SetFillColor(kWhite);

    for (int k=0;k<rcfg.gid[iset].size();k++) legend[iset]->AddEntry(gr[iset][k],sdata.data[rcfg.gid[iset][k]].name.Data(),"lp");
    legend[iset]->Draw();

    TString ofile = TString::Format("%s/visible_volume_vs_ifar_%s.ps",odir_rep.Data(),rcfg.gid_name[iset].Data());
    cout << ofile << endl;
    canvas[iset]->SaveAs(ofile.Data());

    delete canvas[iset];
  }

  ConvertPS2PNG(odir_rep); 		// Convert all ps files contained in odir_rep to png

  // get work directory
  TString work_dir = gSystem->ExpandPathName("$PWD");

  // write report infos file
  TString report_infos =      TString::Format("work  directory     = %s",work_dir.Data());
          report_infos+= "\n"+TString::Format("merge label         = %s",sub_odir.Data());
  WriteReportInfos("postproduction",report_infos,odir_rep);

  // apply cwb_mkhtml to the output plot directory
  TString report_infos_html = "<a#href=\"\"../report_infos.html\"\">report#infos</a>";
  TString report_subtitle = TString::Format("(#%s#)<br><font#color=\"\"black\"\"><h5>%s</h5></font>",
                                              report_infos_html.Data(),work_dir.Data());
  TString cwb_scripts = TString(gSystem->Getenv("CWB_SCRIPTS"));
  char command[1024];
  sprintf(command,"%s/cwb_mkhtml.csh %s '--title Visible#Volumes#vs#IFAR#%s --subtitle %s'",
                  cwb_scripts.Data(),odir_rep.Data(),hrss_at_1Mpc_info.ReplaceAll(" ","#").Data(),report_subtitle.Data());
  Exec(command,false);
}

//______________________________________________________________________________
CWB::xmdc::config 
CWB::xmdc::GetConfig(TString jfname) {
//
// Get XMDC configuration stored in the xmdc file jfname 
//  
// Input:  	jfname		xmdc file name
//
// Output:    	XMDC configuration
//

  // open injections file name
  TFile* jroot = TFile::Open(jfname);
  if((jroot==NULL) || (jroot!=NULL && !jroot->IsOpen())) {
    cout << "CWB::xmdc::GetConfig: Error opening root file " << jfname.Data() << endl;
    exit(1);
  }

  config CFG = GetConfig(jroot);;

  jroot->Close();

  return CFG;
}

//______________________________________________________________________________
CWB::xmdc::config 
CWB::xmdc::GetConfig(TFile* jroot) {
//
// Get XMDC configuration stored in the xmdc jroot 
//  
// Input:  	jroot		xmdc root file pointer
//
// Output:    	XMDC configuration
//

  // get xmdc object
  CWB::xmdc* XMDC = (CWB::xmdc*)jroot->Get(XMDC_OBJECT_NAME);
  if(XMDC==NULL) {cout << "CWB::xmdc::GetConfig: Error - xmdc object not present in the injection jroot file" << endl;exit(1);}

  CheckConfig(XMDC->cfg);

  return XMDC->cfg;
}

//______________________________________________________________________________
CWB::xmdc::summary 
CWB::xmdc::GetSummary(TString jfname) {
//
// Get XMDC summary stored in the xmdc file jfname 
//  
// Input:  	jfname		xmdc file name
//
// Output:    	XMDC summary
//

  // open injections file name
  TFile* jroot = TFile::Open(jfname);
  if((jroot==NULL) || (jroot!=NULL && !jroot->IsOpen())) {
    cout << "CWB::xmdc::GetConfig: Error opening root file " << jfname.Data() << endl;
    exit(1);
  }

  summary SUMMARY = GetSummary(jroot);;

  jroot->Close();

  return SUMMARY;
}

//______________________________________________________________________________
CWB::xmdc::summary 
CWB::xmdc::GetSummary(TFile* jroot) {
//
// Get XMDC summary stored in the xmdc jroot 
//  
// Input:  	jroot		xmdc root file pointer
//
// Output:    	XMDC summary
//

  // get xmdc object
  CWB::xmdc* XMDC = (CWB::xmdc*)jroot->Get(XMDC_OBJECT_NAME);
  if(XMDC==NULL) {cout << "CWB::xmdc::GetConfig: Error - xmdc object not present in the injection jroot file" << endl;exit(1);}

  CheckConfig(XMDC->cfg);

  return XMDC->sdata;
}

//______________________________________________________________________________
CWB::History*
CWB::xmdc::GetHistory(TFile* jroot) {
//
// Get XMDC history stored in the xmdc jroot 
//  
// Input:  	jroot		xmdc root file pointer
//
// Output:    	XMDC history
//

  // get xmdc object
  CWB::History* jhistory = (CWB::History*)jroot->Get("history");
  if(jhistory==NULL) {
    cout << "CWB::xmdc::GetHistory: Error : injection history is not present in the injection jroot file" << endl;exit(1);
  }

  return jhistory;
}

//______________________________________________________________________________
int 
CWB::xmdc::GetVersion(TString jfname) {
//
// Get XMDC version stored in the history of xmdc file jfname 
//  
// Input:  	jfname		xmdc file name
//
// Output:    	XMDC version
//

  // open injections file name
  TFile* jroot = TFile::Open(jfname);
  if((jroot==NULL) || (jroot!=NULL && !jroot->IsOpen())) {
    cout << "CWB::xmdc::GetVersion: Error opening root file " << jfname.Data() << endl;
    exit(1);
  }

  CWB::History* jhistory = GetHistory(jroot);

  TString sxmdc_version = jhistory->GetHistory(const_cast<char*>("XMDC"),const_cast<char*>("XMDC_VERSION"));
  int xmdc_version = sxmdc_version.Atoi();

  jroot->Close();

  return xmdc_version;
}

//______________________________________________________________________________
vector<wavearray<double>> 
CWB::xmdc::GetPSD(TString jfname) {
//
// Get PSD stored in the xmdc file jfname 
//  
// Input:  	jfname		xmdc file name
//
// Output:    	PSD
//

  // open injections file name
  TFile* jroot = TFile::Open(jfname);
  if((jroot==NULL) || (jroot!=NULL && !jroot->IsOpen())) {
    cout << "CWB::xmdc::GetPSD: Error opening root file " << jfname.Data() << endl;
    exit(1);
  }

  // get xmdc object
  CWB::xmdc* XMDC = (CWB::xmdc*)jroot->Get(XMDC_OBJECT_NAME);
  if(XMDC==NULL) {cout << "CWB::xmdc::GetPSD: Error - xmdc object not present in the injection in file -> " << jfname << endl;exit(1);}

  vector<wavearray<double>> PSD = XMDC->psd;

  jroot->Close();

  return PSD;
}

//______________________________________________________________________________
void 
CWB::xmdc::PrintConfig(config jcfg, TString ofname) {
//
// Print XMDC configuration 
//  
// Input:  	jcfg		xmdc configuration
// Input:  	ofname		if != "" the configuration is saved to ofname file
//  

  std::stringstream buffer;
  std::streambuf* old;
  if(ofname!="") { 		// redirect cout to buffer and return contents
    old = std::cout.rdbuf(buffer.rdbuf());
  }

  char sout[1024];

  cout << endl;
  cout << "----------------------------------------------------------------" << endl;
  cout << " XMDC config                                                    " << endl;
  cout << "----------------------------------------------------------------" << endl;
  cout << endl;
  cout << " psd_fname               = " << endl << endl;
  for(int i=0;i<jcfg.psd_fname.size();i++) cout << "  " << jcfg.detector_name[i] << " -> " << jcfg.psd_fname[i] << "\t" << endl;
  cout << endl;
  cout << " detector_name           =  ";
  for(int i=0;i<jcfg.detector_name.size();i++) cout << jcfg.detector_name[i] << "\t";
  cout << endl << endl;
  cout << " umacro_inj_type         = " << jcfg.umacro_inj_type << endl;
  cout << " umacro_func_name        = " << jcfg.umacro_func_name << endl;
  cout << endl;
  cout << " sky_distribution        = " << jcfg.sky_distribution << endl;
  cout << endl;
  cout << " inj_step                = " << jcfg.inj_step << " (sec)" << endl;
  cout << " inj_jitter              = " << jcfg.inj_jitter << " (sec)" << endl;
  cout << " inj_offset              = " << std::setprecision (15) << jcfg.inj_offset << " (sec)" << endl;
  cout << endl;
  cout << " qveto_enable            = " << (jcfg.qveto_enable?"true":"false") << endl;
  cout << endl;
  cout << " tag                     = " << jcfg.tag << endl;
  cout << " seed                    = " << jcfg.seed << endl;
  cout << endl;
  cout << " inj_size                = " << jcfg.inj_size << endl;
  cout << " inj_max_trials          = " << jcfg.inj_max_trials << endl;
  cout << " inj_len                 = " << jcfg.inj_len << " (sec)" << endl;
  cout << " inj_srate               = " << jcfg.inj_srate << " (Hz)" << endl;
  cout << " inj_flow                = " << jcfg.inj_flow << " (Hz)" << endl;
  cout << " inj_fhigh               = " << jcfg.inj_fhigh << " (Hz)" << endl;
  cout << " inj_distance            = " << jcfg.inj_distance << " (Mpc)" << endl;
  cout << " inj_fiducial_fraction   = " << jcfg.inj_fiducial_fraction << endl;
  cout << endl;
  cout << " inj_iota                = " << jcfg.inj_iota << endl;
  cout << endl;
  cout << " injection parameters    = " << endl << endl;
  sprintf(sout,"%*s %*s %*s", 10, "ID", 16, "snr_min", 16, "fiducial_hrss");
  cout << sout << endl << endl;
  for(int i=0;i<jcfg.inj.size();i++) {
    sprintf(sout,"%*d %*.2f %*.2e", 10, i, 16, jcfg.inj[i].snr_min, 16, jcfg.inj[i].fiducial_hrss);
    cout << sout << endl;
  }
  cout << endl;

  if(ofname!="") { 			// save configuration to ofname file
    ofstream out;
    out.open(ofname.Data(),ios::out);
    if(!out.good()) {cout << "CWB::xmdc::PrintConfig: Error Opening output xmdc configuration file : " << ofname << endl;exit(1);}
    out << buffer.str();
    out.close();

    std::cout.rdbuf(old); 		// restore cout 
  }
}

//______________________________________________________________________________
void 
CWB::xmdc::PrintReportConfig(rconfig rcfg) {
//
// Print XMDC report configuration 
//  

  char sout[1024];

  cout << endl;
  cout << "----------------------------------------------------------------" << endl;
  cout << " XMDC Report rconfig                                             " << endl;
  cout << "----------------------------------------------------------------" << endl;
  cout << endl;
  cout << " xmdc_fname              = " << rcfg.xmdc_fname << endl << endl;
  cout << " gid_name                = ";
  cout << "{ ";
  for(int i=0;i<rcfg.gid_name.size()-1;i++) cout << rcfg.gid_name[i] << ", ";
  cout << rcfg.gid_name[rcfg.gid_name.size()-1] << " }" << endl << endl;
  cout << " gid                     = ";
  cout << "{ ";
  for(int i=0;i<rcfg.gid.size();i++) {
    cout << "{ ";
    for(int j=0;j<rcfg.gid[i].size()-1;j++) cout << rcfg.gid[i][j] << ", ";
    cout << rcfg.gid[i][rcfg.gid[i].size()-1] << " } ";
  }
  cout << "}" << endl << endl;
  cout << " visibleV_min            = " << rcfg.visibleV_min << " (" << rcfg.distance_units << "^3)" << endl;
  cout << " visibleV_max            = " << rcfg.visibleV_max << " (" << rcfg.distance_units << "^3)" << endl;
  cout << " visibleV_ifar           = ";
  cout << "{ ";
  for(int i=0;i<rcfg.visibleV_ifar.size()-1;i++) cout << rcfg.visibleV_ifar[i] << ", ";
  cout << rcfg.visibleV_ifar[rcfg.visibleV_ifar.size()-1] << " }" << endl << endl;
  cout << endl;
  cout << " hrss_at_1Mpc            = " << rcfg.hrss_at_1Mpc << endl;;
  cout << " distance_units          = " << rcfg.distance_units << endl;;
  cout << endl;
}

//______________________________________________________________________________
void 
CWB::xmdc::PrintSummary(TString ofname) {
//
// Print XMDC summary 
//  
// Input:  	ofname		if != "" the summary is saved to ofname file
//  

  std::stringstream buffer;
  std::streambuf* old;
  if(ofname!="") { 		// redirect cout to buffer and return contents
    old = std::cout.rdbuf(buffer.rdbuf());
  }

  char sout[1024];

  cout << endl;
  cout << "-------------------------------------------------------------------------------------------------------" << endl;
  cout << " XMDC summary                                                                                          " << endl;
  cout << "-------------------------------------------------------------------------------------------------------" << endl;
  cout << endl;
  TString fiducial_distance_title="";
  if (cfg.sky_distribution=="uv") 	 		fiducial_distance_title="fiducial_distance(Mpc)";
  else if (cfg.sky_distribution.Contains("poly")) 	fiducial_distance_title="fiducial_distance(Mpc)";
  else if (cfg.sky_distribution.Contains("gd")) 	fiducial_distance_title="fiducial_distance(Kpc)";
  sprintf(sout,"%*s %*s %*s %*s %*s %*s", 5, "ID", 20, "name", 16, "nsel", 16, "ntst", 16, "fraction", 24, fiducial_distance_title.Data());
  cout << sout << endl << endl;
  for(int i=0;i<sdata.data.size();i++) {
    int ID                   = sdata.data[i].ID;
    TString name             = sdata.data[i].name;
    int nsel                 = sdata.data[i].nsel;
    int ntst                 = sdata.data[i].ntst;
    double fiducial_distance = sdata.data[i].fiducial_distance;
    double fraction          = (double)nsel/(double)ntst;
    sprintf(sout,"%*d %*s %*d %*d %*.4f %*.4f", 5, ID, 20, name.Data(), 16, nsel, 16, ntst, 16, fraction, 24, fiducial_distance);
    cout << sout << endl;
  }
  cout << endl;

  if(ofname!="") { 			// save configuration to ofname file
    ofstream out;
    out.open(ofname.Data(),ios::out);
    if(!out.good()) {cout << "CWB::xmdc::PrintSummary: Error Opening output xmdc summary file : " << ofname << endl;exit(1);}
    out << buffer.str();
    out.close();

    std::cout.rdbuf(old); 		// restore cout 
  }
}

//______________________________________________________________________________
void 
CWB::xmdc::PrintFiducialHrssAtFixedFiducialFraction(TString ofname) {
//
// Print XMDC fiducial hrss at fiducial fraction 
//  
// Input:  	ofname		if != "" the fiducial hrss is saved to ofname file
//  

  std::stringstream buffer;
  std::streambuf* old;
  if(ofname!="") { 		// redirect cout to buffer and return contents
    old = std::cout.rdbuf(buffer.rdbuf());
  }

  char sout[1024];

  sprintf(sout,"#-------------------------------------------------------------------------------------------------------");
  cout << endl << sout << endl;
  sprintf(sout,"# XMDC fiducial hrss at fiducial fraction = %.5f", cfg.inj_fiducial_fraction);
  cout << sout << endl;
  sprintf(sout,"#-------------------------------------------------------------------------------------------------------");
  cout << sout << endl << endl;
  sprintf(sout,"  cfg.inj                = {                            // {snr_min, fiducial_hrss}");
  cout << sout << endl;

  for(int i=0;i<cfg.inj.size();i++) {
    int ID                   = sdata.data[i].ID;
    TString name             = sdata.data[i].name;
    int nsel                 = sdata.data[i].nsel;
    int ntst                 = sdata.data[i].ntst;

    double fiducial_hrss =  cfg.inj[i].fiducial_hrss*pow(cfg.inj_fiducial_fraction*ntst/nsel,1./3.);

    TString comma = (i<cfg.inj.size()-1) ? "," : "";
    sprintf(sout,"                                { %.2f, %.2e }%s",cfg.inj[i].snr_min,fiducial_hrss,comma.Data());
    cout << sout << endl;
  }

  sprintf(sout,"                           };");
  cout << sout << endl;
  cout << endl;

  if(ofname!="") { 			// save configuration to ofname file
    ofstream out;
    out.open(ofname.Data(),ios::out);
    if(!out.good()) {cout << "CWB::xmdc::PrintFiducialHrssAtFixedFiducialFraction: Error Opening output file : " << ofname << endl;exit(1);}
    out << buffer.str();
    out.close();

    std::cout.rdbuf(old); 		// restore cout 
  }
}

//______________________________________________________________________________
void 
CWB::xmdc::Exec(TString command, bool verbose) {
//
// execute command 
//  
// Input: 	user command 
//

  if(verbose) cout << endl << "exec -> "+command << endl << endl;

  int fderr,fdout;
  fpos_t poserr,posout;
  if(!verbose) {
    // redirect stderr to /dev/null to getrid of messages produced by cwb_mkhtml
    fflush(stderr); fgetpos(stderr, &poserr);
    fderr = dup(fileno(stderr)); freopen("/dev/null", "w", stderr);
    // redirect stdout to /dev/null to getrid of messages produced by html.Convert
    fflush(stdout); fgetpos(stdout, &posout);
    fdout = dup(fileno(stdout)); freopen("/dev/null", "w", stdout);
  }

  int ret=gSystem->Exec(command.Data());

  if(!verbose) {
    // restore the stderr output
    fflush(stderr); dup2(fderr, fileno(stderr)); close(fderr);
    clearerr(stderr); fsetpos(stderr, &poserr);
    // restore the stdout output
    fflush(stdout); dup2(fdout, fileno(stdout)); close(fdout);
    clearerr(stdout); fsetpos(stdout, &posout);
  }

  if(ret) {
    cout << "CWB::xmdc::Exec: Error while executing command:\n\n" << command << endl << endl;
    gSystem->Exit(1);
  }
}

//______________________________________________________________________________
void 
CWB::xmdc::InitMDC(TFile* jroot, TString tmp_fname) {
//
// Initialize MDC using the waveforms stored in the tree waveforms contained in the root file jroot 
//  
// Input:  	jroot		XMDC root file
//		tmp_fname	temporary file name used to add waveforms to MDC
//

  CWB::mdc* MDC;
  CWB_PLUGIN_IMPORT(CWB::mdc*,MDC);

  cout << "tmp_fname = " << tmp_fname << endl;

  // ------------------------------------------------------------------------------
  // get waveforms tree
  // ------------------------------------------------------------------------------ 

  TTree* wtree = (TTree *)jroot->Get(XMDC_TREE_WAVE_NAME);
  if(wtree) {
    if(wtree->GetEntries()==0) {
      cout << "CWB::xmdc::InitMDC: Error no tree entries in root file" << endl;
      gSystem->Exit(1);
    }
  } else {
    cout << "CWB::xmdc::InitMDC: Error tree waveforms not present in the root file" << endl;
    gSystem->Exit(1);
  }
  int wsize = wtree->GetEntries();

  int ID;
  int id;
  string* name  = new string();
  string* hp    = new string();
  string* hc    = new string();
  vector<mdcpar>* par = new vector<mdcpar>;
  
  wtree->SetBranchAddress("ID",      &ID);
  wtree->SetBranchAddress("id",      &id);
  wtree->SetBranchAddress("name",    &name);
  wtree->SetBranchAddress("hp",      &hp);
  wtree->SetBranchAddress("hc",      &hc);
  wtree->SetBranchAddress("par",     &par);

  TString hp_name, hc_name;

  cout << endl << "CWB::xmdc::InitMDC -> start reading of " << wsize << " waveforms ..." << endl << endl;
  for(int i=0;i<wsize;i++) {

    wtree->GetEntry(i);

    TString wname = name->c_str();
    //cout << i << "\t" << ID << "\t" << id << "\t" << wname <<  "\t" << hp->size() <<  "\t" << hc->size() << endl;
    //for(int n=0;n<par->size();n++) cout << n << "\t" << (*par)[n].name << "\t" << (*par)[n].value << "\t" << (*par)[n].svalue << endl;

    unsigned int Pid = gSystem->GetPid();  // used to tag in a unique way the temporary files 

    hp_name = tmp_fname;
    hp_name.ReplaceAll(".txt",TString::Format("_%s_plus_%d.txt",wname.Data(),Pid).Data());

    ofstream hp_out;
    hp_out.open(hp_name.Data(),ios::out);
    if(!hp_out.good()) {cout << "CWB::xmdc::InitMDC: Error Opening File : " << hp_name << endl;exit(1);}
    hp_out << *hp ;
    hp_out.close();

    hc_name = tmp_fname;
    hc_name.ReplaceAll(".txt",TString::Format("_%s_cross_%d.txt",wname.Data(),Pid).Data());

    ofstream hc_out;
    hc_out.open(hc_name.Data(),ios::out);
    if(!hc_out.good()) {cout << "CWB::xmdc::InitMDC: Error Opening File : " << hc_name << endl;exit(1);}
    hc_out << *hc ;
    hc_out.close();

    //cout << hp_name << endl;
    //cout << hc_name << endl;

    if(hp->size()>0 && hc->size()==0) MDC->AddWaveform(wname,hp_name,16384,*par);
    if(hp->size()>0 && hc->size()>0)  MDC->AddWaveform(wname,hp_name,hc_name,16384,*par);

    // remove temporary files
    char command[1024];
    sprintf(command,"/bin/rm %s", hp_name.Data()); CWB::xmdc::Exec(command,false);
    sprintf(command,"/bin/rm %s", hc_name.Data()); CWB::xmdc::Exec(command,false);
  }

  delete wtree;

  MDC->Print();
}

//______________________________________________________________________________
void 
CWB::xmdc::CheckMD5(TString wfname) {
//
// Check if xmdc_file_md5 used for the wave file wfname is equal to the xmdc_file_md5 used to initialize this xmdc object
//  
// Input:  	wfname		cwb wave root file name
//

  // get xmdc_file_name & xmdc_file_md5 from file wfname history
  TFile* wroot = TFile::Open(wfname);
  CWB::History* whistory = GetHistory(wroot);
  TString xmdc_file_name = whistory->GetHistory(const_cast<char*>(XMDC_STAGE),const_cast<char*>("XMDC_FILE_NAME"));
  TString xmdc_file_md5  = whistory->GetHistory(const_cast<char*>(XMDC_STAGE),const_cast<char*>("XMDC_FILE_MD5"));
  wroot->Close();

  // get the xmdc_file_md5 used to initialize this class
  TMD5* md5 = TMD5::FileChecksum(gJFNAME.Data());
  TString jfname_md5 = md5->AsString();
  delete md5;
  if(xmdc_file_md5!=jfname_md5) {
    cout << "CWB::xmdc::ReportVisibleVolume: Error - xmdc_file_md5 used for the wave file wfname is not equal to the xmdc_file_md5 used to initialize this class" << endl << endl;
    cout << "  -> xmdc_file_md5 used for the wave file wfname: " << endl;
    cout << "    " << xmdc_file_name << endl;
    cout << "  -> xmdc_file_md5 used to initialize this xmdc object: " << endl;
    cout << "    " << gJFNAME << endl << endl;
    gSystem->Exit(1);
  }
}

//______________________________________________________________________________
void 
CWB::xmdc::InitConfig() {
//
// Initialize XMDC config with default values
//  

  cfg.seed               =      150914;                 // input seed used to initialize the random number generator
  cfg.inj_size           =      0;                      // input number of injections to be generated
  cfg.inj_max_trials     =      0;                      // input max number of testing trials, if 0 there are no limits
  cfg.tag                =      "";                     // input name used to the output directories and file names
  cfg.umacro_path_name   =      "";                     // input user macro config path name
  cfg.umacro_source_code =      "";                     // input user macro source code

  cfg.inj_len            =      4;                      // injection buffer lenght (sec)
  cfg.inj_srate          =      16384;                  // injection sample rate (Hz)
  cfg.inj_resample       =      4096.;                  // injection resample rate (Hz)
  cfg.inj_flow           =      16;                     // injection low frequency (Hz)
  cfg.inj_fhigh          =      2046;                   // injection high frequency (Hz)
  cfg.inj_distance       =      0.01;                   // (Mpc) used in GetInjections to compute the fiducial distance. If 0 then the fiducial_hrss is used

  cfg.inj_fiducial_fraction =   0.;                     // if > 0 Print/Dump XMDC fiducial hrss at fiducial fraction

  cfg.inj_step           =      100;                    // injection time interval (sec), used by sky_distribution="mngd/mmgd"
  cfg.inj_jitter         =      10;                     // injection time jitter (sec),   used by sky_distribution="mngd/mmgd"
  cfg.inj_offset         =      0;                      // injection time offset (sec),   used by sky_distribution="mngd/mmgd"

  cfg.inj_iota           =      -1;                     // the line of sight is perpendicular to the orbit plane (-1 -> random)

  cfg.qveto_enable       =      false;                  // true/false -> enable/disable qveto estimation (disable -> save execution time)

  cfg.detector_name      =      {"L1","H1"};            // network detector names

  cfg.psd_fname          = {                            // psd file names
                           };

  cfg.inj                = {                            // {snr_min, fiducial_hrss}
                          };

  cfg.umacro_inj_type    =      "external";             // injection type (builtin/external)
  cfg.umacro_func_name   =      "";                     // macro XMDCConfig_GetWaveforms function name

  cfg.sky_distribution   =      "uv";                   // uv/mngd/mmgd
}

//______________________________________________________________________________
void 
CWB::xmdc::ConvertPS2PNG(TString idir) {
//
// Convert all ps files contained in idir to png
//  

  // convert ps plots to png plots
  vector<TString> psList = CWB::Toolbox::getFileListFromDir(idir, ".ps");
  for (int i=0; i<psList.size(); i++) {
    TString oFile = psList[i];
    oFile.ReplaceAll(".ps", ".png");
    char cmd[2048];
    sprintf(cmd, "gs -q -r300 -dSAFER -dBATCH -dNOPAUSE -sDEVICE=pngalpha -sOutputFile=%s -dAutoRotatePages=/None  -c \"<</Orientation 3>> setpagedevice\" -f %s", oFile.Data(), psList[i].Data());
    //cout << cmd << endl;
    gSystem->Exec(cmd);
    //cout << "Removing ps files...." << endl;
    sprintf(cmd, "rm %s", psList[i].Data());
    //cout << cmd << endl;
    Exec(cmd,false);
  }
}

//______________________________________________________________________________
void 
CWB::xmdc::GetQveto(wavearray<double>* wf, float &Qveto, float &Qfactor) { 
//
// Compute the Qveto -> Qveto[0] & Qfactor -> Qveto[1]
//  
// Input:  	wf		waveform time series
//        	Qveto		Qveto[0]
//        	Qfactor		Qveto[1]
//

  #define NTHR 1                  
  #define ATHR 7.58859            

  wavearray<double> x = *wf;; 

  // resample data by a factor 4
  int xsize=x.size();
  x.FFTW(1);
  x.resize(4*x.size());
  x.rate(4*x.rate());
  for(int j=xsize;j<x.size();j++) x[j]=0;
  x.FFTW(-1);

  // extract max/min values and save the absolute values in the array 'a'
  wavearray<double> a(x.size());
  int size=0;
  double dt = 1./x.rate();
  double prev=x[0];
  double xmax=0;
  for (int i=1;i<x.size();i++) {
    if(fabs(x[i])>xmax) xmax=fabs(x[i]);
    if(prev*x[i]<0) {
      a[size]=xmax;
      size++;
      xmax=0;
    }
    prev=x[i];
  }

  // find max value/index ans save on  amax/imax  
  int imax=-1;
  double amax=0;
  for (int i=1;i<size;i++) {
    if(a[i]>amax) {amax=a[i];imax=i;}
  }

/*
  cout << endl;
  cout << "a[imax-2] " << a[imax-2] << endl;
  cout << "a[imax-1] " << a[imax-1] << endl;
  cout << "a[imax]   " << a[imax] << endl;
  cout << "a[imax+1] " << a[imax+1] << endl;
  cout << "a[imax+2] " << a[imax+2] << endl;
  cout << endl;
*/

  // compute Qveto 
  double ein=0;	// energy of max values inside NTHR
  double eout=0;	// energy of max values outside NTHR
  for (int i=0;i<size;i++) {
    if(abs(imax-i)<=NTHR) {
      ein+=a[i]*a[i];
      //cout << i << " ein " << a[i] << " " << amax << endl;
    } else {
      if(a[i]>amax/ATHR) eout+=a[i]*a[i];
      //if(a[i]>amax/ATHR) cout << i << " eout " << a[i] << " " << amax << endl;
    }
  }
  Qveto = ein>0 ? eout/ein : 0.;
  //cout << "Qveto : " << Qveto << " ein : " << ein << " eout : " << eout << endl;

  // compute Qfactor 
  float R = (a[imax-1]+a[imax+1])/a[imax]/2.;
  Qfactor = sqrt(-pow(TMath::Pi(),2)/log(R)/2.);
  //cout << "Qfactor : " << Qfactor << endl;

  return;
}

//______________________________________________________________________________
void 
CWB::xmdc::WriteReportInfos(TString type, TString infos, TString odir, TString umacro_path_name, bool add_run_infos) {
//
// write report infos file
//  
// Input:  	type		xmdc report production/postproduction
// 	  	infos		infos string
// 		odir		output directory
//

  TString ofname = odir+"/report_infos.html";

  TString umacro_name = gSystem->BaseName(umacro_path_name.Data());

  TString xmdc_config_html      = "<a href=\"../../config/xmdc_config.txt\">xmdc config</a>";
  TString xmdc_macro_html       = TString::Format("<a href=\"../../config/%s\">xmdc macro</a>",umacro_name.Data());
  TString xmdc_summary_html     = "<a href=\"../../summary/xmdc_summary.txt\">xmdc summary</a>";
  TString user_parameters_html  = "<a href=\"../user_parameters.C\">user_parameters.C</a>";
  TString user_pparameters_html = "<a href=\"../user_pparameters.C\">user_pparameters.C</a>";

  ofstream out;
  out.open(ofname.Data(),ios::out);
  if(!out.good()) {cout << "CWB::xmdc::WriteReportInfos: Error Opening File : " << ofname << endl;exit(1);}
  out << "<pre>" << endl;
  out << "# ---------------------------------------------------------------------------- " << endl;
  out << "#                                 Report Infos                                 " << endl;
  out << "# ---------------------------------------------------------------------------- " << endl;
  out << endl;
  TString git_infos = "# cWB git\n";
  git_infos+= "\n"+TString::Format("branch              = %s",watversion('b'));
  git_infos+= "\n"+TString::Format("tag                 = %s",watversion('r'));
  git_infos+= "\n"+TString::Format("date                = %s",watversion('T'));
  out << git_infos.Data();
  out << endl;
  out << endl;
  out << "# reference work directory" << endl;
  if(add_run_infos) {
    TString run_infos;
    if(type=="production") {
      if((odir.EndsWith("gd/front"))||(odir.EndsWith("gd/side"))) {
        xmdc_config_html.ReplaceAll("../../","../../../");
        xmdc_macro_html.ReplaceAll("../../","../../../");
        xmdc_summary_html.ReplaceAll("../../","../../../");
      }
      run_infos = "\n"+TString::Format("xmdc config         = %s",xmdc_config_html.Data());
      run_infos+= "\n"+TString::Format("xmdc macro          = %s",xmdc_macro_html.Data());
      run_infos+= "\n"+TString::Format("xmdc summary        = %s",xmdc_summary_html.Data());
    } else if(type=="postproduction") {
      xmdc_config_html.ReplaceAll("../../config","..");
      run_infos = "\n"+TString::Format("xmdc config         = %s",xmdc_config_html.Data());
      run_infos+= "\n"+TString::Format("user_parameters.C   = %s",user_parameters_html.Data());
      run_infos+= "\n"+TString::Format("user_pparameters.C  = %s",user_pparameters_html.Data());
    }
    out << run_infos.Data();
    out << endl;
  }
  out << endl;
  out << infos.Data();
  out << endl;
  out.close();
}

//______________________________________________________________________________
TF1* 
CWB::xmdc::InitPOLY(TString sky_distribution) {
//
// define distance distribution -> p0 + p1*dist^1 + p2*dist^2 [0,1]
//  
// Input:  	sky_distribution	format = "poly --p0=p0 --p1=p1 --p2=p2"
//
// 		p0 -> distance distribution = p0
// 		p1 -> distance distribution = p1*dist^1
// 		p2 -> distance distribution = p2*dist^2	(uniform in volume)
//
// Output:	return the distance distribution function
//

  TF1* dist = NULL; 
  if(sky_distribution.Contains("poly")) {	

    // get p0,p1,p2 parameters from sky_distribution
    double p0=0,p1=0,p2=0;
    TObjArray* token = sky_distribution.Tokenize(TString(' '));
    for(int j=0;j<token->GetEntries();j++) {

      TObjString* tok = (TObjString*)token->At(j);
      TString stok = tok->GetString();

      if(stok.Contains("--p0=")) {
        stok.Remove(0,stok.Last('=')+1);
        p0 = stok.Atof();;
      } else if(stok.Contains("--p1=")) {
        stok.Remove(0,stok.Last('=')+1);
        p1 = stok.Atof();;
      } else if(stok.Contains("--p2=")) {
        stok.Remove(0,stok.Last('=')+1);
        p2 = stok.Atof();;
      }
    }

    dist = new TF1("dist","[0]+[1]*pow(x,1)+[2]*pow(x,2)",0,1);

    dist->SetParameter(0,p0);
    dist->SetParameter(1,p1);
    dist->SetParameter(2,p2);
    double integral = dist->Integral(0,1);

    // normalize dist distribution to 1
    dist->SetParameter(0,p0/integral);
    dist->SetParameter(1,p1/integral);
    dist->SetParameter(2,p2/integral);
  }

  return dist;
}
