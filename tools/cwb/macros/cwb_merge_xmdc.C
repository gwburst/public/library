/*
# Copyright (C) 2024 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


// merge output log frame files : used by the cwb_merge command

#define CCAST(PAR) const_cast<char*>(PAR)

{
  CWB::Toolbox TB;

  TB.checkFile(gSystem->Getenv("CWB_ROOTLOGON_FILE"));
  TB.checkFile(gSystem->Getenv("CWB_PARAMETERS_FILE"));
  TB.checkFile(gSystem->Getenv("CWB_UPARAMETERS_FILE"));

  // merge mdc log files produced by cwb_xmdc)fra,e.C
  if(simulation!=4)
    {cout << "cwb_merge_xmdc.C - Error : simulation must be = 4 !!!" <<  endl;gSystem->Exit(1);}

  int iversion=0;
  for(int n=0;n<nfactor;n++) {
    iversion=(int)-TMath::Nint(factors[0]+n);
    char cmd[128]; sprintf(cmd,"iversion = %d",iversion);
    EXPORT(int,iversion,cmd)
    gROOT->Macro(gSystem->ExpandPathName("$CWB_MACROS/cwb_merge_log.C"));
  }

  // create list of frames files
  TString frameListFile = TString::Format("%s/frames_%s.lst",merge_dir,data_label);
  char cmd[128]; sprintf(cmd,"ls $PWD/output/*.gwf > %s",frameListFile.Data());
  cout << cmd << endl;
  gSystem->Exec(cmd);
 
  gSystem->Exit(0);
}
