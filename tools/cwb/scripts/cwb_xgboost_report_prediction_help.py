
# -----------------------------------------------------------------------------------------------------------
#
# Report Prediction Example:
#
# cwb_xgboost report  '--type prediction --subtype type --config report_config.py'
#
# where: type = hrho, lrho, hchirp, qaqp, roc, efreq
#
# to select a more subtype plots uses (eg: roc,efreq):
#
# cwb_xgboost report  '--type prediction --subtype roc/efreq --config report_config.py'
#
# -----------------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------
# report configuration file: report_config.py
# --------------------------------------------------------------------------------

# ----------------------------------
# input root files
# ----------------------------------

wave_rhor_fname = 'merge/wave_wlabel.M1.V_hvetoLH.XGB_rthr3_v0.S_ifar.root'				# wave root file with xgb statistic (all)
wave_rho0_fname = 'merge/wave_wlabel.M1.V_hvetoLH.S_bin1a_cut.S_bin1b_cut.S_bin2_cut.root'		# wave root file with pp-cuts statistic (roc/efreq)

mdc_rhor_fname=wave_rhor_fname.replace("wave_","mdc_")							# mdc root file with xgb statistic (roc)
mdc_rho0_fname=wave_rho0_fname.replace("wave_","mdc_")							# mdc root file with pp-cuts statistic (roc)

sim_rhor_fname = wave_rhor_fname
bkg_rhor_fname = 'path/wlabel/merge/wave_wlabel.M1.V_hvetoLH.C_xgb_cut.X_Test_F50.XGB_rthr3_v0.root'	# background root file (hrho/lrho/hchirp/qaqp)

# -----------------------------------------------------------
# roc: efficiency vs far
#
# plot_fname:     output plot file name
# far_inf:        far inferior limit
# far_sup:        far superior limit
# -----------------------------------------------------------

PLOT['roc']  = 'plot_fname=xgb/v0/roc_new_format.png far_inf=0.001 far_sup=10'
ROC['GA:rhor'] = 'wave_fname='+wave_rhor_fname+' line_color=red  wave_cuts=type[1]>=1&&type[1]<=4&&factor!=0'
ROC['GA:rho0'] = 'wave_fname='+wave_rho0_fname+' line_color=blue wave_cuts=type[1]>=1&&type[1]<=4&&factor!=0 trials_factor=3'
ROC['GA:rhor'] += ' mdc_fname='+mdc_rhor_fname+' mdc_cuts=type>=1&&type<=4&&factor!=0'
ROC['GA:rho0'] += ' mdc_fname='+mdc_rho0_fname+' mdc_cuts=type>=1&&type<=4&&factor!=0'

# -----------------------------------------------------------
# qaqp: Qa vs Qp
#
# plot_fname:     output plot file name
# rho_name:       rho name (default = 'rho0') used to select the background entries (rho_name>rho_thr)
# rho_thr:        rho threshold (default = 8) used to select the background entries (rho_name>rho_thr)
# rho_label:      rho label (default = 'rho0') used in the legend
# qfactor:        qfactor value (default = 0.15) used to draw the dashed line Qa = qfactor/(Qp-qoffset)
# qoffset:        qoffset value (default = 0.8)  used to draw the dashed line Qa = qfactor/(Qp-qoffset)
# -----------------------------------------------------------

PLOT['qaqp'] = 'plot_fname=xgb/v0/qaqp_new_format.png rho_name=rho0 rho_thr=9 rho_label=rho0 qfactor=0.15 qoffset=0.6 qa_sup=6.0 qp_sup=10.0'
QAQP['sim'] = 'wave_fname='+wave_rhor_fname+' wave_cuts=type[1]>=1&&type[1]<=4&&factor!=0'
QAQP['bkg'] = 'wave_fname='+bkg_rhor_fname+' marker_color=red marker_size=150'

# -----------------------------------------------------------
# efreq: efficiency vs frequency[0]
#
# plot_fname:     output plot file name
# fmin:           xaxis minimum frequency
# fmax:           xaxis maximum frequency
# fbin:           xaxis number of bins
# ifar:           ifar threshold
# -----------------------------------------------------------

PLOT['efreq']  = 'plot_fname=xgb/v0/efreq_new_format.png fmin=20 fmax=1000 fbin=44 ifar=100'
EFREQ['rho_r'] = 'ifname='+wave_rhor_fname+' color=red'
EFREQ['rho_0'] = 'ifname='+wave_rho0_fname+' color=blue'

# -----------------------------------------------------------
# hrho: sim/bkg rho histogram
#
# plot_fname:     output plot file name
# rho_label:      rho label (default = 'rho0') used in the legend
# -----------------------------------------------------------

PLOT['hrho'] = 'plot_fname=xgb/v0/hrho_new_format.png rho_label=$rho_r$'
HRHO['sim'] = 'ifname='+sim_rhor_fname+' rhoid=1 color=green'
HRHO['bkg'] = 'ifname='+bkg_rhor_fname+' rhoid=1 color=blue'

# -----------------------------------------------------------
# lrho: likelihood vs rho
#
# plot_fname:     output plot file name
# rho_label:      rho label (default = 'rho0') used in the legend
# -----------------------------------------------------------

PLOT['lrho'] = 'plot_fname=xgb/v0/lrho_new_format.png rho_label=$rho_r$'
LRHO['sim'] = 'ifname='+sim_rhor_fname+' rhoid=1 color=green'
LRHO['bkg'] = 'ifname='+bkg_rhor_fname+' rhoid=1 color=blue rho0_capvalue=8'

# -----------------------------------------------------------
# hchirp: sim/bkg chirp histogram
#
# plot_fname:     output plot file name
# rho_label:      rho label (default = 'rho0') used in the legend
# -----------------------------------------------------------

PLOT['hchirp'] = 'plot_fname=xgb/v0/hchirp_new_format.png rho_label=$rho_r$'
HCHIRP['sim'] = 'ifname='+sim_rhor_fname+' color=green'
HCHIRP['bkg'] = 'ifname='+bkg_rhor_fname+' color=blue'


