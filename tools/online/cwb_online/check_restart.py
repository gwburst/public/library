#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago, Igor Yakushin, Sergey Klimenko
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess, sys, os

def check_restart(conf,log,opt):
  if (not os.path.exists(log)):
      subprocess.getstatusotuput("touch %s"%log)
  #com="fuser %s"%(log)
  #a=subprocess.getstatusoutput(com)
  # print a
  #processes=[]
  #try:
  #    processes=[x for x in [x.strip() for x in a[-1].split(":")[-1].split(" ")] if len(x)>0]
  #    print processes
  #except Exception as e:
  #    pass
  #    print e
  #if(len(processes)==0):
  com="pgrep -a -f 'cwb_online_exec %s %s'"%(conf,opt)
  a=subprocess.getstatusoutput(com)
  if (len(a[1])==0):
      t=subprocess.getstatusoutput("date")[-1].strip()
      f=open(log,"a")
      print("=|"*30, file=f)
      print("Restarting at %s"%(t), file=f)
      print("=|"*30, file=f)    
      f.close()
      com="nohup cwb_online_exec %s %s >> %s 2>&1 &"%(conf,opt,log)
      print(com)
      a=subprocess.getstatusoutput(com)
      print(a[1])
