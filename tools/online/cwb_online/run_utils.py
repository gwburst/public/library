#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago, Igor Yakushin, Sergey Klimenko
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess
import glob
from subprocess import Popen, PIPE
from optparse import OptionParser
import math

def evalfunc(x,name):
  return eval(name.replace("sqrt","math.sqrt").replace("log10","math.log10"))

def time_convert(string):
    return subprocess.getstatusoutput("lal_tconvert %s"%string)

def get_time(run_offset=0):
    a=time_convert("now")
    return int(a[1].strip())-run_offset

def sconvert(gps):
    a=time_convert(gps)
    return a[1].strip()

def tconvert(gps,gtype=int):
    a=time_convert("%s"%(gtype(float(gps))))
    return a[1].strip()

def totable(header,rows):
    t="<table border=1>"
    h="<tr><th>"+"</th><th>".join(header)+"</th></tr>"
    b=["<tr><td>"+"</td><td>".join(x)+"</td></tr>" for x in rows]
    f="</table>"
    return t+"\n"+h+"\n".join(b)+"\n"+f

def add_counter_column(rows):
    return [[str(x[0])]+x[1] for x in zip(list(range(1,len(rows)+1)),rows)]

def getnumberfromstring(s):
  try:
    num=float(s)
    if (num.is_integer()):
        num=int(num)
  except:
    num=s
  return num

def setparse(istring):
   splits=istring.split()
   parser = OptionParser()
   (options, args) = parser.parse_args()
   for s in splits:
    if (s.startswith("-")):
      args=s.replace("-","")
    else:
      setattr(options,args,getnumberfromstring(s))
   return options

def get_date_from_gps(ctime):
    a=time_convert("-f \"%Y %m %d\" "+repr(ctime))
    t=a[1].replace(" ","-")
    return t

def get_start_of_the_day_gps(ctime):
    a=time_convert("-f \"%D %T\" "+repr(ctime))
    t=a[1].split()
    timezero="00:00:00"
    a=time_convert(t[0]+" "+timezero)
    day=int(a[1].strip())
    return day

def get_start_of_the_day_string(ctime):
    timezero="00:00:00"
    a=time_convert(ctime+" "+timezero)
    day=int(a[1].strip())
    return day

class tdate:
  def __init__(self,date):
    if (isinstance(date,str)):
      self.date=date
    else:
      self.date=get_date_from_gps(date)
    self.setvar()
  def setvar(self):
      self.year=int(self.date.split("-")[0])
      self.month=int(self.date.split("-")[1])
      self.day=int(self.date.split("-")[2])
      self.time=get_start_of_the_day_string(self.date)

class row:
  def __init__(self,lines,cWB_conf):
    name=lines[0]
    var=lines[1]
    if (var.find(".png")!=-1):
      self.png=True
    else:
      self.png=False
    roun=lines[2]
    expr=lines[3]
    expl=lines[4]
    if (name.find("*")!=-1):
      self.det=True
      self.name=[name.replace("*","")]
      self.roun=[roun]*len(cWB_conf.ifos)
      self.expr=[expr]*len(cWB_conf.ifos)
      self.var=list(map(lambda x: "%s[%i]"%(var,x),range(len(cWB_conf.ifos))))
      self.expl=expl+" for each detector"
    else:
      self.det=False
      self.name=name.split(",")
      self.roun=roun.split(",")
      self.expr=expr.split(",")
      self.var=var.split(",")
      if (len(self.roun)!=len(self.var)):
         self.roun=[roun]*len(self.var)
      if (len(self.expr)!=len(self.var)):
         self.expr=[expr]*len(self.var)
      self.expl=expl

def getrows(ifile,cWB_conf):
  names=[]
  rows=[]
  lines=open(ifile).readlines()
  for l in lines:
    if (len(l.strip())==0):
      rows.append(row(names,cWB_conf))
      names=[]
    else:
      if (len(names)<5):
         names.append(l.strip())
      else:
         names[4]+="\n"+l.strip()
  if (len(l.strip())>0):
    rows.append(row(names,cWB_conf))
  return rows

def getoutputcommand(command):
    process = Popen(command.split(), stdout=PIPE, stderr=PIPE)
    out = process.stdout.read()
    err = process.stderr.read()
    returncode = process.wait()
    if returncode:
        raise Exception(returncode, err)
    else:
        return out.decode('utf-8').splitlines()

def getext(ifile):
   ifiles=glob.glob("%s.*"%ifile)
   if (len(ifiles)!=1):
     print("error file")
     print("%s.*"%ifile,ifiles)
     exit()
   else:
     ext=ifiles[0].split(".")[-1]
     return ext 

def getlimits(limits):
  if isinstance(limits,list):
    if (len(limits)!=2):
      return -1
    else:
      return [float(limits[0]),float(limits[1])]
  else:
    return [0.1,float(limits)]

def add_rootrc(idir):
    f=open("%s/.rootrc"%idir,"w")
    print("Rint.Logon:              $CWB_ROOTLOGON_FILE",file=f)
    f.close()
