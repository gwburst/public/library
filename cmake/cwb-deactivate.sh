#!/usr/bin/env bash

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_HOME_CWB}" ]; then
	export HOME_CWB="${BACKUP_HOME_CWB}"
	unset BACKUP_HOME_CWB
# no backup, just unset
else
	unset HOME_CWB
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_ROOTLOGON_FILE}" ]; then
	export CWB_ROOTLOGON_FILE="${BACKUP_CWB_ROOTLOGON_FILE}"
	unset BACKUP_CWB_ROOTLOGON_FILE
# no backup, just unset
else
	unset CWB_ROOTLOGON_FILE
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_SCRIPTS}" ]; then
	export CWB_SCRIPTS="${BACKUP_CWB_SCRIPTS}"
	unset BACKUP_CWB_SCRIPTS
# no backup, just unset
else
	unset CWB_SCRIPTS
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_MACROS}" ]; then
	export CWB_MACROS="${BACKUP_CWB_MACROS}"
	unset BACKUP_CWB_MACROS
# no backup, just unset
else
	unset CWB_MACROS
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_PARAMETERS_FILE}" ]; then
	export CWB_PARAMETERS_FILE="${BACKUP_CWB_PARAMETERS_FILE}"
	unset BACKUP_CWB_PARAMETERS_FILE
# no backup, just unset
else
	unset CWB_PARAMETERS_FILE
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_ANALYSIS}" ]; then
	export CWB_ANALYSIS="${BACKUP_CWB_ANALYSIS}"
	unset BACKUP_CWB_ANALYSIS
# no backup, just unset
else
	unset CWB_ANALYSIS
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_NODE_DATA_DIR}" ]; then
	export NODE_DATA_DIR="${BACKUP_NODE_DATA_DIR}"
	unset BACKUP_NODE_DATA_DIR
# no backup, just unset
else
	unset NODE_DATA_DIR
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CONDOR_LOG_DIR}" ]; then
	export CONDOR_LOG_DIR="${BACKUP_CONDOR_LOG_DIR}"
	unset BACKUP_CONDOR_LOG_DIR
# no backup, just unset
else
	unset CONDOR_LOG_DIR
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_WWW_PUBLIC_DIR}" ]; then
	export CWB_ANALYSIS="${BACKUP_WWW_PUBLIC_DIR}"
	unset BACKUP_WWW_PUBLIC_DIR
# no backup, just unset
else
	unset WWW_PUBLIC_DIR
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_PARAMETERS_FILE}" ]; then
	export CWB_PARAMETERS_FILE="${BACKUP_CWB_PARAMETERS_FILE}"
	unset BACKUP_CWB_PARAMETERS_FILE
# no backup, just unset
else
	unset CWB_PARAMETERS_FILE
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_UPARAMETERS_FILE}" ]; then
	export CWB_UPARAMETERS_FILE="${BACKUP_CWB_UPARAMETERS_FILE}"
	unset BACKUP_CWB_UPARAMETERS_FILE
# no backup, just unset
else
	unset CWB_UPARAMETERS_FILE
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_EPARAMETERS_FILE}" ]; then
	export CWB_EPARAMETERS_FILE="${BACKUP_CWB_EPARAMETERS_FILE}"
	unset BACKUP_CWB_EPARAMETERS_FILE
# no backup, just unset
else
	unset CWB_EPARAMETERS_FILE
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_PARMS_FILES}" ]; then
	export CWB_PARMS_FILES="${BACKUP_CWB_PARMS_FILES}"
	unset BACKUP_CWB_PARMS_FILES
# no backup, just unset
else
	unset CWB_PARMS_FILES
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_EMPARAMETERS_FILE}" ]; then
	export CWB_EMPARAMETERS_FILE="${BACKUP_CWB_EMPARAMETERS_FILE}"
	unset BACKUP_CWB_EMPARAMETERS_FILE
# no backup, just unset
else
	unset CWB_EMPARAMETERS_FILE
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_PPARAMETERS_FILE}" ]; then
	export CWB_PPARAMETERS_FILE="${BACKUP_CWB_PPARAMETERS_FILE}"
	unset BACKUP_CWB_PPARAMETERS_FILE
# no backup, just unset
else
	unset CWB_PPARAMETERS_FILE
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_UPPARAMETERS_FILE}" ]; then
	export CWB_UPPARAMETERS_FILE="${BACKUP_CWB_UPPARAMETERS_FILE}"
	unset BACKUP_CWB_UPPARAMETERS_FILE
# no backup, just unset
else
	unset CWB_UPPARAMETERS_FILE
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_EPPARAMETERS_FILE}" ]; then
	export CWB_EPPARAMETERS_FILE="${BACKUP_CWB_EPPARAMETERS_FILE}"
	unset BACKUP_CWB_EPPARAMETERS_FILE
# no backup, just unset
else
	unset CWB_EPPARAMETERS_FILE
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_PPARMS_FILES}" ]; then
	export CWB_PPARMS_FILES="${BACKUP_CWB_PPARMS_FILES}"
	unset BACKUP_CWB_PPARMS_FILES
# no backup, just unset
else
	unset CWB_PPARMS_FILES
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_NETC_FILE}" ]; then
	export CWB_NETC_FILE="${BACKUP_CWB_NETC_FILE}"
	unset BACKUP_CWB_NETC_FILE
# no backup, just unset
else
	unset CWB_NETC_FILE
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_LAG_NUMBER}" ]; then
	export CWB_LAG_NUMBER="${BACKUP_CWB_LAG_NUMBER}"
	unset BACKUP_CWB_LAG_NUMBER
# no backup, just unset
else
	unset CWB_LAG_NUMBER
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_SLAG_NUMBER}" ]; then
	export CWB_SLAG_NUMBER="${BACKUP_CWB_SLAG_NUMBER}"
	unset BACKUP_CWB_SLAG_NUMBER
# no backup, just unset
else
	unset CWB_SLAG_NUMBER
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_HTML_INDEX}" ]; then
	export CWB_HTML_INDEX="${BACKUP_CWB_HTML_INDEX}"
	unset BACKUP_CWB_HTML_INDEX
# no backup, just unset
else
	unset CWB_HTML_INDEX
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_HTML_HEADER}" ]; then
	export CWB_HTML_HEADER="${BACKUP_CWB_HTML_HEADER}"
	unset BACKUP_CWB_HTML_HEADER
# no backup, just unset
else
	unset CWB_HTML_HEADER
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_HTML_BODY_PROD}" ]; then
	export CWB_HTML_BODY_PROD="${BACKUP_CWB_HTML_BODY_PROD}"
	unset BACKUP_CWB_HTML_BODY_PROD
# no backup, just unset
else
	unset CWB_HTML_BODY_PROD
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_MANPATH}" ]; then
        export MANPATH="${BACKUP_MANPATH}"
        unset BACKUP_MANPATH
fi

# reinstate the backup from outside the environment
if [ ! -z "${_USE_HEALPIX}" ]; then
	unset _USE_HEALPIX
fi

# reinstate the backup from outside the environment
if [ ! -z "${_USE_EBBH}" ]; then
	unset _USE_EBBH
fi

# reinstate the backup from outside the environment
if [ ! -z "${_USE_LAL}" ]; then
	unset _USE_LAL
fi

# reinstate the backup from outside the environment
if [ ! -z "${BACKUP_CWB_GWAT}" ]; then
        export CWB_GWAT="${BACKUP_CWB_GWAT}"
        unset BACKUP_CWB_GWAT
# no backup, just unset
else
        unset CWB_GWAT
fi
