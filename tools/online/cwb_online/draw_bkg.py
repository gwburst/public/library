#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago, Igor Yakushin, Sergey Klimenko, Marek Szczepanczyk                                                                                   
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import glob, os, sys, subprocess,pickle

from ligo.segments import segment, segmentlist
from ligo.segments.utils import tosegwizard, fromsegwizard
from cwb_online.root_python import merge_trees
import cwb_online.xgbonline as xgbonline
from cwb_online.run_utils import add_rootrc
import uproot, datetime
import numpy as np

def splitfiles(ifile,ofile):
    tomove=[]
    f=open(ifile,"r")
    tfile=ofile.replace(".txt","_inf.txt.tmp")
    tomove.append(tfile)
    ff=open(tfile,"w")
    n=0
    npart=0
    while True:
       line=f.readline()
       if (not line):
         break
       n+=1
       npart+=1
       rho=float(line.split(" ")[2])
       if (npart==1000000):
          ff.close()
          tfile=ofile.replace(".txt","_%g_%i.txt.tmp"%(rho,n))
          tomove.append(tfile)
          ff=open(tfile,"w")
          npart=0
       print(line,file=ff,end="")
    ff.close()
    subprocess.getstatusoutput("rm %s"%ofile.replace(".txt","_*.txt"))
    for t in tomove:
      subprocess.getstatusoutput("mv %s %s"%(t,t.replace(".tmp","")))

def findced(cWB_conf,odir):
    lines=open("%s/body.html"%odir).readlines()
    for l in lines:
       if (l.find("href")!=-1 and l.find("ced")!=-1):
         cedstring=l.split("\"")[1]
         launchced(cWB_conf,cedstring,odir)

def launchced(cWB_conf,cedstring,odir):
   try:
      print(cedstring)
      line=cedstring.split("_")
      (lag,slag,time)=(int(line[6].replace("lag","")),int(line[5].replace("slag","")),line[9])
      print("%i %i %s"%(lag,slag,time))
      tmpfile="%s/user_parameters_bkg.C"%(cWB_conf.config_dir)
      tmpfile=tmpfile.replace(".C","_split.C")
      olddir=os.getcwd()
      if (slag>0):
         newdir="%s/%s_%i/%s"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir,slag,time[:5])
      else:
         newdir="%s/%s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir,time[:5])
      os.chdir(newdir)
      bkg_dirs=glob.glob("*")
      bkg_dir=[x for x in bkg_dirs if int(x.split("-")[0])<=float(time) and int(x.split("-")[1])>float(time)]
      print("%s/%s"%(newdir,bkg_dir[0]))
      os.chdir(bkg_dir[0])
      if (not os.path.exists("ced")):
         subprocess.getstatusoutput("mkdir ced")
      if (cWB_conf.bkg_split>1):
        daglines=open("condor/%s.dag"%(cWB_conf.label)).readlines()
        slag_string=filter(lambda y: y.find("SLAG_SHIFT")!=-1,daglines[1].split(" "))[0].replace("SLAG_SHIFT=","").replace("\n","")
      else:
        daglines=open("condor/%s.sub"%(cWB_conf.label)).readlines()
        slag_string=filter(lambda y: y.find("SLAG_SHIFT")!=-1,daglines[4].split(";"))[0].replace("SLAG_SHIFT=","").replace("\n","")
      if (not os.path.exists("start_ced_%s_%i"%(time,lag))):
         com="%s/bin/online_ced_bkg %s %s 1 %i %i"%(os.environ['HOME_WAT_INSTALL'],tmpfile,slag_string,int(float(time)),lag)
         print(com)
         subprocess.getstatusoutput(com)
         subprocess.getstatusoutput("touch start_ced_%s_%i"%(time,lag))
      else:
         ced_dir="ced/ced_%s_%s_%s_slag%i_lag%i_1_job1/%s"%(bkg_dir[0].split("-")[0],cWB_conf.bkg_job_duration,bkg_dir[0],0,lag,cedstring.split("/")[len(cedstring.split("/"))-1])
         link="mkdir -p %s/%s/ced/%s;ln -s %s/%s/%s %s/%s/ced/%s"%(olddir,odir,cedstring.split("/")[1],newdir,bkg_dir[0],ced_dir,olddir,odir,cedstring.split("/")[1])
         print(link)
         subprocess.getstatusoutput(link)
      os.chdir(olddir)
   except:
      print("ced not done")
      os.chdir(olddir)

def merge(cWB_conf,namefile,version,rootfiles,labels=False):
        subprocess.getstatusoutput("rm -rf %s;mkdir %s"%(namefile,namefile)) 
        if (os.environ['SITE_CLUSTER']=="CASCINA"):
           add_rootrc(namefile)
        os.chdir(namefile)
        subprocess.getstatusoutput("mkdir -p config output condor merge report/postprod")
        subprocess.getstatusoutput("cp %s config/user_parameters.C"%"%s/user_parameters_bkg.C"%(cWB_conf.config_dir))
        subprocess.getstatusoutput("cp %s config/user_pparameters.C"%"%s/user_pparameters.C"%(cWB_conf.config_dir))
        for rootfile in rootfiles:
          if (labels==True):
            subprocess.getstatusoutput("%s/bin/cwb_clonedir %s ../%s '--output merge'"%(os.environ['HOME_WAT_INSTALL'],rootfile,namefile))
            label=rootfile.split("/")[-1]
            subprocess.getstatusoutput("mv output/wave_%s.M1.root output/wave_%s.M1_job%i.root"%(label,label,1))
          else:
            subprocess.getstatusoutput("ln -sf %s output/"%(rootfile))
           
        subprocess.getstatusoutput("""%s/bin/cwb_merge %s"""%(os.environ['HOME_WAT_INSTALL'],version))
def report_cuts(cWB_conf,namefile,version):
        subprocess.getstatusoutput("%s/bin/cwb_report %s create"%(os.environ['HOME_WAT_INSTALL'],version))
        pp_dir=glob.glob("report/postprod/*")
        subprocess.getstatusoutput("rm -rf ../plot")
        subprocess.getstatusoutput("mv %s ../plot"%(pp_dir[0]))
        subprocess.getstatusoutput("cp ../plot/data/live.txt ../plot_live.txt")
        subprocess.getstatusoutput("cp ../plot/data/events_sorted.txt ../plot_events_sorted.txt")
        if (len(cWB_conf.Cuts_list)>1):
            T_Cuts_list=["OR_cut"]
        else:
            T_Cuts_list=[]
        for n in cWB_conf.Cuts_list:
            T_Cuts_list.append(n)
        print(repr(T_Cuts_list))
        for n in T_Cuts_list:
           com="""%s/bin/cwb_setcuts %s '--tcuts %s --label %s' """%(os.environ['HOME_WAT_INSTALL'],version,n,n)
           print(com)
           subprocess.getstatusoutput(com)
           if (os.path.exists("merge/live_%s.%s.C_%s.root"%(namefile,version,n))):
             com="%s/bin/cwb_report %s.C_%s create"%(os.environ['HOME_WAT_INSTALL'],version,n)
             subprocess.getstatusoutput(com)
             pp_dir=glob.glob("report/postprod/*")
             print(pp_dir)
             print(subprocess.getstatusoutput("rm -rf ../plot%s"%n))
             print(subprocess.getstatusoutput("mv %s ../plot%s"%(pp_dir[0],n)))
             subprocess.getstatusoutput("cp ../plot%s/data/live.txt ../plot%s_live.txt"%(n,n))
             subprocess.getstatusoutput("cp ../plot%s/data/events_sorted.txt ../plot%s_events_sorted.txt"%(n,n))
           else:
             print("no event in this class")
             print(subprocess.getstatusoutput("rm -rf ../plot%s"%n))
def clean(cWB_conf,namefile):
        subprocess.getstatusoutput("rm -rf ../merge")
        subprocess.getstatusoutput("mv merge ../.")
        os.chdir("../")
        subprocess.getstatusoutput("rm -rf %s"%(namefile))
def report_xgb(cWB_conf,namefile,version):
        xgb=xgbonline.xgbinfo(cWB_conf.xgboost)
        xgb.fixmissing(cWB_conf)
        label="all"
        outlabel="%s.C_%s.XGB_rthr0_%s"%(version,label,label)
        if (len(xgb.labels)==1):
          l=xgb.labels[0]
          m=xgb.models[0]
          ifile="merge/wave_%s.%s.root"%(namefile,version)
          ofile="merge/wave_%s.%s.XGB_rthr0_%s.root"%(namefile,version,l)
          xgbonline.apply_xgboost(xgb,ifile,ofile,cWB_conf.config_dir,l,l)
          subprocess.getstatusoutput("mv merge/wave_%s.%s.XGB_rthr0_%s.root merge/wave_%s.%s.root"%(namefile,version,l,namefile,outlabel))
        else:
          for l,c,m in zip(xgb.labels,xgb.cuts,xgb.models):
            com="""%s/bin/cwb_setcuts %s '--wcuts (%s) --label %s' """%(os.environ['HOME_WAT_INSTALL'],version,c,l)
            subprocess.getstatusoutput(com)
            ifile="merge/wave_%s.%s.C_%s.root"%(namefile,version,l)
            ofile="merge/wave_%s.%s.C_%s.XGB_rthr0_%s.root"%(namefile,version,l,l)
            xgbonline.apply_xgboost(xgb,ifile,ofile,cWB_conf.config_dir,l,l)
          files=list(map(lambda x: "merge/wave_%s.%s.C_%s.XGB_rthr0_%s.root"%(namefile,version,x,x),xgb.labels))
          merge_trees(files,"waveburst","merge/wave_%s.%s.root"%(namefile,outlabel))
        for itype,ext in zip(["merge","live"],["lst","root"]):
           subprocess.getstatusoutput("ln -s %s_%s.%s.%s merge/%s_%s.%s.%s"%(itype,namefile,version,ext,itype,namefile,outlabel,ext))
        postprod(outlabel,cWB_conf)
def postprod(outlabel,cWB_conf):
        com="%s/bin/cwb_report %s create"%(os.environ['HOME_WAT_INSTALL'],outlabel)
        print(com)
        subprocess.getstatusoutput(com)
        pp_dir=glob.glob("report/postprod/*")
        print(pp_dir)
        print(subprocess.getstatusoutput("rm -rf ../plotxgb"))
        print(subprocess.getstatusoutput("mv %s ../plotxgb"%pp_dir[0]))
        subprocess.getstatusoutput("cp ../plotxgb/data/live.txt ../plotxgb_live.txt")
        if (hasattr(cWB_conf,"splitfiles")): 
           splitfiles("../plotxgb/data/events_sorted.txt", "../plotxgb_events_sorted.txt")
        else:
           subprocess.getstatusoutput("cp ../plotxgb/data/events_sorted.txt ../plotxgb_events_sorted_tmp.txt")
           subprocess.getstatusoutput("mv ../plotxgb_events_sorted_tmp.txt ../plotxgb_events_sorted.txt")
def pastro_file(cWB_conf,namefile,version):
    #working only with xgboost
    if (hasattr(cWB_conf,"xgboost")):
        label="all"
        outlabel="%s.C_%s.XGB_rthr0_%s"%(version,label,label)
        if (hasattr(cWB_conf,"fast_xgboost")):
            outlabel="%s"%(version)
        fbkg="merge/wave_%s.%s.root"%(namefile,outlabel)
        # Convert inputs to format readable by machine learning tools
        eventbkg = uproot.open("%s:waveburst"%fbkg)   #open waveburst tree
        # Background
        df = eventbkg.arrays(["lag","slag","rho","time"],library="pd")
        nzl = df[(df["rho[1]"]>0) & ((df["lag[2]"]!=0) | (df["slag[2]"]!=0))]
        nzl = nzl.loc[:,["rho[1]"]]            # Extract columns
        np.savetxt('pastro_data_nzl.txt', nzl.values, fmt='%f')
        subprocess.getstatusoutput('cp pastro_data_nzl.txt ../pastro_data_nzl.txt')

def getlivefromdir(idir,zero=True):
    if (zero==True):
        segments_file="%s/segments.txt"%(idir)
        if(os.path.exists(segments_file)):
             done = open(segments_file)
             done_segments = fromsegwizard(done);
             done.close()
             return abs(done_segments)
        else:
             return 0
    else:
        ifile="%s_live.txt"%idir
        if (os.path.exists(ifile)):
          f=open(ifile)
          lines=[x for x in f.readlines() if x.find("nonzero")==0]
          f.close()
          return float(lines[0].replace("nonzero lags live=",""))
        else:
          return 0.
def significance(cWB_conf, dir, rho_th, plotdir):
     if (os.path.exists("%s/segments.txt"%(dir))):
        f=open("%s/segments.txt"%(dir))
        times=[x for x in f.readlines() if len(x.strip().split(" "))>0]
        f.close()
        start=[float(x.split(" ")[0]) for x in times]
        stop=[float(x.split(" ")[1]) for x in times]
     if (os.path.exists("%s/%s_live.txt"%(dir,plotdir))):
        f=open("%s/%s_live.txt"%(dir,plotdir))
        lines=[x for x in f.readlines() if x.find("nonzero")==0]
        f.close()
        nonzero_live = float(lines[0].replace("nonzero lags live=",""))
     chosen="%s/%s_events_sorted.txt"%(dir,plotdir)
     n=0
     if (hasattr(cWB_conf,"splitfiles")):
        files=glob.glob("%s/%s_events_sorted_*_*.txt"%(dir,plotdir))
        selec=list(filter(lambda x: float(x.split("_")[-2])>rho_th, files))
        if (len(selec)>0):
          chosen=sorted(selec)[0]
          n=int(chosen.split("_")[-1].replace(".txt",""))
        elif (os.path.exists("%s/%s_events_sorted_inf.txt"%(dir,plotdir))):
          chosen="%s/%s_events_sorted_inf.txt"%(dir,plotdir)
     if (os.path.exists(chosen)):
        f=open(chosen)
        lines=f.readlines()
        f.close()
        for l in lines:
            line=[x for x in l.strip().split(" ") if len(x)>0]
            (rho,cc,neted,penalty)=(float(line[2]),float(line[3]),float(line[10]),float(line[11]))
            if (rho>rho_th):
               n=n+1
        if (hasattr(cWB_conf,"Cuts_trials")):
          trials=cWB_conf.Cuts_trials
        else:
          if (hasattr(cWB_conf,"Cuts_list")):
            trials=len(cWB_conf.Cuts_list)
          else:
            trials=1
        if (n==0):
            m=1*trials
        else:
            m=n*trials
        del lines
        del times
     try:
        return [n,m/float(nonzero_live),start[0],stop[-1],sum(stop)-sum(start)]
     except:
        return [-1,-1,-1,-1,-1]


def last_N(cWB_conf,options):
     if(not hasattr(options,"offset")):
                options.offset=0

     if(not hasattr(options,"mdir")):
                options.mdir=0

     options.m=options.mdir
     if (options.mdir==0):
             options.m=int(3600./cWB_conf.bkg_job_duration+0.5)

     if(not hasattr(options,"rho")):
                options.rho=0.

     if(not hasattr(options,"plotdir")):
                options.plotdir="plot"
                if (hasattr(cWB_conf,"xgboost")):
                   options.plotdir="plotxgb"

     if(options.recompute==0):
                        dir="%s/FOM_%d_%d_%d"%(options.odir, options.N, options.offset, options.mdir)
                        z=significance(cWB_conf, dir,options.rho,options.plotdir)
                        print("""%d %g %d %d %d"""%(z[0],z[1],z[2],z[3],z[4]))
                        return("""%d %g %d %d %d"""%(z[0],z[1],z[2],z[3],z[4]))

     big_dirs_N=glob.glob("%s/FOM_daily_????-??-??"%options.odir)
     big_dirs_N.sort()

     # Selection of dates for creating reports after 
     # This is used when the event production significantly changes on a date 'date_cut'
     # and events from before this change will not be taken into producing reports.
     # Used for the change on Aug 21, 2023: https://wiki.ligo.org/Bursts/O4cWBonlineLog
     if (hasattr(cWB_conf,"date_cut")):
       date_cut = cWB_conf.date_cut
       if (date_cut == ''):
         print("draw_bkg.py: no date selection. Date format: YYYY-MM-DD")
       else:
         try:
           datetime.date.fromisoformat(date_cut)
         except ValueError:
           raise ValueError("draw_bkg.py: date should be in YYYY-MM-DD format")
         considered_days=[]
         for j in range(len(big_dirs_N)):
           a = big_dirs_N[j] # e.g. '/home/waveburst/online/O4_L1H1_BBH_ONLINE/TIME_SHIFTS/POSTPRODUCTION/FOM_daily_2023-06-08'
           date_dir = a[-10:] # e.g. '2023-06-08'
           if date_cut <= date_dir:
             considered_days.append(a)
         big_dirs_N = considered_days

     if (options.N==0):
        considered_days=big_dirs_N
     else:
        considered_days=[]
        j=len(big_dirs_N)-1
        live=0.
        while(j>=0 and live<=86400*options.N):
           idir=big_dirs_N[j]
           considered_days.append(idir)
           live+=getlivefromdir(idir)
           j-=1
     if (len(considered_days)==0):
            print("no background jobs in desired interval")
     else:

        dir="%s/FOM_%d_%d_%d"%(options.odir, options.N, options.offset, options.mdir)
        subprocess.getstatusoutput("mkdir %s"%(dir))
        olddir=os.getcwd()
        os.chdir(dir)
        namefile="cwb_bkg"
        version="M1"
        merge(cWB_conf,namefile,version,considered_days,True)
        if (hasattr(cWB_conf,"Cuts_list")):
           report_cuts(cWB_conf,namefile,version)
        if (hasattr(cWB_conf,"xgboost")):
           if (hasattr(cWB_conf,"fast_xgboost")):
              postprod(version,cWB_conf)
           else:
              report_xgb(cWB_conf,namefile,version)
        if (hasattr(cWB_conf,"pastro")):
           pastro_file(cWB_conf,namefile,version)
        clean(cWB_conf,namefile)
        os.chdir(olddir)
        f=open("%s/segments.txt"%(dir),"w")
        for b in considered_days:
          lines=open("%s/segments.txt"%(b)).readlines()
          print("".join(lines),file=f,end="")
        f.close()
        if (hasattr(cWB_conf,"pastro")):
          live0=getlivefromdir(dir)
          liveT=getlivefromdir("%s/plotxgb"%dir,False)
          np.savetxt( '%s/pastro_data_rlive.txt'%dir, [live0/liveT], fmt='%1.9e')
          np.savetxt( '%s/pastro_data_live.txt'%dir,  [[live0,liveT]], fmt='%d')

        print(dir,options.plotdir)
        z=significance(cWB_conf, dir,options.rho,options.plotdir)
        print("""%d %g %d %d %d"""%(z[0],z[1],z[2],z[3],z[4]))

def intersects(path,interval):
        s=segmentlist([segment(list(map(int,path.split("/")[-2].split("-"))))])
        intersection=s & interval; intersection.coalesce()
        if(abs(intersection)>0):
                return True
        else:
                return False

def query_rate(cWB_conf,options):
        
     considered_jobs=["%s/output"%("/".join(x.split("/")[:-1])) for x in glob.glob("%s/%s/*/finished"%(options.idir,options.day))]
     started_jobs=["/".join(x.split("/")) for x in glob.glob("%s/%s/*/start"%(options.idir,options.day))]

     if(len(considered_jobs)==0):
                print("No completed jobs found in the specified time interval")
     else:
        considered_segments=segmentlist([segment(list(map(int,x.split("/")[-2].split("-")))) for x in considered_jobs]);considered_segments.coalesce()
        started_segments=segmentlist([segment(list(map(int,x.split("/")[-2].split("-")))) for x in started_jobs]);started_segments.coalesce()
        missing_segments=started_segments - considered_segments
        rootfiles=[]
        for b in considered_jobs:
                rfiles=glob.glob("%s/w*.root"%b)
                rootfiles+=rfiles
        print(options.odir)
        segments_file="%s/segments.txt"%(options.odir)
        dodir=1
        if(os.path.exists(segments_file)):
             done = open(segments_file)
             done_segments = fromsegwizard(done); done_segments.coalesce()
             done.close()
             notdone=considered_segments & ~done_segments; notdone.coalesce()
             if (len(notdone)==0):
                 dodir=0
        print("dodir: %i"%dodir)
        if(dodir==1):    
            subprocess.getstatusoutput("mkdir %s"%(options.odir))
            f=open("%s/segments.txt"%(options.odir),"w")
            for seg in considered_segments:
                print("%s %s"%(seg[0],seg[1]), file=f)
            f.close()
            olddir=os.getcwd()
            os.chdir(options.odir)
            namefile="cwb_bkg"
            version="M1"
            merge(cWB_conf,namefile,version,rootfiles)
            if (hasattr(cWB_conf,"Cuts_list")):
               report_cuts(cWB_conf,namefile,version)
            if (hasattr(cWB_conf,"xgboost")):
               report_xgb(cWB_conf,namefile,version)
            clean(cWB_conf,namefile)
            for itype in ["wave","live"]:
              subprocess.getstatusoutput("ln -s %s_%s.%s.root merge/%s_FOM_daily_%s.%s.root"%(itype,namefile,version,itype,options.day,version))
            if (hasattr(cWB_conf,"fast_xgboost")):
              subprocess.getstatusoutput("ln -sf wave_%s.%s.C_all.XGB_rthr0_all.root merge/wave_FOM_daily_%s.%s.root"%(namefile,version,options.day,version))
            os.chdir(olddir)
        if(len(missing_segments)>0):
            f=open("%s/missing.txt"%(options.odir),"w")
            for seg in missing_segments:
                print("%s %s"%(seg[0],seg[1]), file=f)
            f.close()
        else:
            subprocess.getstatusoutput("rm %s/missing.txt"%(options.odir))

