ligo_data_find --observatory=H --type=H1_LDAS_C02_L2 --gps-start-time=955609344 --gps-end-time=955609544 --url-type=file

FrDisplayPROC -t H1:LDAS-STRAIN -i /data/node207/frames/S6/LDAShoftC02/LHO/H-H1_LDAS_C02_L2-9556/H-H1_LDAS_C02_L2-955609344-128.gwf
FrDisplayPROC -t H1:LDAS-STRAIN -i /data/node207/frames/S6/LDAShoftC02/LHO/H-H1_LDAS_C02_L2-9556/H-H1_LDAS_C02_L2-95560*

-------------------------------------------------------------------------------
FrDisplayPROC -t H1:LDAS-STRAIN -i ldf -o H -s H1_LDAS_C02_L2 -f 955609344 -l 955609544

-------------------------------------------------------------------------------
FrDisplayPROC -t G1:DER_DATA_H -i ldf -o G -s G1_RDS_C01_L3 -f 991247040 -l 991248040

-------------------------------------------------------------------------------
FrDisplayPROC -t G1:DER_DATA_H -i ldf -o G -s G1_RDS_C01_L3 -f 991247040 -l 991248040 -k "-Bu -Hp -o 6 -a 50"

-------------------------------------------------------------------------------
FrDisplayPROC -t G1:DER_DATA_H -i ldf -o G -s G1_RDS_C01_L3 -f 991247040 -l 991258040 -x "-decimateby 16"

-------------------------------------------------------------------------------
FrDisplayPROC -t H1:LDAS-STRAIN -i ldf -o H -s H1_LDAS_C02_L2 -f 955609344 -l 955619944 -x "-decimateby 16"

-------------------------------------------------------------------------------
ligo_data_find --observatory=H --type=H1_NINJA2_GAUSSIAN --gps-start-time=874645000 --gps-end-time=874649000 --url-type=file
FrDisplayADC -t H1:LDAS-STRAIN -i /data/node226/frames/NINJA2/LHO/H-H1_NINJA2_GAUSSIAN-8746/H-H1_NINJA2_GAUSSIAN-874641340-4096.gwf

-------------------------------------------------------------------------------
FrDisplayADC -t V1:Sc_OB_Gr_Coil1 -i /archive/home/vedovato/frames/V1_RAW_TST/V-raw-986188050-150.gwf

-------------------------------------------------------------------------------
FrDisplayADC -t "V1:Sc_OB_Gr_Coil1 V1:Sc_OB_tyCorr" -i /archive/home/vedovato/frames/V1_RAW_TST/V-raw-986188050-150.gwf

-------------------------------------------------------------------------------
FrDisplayADC -d 4 -i /archive/home/vedovato/frames/V1_RAW_TST/V-raw-986188050-150.gwf

-------------------------------------------------------------------------------
FrDisplayADC -t "V1:Sc_OB_Gr_Coil1 V1:Sc_OB_tyCorr V1:Pr_B2_DC" -i /archive/home/vedovato/frames/V1_RAW_TST/V-raw-986188050-150.gwf -k "-Bu -Hp -o 6 -a 50"

-------------------------------------------------------------------------------
FrDisplayPROC -t H1:LDAS-STRAIN -i ldf -o H -s H1_LDAS_C02_L2 -f 955609344 -l 955619944 -x "-decimateby 4 -downmix 601 -decimategain 48"
FrDisplayPROC -t H1:LDAS-STRAIN -i ldf -o H -s H1_LDAS_C02_L2 -f 955609344 -l 955619944 -x "-decimateby 16 -uscaleby 0.5 -decimategain 48"

-------------------------------------------------------------------------------
FrDump -d 0 -i /data/node207/frames/S6/LDAShoftC02/LHO/H-H1_LDAS_C02_L2-9556/H-H1_LDAS_C02_L2-95560* > ffl/h1_cit.ffl

FrDisplayPROC -t H1:LDAS-STRAIN -i $FRDISPLAY_DIR/ffl/h1_cit.ffl

// List ADC channels
FrDisplayADC -i /archive/home/vedovato/frames/V1_RAW_TST/V-raw-986188050-150.gwf
// List PROC channels
FrDisplayPROC -i ldf -o H -s H1_LDAS_C02_L2 -f 955609344 -l 955619944
FrDisplayPROC -i /atlas/data/d14/LSC/H/95560/H-H1_LDAS_C02_L2-955609344-128.gwf
FrDisplayPROC -i /atlas/data/d14/LSC/H/95560/H-H1_LDAS_C02_L2-955609* -t 3

