import os
import sys
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors
import matplotlib.cm as cm
import matplotlib.ticker as ticker
from matplotlib.colors import Normalize, SymLogNorm
from matplotlib import pyplot as plt, colors
#import cwb_xgboost as cwb

from cycler import cycler

# set matplotlib 
mpl.use('Agg')
plt.rcParams.update(plt.rcParamsDefault)
size=20
params = {'legend.fontsize': 'large',
          'figure.figsize': (8,5),
          'axes.labelsize': size,
          'axes.titlesize': size,
          'xtick.labelsize': size*0.8,
          'ytick.labelsize': size*0.8,
          'legend.fontsize': size*0.8,
          'axes.titlepad': 7,
          'font.family': 'STIXGeneral',
          'font.weight': 'medium',
          'xtick.major.size': 10,
          'ytick.major.size': 10,
          'xtick.minor.size': 5,
          'ytick.minor.size': 5,
          'mathtext.fontset': 'stix',
          }
plt.rcParams.update(params)

def adjust_lightness(color, amount=0.5):
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], max(0, min(1, amount * c[1])), c[2])


def load_files(ifile_V1, ifile_V2):

   if not os.path.isfile(ifile_V1):
      print ("\nError: file ", ifile_V1, " doesn't exist")
      exit(1)
 
   exclude = [i for i, line in enumerate(open(ifile_V1)) if line.startswith('#')]
   df_V1 = pd.read_csv(ifile_V1,header=None,delim_whitespace=True,skiprows=exclude) # skip lines starting with #
   df_V1['rtype_err'] = df_V1[ertype_index]

   if(ifile_V2==''): return df_V1, 'none'

   if not os.path.isfile(ifile_V2):
      print ("\nError: file ", ifile_V2, " doesn't exist")
      exit(1)

   exclude = [i for i, line in enumerate(open(ifile_V2)) if line.startswith('#')]
   df_V2 = pd.read_csv(ifile_V2,header=None,delim_whitespace=True,skiprows=exclude) # skip lines starting with #

   # check if input V1,V2 injection names are consistent
   for index,name in enumerate(df_V1[name_index].values):
     if(name!=df_V2[name_index][index]):
        print("df_V2",index,name)
        print("\ndata frame V1\n")
        print("\n",df_V1,"\n")
        print("\ndata frame V2\n")
        print("\n",df_V2,"\n")
        print("\nError: names defined in the input files are not compatibles")
        print("\n",ifile_V1)
        print("\n",ifile_V2)
        print("\n",index,name,df_V2[name_index][index],"\n")
        exit(1)

   # add gains to dataframe V2
   #df_V2['ratio_v_err'] = np.sqrt(((df_V2[2]**2)/(df_V1[2]**4))*((df_V1[4])**2) + (1/df_V1[2]**2)*((df_V2[4])**2))
   df_V2['rtype_gain']   = 100*((df_V2[rtype_index]/df_V1[rtype_index])-1.0)
   #df_V2['Gain_v_err']  = df_V2['ratio_v_err']*100
   df_V2['rtype_err']    = df_V2[ertype_index]

   return df_V1, df_V2


def getopts(arguments):
   """
   function purpose:
      get command line input options
   """

   rtype='visible_volume'   # report type visible_volume(def)/hrss50/dist50
   idir1=''                 # input directory 1
   idir2=''                 # input directory 2
   ilabel1=''               # input label 1
   ilabel2=''               # input label 2
   odir=''                  # output directory
   subtitle=''              # substring used in the title
   ifar='100'               # ifar
   ymin=0                   # min rtype
   ymax=0                   # max rtype
   yunits='Mpc'             # only for rtype=visible_volume/dist50 -> visible_volume/dist50 units
   tagsort=''               # tag used to sort the display of plots in the report html page
   errorbar=False           # add error bar to the final plot
   verbose=False
   how2use="\n how to use -> python cwb_xmdc_report.py rtype=report_type idir1=idir_path1 idir2=idir_path2\n"

   # get input paraameters
   for s in arguments:
    if (s.find("rtype=")!=-1):
      rtype=s.replace("rtype=","")
    if (s.find("idir1=")!=-1):
      idir1=s.replace("idir1=","")
    if (s.find("idir2=")!=-1):
      idir2=s.replace("idir2=","")
    if (s.find("ilabel1=")!=-1):
      ilabel1=s.replace("ilabel1=","")
    if (s.find("ilabel2=")!=-1):
      ilabel2=s.replace("ilabel2=","")
    if (s.find("odir=")!=-1):
      odir=s.replace("odir=","")
    if (s.find("subtitle=")!=-1):
      subtitle=s.replace("subtitle=","")
    elif (s.find("ifar=")!=-1):
      ifar=s.replace("ifar=","")
    elif (s.find("ymin=")!=-1):
      ymin=float(s.replace("ymin=",""))
    elif (s.find("ymax=")!=-1):
      ymax=float(s.replace("ymax=",""))
    if (s.find("yunits=")!=-1):
      yunits=s.replace("yunits=","")
    if (s.find("tagsort=")!=-1):
      tagsort=s.replace("tagsort=","")
    elif (s.find("errorbar")!=-1):
      errorbar=True
    elif (s.find("verbose")!=-1):
      verbose=True

   if((rtype!='visible_volume') and (rtype!="hrss50") and (rtype!="dist50")):
     print("\nerror: wrong rtype parameter: must be visible_volume/hrss50/dist50\n")
     print(how2use)
     exit(1)

   if(idir1==''):
     print("\nerror: missing input directory idir1 parameter\n")
     print(how2use)
     exit(1)
 
   if(odir==''):
     print("\nerror: missing output directory odir parameter\n")
     print(how2use)
     exit(1)

   print("\ninput parameters\n")
   print(" report type                            = ", rtype)
   print(" input directory 1                      = ", idir1)
   print(" input directory 2                      = ", idir2)
   print(" input label 1                          = ", ilabel1)
   print(" input label 2                          = ", ilabel2)
   print(" output directory                       = ", odir)
   print(" subtitle                               = ", subtitle)
   print(" input ifar option                      = ", ifar+"y")
   if(rtype=="visible_volume"):
     print(" units                                  = ", yunits)
     print(" min visible volume                     = ", ymin)
     print(" max visible volume                     = ", ymax)
   elif(rtype=="hrss50"):
     print(" min hrss50%                            = ", ymin)
     print(" max hrss50%                            = ", ymax)
   elif(rtype=="dist50"):
     print(" units                                  = ", yunits)
     print(" min dist50%                            = ", ymin)
     print(" max dist50%                            = ", ymax)
   print(" tag sort                               = ", tagsort)
   print(" errorbar                               = ", errorbar)
   print(" verbose                                = ", verbose)
   print("\n")

   return rtype, idir1, idir2, ilabel1, ilabel2, odir, subtitle, ifar, ymin, ymax, yunits, tagsort, errorbar, verbose


if __name__ == "__main__":

   # get input user parameters
   rtype,idir1,idir2,ilabel1,ilabel2,odir,subtitle,ifar,ymin,ymax,yunits,tagsort,errorbar,verbose=getopts(sys.argv[1:])

   if(rtype=="visible_volume"):
     rtitle = "Visible Volume"
     name_index   = 1
     rtype_index  = 6
     ertype_index = 7
   elif(rtype=="hrss50"):
     rtitle = "hrss50%"
     name_index   = 8
     rtype_index  = 2
     ertype_index = 4
   elif(rtype=="dist50"):
     rtitle = "dist50%"
     name_index   = 8
     rtype_index  = 2
     ertype_index = 4

   ulabel_V1 = ilabel1
   ifile_V1 = idir1+"/"+rtype+"_vs_ifar"+str(ifar)+"y.txt"
   if(not os.path.exists(ifile_V1)):
     print("\nError: file\n"+" "+ifile_V1+"\ndoesn't exist\n")
     exit(1)

   ulabel_V2 = ilabel2
   ifile_V2 = ''
   if(idir2!=''):
     ifile_V2 = idir2+"/"+rtype+"_vs_ifar"+str(ifar)+"y.txt"
     if(not os.path.exists(ifile_V2)):
       print("\nError: file\n"+" "+ifile_V2+"\ndoesn't exist\n")
       exit(1)

   if(verbose):
     print("ulabel_V1 =",ulabel_V1)
     print("ifile_V1  =",ifile_V1)
     print("ulabel_V2 =",ulabel_V2)
     print("ifile_V2  =",ifile_V2)

   df_V1,df_V2 = load_files(ifile_V1, ifile_V2)

   if not(os.path.exists(odir)): os.makedirs(odir, exist_ok=True)       # creates the output dir
   if(tagsort==''):
     ofile = odir+"/plot_"+rtype+"_ifar"+str(ifar)+"y"+".png"
   else:
     ofile = odir+"/"+tagsort+"_plot_"+rtype+"_ifar"+str(ifar)+"y"+".png"

   if(subtitle!=''):
     plot_title = rtitle+' '+subtitle+' @ IFAR='+ifar+'y'
   else:
     plot_title = rtitle+' @ IFAR='+ifar+'y'

   # sort by order
   #df_V1['order'] = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
   #df_V1 = df_V1.sort_values(by='order')

   df_V1['labels'] = df_V1[name_index] 

   # https://matplotlib.org/stable/gallery/color/named_colors.html
   #color_1 = ['red','darkblue','darkorange','darkgreen','darkmagenta','darkolivegreen','darkgray','darkviolet']
   color_1 = ['tab:blue','tab:orange','tab:green','tab:red','tab:purple','black','tab:brown','tab:pink','tab:gray','tab:olive','tab:cyan','gold','darkviolet','blue']
   # Define the dark and light color cycles
   dark_colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf', '#393b79', '#637939', '#8c6d31', '#843c39']
   light_colors = ['#aec7e8', '#ffbb78', '#98df8a', '#ff9896', '#c5b0d5', '#c49c94', '#f7b6d2', '#c7c7c7', '#dbdb8d', '#9edae5', '#9c9ede', '#b5cf6b', '#e7ba52', '#d6616b']

   #color_2 = ['coral','blue','orange','green','magenta','yellowgreen','gray','violet']
   color_2 = []
   #light_colors = []
   for string in color_1:
     color_2.append(adjust_lightness(string, 0.8))
    # light_colors.append(adjust_lightness(string, 1.2))
   color_V1=[]
   color_V2=[]
   label =df_V1['labels'][0] 
   icolor=0
   for index,data in enumerate(df_V1['labels'].values):
     # extract most significant info from the name, eg: SGE70Q100 -> SGEQ100
     data_tag  = ''.join([i for i in data  if not i.isdigit()])
     label_tag = ''.join([i for i in label if not i.isdigit()])
     if(data.find("Q")!=-1):  data_tag  += data.split("Q",1)[1]
     if(label.find("Q")!=-1): label_tag += label.split("Q",1)[1]
     if(data_tag!=label_tag):
       label=data
       icolor+=1
     color_V1.append(color_1[icolor%len(color_1)]) 
     color_V2.append(color_2[icolor%len(color_2)]) 

   if(verbose):
     print("\ndata frame V1\n")
     print(df_V1)

   #figure
   fig, ax = plt.subplots(figsize=(15,8));
   plt.gcf().subplots_adjust(bottom=0.31)
   plt.gcf().subplots_adjust(top=0.9)
   plt.gcf().subplots_adjust(left=0.10)
   plt.gcf().subplots_adjust(right=0.95)

   y = np.arange(0,df_V1.shape[0],1)
   if(ifile_V2!=''):
    width=0.8
   else:
    width=1.2
    # Set the color cycles
   plt.rc('axes', prop_cycle=(cycler(color=dark_colors)))
   #ax.bar(y,(df_V1[rtype_index].values), width=width-0.4,alpha=0.6 , color=color_V1, label=ulabel_V1,ec='gray');
   #ax.bar(y,(df_V1[rtype_index].values), width=width-0.4,alpha=0.6 , color=color_1, label=ulabel_V1,ec='gray');
   ax.bar(y,(df_V1[rtype_index].values), width=width-0.4,alpha=0.6 , color=dark_colors, label=ulabel_V1,ec='gray');

   if(ifile_V2!=''): 
     # sort by order
     #df_V2['order'] = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
     #df_V2 = df_V2.sort_values(by='order')

     df_V2['labels'] = df_V2[name_index] 
     if(verbose):
       print("\n")
       print("\ndata frame V2\n")
       print(df_V2)
       print("\n")
       print(df_V2[['rtype_gain',8,'labels']])
       print("\n")
     # Set the color cycle for the second set of data
     plt.gca().set_prop_cycle(cycler(color=light_colors))
     #ax.bar(y,(df_V2[rtype_index].values), width=width,    alpha=0.4 , color=color_V2, label=ulabel_V2,edgecolor='gray');
    # ax.bar(y,(df_V2[rtype_index].values), width=width,    alpha=0.4 , color=color_2, label=ulabel_V2,edgecolor='gray');
     ax.bar(y,(df_V2[rtype_index].values), width=width,    alpha=0.4 , color=light_colors, label=ulabel_V2,edgecolor='gray');
     if(errorbar): ax.errorbar(y,(df_V2[rtype_index].values), yerr=(df_V2['rtype_err'].values), fmt='.', color='black', elinewidth=1)
   else:
     if(errorbar): ax.errorbar(y,(df_V1[rtype_index].values), yerr=(df_V1['rtype_err'].values), fmt='.', color='black', elinewidth=1)

   plt.legend(prop={'size': 15}, loc='upper right')#, framealpha=1);

   leg = ax.get_legend()
   leg.legend_handles[0].set_color('k')
   if(ifile_V2!=''): leg.legend_handles[1].set_color('gray')

   ax.set_xticks(y)
   plt.yscale('log')
   ax.tick_params(axis='x',labelsize=20)
   ax.set_xticklabels(df_V1['labels'])
   ax.tick_params(axis='y',labelsize=20);
   plt.setp(ax.get_xticklabels(), rotation=45, ha="right",rotation_mode="anchor",fontsize=15)

   if(rtype=="visible_volume"): plt.ylabel(rtitle+" ("+yunits+")", fontsize=20);
   elif(rtype=="dist50"):       plt.ylabel(rtitle+" ("+yunits+")", fontsize=20);
   else:                        plt.ylabel(rtitle, fontsize=20);
   ax.set_xlim(left=-0.5,right=len(y)-0.5)
   ax.grid(axis='y', linestyle=':')
   ax.grid(axis='x', linestyle=':')

   if(ymin<=0): ymin = 0.5*min(min(df_V1[rtype_index].values),min(df_V2[rtype_index].values))
   if(ymax<=0): ymax = 2.0*max(max(df_V1[rtype_index].values),max(df_V2[rtype_index].values))
   ax.set_ylim(bottom=ymin,top=ymax)

   plt.title(plot_title)

   # plot rtype gain
   if(ifile_V2!=''): 
     for index,data in enumerate(df_V2['rtype_gain'].values):
        pos = y[index]  
        data_r = np.format_float_positional(data, precision=2,unique=False, fractional=False, trim='-')
        if(data>0): 
           data_s = "+"+str(data_r)+"%"
           if(rtype=="visible_volumes"): color_text = "green"
           else:                         color_text = "red"
        else:       
           data_s = str(data_r)+"%"
           if(rtype=="visible_volumes"): color_text = "red"
           else:                         color_text = "green"
        if (df_V2['rtype_gain'].values[index]>1):
           plt.text(x=pos-0.4 , y=0.55*(df_V2[rtype_index].values[index]*2+df_V2[ertype_index].values[index]),
                    s=data_s ,rotation='horizontal', fontdict=dict(fontsize=13), color=color_text);
        else:
           plt.text(x=pos-0.4 , y=0.55*(df_V1[rtype_index].values[index]*2+df_V1[ertype_index].values[index]),
                    s=data_s , rotation='horizontal',fontdict=dict(fontsize=13), color=color_text);
 
   plt.tight_layout()
   plt.savefig(ofile)

   print('\noutput file\n',ofile,'\n')

