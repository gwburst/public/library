
#### cWB 2G library dependancies


* root, an object oriented framework for large scale data analysis; 
* lal libraries
  - lalsimulation
  - lalburst
  - lalinspiral
  - libmetaio

* HEALPix, an acronym for Hierarchical Equal Area isoLatitude Pixelization of a sphere
* CFITSIO, a library of C and Fortran subroutines for reading and writing data files in FITS (Flexible Image Transport System) data format
* LibFramel (Frame Library), a dedicated software for frame manipulation including file input/output
* CVODE (used by eBBH) solves initial value problems for ordinary differential equation (ODE) systems
 

