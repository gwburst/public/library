#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago, Igor Yakushin, Sergey Klimenko, Roberto De Pietri, Marek Szczepanczyk
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os, sys, glob
import math
from cwb_online.run_utils import get_time, getrows, getnumberfromstring, evalfunc, setparse, get_time, getlimits
from array import array
import ROOT
import cppyy
ROOT.gSystem.Load("%s/lib64/cwb.so"%os.environ["HOME_WAT_INSTALL"])
from ligo.segments import segment, segmentlist
from ligo.segments.utils import fromsegwizard
import cwb_online.draw_bkg as draw_bkg
from ligo.gracedb.rest import GraceDb, HTTPError
import cwb_online.xgbonline as xgbonline
import subprocess
import cwb_online.pastro_calculation as pastro_calculation
import ligo.skymap.io
import astropy
import numpy as np
import json
from healpy.pixelfunc import reorder
from astropy.table import Table, Column

class Trigger:
    def __init__(self,cWB_conf,filename,itree,n,var,job=None,dets=None,run_offset=0):
     if (n<itree.GetEntries()):
        self.root_orig=filename
        self.n=n
        self.tree=itree.CopyTree("Entry$==%i"%n)
        self.tree.SetDirectory(0)
        for v in var:
          value=getattr(itree,v)
          try:
            N=len(v)
            setattr(self,v,list(map(lambda x: x, value)))
          except:
            setattr(self,v,value)
        self.job=job
        self.DQ=[".."]
        self.comments=".."
        self.id=-1
        self.ced_link=""
        if(self.job != None):
            trigger_dir="OUTPUT.merged/TRIGGERS/%s"%self.time[0]
            self.dir="%s/%s"%(self.job.dir,trigger_dir)
            self.path="%s/%s/trigger.txt"%(self.job.dir, trigger_dir)
            self.root_file="%s/%s/trigger.root"%(self.job.dir, trigger_dir)
            self.dir_link="%s/%s"%(self.job.link, trigger_dir)
            self.link="%s/%s/trigger.txt"%(self.job.link, trigger_dir)
            subprocess.getstatusoutput("mkdir -p %s"%self.dir)
            self.dumptree()
        self.cutclass=""
        self.sgnf=[-1]*5
        self.upper_limit=False
        self.send_time=-1
        self.cat_passed_html=""
        self.inj_found=False
        self.creation_time=get_time(run_offset)
        self.graceid=""
        self.gracedb_link=""
        self.topublish=True
        if hasattr(cWB_conf,'time_boundaries'):
             self.check_boundaries(cWB_conf)
        if (hasattr(cWB_conf,"followup")):
            self.selected=3
            self.ifar=0.
            self.ifar_r=0.
            if (hasattr(cWB_conf,"pastro")):
                self.pastro=""
        else:
          if (hasattr(cWB_conf,"Cuts_list")):
            self.getclass(cWB_conf)
          if (hasattr(cWB_conf,"xgboost")):
            self.getxgb(cWB_conf)
          self.significance(cWB_conf)
          self.selected=self.select(cWB_conf)
          if (hasattr(cWB_conf,"pastro")):
              self.get_pastro(cWB_conf)
          if (len(list(filter(lambda x: x.inj==True, dets)))>0):
            self.getinjselect(cWB_conf,dets)
        if(self.job != None):
            self.dump(cWB_conf,dets)
        self.cleantodump()
    def check_boundaries(self,cWB_conf):
        t_lim=getlimits(cWB_conf.time_boundaries)
        if (self.duration[0]<t_lim[0]):
            delay_time=t_lim[0]
        elif (self.duration[0]>t_lim[1]):
            delay_time=t_lim[0]
        else:
            delay_time=self.duration[0]
        if (self.job.end-self.time[0]<delay_time):
            self.topublish=False
    def gettree(self):
        iroot = ROOT.TFile(self.root_file)
        itree = iroot.Get("waveburst")
        isize = itree.GetEntries()       
        if (self.root_file.find("xgb")!=-1):
           self.tree=itree.CopyTree("Entry$==0")
        elif (self.n<itree.GetEntries()):
           self.tree=itree.CopyTree("Entry$==%i"%self.n)
        self.tree.SetDirectory(0)
        iroot.Close()
    def dumptree(self,ofile=None):
        if (not hasattr(self,"tree")):
           self.gettree()
        if (not ofile):
          ofile=self.root_file
        oroot=ROOT.TFile(ofile,"RECREATE")
        self.tree.Write()
        oroot.Close()
    def cleantodump(self):
      if (hasattr(self,"tree")):
        delattr(self,"tree")
      if (hasattr(self,"Psm")):
        delattr(self,"Psm")
    def dump(self,cWB_conf,cwb_dets):
            dets=self.tree.GetUserInfo()
            EVT=ROOT.netevent(self.tree,len(dets))
            for i in range(len(dets)):
               EVT.ifoList.push_back(dets.At(i))
            EVT.dopen(self.path,"w")
            EVT.GetEntry(0);
            # Calculate event_time and SNR:
            event_time = 0.0
            event_snr  = 0.0
            if (hasattr(cWB_conf,"gracedb_search")):
              if cWB_conf.gracedb_search=="BBH":
                event_time = float(self.gps[0]) + float(self.chirp[7]) # segment[0] is gps[0] in netevent.hh
                event_snr  = np.sqrt(float(self.likelihood) - float(self.neted[3])) # cWB-BBH SNR: https://dcc.ligo.org/LIGO-G2301201
              else:
                event_time = float(self.time[0])
                event_snr  = np.sqrt(float(self.likelihood))
            EVT.Dump();
            EVT.dclose()
            f=open(self.path,"a")
            # Info requested by LL team:
            # https://git.ligo.org/computing/gracedb/server/-/issues/292
            if (hasattr(cWB_conf,"followup")):
              far = 0.
              far_day = 0.
            else:
              far     = self.sgnf[2].split(" ")[1] # FAR week
              far_day = self.sgnf[1].split(" ")[1] # FAR day
            print("event_time: %10.4f"%(event_time),file=f)
            print("event_snr:  %1.6e"%(event_snr),  file=f)
            print("far:        %s"%(far),file=f)
            for ifo in cwb_dets:
              try:
                channel_reads="%s,%s"%(channel_reads,ifo.channel_name[0])
              except:
                channel_reads="%s"%(ifo.channel_name[0])
              try:
                t_lldq_flags="%s"%(",".join( map(lambda x,y: "%s:%s"%(x,y),ifo.cat1_channel,ifo.cat1_bitmask)))
              except:
                t_lldq_flags="%s"%("%s:%s"%(ifo.cat1_channel,ifo.cat1_bitmask))
              try:
                lldq_flags="%s,%s"%(lldq_flags,t_lldq_flags)
              except:
                lldq_flags="%s"%(t_lldq_flags)
            print("""
code version: %s
hoft version: %s,%s
lldq_flags: %s
"""%(cWB_conf.code_version,cWB_conf.hoft_version,channel_reads,lldq_flags), file=f)
            self.addinfotofile(cWB_conf,f)
            fit_file="%s/OUTPUT/skymap_%.3f.fits"%(self.job.dir,self.time[0])
            fits_skymap_link="%s/cwb.multiorder.fits"%(self.dir)
            if (hasattr(cWB_conf,"followup")): 
               fits_skymap_link=self.getnamefromnetwork(cWB_conf)
            ced_link=self.compute_ced(cWB_conf)
            print("far_day: %s"%(far_day),file=f)
            print("fits_skymap_link: %s"%(fits_skymap_link),file=f)
            print("ced_link: %s"%(ced_link),file=f)
            f.close()
    def getnamefromnetwork(self,cWB_conf):
        if (hasattr(cWB_conf,"network")):
           t_net=cWB_conf.network
        else:
           t_net="".join(list(map(lambda x: x[0],cWB_conf.ifos)))
        return "%s/cwb.%s.multiorder.fits"%(self.dir,t_net)
    def getclass(self,cWB_conf):
        Cuts_file="%s/Cuts.hh"%(cWB_conf.config_dir)
        ROOT.gROOT.LoadMacro(Cuts_file)
        cut_names=cWB_conf.Cuts_list
        iroot = ROOT.TFile(self.root_file)
        itree = iroot.Get("waveburst")
        otree=itree.CopyTree("Entry$==%i"%self.n)
        if (not isinstance(cut_names,list)):
          cut_names=[cut_names]
        for c in cut_names:
           g=ROOT.gROOT.GetListOfGlobals().FindObject(c)
           cut = cppyy.bind_object(g.GetAddress(),'TCut')
           otree.Draw("run",cut,"goff")
           if (otree.GetSelectedRows()==1):
             self.cutclass=c
        iroot.Close()
    def getxgb(self,cWB_conf):
        xgb=xgbonline.xgbinfo(cWB_conf.xgboost)
        xgb.fixmissing(cWB_conf)
        self.cutclass=""
        for l,f,m in zip(xgb.labels,xgb.freqs,xgb.models):
           if (self.frequency[0]>f[0] and self.frequency[0]<=f[1]):
             self.cutclass=l
             ifile=self.root_file
             ofile=ifile.replace(".root","_xgb.root")
             xgbonline.apply_xgboost(xgb,ifile,ofile,cWB_conf.config_dir,l,l)
             self.root_file=ofile
             iroot = ROOT.TFile(self.root_file)
             itree = iroot.Get("waveburst")
             itree.GetEntry(0)
             value=getattr(itree,"rho")
             setattr(self,"rho",list(map(lambda x: x, value)))
             self.tree=itree.CopyTree("Entry$==0")
             self.tree.SetDirectory(0)
             iroot.Close()
    def getinjselect(self,cWB_conf,dets):
        for ifo in dets:
            inj_found=False
            for inj in getattr(ifo,"inj_name"):
                inj_seg=ifo.readfile(ifo.getcatname(inj,cWB_conf),True)
                if (len(inj_seg)>0):
                     for seg in inj_seg:
                         if (float(seg[0])<float(self.time[0]) and float(seg[1])>float(self.time[0])):
                            inj_found=True
                            if (self.inj_found==False):
                               self.cat_passed_html+="INJ %s %s"%(ifo.name,inj)
                            else:
                               self.cat_passed_html+="<br>INJ %s %s"%(ifo.name,inj)
                            self.inj_found=True
    def select(self,cWB_conf): #0 - no extra thresholds, 1 - coherent, 2 - online publishing, 3 - external collaboration, 4 - offline threshold
        print(list(map(type, [self.netcc[0], self.rho[cWB_conf.id_rho]]))); sys.stdout.flush()
        if (hasattr(cWB_conf,"Cuts_list") or hasattr(cWB_conf,"xgboost")):
          coherent=len(self.cutclass)>0
        else:
          if (hasattr(cWB_conf,"th_cc")):
              th_cc=cWB_conf.th_cc
          else:
              th_cc=0.6
          coherent=float(self.netcc[0])>th_cc
        offline=self.ifar_v<=cWB_conf.th_far_off and coherent
        lumin=self.ifar_v<=cWB_conf.th_far_lum and coherent
        if(offline):
            return 4
        elif(lumin):
            return 3
        elif(coherent):
            return 1
        else:
            return 0
    def table_header(self,cWB_conf,id=False):
       ifile="%s/header.txt"%cWB_conf.config_dir
       rows=getrows(ifile,cWB_conf)
       res="<tr>"
       if(id):
         res+="<th>id</th>"
         if (hasattr(cWB_conf,"gracedb_group")):
            res+="<th>Gracedb</th>"
         res+="<th>ced</th>"
         if (hasattr(cWB_conf,"pastro")):
            res+="<th>pastro</th>"
       else:
         rows=list(filter(lambda x: x.png==False, rows))
       for r in rows:
        if (r.det==True):
          res+="<th>%s</th>"%"<br>".join(list(map(lambda x: "%s%s"%(r.name[0],x),cWB_conf.ifos)))
        else:
          res+="<th>%s</th>"%"<br>".join(r.name)
       res+="</tr>"
       return res
    def compute_ced(self,cWB_conf,web="link"):
        ced_link = "%s/OUTPUT/%s"%(getattr(self.job,web),self.compute_ced_dir(cWB_conf))
        return ced_link
    def compute_ced_dir(self,cWB_conf):
        [seg_start,seg_stop]=self.job.dir.split("/")[-1].split("-")
        seg_white=self.job.back
        ced_dir = "ced_%s_%s_%s-%s_slag0_lag0_1_job1/"%(seg_white,cWB_conf.seg_duration, seg_start, seg_stop)
        for i in range(len(cWB_conf.ifos)):
            ced_dir += cWB_conf.ifos[i]
        for i in range(len(cWB_conf.ifos)):
            ced_dir += "_%.3f"%(float(self.start[i]))
        return ced_dir
    def get_ced_link(self):
        print("In get_ced_link")
        t="%s/OUTPUT/ced*/*%.3f*"%(self.job.dir,float(self.start[0]))
        a=glob.glob(t)
        if(len(a)>1):
            print("Warning: there are more than one CEDs: %s"%("|".join(a)))
        elif(len(a)==0):
            print("Warning: No CEDs found satisfying %s"%(t))
            return "NA"
        print("Found ced: %s"%(a[0]))
        ced_link="https://ldas-jobs.ligo.caltech.edu/~waveburst/" + "/".join(a[0].split("/")[3:])
        return ced_link
    def getvalue(self,name,roun,expr,cWB_conf):
      if (name.find(".png")!=-1):
         image_path="%s/%s"%(self.dir,name)
         if (os.path.exists(image_path)):
            value="<img src=\"%s/%s\" width=200>"%(self.dir_link,name)
         else:
            value=""
      elif (name.find("[")!=-1 and name.find("]")!=-1):
        split=name.split("[")
        name=split[0]
        pos=int(split[1].replace("]",""))
        value=getattr(self,name)[pos]
      else:
        value=getattr(self,name)
        if (isinstance(value,list)):
          value=value[0]
      if (not isinstance(getnumberfromstring(str(value)),str)):
        if (expr!="="):
          value=evalfunc(getnumberfromstring(str(value)),expr)
        if (roun!="="):
          value=roun%(getnumberfromstring(str(value)))
      return str(value)
    def to_html_table_row(self,cWB_conf,id=-1):
        ifile="%s/header.txt"%cWB_conf.config_dir
        rows=getrows(ifile,cWB_conf)
        res="<tr>"
        if (id>0):
         if (hasattr(cWB_conf,"gracedb_group")):
          if(len(self.graceid)>0):
               gracedbid="<td><a href=\"%s\">%s</a></td>"%(self.gracedb_link,self.graceid)
          else:
               gracedbid="<td></td>"
         else:
          gracedbid=""
         if (os.path.exists(self.compute_ced(cWB_conf,"dir"))):
            ced_link="<a href=\"%s/\">ced</a>"%(self.compute_ced(cWB_conf))
         else:
            ced_link=""
         if (os.path.exists(self.path)):
            id_link="<a href=\"%s\">%i</a>"%(self.link,id)
         else:
            id_link="%i"%id
         res+="<td>%s</td>%s<td>%s</td>"%(id_link,gracedbid,ced_link)
         if (hasattr(cWB_conf,"pastro")):
             res+="<td>%s</td>"%self.get_pastro_string()
        else:
          rows=list(filter(lambda x: x.png==False, rows))
        for r in rows:
          res+="<td align=right>"
          res+="<br>".join(list(map(lambda x,y,z: self.getvalue(x,y,z,cWB_conf),r.var,r.roun,r.expr)))
          res+="</td>"
        res+="</tr>"
        return res
    def significance(self,cWB_conf):
        qvetodir="plot%s"%self.cutclass
        if (hasattr(cWB_conf,"xgboost")):
          qvetodir="plotxgb"

        bkg_postprod_dir="%s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.postprod_dir)
        #a=draw_bkg.last_N(cWB_conf,setparse("-N 0 -recompute 0 -odir %s -rho %f -plotdir %s"%(bkg_postprod_dir,float(self.rho[cWB_conf.id_rho]),qvetodir)))
        b=draw_bkg.last_N(cWB_conf,setparse("-N 1 -recompute 0 -odir %s -rho %f -plotdir %s"%(bkg_postprod_dir,float(self.rho[cWB_conf.id_rho]),qvetodir)))
        c=draw_bkg.last_N(cWB_conf,setparse("-N 7 -recompute 0 -odir %s -rho %f -plotdir %s"%(bkg_postprod_dir,float(self.rho[cWB_conf.id_rho]),qvetodir)))
        #self.sgnf[0]=a#a[1]
        self.sgnf[1]=b#[1]
        self.sgnf[2]=c#[1]
        rate=float(self.sgnf[2].split(" ")[1])
        num=int(self.sgnf[2].split(" ")[0])
        ifar=1./(rate*3600*24*365)
        if (ifar<1.e-2):
          ifar1_s="%.2e"%ifar
        else:
          ifar1_s="%.2f"%ifar
        if (num==0):
           ifar1_s=">%s"%ifar1_s
           self.upper_limit=True
        self.ifar=ifar1_s
        self.ifar_v=rate
    def get_pastro(self,cWB_conf):
        txt_zerolag="%s/pastro_data_zl.txt"%self.dir
        f=open(txt_zerolag,"w")
        #print("%s,%s,%s"%(str(self.rho[1]),str(self.time[0]),str(1./self.ifar_v)),file=f)
        print("%s,%s,%s"%(str(self.rho[1]),str(self.time[0]),str(1./self.ifar_v)),file=f)
        f.close()
        txt_time_lag="%s/%s/FOM_7_0_0/pastro_data_nzl.txt"%(cWB_conf.bkg_run_dir,cWB_conf.postprod_dir)
        txt_background_ratio = "%s/%s/FOM_7_0_0/pastro_data_rlive.txt"%(cWB_conf.bkg_run_dir,cWB_conf.postprod_dir)
        injection_files="%s/pastro_data_sim.txt"%(cWB_conf.config_dir)
        if (os.path.exists(txt_time_lag) and os.path.exists(txt_background_ratio) and self.rho[1]>=0.01):
            p_b=float(pastro_calculation.get_pastro(txt_zerolag,txt_time_lag,txt_background_ratio,injection_files))
            #f=open("%s/pastro.txt"%self.dir,"w")
            #print("pastro: %g\nodds: %g"%(1.0-p_b,(1.0 - p_b) / p_b),file=f)
            #f.close()
            pastro = {"Terrestrial": p_b, "BNS": 0.0, "BBH": 1.0-p_b, "NSBH": 0.0}
            jsonString = json.dumps(pastro)
            jsonFile = open("%s/cwb.p_astro.json"%self.dir, "w")
            jsonFile.write(jsonString)
            jsonFile.close()
            self.pastro=jsonString
        else:
            self.pastro=""
    def get_pastro_string(self):
        if (self.pastro==""):
            return ""
        else:
            lines=self.pastro[1:-1].split(",")
            ss=[]
            for l in lines:
                s=l.split(":")
                v=float(s[1])
                if (v==0.):
                    ss.append("%s: 0"%s[0]) 
                elif (v<0.01):
                    ss.append("%s: %.2e"%(s[0],v)) 
                else:
                    ss.append("%s: %.2f"%(s[0],v)) 
            return "<br>".join(ss)
                #print(l,float(s[1]),"%s: %.3f"%(s[0],))
    def addinfotofile(self,cWB_conf,f):
        b=self.sgnf[1]
        c=self.sgnf[2]
        print("## number of events with rho>= rho of the event | corresponding rate | start time of the analyzed segment list | end time of the analyzed segment list | duration of the analyzed segment list", file=f)
        try:
           trials=cWB_conf.Cuts_trials
        except:
          try:
            trials=len(cWB_conf.Cuts_list)
          except:
            trials=1
        print("Applied trials factor: %i"%(trials), file=f)
        print("#significance based on the last day", file=f)
        print(b, file=f)
        print("#significance based on the last week", file=f)
        print(c, file=f)
        if (self.upper_limit==True):
            print("far_is_upper_limit: True", file=f)
        print("\n%s: %s"%(os.environ['SITE_CLUSTER'],self.path), file=f)
        if (hasattr(cWB_conf,"production_dir")):
            subprocess.getstatusoutput("gunzip %s/input/*.gz"%self.job.dir)
        lines=open("%s/input/burst.in"%(self.job.dir)).readlines()
        print("CAT1 %s"%("".join(lines)), file=f)
        for ifo in cWB_conf.ifos:
          lines=open("%s/input/%s_cat2.in"%(self.job.dir,ifo)).readlines()
          print("CAT2 %s %s"%(ifo,"".join(lines)), file=f)
        if (hasattr(cWB_conf,"production_dir")):
            self.job.zip_files()
        f.flush()
        print("After adding info to the trigger")
    def getclient(self,cWB_conf):
       if (hasattr(cWB_conf,"gracedb_client")):
          client = GraceDb(cWB_conf.gracedb_client)
       else:
          client = GraceDb()
       return client
    def getgraceid(self,cWB_conf):
        graceid=""
        client=self.getclient(cWB_conf)
        selection = "pipeline: cwb group: %s search: %s %.3f .. %.3f"%(cWB_conf.gracedb_group,cWB_conf.gracedb_search,float(self.time[0])-1,float(self.time[0])+1)
        if (hasattr(cWB_conf,"gracedb_label")):
            selection="%s label: %s"%(selection,cWB_conf.gracedb_label)
        #print(selection)
        neighbours = client.events(selection)
        for e in neighbours:
            neigh_dets=e['instruments'].split(",")
            if (len(neigh_dets)<len(cWB_conf.ifos)):
                graceid = e['graceid']
        return graceid
    def followup_skymap(self,cWB_conf,triggers_send_time):
       client=self.getclient(cWB_conf)
       if (self.graceid!=""):
         self.comments="Sent to GraceDB at %s, delay=%.3f"%(repr(triggers_send_time),float(triggers_send_time) - float(self.time[0]))
         if (hasattr(cWB_conf,"gracedb_group")):
           self.send_skymap(cWB_conf,client)
    def to_external_collaborations(self,cWB_conf,triggers_send_time):
     if(hasattr(cWB_conf,"th_far_mail")):
       th_far_mail=cWB_conf.th_far_mail
     else:
       th_far_mail=cWB_conf.th_far_lum
     if (self.graceid==""):
       self.comments="Sent to GraceDB at %s, delay=%.3f"%(repr(triggers_send_time),float(triggers_send_time) - float(self.time[0]))
       client=self.getclient(cWB_conf)
       if (hasattr(cWB_conf,"gracedb_group") and self.ifar_v>0.):
           self.send_to_gracedb(cWB_conf,client,triggers_send_time)
           self.send_skymap(cWB_conf,client)
           if (hasattr(cWB_conf,"pastro") and self.rho[1]>0.01):
             self.send_pastro(cWB_conf,client)
           self.send_CED(cWB_conf,client)
           self.send_PE(cWB_conf,client)
       else:
           self.graceid="-"
       self.gracedb_link="https://gracedb.ligo.org/events/view/%s"%(self.graceid)
       if (hasattr(cWB_conf,"emails") and (self.ifar_v<th_far_mail or self.ifar_v<0.)):
         self.send_mail(cWB_conf)
     else:
       self.comments+="<br>Uploaded at %s (delay=%.3f)"%(repr(triggers_send_time),float(triggers_send_time) - float(self.time[0]))
    def send_to_gracedb(self,cWB_conf,client,triggers_send_time):
         try:
             r = client.createEvent(cWB_conf.gracedb_group, cWB_conf.gracedb_analysis, self.path, search=cWB_conf.gracedb_search)
             r = r.json()
             graceid = r["graceid"]
             self.graceid=graceid
             if (hasattr(cWB_conf,"gracedb_label")):
                client.writeLabel(self.graceid,cWB_conf.gracedb_label)
         except HTTPError as e:
            print("Error on sending to gracedb")
            print(e) 
    def multiorder(self,ifile,ofile, ifos):
       hdus = astropy.io.fits.open(ifile)
       hdr   = hdus[1].header
       table = ligo.skymap.io.read_sky_map(hdus, moc=True)
       hdus.close()
       table["PROBDENSITY"]=table["PROBDENSITY"].astype(np.float64)
       table.meta['instruments'] = set(ifos)
       table.meta['origin'] = 'LIGO/Virgo/KAGRA'
       table.meta['objid']       = self.graceid
       ligo.skymap.io.write_sky_map(ofile, table)
    def reducemap(self,ifile,ofile):
       hdus = astropy.io.fits.open(ifile)
       table = ligo.skymap.io.read_sky_map(hdus,moc=True)
       skymap = ligo.skymap.io.read_sky_map(hdus)
       hdus.close()
       nest_skymap =  reorder(skymap[0],  r2n=True)
       n=int(len(nest_skymap)/4)
       new_table=[]
       for i in range(n):
           ni=i*4
           new_table.append(nest_skymap[ni]+nest_skymap[ni+1]+nest_skymap[ni+2]+nest_skymap[ni+3])
       ring_table =  reorder(new_table,  n2r=True)
       table.remove_column('PROBDENSITY')
       table.remove_column('UNIQ')
       table.add_column(Column(ring_table,name='PROB'),0)
       ligo.skymap.io.write_sky_map(ofile,table)
    def send_skymap(self,cWB_conf,client):
          fit_file="%s/OUTPUT/skymap_%.3f.fits"%(self.job.dir,self.time[0])
          fit_file_cwb="%s/cwb.multiorder.fits"%(self.dir)
          if (hasattr(cWB_conf,"followup")):
               fit_file_cwb=self.getnamefromnetwork(cWB_conf)
          print("fit_file = %s"%(fit_file))
          print("fit_file_cwb = %s"%(fit_file_cwb))
          #ADD ALSO UNFLATTENED SKYMAP
          self.multiorder(fit_file,fit_file_cwb,cWB_conf.ifos)
          if (hasattr(cWB_conf,"skymap_size")):
             if (os.stat(fit_file_cwb).st_size/1024.>cWB_conf.skymap_size):
                subprocess.getstatusoutput("mv %s %s"%(fit_file_cwb,fit_file_cwb.replace(".fits","_big.fits"))) 
                reduce_file_cwb=fit_file_cwb.replace("multiorder","reduced")
                self.reducemap(fit_file,reduce_file_cwb)
                self.multiorder(reduce_file_cwb,fit_file_cwb,cWB_conf.ifos)
          message="cWB skymap, detectors: %s, working dir: %s, cluster: %s"%("".join(cWB_conf.ifos),cWB_conf.online_dir,os.environ['SITE_CLUSTER'])
          try:
            r = client.writeLog(self.graceid, message, filename=fit_file_cwb, tag_name=["sky_loc"])
            client.writeLabel(self.graceid,"SKYMAP_READY")
          except:
            print("no skymap sent")
    def send_pastro(self,cWB_conf,client):
          pastro_file="%s/cwb.p_astro.json"%self.dir
          try:
            r = client.writeLog(self.graceid, "cWB pastro", filename=pastro_file, tag_name="p_astro")
            client.writeLabel(self.graceid,"PASTRO_READY")
          except:
            print("no pastro sent")
    def send_CED(self,cWB_conf,client):
          ced_dir="%s/OUTPUT/%s"%(self.job.dir,self.compute_ced_dir(cWB_conf))
          try:
            r = client.writeLog(self.graceid, "<a href=\"%s\">cWB CED</a>"%(self.compute_ced(cWB_conf)), tag_name="analyst_comments")
          except:
            print("no CED sent")
    def send_PE(self,cWB_conf,client):
          html_code="""
<table>
<tr><th colspan=2>cWB parameter estimation</th></tr>
<tr><td>Frequency [Hz]</td><td align=right>%.3f</td></tr>
<tr><td>Bandwidth [Hz]</td><td align=right>%.3f</td></tr>
<tr><td>Duration [ms]</td><td align=right>%.2f</td></tr>
<tr><td>Hrss</td><td align=right>%.3e</td></tr>
</table>
"""%(float(self.frequency[0]),float(self.bandwidth[0]),float(self.duration[0])*1000.,float(self.strain[0]))
          print(html_code)
          try:
            r = client.writeLog(self.graceid, html_code, tag_name="pe")
          except:
            print("no pe sent")
    def send_mail(self,cWB_conf):
        if (self.ifar_v<0):
         ifar1_f="WARNING NEG IFAR %s"%self.ifar_v
        else:
         ifar1_f=self.ifar
        msg=\
"""Subject:  %s %s: Event GPS %s %s

%s
network = %s, injection: %s, search: %s
rho = %s, netcc = %s, Far: %s Hz, %s
frequency band: [%.2f,%.2f] Hz, %s

CED: %s

Gracedb entry: %s
-------------------------------------------------------------------
File on %s: %s
File on Web: %s
-------------------------------------------------------------------
Main online page: %s
        """%("".join(cWB_conf.ifos), cWB_conf.gracedb_search, self.time[0], ifar1_f,
             cWB_conf.title,
             "".join(cWB_conf.ifos), self.inj_found, cWB_conf.gracedb_search,
             self.rho[cWB_conf.id_rho], self.netcc[0], self.ifar_v, ifar1_f, float(self.low[0]) ,float(self.high[0]), self.cutclass,
             self.compute_ced(cWB_conf), self.gracedb_link, os.environ['SITE_CLUSTER'],
             self.path, self.link, cWB_conf.web_link)
        print("="*30)
        print(msg)
        print("="*30)
        try:
          com="echo \"%s\" | /usr/sbin/sendmail -F %sonline-%s-%s %s"%(msg,cWB_conf.gracedb_analysis,cWB_conf.gracedb_search,"".join(cWB_conf.ifos),",".join(cWB_conf.emails))
          a=subprocess.getstatusoutput(com)
        except HTTPError as e:
          print("Error on sending mail")
          print(e)

def root_dump_2_triggers(cWB_conf,fn,job,dets,run_offset):
    iroot = ROOT.TFile(fn)
    itree = iroot.Get("waveburst")
    isize = itree.GetEntries()
    var_sel=list(map(lambda x: x.GetFullName().Data(),itree.GetListOfBranches()))
    print("N",isize)
    triggers=[]
    if(isize>0):
      for i in range(isize):
        triggers.append(Trigger(cWB_conf,fn,itree,i,var_sel,job,dets,run_offset))
    iroot.Close()
    return triggers
