#  ---------------------------------------------------------------
#  Plot ascii logo
#  -----------------------------------------------------------------

if hash toilet 2>/dev/null; then
        toilet -f bigmono12 --filter border  --metal \   cWB \
else
        echo -e " \n
BEWARE: cWB ascii logo has not been printed due to a missing command on this system: \n
if possible, please install the \"toilet\" command.                             "
fi

conda activate /data/dev/cWB/cwb-xgb      # New conda environment with latext XGBoost


#  -----------------------------------------------------------------
#  WAT env for CIT CLUSTER
#  -----------------------------------------------------------------


  export SITE_CLUSTER="CIEMAT"
  export BATCH_SYSTEM="CONDOR"

#  this section is specific for the CWB pipeline
#  -----------------------------------------------------------------


  export CWB_ANALYSIS="2G"

  export CWB_CONFIG="/data/dev/cWB/config_o4"

  export HOME_WWW="http://gaevirgo01.ciemat.es/virgo/"
  export HOME_CED_WWW="http://gaevirgo01.ciemat.es/virgo/ced"
  export HOME_CED_PATH="/data/prod/www/cWB/ced"
  export CWB_DOC_URL="https://ldas-jobs.ligo.caltech.edu/~waveburst/waveburst/WWW/doc"
  export CWB_REP_URL="https://ldas-jobs.ligo.caltech.edu/~waveburst/waveburst/WWW/reports"
  export CWB_GIT_URL="https://git.ligo.org/cWB"

  export HOME_WAT_FILTERS="/data/dev/cWB/config_o4/XTALKS"
  export HOME_BAUDLINE="${HOME_LIBS}/BAUDLINE/baudline_1.08_linux_x86_64"
  export HOME_ALADIN="${HOME_LIBS}/ALADIN/Aladin.v7.533"

  export CWB_USER_URL="https://ldas-jobs.ligo.caltech.edu/~${USER}/reports"

  export CWB_PEGASUS_WATENV="/cvmfs/virgo.infn.it/waveburst/SOFT/WAT/trunk/cnaf_watenv.sh"
  export CWB_PEGASUS_SITE="creamce_cnaf"



  #export LD_LIBRARY_PATH=${HOME_LIBS}/SYSLIBS:$LD_LIBRARY_PATH

#  -----------------------------------------------------------------
#  DO NOT MODIFY !!!
#
#  1) In this section the HOME_WAT env is automatically initialized
#  2) Init Default cWB library
#  3) Init Default cWB config
#  -----------------------------------------------------------------


   MYSHELL=`readlink /proc/$$/exe`
  if [[  "$MYSHELL" =~ "tcsh"  ]]; then
    echo "\nEntering in TCSH section..."
     CWB_CALLED=($_)
    if [[=$#CWB_CALLED = 2 ]]; then # alias
      set CWB_CALLED=`alias=$CWB_CALLED`
    fi
    if [[  "$CWB_CALLED" != ""  ]]; then
       WATENV_SCRIPT=`readlink -f $CWB_CALLED[2]`
    else
      echo "\nError: script must be executed with source command"
      return 0 1
    fi
     script_dir=`dirname $WATENV_SCRIPT`
  fi
  if [[  "$MYSHELL" =~ "bash"  ]]; then
    echo ""
    echo "Entering in BASH section..."
    script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) #_SKIP_CSH2SH_
  fi
  if [[  "$MYSHELL" =~ "zsh"  ]]; then
    echo "\nEntering in ZSH section..."
    script_dir=$( cd "$( dirname "${(%):-%N}" )" && pwd )        #_SKIP_CSH2SH
 fi
  export HOME_WAT=$script_dir
  export HOME_WAT_INSTALL="${HOME_WAT}/tools/install"
  #export HOME_WAT_INSTALL="${HOME_WAT}/tools/install_test_fix-cwb_report_sim.C"


  #source $HOME_WAT/tools/config.sh     # NEW
  source $CWB_CONFIG/setup.sh


  export CWB_HTML_INDEX="${CWB_MACROS}/html_templates/html_index_template_modern.txt"

  source $HOME_WAT/tools/install/etc/cwb/cwb-activate.sh   #Comment this if it is your your first compilation
  [[ ":$PATH:" =~ ":$HOME_WAT_INSTALL/bin:" ]] || PATH="$HOME_WAT_INSTALL/bin:$PATH"
  source $CWB_SCRIPTS/cwb_watenv.sh

#TO BE REMOVED LATER      # NEW
  export CWB_NETS_FILE="${CWB_SCRIPTS}/cwb_net.sh"
  export CWB_MPARAMETERS_FILE="./merge/cwb_user_parameters.C"
 # export NODE_DATA_DIR="tmp"
  export NODE_DATA_DIR=/local/${USER}
  export CONDOR_LOG_DIR="$PWD/condor_logs"
  export WWW_PUBLIC_DIR="${HOME}/public_html/reports"
  #export _USE_OSG=1        # comment if not OSG
  #export CWB_CONTAINER_IMAGE="docker://containers.ligo.org/francesco.salemi/cwb_docker:debug2"   # comment if not running with container
  #export CWB_CONTAINER_IMAGE="docker://containers.ligo.org/joshua.willis/cwb_docker:devel"
  export _USE_CMAKE=1
  export PYTHONPATH=${HOME_WAT_INSTALL}/lib/python3.10/site-packages/:${PYTHONPATH}



  export CWB_TOOLBOX="${HOME_WAT}/tools/toolbox"
  export CWB_SKYPLOT="${HOME_WAT}/tools/skyplot"
  export CWB_GWAT="${HOME_WAT}/tools/gwat"
  export CWB_EBBH="${HOME_WAT}/tools/eBBH"
  export CWB_HISTORY="${HOME_WAT}/tools/history"
  export CWB_STFT="${HOME_WAT}/tools/stft"
  export CWB_BICO="${HOME_WAT}/tools/bico"
  export CWB_FILTER="${HOME_WAT}/tools/filter"
  export CWB_FRAME="${HOME_WAT}/tools/frame"
  export CWB_WAVESCAN="${HOME_WAT}/tools/wavescan"
  export CWB_ONLINE="${HOME_WAT}/tools/online"
  export HOME_WAVEBICO="${HOME_WAT}/tools/wavebico"
  export HOME_WAVEMDC="${HOME_WAT}/tools/wavemdc"

  # export=ROOT_INCLUDE_PATH: used for include path by macros
  export ROOT_INCLUDE_PATH="$CWB_CONFIG"

  export HOME_CWB="${HOME_WAT_INSTALL}/etc/cwb"
  export CWB_DIR_NAME="${HOME_CWB}/macros"
  export CWB_SCRIPTS="${HOME_CWB}/scripts"
  export CWB_MACROS="${HOME_CWB}/macros"
  export CWB_PLUGINS="${HOME_CWB}/plugins"
  export CWB_ROOTLOGON_FILE="${CWB_MACROS}/cwb_rootlogon.C"

  #export WMDC_CONFIG="./wmdc_config.C"
  alias wmdc_condor="${HOME_WAVEMDC}/wmdc_condor.csh"

  #export HOME_FRDISPLAY="${HOME_WAT}/tools/frdisplay"
  export CWB_NETS_FILE="${CWB_SCRIPTS}/cwb_net.sh"

  if [[ -z $_USE_OSG  ]]; then
    echo -n ""
  else
    export CWB_NETS_FILE="${CWB_SCRIPTS}/cwb_net_osg.sh"
  fi
