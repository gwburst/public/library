#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago, Igor Yakushin, Sergey Klimenko
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from cwb_online.run_utils import get_time, get_date_from_gps, tdate, tconvert, time_convert
import subprocess, os, sys, glob, time
import calendar

def make_month(year,month,job_dir):
    print("year=%s month=%s"%(repr(year),repr(month)))
    cal = calendar.TextCalendar(calendar.SUNDAY)
    a=cal.monthdayscalendar(year, month)
    b=[]
    namemonths=['January','February','March','April','May','June','July','August','September','October','November','December']
    monthname=[namemonths[month-1], '%i'%year]
    monthweek= ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
    b.append(monthname)
    b.append(monthweek)
    for i in a:
      u=[]
      for w in i:
         if (w!=0):
            u.append('%u'%w)
      b.append(u);
    title=" ".join(b[0])
    header="<tr><th>%s</th></tr>"%("</th><th>".join(b[1]))
    if(len(b[2])<7):
        c=['']*(7-len(b[2]))
        b[2]=c+b[2]
    if(len(b[-1])<7):
       c=['']*(7-len(b[-1]))
       b[-1]=b[-1]+c
    rows=[header]
    for i in range(2,len(b)):
        row=[]
        for d in b[i]:
            if(len(d)>0):
                g=time_convert("%d/%s/%d"%(month,d,year))
                gps=int(g[-1].strip())
                h=glob.glob("%s/%s"%(job_dir,get_date_from_gps(gps)))
                if(len(h)==1):
                    mydir="/".join(h[0].split("/")[-3:])
                    row.append("<td><a href=\"%s/day.html\" target=\"Data\">%s</a></td>"%(mydir,d))
                elif(len(h)>1):
                    print("Something is wrong")
                else:
                    row.append("<td>%s</td>"%(d))
            else:
                row.append("<td></td>")
        srow="<tr>"+"\n".join(row)+"</tr>"
        rows.append(srow)
    table="<table border=1>\n%s\n</table>"%("\n".join(rows))
    return "<h4>%s</h4>\n%s"%(title,table)

def increment_month(year,month):
    if(month<=11):
        return [year,month+1]
    else:
        return [year+1,1]

def run_calendar(cWB_conf,days):
  rpath="%s/daily"%cWB_conf.summaries_dir
  rootdir="%s/%s"%(cWB_conf.web_dir,rpath)

  dirs=list(map(lambda x: "%s/%s"%(rootdir,x),days))
  dates=[]
  for d in dirs:
    dates.append(tdate(d.split("/")[-1]))
  dates=sorted(dates,key=lambda x: x.time)
  print(list(map(lambda x: "%s %.3f %i %i %i"%(x.date,x.time,x.year,x.month,x.day),dates)))

  start_year=dates[0].year
  start_month=dates[0].month
  end_year=dates[-1].year
  end_month=dates[-1].month

  current_year=start_year
  current_month=start_month

  t=get_time()
  t1=tconvert(t)

  months=[]
  months.append(make_month(current_year,current_month,rootdir))
  if(start_year!=end_year or start_month!=end_month):
    [current_year,current_month]=increment_month(current_year,current_month)
    while(current_year!=end_year or current_month!=end_month):
        months.append(make_month(current_year,current_month,rootdir))
        [current_year,current_month]=increment_month(current_year,current_month)
    months.append(make_month(end_year,end_month,rootdir))

  ff=open("%s/calendar.html"%cWB_conf.web_dir,"w")
  print("""
<html>
<body>

<h3>Calendar</h3>
""", file=ff)

  print("\n".join(months), file=ff)
  print("</body>", file=ff)
  print("</html>", file=ff)

  ff.flush()
  ff.close()

