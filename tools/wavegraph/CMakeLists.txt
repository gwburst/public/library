project(
    WAVEGRAPH
    LANGUAGES CXX
)

#Include the master CMakeLists.txt
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(WAVEGRAPH_SRC wavepath.cc wavegraph.cc)

#Get all the header files
string(REGEX REPLACE "[.]cc" ".hh" WAVEGRAPH_HDRS "${WAVEGRAPH_SRC}")

#Make a local copy of the CWB_OPTS
set(WAVEGRAPH_OPTS ${CWB_OPTS})

#Make a local copy of the CWB_INCLUDE_DIRS
set(WAVEGRAPH_INCLUDE_DIRS ${CWB_INCLUDE_DIRS})

#Create the library
add_library(WAVEGRAPH SHARED ${WAVEGRAPH_SRC})


#Set the target properties
set_target_properties(WAVEGRAPH PROPERTIES PREFIX "")
set_target_properties(WAVEGRAPH PROPERTIES OUTPUT_NAME "wavegraph")
set_target_properties(WAVEGRAPH PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib)

#Add all the include directories to the target
target_include_directories(WAVEGRAPH PRIVATE ${WAVEGRAPH_INCLUDE_DIRS})

target_link_libraries(WAVEGRAPH WAT ROOT::Core)

#Generate the root dictionary
ROOT_GENERATE_DICTIONARY(wavegraph_dict ${WAVEGRAPH_HDRS}
    LINKDEF wavegraph_LinkDef.h
    MODULE ${PROJECT_NAME})

install(TARGETS WAVEGRAPH)
install(FILES ${WAVEGRAPH_HDRS}
        TYPE INCLUDE)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_SHARED_LIBRARY_PREFIX}${PROJECT_NAME}_rdict.pcm
        TYPE LIB)
