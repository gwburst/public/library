#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago, Igor Yakushin, Sergey Klimenko
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import glob
import math
from cwb_online.run_utils import getnumberfromstring
from cwb_online.run_utils import getoutputcommand
from cwb_online.frame import FRAME
import ligo.segments as seg
import ligo.segments.utils as seg_ut
from lal import LIGOTimeGPS
from functools import reduce
import subprocess
import framel

class detector:
    def __init__(self, name, ifile, bkg=False):
        self.name=name
        self.obs=name[0]
        self.path=os.path.abspath(ifile)
        lines=[y for y in [x.replace("\n","") for x in open(ifile).readlines()] if len(y)>0]
        self.DQ=[]
        self.inj=False
        append=True
        for line in lines:
           if (len(line.strip())==0):
             append=True
           elif (line.find("#")!=-1):
                itype=line.replace("#","")
                if (itype.find("CLUSTER_")!=-1):
                  if (itype.find("CLUSTER_%s:"%os.environ['SITE_CLUSTER'])==-1):
                    append=False
                  else:
                    append=True
                    itype=itype.replace("CLUSTER_%s:"%os.environ['SITE_CLUSTER'],"")
                if (itype.find("cat")!=-1 or itype.find("veto")!=-1):
                  self.DQ.append(itype)
                  if (append==True):
                    setattr(self,"%s_int"%(itype),True)
                    if (itype.find("veto")!=-1):
                      setattr(self,"%s_int"%(itype),False)
                if (itype.find("inj")!=-1):
                 self.inj=True
           else:
                a=[x for x in line.split(" ") if len(x)>0]
                attr_name="%s_%s"%(itype,a[0].replace(":",""))
                b=[]
                for c in a[1:]:
                  if (c):
                    b.append(getnumberfromstring(c.replace("(DET)",self.name).replace("(OBS)",self.obs)))
                if (append==True):
                  setattr(self,attr_name,b)
        if (bkg==False):
          self.previouslist=glob.glob("%s/*.gwf"%self.path_zero[0])
        else:
          count_qm=self.path_bkg[0].count("?")
          string_qm="?"*count_qm
          if (count_qm==0):
              self.previouslist=glob.glob("%s*.gwf"%self.path_bkg[0])
          else:
              idirs=glob.glob("/".join(self.path_bkg[0].split("/")[:-1]))
              idirs.sort()
              self.previouslist=glob.glob("%s/*.gwf"%idirs[-1])
        if (len(self.previouslist)>0):
            first_frame=FRAME(self.previouslist[0])
            self.frame_name=first_frame.pfn.replace("%i"%first_frame.start,"TIME")
            self.frame_duration=first_frame.duration
            self.start_read=first_frame.start
        if (bkg==True):
            self.frame_name="/".join(self.path_bkg[0].split("/")[:-1])+"/"+self.frame_name.split("/")[-1]
            all_frames=list(map(lambda x: FRAME(x), self.previouslist))
            all_durations=list(map(lambda x: x.duration, all_frames))
            temp_frame_duration=max(set(all_durations), key=all_durations.count) 
            if (temp_frame_duration!=self.frame_duration):
                self.frame_name=self.frame_name.replace("%i"%self.frame_duration,"%i"%temp_frame_duration)
                self.frame_duration=temp_frame_duration
        else:
          self.last_read=-1
          self.segments={}
          for cat in self.DQ:
              self.segments[cat]=seg.segmentlist([])
          if (self.inj):
              self.inj_channel=self.inj_channel[0]
              for inj in getattr(self,"inj_name"):
                   self.segments[inj]=seg.segmentlist([])
    def getpathfromgps(self,gps):
        if (self.frame_duration==1):
          ofile=self.frame_name.replace("TIME","%i"%gps)
        else:
          fs=self.frame_duration
          new_gps = int(gps/fs)*fs
          ofile=self.frame_name.replace("TIME","%i"%new_gps)
        count_qm=ofile.count("?")
        if (count_qm>0):
          ofile=ofile.replace("?"*count_qm,str(gps)[:count_qm])
        return ofile
    def getframelist(self,start,stop):
       first=FRAME(self.getpathfromgps(start))
       time=start-10.#first.duration
       frame_list=[]
       while(time<=stop+10.):#first.duration):
         frame_list.append(self.getpathfromgps(time))
         time+=first.duration
       return frame_list
    def findsegment(self,cWB_conf):
          newlist=glob.glob("%s/*.gwf"%self.path_zero[0])
          newlist.sort()
          frames=[FRAME(x) for x in newlist]
          frames_seg=seg.segmentlist([seg.segment(x.start,x.end) for x in frames])
          frames_seg.coalesce()
          diff=[x for x in frames if float(x.start)> float(self.last_read)]
          if (len(diff)>0):
             for ff in diff:
               print("ifile",ff.pfn)
               self.last_read=ff.start 
               for cat in self.DQ:
                  ttseg=self.getsegments_fromcat(ff,cat)
                  if (getattr(self,"%s_int"%cat)):
                       ttseg=self.getsegmentint(ttseg)
                  if (abs(ttseg)>0):
                     self.segments[cat]|=ttseg
               if (self.inj):
                  ttseg=self.getsegments_fromcat(ff,"inj")
                  for inj,tseg in zip(getattr(self,"inj_name"),ttseg):
                     if (abs(tseg)>0):
                       self.segments[inj]|=tseg
             for cat in self.DQ:
                self.segments[cat].coalesce()
             if (self.inj):
               for inj in getattr(self,"inj_name"):
                  self.segments[inj].coalesce()
          return frames_seg
    def getsegmentint(self,tseg):
        new_tseg=seg.segmentlist([])
        for sseg in tseg:
          start=math.ceil(float(sseg[0]))
          stop=math.floor(float(sseg[1]))
          if (stop>start):
            new_tseg.append(seg.segment(start,stop))
        return new_tseg.coalesce()
    def getsegments_fromcat(self,ff,cattype, channel="", rate=0):
        if (cattype=="inj"):
          channels=getattr(self,"inj_name")
          rates=[self.inj_rate]*len(channels)
        else:
          channels=getattr(self,"%s_channel"%cattype)
          rates=getattr(self,"%s_rate"%cattype)
        if hasattr(self,"%s_bitmask"%cattype):
             bitmasks=getattr(self,"%s_bitmask"%cattype)
        else:
             bitmasks=[0]*len(rates)
        segs=[]
        for index in range(len(channels)):
           c=channels[index]
           r=rates[index]
           b=bitmasks[index]
           if (cattype=="inj"):
             bit=self.inj_bits
           else:
             bit=self.getbitmask_fromfile(ff.pfn,c)
             if (self.inj):
               if (self.inj_channel==c):
                self.inj_bits=bit
                self.inj_rate=r
           dt=1./r
           if (b==0):
             found=[x==1 for x in bit]
           else:
              states=[x & abs(b) for x in bit]
              print(c,bit,states)
              if (b<=0):
                found=[x!=abs(b) for x in states]
              else:
                found=[x==b for x in states]
           tsegs=seg.segmentlist([])
           for i in range(0,len(found)):
              if (found[i]==True):
                start_seg=LIGOTimeGPS(ff.start)+dt*i
                stop_seg=LIGOTimeGPS(start_seg)+dt
                tsegs.append(seg.segment(start_seg,stop_seg))
           tsegs.coalesce()
           segs.append(tsegs)
        if (cattype=="inj"):
          return segs
        else:
          return reduce(lambda x,y,: (x & y).coalesce(), segs)
    def getbitmask_fromfile(self,ifile,channel_name):
      bitmask=[]
      
      try:
        if (os.path.exists(ifile)):
          a=framel.frgetvect(ifile,channel_name)
          bitmask=list(map(lambda x: int(x), a[0]))
      except Exception as e:
        if (os.path.exists(ifile)):
          print("READFRAMEERROR",e)
          com="/usr/bin/env FrameDataDump -I%s -C%s -d -a"%(ifile,channel_name)
          print(com)
          bitmask=list(map(lambda x: int(x), getoutputcommand(com)))
      return bitmask
    def readfile(self,ofile,float_num=False):
     if (os.path.exists(ofile)):
       f=open(ofile)
       if (float_num):
         file_seg=seg_ut.fromsegwizard(f,LIGOTimeGPS)
       else:
         file_seg=seg_ut.fromsegwizard(f)
       f.close()
       return file_seg
     else:
       return seg.segmentlist([])
    def setcats(self,cWB_conf):
       for cat in self.DQ:
         if (getattr(self,"%s_int"%cat)):
            float_num=False
         else:
            float_num=True
         if (os.path.exists(self.getcatname(cat,cWB_conf))):
           self.segments[cat]=self.readfile(self.getcatname(cat,cWB_conf),float_num)
    def getcatname(self,cat,cWB_conf):
       return "%s/%s/%s_%s.txt"%(cWB_conf.run_dir,cWB_conf.seg_dir,self.name,cat)
    def getcat(self,cat):
       if (cat in self.DQ):
          return self.segments[cat]
       elif (self.inj and cat in getattr(self,"inj_name")):
          return self.segments[cat]
       else:
          return seg.segmentlist([])
    def savefile(self,tseg,ofile,float_num):
        f=open("%s.tmp"%ofile,"w")
        if (float_num):
           seg_ut.tosegwizard(f,tseg,True,LIGOTimeGPS)
        else:
           seg_ut.tosegwizard(f,tseg,True)
        f.close()
        subprocess.getstatusoutput("mv %s.tmp %s"%(ofile,ofile))
    def updatefile(self,tseg,ofile, float_num=True, get=False):
      old_seg=seg.segmentlist([])
      if (abs(tseg)>0):
        if (not os.path.exists(ofile)):
          self.savefile(tseg,ofile,float_num)
          old_seg=tseg
        else:
          subprocess.getstatusoutput("cp %s %s.tmp"%(ofile,ofile))
          old_seg=self.readfile(ofile,float_num)
          old_seg|=tseg
          subprocess.getstatusoutput("rm %s.tmp"%ofile)
          self.savefile(old_seg,ofile,float_num)
      if (get):
        last_seg=seg.segmentlist([])
        if (len(old_seg)>0):
          last_seg.append(old_seg[-1])
        return last_seg
      else:
        return seg.segmentlist([])
    def update_segments(self,cWB_conf,force=True):
       for cat in self.DQ:
           fn_ifo_global=self.getcatname(cat,cWB_conf)
           ifo_segment=self.getcat(cat)
           if (force==True or len(ifo_segment)>1):
             self.segments[cat]=self.updatefile(ifo_segment,fn_ifo_global,False,True)
       if (self.inj):
         for inj in getattr(self,"inj_name"):
           fn_ifo_global=self.getcatname(inj,cWB_conf)
           ifo_segment=self.getcat(inj)
           if (force==True or len(ifo_segment)>1):
             self.segments[inj]=self.updatefile(ifo_segment,fn_ifo_global,False,False)
