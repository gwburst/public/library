project(
    GWAT
    LANGUAGES CXX
)

#Include the master CMakeLists.txt
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(GWAT_SRC gnetcluster.cc gnetwork.cc  gskymap.cc  gwavearray.cc  gwseries.cc)

#Get all the header files
string(REGEX REPLACE "[.]cc" ".hh" GWAT_HDRS "${GWAT_SRC}")

list(APPEND GWAT_HDRS gwat.hh)

#Make a local copy of the CWB_OPTS
set(GWAT_OPTS ${CWB_OPTS})

#Make a local copy of the CWB_INCLUDE_DIRS
set(GWAT_INCLUDE_DIRS ${CWB_INCLUDE_DIRS})

#Append the stft directory
list(APPEND GWAT_INCLUDE_DIRS ${CMAKE_CURRENT_LIST_DIR}/../stft)

#Create the library
add_library(${PROJECT_NAME} SHARED ${GWAT_SRC})

#Set the target properties
set_target_properties(${PROJECT_NAME} PROPERTIES PREFIX "")
set_target_properties(${PROJECT_NAME} PROPERTIES OUTPUT_NAME "gwat")
set_target_properties(${PROJECT_NAME} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib)

target_link_libraries(${PROJECT_NAME} PUBLIC WAT STFT ROOT::Core ROOT::GenVector)

#Generate the root dictionary
ROOT_GENERATE_DICTIONARY(gwat_dict ${GWAT_HDRS}
    LINKDEF gwat_LinkDef.h
    MODULE ${PROJECT_NAME})

#Add all the include directories to the target
target_include_directories(${PROJECT_NAME} PRIVATE ${GWAT_INCLUDE_DIRS})

install(TARGETS ${PROJECT_NAME})
install(FILES ${GWAT_HDRS}
        TYPE INCLUDE)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_SHARED_LIBRARY_PREFIX}${PROJECT_NAME}_rdict.pcm
        TYPE LIB)
install(
    DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/data
    DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}/cwb/
)

set(CWB_GWAT "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_SYSCONFDIR}/cwb/" CACHE INTERNAL "CWB_GWAT")
