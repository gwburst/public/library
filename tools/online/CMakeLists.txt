project(
    CWB_ONLINE
    LANGUAGES NONE
    DESCRIPTION "cWB pipeline source library"
)

set(Python3_FIND_STRATEGY "LOCATION")
find_package(Python3 REQUIRED COMPONENTS Interpreter Development)
set(Python3_VERSION_XY "${Python3_VERSION_MAJOR}.${Python3_VERSION_MINOR}")

#Check if site-packages location is writeable
execute_process(
        COMMAND "test -w ${Python3_SITEARCH}"
        RESULT_VARIABLE writable
)

if(${writable} EQUAL 0)
    set(TARGET_SP_DIR ${Python3_SITEARCH})
    set(TARGET_INSTALL_DIR ${Python3_SITEARCH}/cwb_online)
else()

    #If not writebale then create a python lib directory from the CMAKE_INSTALL_PREFIX
    execute_process(
        COMMAND "${Python3_EXECUTABLE}" "-c"
        "from distutils import sysconfig;print(sysconfig.get_python_lib(plat_specific=True, prefix=''))"
        RESULT_VARIABLE _Python3_RESULT
        OUTPUT_VARIABLE Python3_INSTALL_DIR
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    # install python library
    set(TARGET_SP_DIR "${CMAKE_INSTALL_PREFIX}/${Python3_INSTALL_DIR}")
    set(TARGET_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}/${Python3_INSTALL_DIR}/cwb_online")
endif()

#Install the files in the desired locations
install(
   DIRECTORY  ${CMAKE_CURRENT_SOURCE_DIR}/cwb_online/
   DESTINATION ${TARGET_INSTALL_DIR}
   FILES_MATCHING PATTERN "*.py"
)

#Configure the cwb_online_exec file with CMake variables
configure_file(
     bin/cwb_online_exec.in
     cwb_online_exec
     @ONLY
)

#install the executable cwb_online_exec
install(
   FILES ${CMAKE_CURRENT_BINARY_DIR}/cwb_online_exec
   TYPE BIN
   PERMISSIONS OWNER_WRITE
               OWNER_READ GROUP_READ WORLD_READ
               OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE
)

#Install the files in the bin directory
file(GLOB files "bin/*.sh" "bin/*.csh" "bin/*.py")
install_files(${files})

# create a egg-info file for pip
set(EGG_INFO_DIR "cwb_online-${cwb_online_VERSION}-py${Python3_VERSION_XY}.egg-info")
file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/PKG-INFO" "\
Metadata-Version: 2.1
Name: ${PROJECT_NAME}
Version: ${PROJECT_VERSION}
Summary: ${PROJECT_DESCRIPTION}
Author: Marco Drago
Author-email: marco.drago@uniroma1.it
Home-Page: ${PROJECT_HOMEPAGE_URL}
License: LGPL-2.1-or-later
Provides: ${PROJECT_NAME}
")
file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/requires.txt" "\
numpy
")
install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/PKG-INFO ${CMAKE_CURRENT_BINARY_DIR}/requires.txt
    DESTINATION ${TARGET_SP_DIR}/${EGG_INFO_DIR}/
)
