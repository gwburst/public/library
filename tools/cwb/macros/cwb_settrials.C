/*
# Copyright (C) 2021 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


// apply trials factor to the ifar leaf on the selected entries in the merged wave root file 

{
  int estat;
  Long_t id,size,flags,mt;
  char cmd[1024];

  TString mdir = merge_dir;

  // get merge label
  TString cwb_merge_label;
  if(gSystem->Getenv("CWB_MERGE_LABEL")==NULL) {
    cout << "cwb_settrials Error : environment CWB_MERGE_LABEL is not defined!!!" << endl;exit(1);
  } else {
    cwb_merge_label=TString(gSystem->Getenv("CWB_MERGE_LABEL"));
  }

  // get trials value
  int cwb_settrials_factor=-1;
  if(gSystem->Getenv("CWB_SETTRIALS_FACTOR")==NULL) {
    cout << endl << "cwb_settrials Error : environment CWB_SETTRIALS_FACTOR is not defined!!!" << endl << endl;
    gSystem->Exit(1);
  } else {
    if(TString(gSystem->Getenv("CWB_SETTRIALS_FACTOR")).IsDigit()) {
      cwb_settrials_factor=TString(gSystem->Getenv("CWB_SETTRIALS_FACTOR")).Atoi();
    } else {
      cout << endl << "cwb_settrials Error : environment CWB_SETTRIALS_FACTOR is not a positive integer number!!!" << endl << endl;
      gSystem->Exit(1);
    }
  }
  if(cwb_settrials_factor<=1) {
    cout << endl << "cwb_settrials Error : environment CWB_SETTRIALS_FACTOR is not an integer number >1 !!!" << endl << endl;
    gSystem->Exit(1);
  }

  // create input wave root wave file name
  char iwfname[1024];  
  sprintf(iwfname,"wave_%s.%s.root",data_label,cwb_merge_label.Data());

  // create output wave root cuts file name
  TString owfname = mdir+"/"+iwfname;
  char strials[32];sprintf(strials,"trials%d",cwb_settrials_factor);
  owfname.ReplaceAll(".root",TString(".T_")+strials+".root");

  // apply setTrials to the wave file
  CWB::Toolbox::setTrials(iwfname,mdir,mdir,"waveburst",cwb_settrials_factor);

  // create a merge*.lst file name & run selection cuts
  char ilstfname[1024];  
  sprintf(ilstfname,"merge_%s.%s.lst",data_label,cwb_merge_label.Data());
  TString olstfname = owfname;
  olstfname.ReplaceAll("wave_","merge_");
  olstfname.ReplaceAll(".root",".lst");
  olstfname.Remove(0,olstfname.Last('/')+1);	// strip path
  cout << olstfname << endl;
  estat = gSystem->GetPathInfo(mdir+"/"+ilstfname,&id,&size,&flags,&mt);
  if (estat==0) {
    sprintf(cmd,"cd %s;ln -sf %s %s",mdir.Data(),ilstfname,olstfname.Data());
    cout << cmd << endl;
    gSystem->Exec(cmd);
  }

  // for simulation!=0 create a symbolic link to mdc*.root file name
  if(simulation) {
    char ilfname[1024];
    sprintf(ilfname,"mdc_%s.%s.root",data_label,cwb_merge_label.Data());
    TString olfname = owfname;
    olfname.ReplaceAll("wave_","mdc_");
    olfname.Remove(0,olfname.Last('/')+1);      // strip path
    cout << olfname << endl;
    estat = gSystem->GetPathInfo(mdir+"/"+ilfname,&id,&size,&flags,&mt);
    if(estat==0) {
      sprintf(cmd,"cd %s;ln -sf %s %s",mdir.Data(),ilfname,olfname.Data());
      cout << cmd << endl;
      gSystem->Exec(cmd);
    }
  }

  // for simulation=0 create a symbolic link to live*.root file name
  if(!simulation) {
    char ilfname[1024];
    sprintf(ilfname,"live_%s.%s.root",data_label,cwb_merge_label.Data());
    TString olfname = owfname;
    olfname.ReplaceAll("wave_","live_");
    olfname.Remove(0,olfname.Last('/')+1);      // strip path
    cout << olfname << endl;
    estat = gSystem->GetPathInfo(mdir+"/"+ilfname,&id,&size,&flags,&mt);
    if(estat==0) {
      sprintf(cmd,"cd %s;ln -sf %s %s",mdir.Data(),ilfname,olfname.Data());
      cout << cmd << endl;
      gSystem->Exec(cmd);
    }
  }

  exit(0);
}
