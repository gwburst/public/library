# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Gabriele Vedovato, Mishra Tanmaya, Sergey Klimenko 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import io
import os
import sys
import ctypes
import ROOT
import numpy as np
import pandas as pd
#from ROOT import *
from ROOT import gSystem
from ctypes import *
import string
import time
import matplotlib.pyplot as plt
import cwb_xgboost as cwb

import matplotlib
matplotlib.use('Agg')
from matplotlib import rcParams
from matplotlib.colors import LogNorm

#plotting options
rcParams["text.usetex"] = True
rcParams["font.serif"] = "Times New Roman"
rcParams["font.family"] = "Serif"
rcParams["xtick.labelsize"]=28
rcParams["ytick.labelsize"]=28
rcParams["legend.fontsize"]=22
rcParams["axes.labelsize"]=28

# load wavelet library
if(os.getenv('_USE_CMAKE')==None):
  gSystem.Load("wavelet.so")
else:
  gSystem.Load("%s/lib64/cwb.so"%os.environ["HOME_WAT_INSTALL"])

def getopts(arguments):

   config=""		# python config file
   options=""		# config options used in python config file
   subtype=""		# plot type
   verbose=False
   how2use="\n how to use -> python3 cwb_xgboost_report_roc.py subtype=roc/hrho/hchirp/efreq/lrho/qaqp config=config.py\n"

   for s in arguments:
    if (s.find("config=")!=-1):
      config=s.replace("config=","")
    if (s.find("options=")!=-1):
      options=s.replace("options=","")
    if (s.find("subtype=")!=-1):
      subtype=s.replace("subtype=","")
    elif (s.find("verbose")!=-1):
      verbose=True
   if (len(config)==0):
     print("Error, no input config specified")
     print(how2use)
     exit()

   # check plot type option
   if (subtype.find("hrho")==-1) and (subtype.find("hchirp")==-1) and (subtype.find("roc")==-1) and (subtype.find("efreq")==-1) and (subtype.find("lrho")==-1) and (subtype.find("qaqp")==-1):
     print("Error, not available sybtype option")
     print(how2use)
     exit()

   print("\ninput cwb_xgboost parameters\n")
   print(" input plot type file                   = ", subtype)
   print(" input config.py file                   = ", config)
   print(" input config options                   = ", options)
   print(" verbose                                = ", verbose)
   print("\n")

   return subtype,config,options,verbose

def getEntries(xfile,dtype,data_cuts=None):
   """
   function purpose:
      read mdc/wave injections root file

   input params:
      xfile:      input mdc/wave root file path
      dtype:      mdc/waveburst
      data_cuts:  select injections, same syntax used to cut root data tree. Eg: 'factor>=9'

   return:
      return of selected number of injections
   """

   print("Start reading the input mdc/wave ROOT file ... \n\n ", xfile, "\n")
   if (xfile.find(".py")==-1):
     df = ROOT.RDataFrame(dtype, xfile)   # read wave root file
   else:
     # load xfile list from python xfile
     execfile(xfile,globals())
     print('\nInput file list\n')
     for ifname in flist:
       print(ifname)
       if not os.path.isfile(ifname):
         print ("\nError: file ", ifname, " doesn't exist")
         exit(1)
     print('\n')
     df = ROOT.RDataFrame(dtype, flist)   # read list mdc/wave root file

   if(data_cuts!=None): df = df.Filter(data_cuts)      # cut data

   return df.Count().GetValue()

def eff_vs_far(sdf, far_label, tot_num_events, far_loglow = -3.0, far_loghigh = 1.0, num_steps=5000, trials_factor=1):
   """
   function purpose:

   to calculate the detection efficiency at different FARs for a given model.

   function options:
   
   :sdf: input pandas.DataFrame which contains two columns: test statistic and rate (output from Rate_vs_Rho function)
   :far_label: label of the FAR column in 'sdf' used to calculate detection efficiency

   plotting options:

   :far_loglow: 
   :far_loghigh: 
   :num_steps:

   :returns: a pandas.Series object with a single column of 'far' values

   """

   far_thresh = np.logspace(far_loglow, far_loghigh, num_steps)
   frac = np.zeros(num_steps)
   efrac = np.zeros(num_steps)
   sim_far_df = sdf[far_label]
   for idx, far in enumerate(far_thresh):
       num_events = sim_far_df[sim_far_df <= far/trials_factor].shape[0]
       frac[idx] = float(num_events) / float(tot_num_events)
       efrac[idx] = np.sqrt(float(num_events)) / float(tot_num_events)
   frac_df = pd.DataFrame({'far': far_thresh, 'frac': frac, 'efrac': efrac})
   return frac_df


if __name__ == "__main__":

   ROOT.ROOT.EnableImplicitMT()		#### BEWARE -> reshuffle entries -> the results are not reproducible !!!

   # get input user parameters
   subtype,config,options,verbose=getopts(sys.argv[1:])

   # load default xgb_config function
   cwb.execfile(os.environ['CWB_SCRIPTS']+'/cwb_xgboost_config.py',globals(),locals())

   # get ML_options
   search='bbh'	# is a dummy value not used to get ML_options
   nifo=2	# is a dummy value not used to get ML_options
   dummy1,dummy2,dummy3,dummy4,ML_options=xgb_config(search,nifo)      # get ML_caps params
   if (len(config)!=0):
     # load user config 
     PLOT  = {}
     ROC   = {}
     HCHIRP= {}
     HRHO  = {}
     LRHO  = {}
     EFREQ = {}
     QAQP  = {}
     # load user xgb_config function, overwrite the default configuration
     cwb.execfile(config,globals(),locals())

   # add parameters used to select sim events
   if('factor' not in ML_options['readfile(vars)']): ML_options['readfile(vars)'].append('factor')
   if('type'   not in ML_options['readfile(vars)']): ML_options['readfile(vars)'].append('type')

   # print configuration
   cwb.print_config("","","","",ML_options)
   print('\nPLOT\n\n',PLOT)
   print('\nROC\n\n',ROC)
   print('\nHRHO\n\n',HRHO)
   print('\nHCHIRP\n\n',HCHIRP)
   print('\nLRHO\n\n',LRHO)
   print('\nEFREQ\n\n',EFREQ)
   print('\nQAQP\n\n',QAQP)

   if (subtype.find("roc")!=-1):

      print("\n -> Plotting ROC ...\n")

      # get user parameters
      # eg: PLOT['roc']  = ['xgb/v3/roc.png','','',0.001,10]
      # or: PLOT['roc']  = 'plot_fname=xgb/v8/wnb_roc.png far_inf=0.001 far_sup=1' 
      #
      # far_inf         far inferior limit
      # far_sup         far superior limit

      try:
        PLOT['roc']
      except:
        print("\nError: PLOT['roc'] must be provided in the user report config file\n");exit(1)

      if(type(PLOT['roc'])==list):	# old format
        try: 
          xinf = PLOT['roc'][3] 
        except:
          xinf = 0.001
        try: 
          xsup = PLOT['roc'][4] 
        except:
          xsup = 10

        ofname = PLOT['roc'][0]
      elif(type(PLOT['roc'])==str):	# new format
        xinf   = float(cwb.getopt(PLOT['roc'],'far_inf',0.001))
        xsup   = float(cwb.getopt(PLOT['roc'],'far_sup',10))
        yinf   = float(cwb.getopt(PLOT['roc'],'deteff_inf',0.0))
        ysup   = float(cwb.getopt(PLOT['roc'],'deteff_sup',1.0))
        ptitle = cwb.getopt(PLOT['roc'],'plot_title','ROC')
        ofname = cwb.getopt(PLOT['roc'],'plot_fname','')
      else: print("\nError: wrong PLOT['roc'] format\n");exit(1)

      if (ofname.find(".png")==-1):
        print("\nError: the output plot name must have the '.png' extension\n");exit(1)

      vpd = {}
      nwave = {}
      nmdc = {}
      nwave_max = 0
      if(len(ROC)==0): print("\nError: wrong ROC length, must be > 0\n");exit(1)
      for roc_name, params in ROC.items():
         # get user parameters
         # eg: ROC['WNB:rhor'] = [sim_rhor_fname,'red','type[1]>=18&&type[1]<=20&&factor<=9']
         # or: ROC['WNB:rhor'] = 'wave_fname='+sim_rhor_fname+' line_color=red wave_cuts=type[1]>=18&&type[1]<=20&&factor<=9'
         mdc_fname='';mdc_cuts=None
         if(type(ROC[roc_name])==list):		# old format
           wave_fname = params[0]
           try:
             wave_cuts = params[2]
           except:
             wave_cuts = None
         elif(type(ROC[roc_name])==str):	# new format
           wave_fname = cwb.getopt(ROC[roc_name],'wave_fname','')
           wave_cuts  = cwb.getopt(ROC[roc_name],'wave_cuts',None)
           mdc_fname  = cwb.getopt(ROC[roc_name],'mdc_fname',None)
           mdc_cuts   = cwb.getopt(ROC[roc_name],'mdc_cuts',None)
         else: print("\nError: wrong ROC['"+roc_name+"'] format\n");exit(1)
         if not os.path.exists(wave_fname): print("\nerror, file ",wave_fname," not exist\n"); exit(1)

         # read input root sim wave file
         spd=cwb.readfile(wave_fname,1,1,{},ML_options,verbose,False,True,wave_cuts)
         nwave[roc_name] = spd.shape[0]
         vpd[roc_name]=spd
         if(nwave[roc_name]>nwave_max): nwave_max=nwave[roc_name]
         print("\ntotal num of detected events = ",nwave[roc_name],"\n")

         # if mdc_fname!='' then read number of injections from input root sim mdc file
         nmdc[roc_name] = -1
         if(mdc_fname!=''):
            if not os.path.exists(mdc_fname): print("\nerror, file ",mdc_fname," not exist\n"); exit(1)
            nmdc[roc_name] = getEntries(mdc_fname,"mdc",mdc_cuts)
            print("\ntotal num of injected events = ",nmdc[roc_name],"\n")

      # plot ROC
      plt.figure(figsize=(10,7))
      for roc_name, params in ROC.items():
         # get user parameters
         trials_factor = 1
         if(type(ROC[roc_name])==list):		# old format
           color     = params[1]
         elif(type(ROC[roc_name])==str):	# new format
           color         = cwb.getopt(ROC[roc_name],'line_color',None)
           dashes        = cwb.getopt(ROC[roc_name],'line_dashes','1,0')
           dashes        = np.array( dashes.split(','), dtype=np.uint8 )
           mdc_fname     = cwb.getopt(ROC[roc_name],'mdc_fname',None)
           trials_factor = int(cwb.getopt(ROC[roc_name],'trials_factor',1))
           if(trials_factor<=0):
             print("\nError: wrong trials_factor option: must be > 0\n");exit(1)
         else: print("\nError: wrong ROC['"+roc_name+"'] format\n");exit(1)
         normalization=nwave_max
         if(mdc_fname!=''): normalization=nmdc[roc_name];
         eff = eff_vs_far(vpd[roc_name], far_label='far', far_loglow = np.log10(xinf), far_loghigh = np.log10(xsup), tot_num_events=normalization, trials_factor=trials_factor)
         plt.plot(eff['far'], eff['frac'], c=color, label=roc_name, dashes=[dashes[0],dashes[1]])

      plt.title(r'$\textrm{'+ptitle+'}$', fontsize=30, pad=20)
      plt.legend(loc='lower right', fontsize=15)
      plt.ylabel('Detection efficiency')
      plt.xlabel('False alarm rate (yr$^{-1}$)')
      plt.axis([xinf,xsup,yinf,ysup])
      plt.xscale('log')
      plt.tight_layout()
      plt.grid()
      os.makedirs(os.path.dirname(ofname), exist_ok=True)
      print('dump file -> ', ofname)
      plt.savefig(ofname)
      plt.close()

      # if only one ROC is defined then the ROC values are also dumped to txt file
      if(len(ROC)==1):
        ofname=ofname.replace(".png",".txt")
        print('dump file -> ', ofname)
        with open(ofname, 'w') as f:
          for index, row in eff.iterrows():
            #print(row['far'], row['frac'])
            f.write(str(round(row['far'],8))+'\t'+str(round(row['frac'],8))+'\t'+str(round(row['efrac'],8))+'\n')

   if (subtype.find("hrho")!=-1):

      print("\n -> Plotting HRHO ...\n")

      # get user parameters
      # eg: PLOT['hrho' = ['xgb/v3/hrhor.png','','$rho_r$']
      # or: PLOT['hrho'] = 'plot_fname=xgb/v3/hrho.png rho_label=$rho_r$' 
      # 
      # rho_label         rho label (default = 'rho0') used in the legend

      try:
        PLOT['hrho']
      except:
        print("\nError: PLOT['hrho'] must be provided in the user report config file\n");exit(1)

      if(type(PLOT['hrho'])==list):	# old format
        ofname    = PLOT['hrho'][0]
        rho_label = PLOT['hrho'][2]
      elif(type(PLOT['hrho'])==str):	# new format
        ofname    = cwb.getopt(PLOT['hrho'],'plot_fname','')
        rho_label = cwb.getopt(PLOT['hrho'],'rho_label','')
      else: print("\nError: wrong PLOT['hrho'] format\n");exit(1)

      binhist = np.linspace(0,40.0,180)
      fig = plt.figure(figsize=(10,7))
      ax = fig.add_subplot(111)

      if(len(HRHO)!=2): print("\nError: wrong HRHO length, must be 2\n");exit(1)
      for hrho_name, params in HRHO.items():
         # get user parameters
         if(type(HRHO[hrho_name])==list):	# old format
           ifname = params[0]
           rhoid  = params[1]
           color  = params[2]
         elif(type(HRHO[hrho_name])==str):	# new format
           ifname = cwb.getopt(HRHO[hrho_name],'ifname','')
           rhoid  = int(cwb.getopt(HRHO[hrho_name],'rhoid',0))
           color  = cwb.getopt(HRHO[hrho_name],'color','black')
         else: print("\nError: wrong HRHO['"+hrho_name+"'] format\n");exit(1)

         # read input root sim wave file
         if not os.path.exists(ifname): print("\nerror, file ",ifname," not exist\n"); exit(1)
         if(hrho_name=='bkg'):
            exclude_zl=True
         else:
            exclude_zl=False
         pd=cwb.readfile(ifname,1,1,{},ML_options,verbose,exclude_zl,False,None)
         ax.hist(pd['rho'+str(rhoid)],bins=binhist,alpha=0.5,label=hrho_name,log=True,color=color)
         ax.set_xlabel(rho_label)

      ax.set_ylabel('Number of events')
      ax.legend(loc='upper right')
      plt.title(r'$\textrm{Detection Statistic Distributions}$', fontsize=30, pad=20)
      plt.tight_layout()
      print('dump file -> ', ofname)
      os.makedirs(os.path.dirname(ofname), exist_ok=True)
      plt.savefig(ofname)
      plt.close()

   if (subtype.find("hchirp")!=-1):

      print("\n -> Plotting HCHIRP ...\n")

      # get user parameters
      # eg: PLOT['hchirp'  = ['xgb/v3/hchirp.png','','$rho_r$']
      # or: PLOT['hchirp'] = 'plot_fname=xgb/v3/hchirp.png rho_label=$rho_r$' 
      # 
      # rho_label         rho label (default = 'rho0') used in the legend

      try:
        PLOT['hchirp']
      except:
        print("\nError: PLOT['hchirp'] must be provided in the user report config file\n");exit(1)

      if(type(PLOT['hchirp'])==list):	# old format
        ofname    = PLOT['hchirp'][0]
        chirp_label = PLOT['hchirp'][2]
      elif(type(PLOT['hchirp'])==str):	# new format
        ofname    = cwb.getopt(PLOT['hchirp'],'plot_fname','')
        chirp_label = cwb.getopt(PLOT['hchirp'],'chirp_label','chirp')
      else: print("\nError: wrong PLOT['hchirp'] format\n");exit(1)

      binhist = np.linspace(0,100.0,200)
      fig = plt.figure(figsize=(10,7))
      ax = fig.add_subplot(111)

      if(len(HCHIRP)!=2): print("\nError: wrong HCHIRP length, must be 2\n");exit(1)
      for hchirp_name, params in HCHIRP.items():
         # get user parameters
         if(type(HCHIRP[hchirp_name])==list):	# old format
           ifname = params[0]
           color  = params[1]
         elif(type(HCHIRP[hchirp_name])==str):	# new format
           ifname = cwb.getopt(HCHIRP[hchirp_name],'ifname','')
           color  = cwb.getopt(HCHIRP[hchirp_name],'color','black')
         else: print("\nError: wrong HCHIRP['"+hchirp_name+"'] format\n");exit(1)

         # read input root sim wave file
         if not os.path.exists(ifname): print("\nerror, file ",ifname," not exist\n"); exit(1)
         if(hchirp_name=='bkg'):
            exclude_zl=True
         else:
            exclude_zl=False
         # add chirp parameters used in readfile
         if('chirp' not in ML_options['readfile(vars)']): ML_options['readfile(vars)'].append('chirp')
         pd=cwb.readfile(ifname,1,1,{},ML_options,verbose,exclude_zl,False,None)
         ax.hist(pd['chirp1'],bins=binhist,alpha=0.5,label=hchirp_name,log=True,color=color)
         ax.set_xlabel(chirp_label)

      ax.set_ylabel('Number of events')
      ax.legend(loc='upper right')
      plt.title(r'$\textrm{Chirp Distributions}$', fontsize=30, pad=20)
      plt.tight_layout()
      print('dump file -> ', ofname)
      os.makedirs(os.path.dirname(ofname), exist_ok=True)
      plt.savefig(ofname)
      plt.close()

   if (subtype.find("efreq")!=-1):

      print("\n -> Plotting EFREQ ...\n")

      # get user parameters
      # eg: PLOT['efreq'] = ['xgb/v3/efreq.png','','',20,1000,44,100]
      # or: PLOT['efreq'] = 'plot_fname=xgb/v3/efreq.png fmin=20 fmax=1000 fbin=44 ifar=100' 
      # 
      # fmin		xaxis minimum frequency
      # fmax		xaxis maximum frequency
      # fbin		xaxis number of bins
      # ifar		ifar threshold

      try:
        PLOT['efreq']
      except:
        print("\nError: PLOT['efreq'] must be provided in the user report config file\n");exit(1)

      if(type(PLOT['efreq'])==list):	# old format
        ofname = PLOT['efreq'][0]
        fmin   = PLOT['efreq'][3]
        fmax   = PLOT['efreq'][4]
        fbin   = PLOT['efreq'][5]
        ifar   = PLOT['efreq'][6]
      elif(type(PLOT['efreq'])==str):	# new format
        ofname = cwb.getopt(PLOT['efreq'],'plot_fname','')
        fmin   = float(cwb.getopt(PLOT['efreq'],'fmin',20.0))
        fmax   = float(cwb.getopt(PLOT['efreq'],'fmax',1000.0))
        fbin   = int(cwb.getopt(PLOT['efreq'],'fbin',100.0))
        ifar   = float(cwb.getopt(PLOT['efreq'],'ifar',100.0))
      else: print("\nError: wrong PLOT['efreq'] format\n");exit(1)

      # eff vs freq at IFAR = ifar yr
      fig=plt.figure(figsize=(10,7))
      ax = fig.add_subplot(111)

      vpd = {}
      vhist_sim = {}
      max_nevents = 0
      bins = np.linspace(fmin,fmax,fbin)
      if(len(EFREQ)==0): print("\nError: wrong EFREQ length, must be > 0\n");exit(1)
      for efreq_name, params in EFREQ.items():
         # get user parameters
         if(type(EFREQ[efreq_name])==list):	# old format
           ifname = params[0]
         elif(type(EFREQ[efreq_name])==str):	# new format
           ifname = cwb.getopt(EFREQ[efreq_name],'ifname','')
         else: print("\nError: wrong EFREQ['"+efreq_name+"'] format\n");exit(1)

         # read input root sim wave file
         if not os.path.exists(ifname): print("\nerror, file ",ifname," not exist\n"); exit(1)
         # add frequency parameters used in readfile
         if('frequency' not in ML_options['readfile(vars)']): ML_options['readfile(vars)'].append('frequency')
         spd = cwb.readfile(ifname,1,1,{},ML_options,verbose,False,True,None)

         spd = spd[spd['far']**(-1)>0]	# remove vetoed events
         index_rho = np.where((spd['far']**(-1))>ifar)[0]
         hist_sim_rho,bins_sim_rho = np.histogram((spd["frequency0"]).take(index_rho),bins=bins)
         hist_all,bins_all = np.histogram((spd["frequency0"]),bins=bins)
         vhist_sim[efreq_name] = hist_sim_rho

         nevents = spd.shape[0]
         if(nevents>max_nevents):
            max_nevents=nevents
            hist_max = hist_all
            bins_max = bins_all

      for efreq_name, params in EFREQ.items():
         # get user parameters
         if(type(EFREQ[efreq_name])==list):	# old format
           color  = params[1]
         elif(type(EFREQ[efreq_name])==str):	# new format
           color  = cwb.getopt(EFREQ[efreq_name],'color','black')
         else: print("\nError: wrong EFREQ['"+efreq_name+"'] format\n");exit(1)

         df = bins_max[1]-bins_max[0] 
         ax = plt.plot(bins_max[:-1]+df,vhist_sim[efreq_name]/hist_max,label=r"$"+efreq_name+"$ + IFAR $>$ "+str(ifar),ls="--",marker='o',color=color,alpha = 0.5)

      plt.legend(fontsize=20)
      plt.xlabel("freq[0]",fontsize=20)
      plt.ylabel("Detection efficiency at IFAR = "+str(ifar)+"yr",fontsize=20)
      plt.axvline(x=100,color='k',ls=":",label="f[0]=100Hz")
      plt.grid()
      plt.title(r'$\textrm{Central Frequency Distributions}$', fontsize=30, pad=20)
      plt.tight_layout()
      print('dump file -> ', ofname)
      os.makedirs(os.path.dirname(ofname), exist_ok=True)
      plt.savefig(ofname)
      plt.close()

   if (subtype.find("lrho")!=-1):

      print("\n -> Plotting LRHO ...\n")

      # get user parameters
      # eg: PLOT['lrho'] = ['xgb/v3/lrhor.png','','$rho_r$']
      # or: PLOT['lrho'] = 'plot_fname=xgb/v3/lrho.png rho_label=$rho_r$' 
      # 
      # rho_label         rho label (default = 'rho0') used in the legend

      try:
        PLOT['lrho']
      except:
        print("\nError: PLOT['lrho'] must be provided in the user report config file\n");exit(1)

      if(type(PLOT['lrho'])==list):	# old format
        ofname    = PLOT['lrho'][0]
        rho_label = PLOT['lrho'][2]
      elif(type(PLOT['lrho'])==str):	# new format
        ofname    = cwb.getopt(PLOT['lrho'],'plot_fname','')
        rho_label = cwb.getopt(PLOT['lrho'],'rho_label','')
      else: print("\nError: wrong PLOT['lrho'] format\n");exit(1)

      nbins = 500
      g, ax = plt.subplots(figsize=(10, 7))

      if(len(LRHO)!=2): print("\nError: wrong LRHO length, must be 2\n");exit(1)
      for lrho_name, params in LRHO.items():
         # get user parameters
         if(type(LRHO[lrho_name])==list):	# old format
           ifname = params[0]
           rhoid  = params[1]
           color  = params[2]
           try:
             rho0_capvalue = params[3]
           except:
             rho0_capvalue = 0
         elif(type(LRHO[lrho_name])==str):	# new format
           ifname = cwb.getopt(LRHO[lrho_name],'ifname','')
           rhoid  = int(cwb.getopt(LRHO[lrho_name],'rhoid',0))
           color  = cwb.getopt(LRHO[lrho_name],'color','black')
           rho0_capvalue = float(cwb.getopt(LRHO[lrho_name],'rho0_capvalue',0))
         else: print("\nError: wrong LRHO['"+lrho_name+"'] format\n");exit(1)

         # read input root sim wave file
         if not os.path.exists(ifname): print("\nerror, file ",ifname," not exist\n"); exit(1)
         if(lrho_name=='bkg'):
            exclude_zl=True
         else:
            exclude_zl=False
         # add likelihood parameters used in readfile
         if('likelihood' not in ML_options['readfile(vars)']): ML_options['readfile(vars)'].append('likelihood')
         pd=cwb.readfile(ifname,1,1,{},ML_options,verbose,exclude_zl,False,None)

         rho_name = 'rho'+str(rhoid) 
         if(lrho_name=='sim'):
            print(pd[pd[rho_name] == 0].shape[0])
            pd.loc[pd[rho_name] == 0, rho_name]=1e-2
            h = ax.hist2d(0.5*np.log10(pd['likelihood']), np.log10(pd[rho_name]), bins=nbins,cmap = 'jet', norm=LogNorm(),label=lrho_name)
            g.colorbar(h[3], ax=ax)

         if(lrho_name=='bkg'):
            rho0_capname  = cwb.getcapname('rho0',rho0_capvalue)
            pd[rho0_capname] = pd['rho0']
            pd.loc[pd[rho0_capname]>rho0_capvalue, rho0_capname] = rho0_capvalue

            pd1 = pd[pd[rho0_capname]==rho0_capvalue].copy()
            print(pd[pd[rho0_capname]==rho0_capvalue].shape[0])
            print(pd1[pd1[rho_name] == 0].shape[0])
            pd1.loc[pd1[rho_name] == 0, rho_name]=1e-2
            ax.scatter(0.5*np.log10(pd1['likelihood']), np.log10(pd1[rho_name]), marker = ".", color = 'black',label=lrho_name)

            #pd.loc[0.5*np.log10(pd['likelihood']) < 1.2, rho_name]=1e-2
            #ax.scatter(0.5*np.log10(pd['likelihood']), np.log10(pd[rho_name]), marker = ".", color = 'black',label=lrho_name)

      ax.set_xlabel('0.5*log10(likelihood)')
      ax.set_ylabel(r'log10('+rho_label+')')
      ax.legend(loc='upper left')
      plt.title(r'$\textrm{Detection Statistic vs Likelihood distributions}$', fontsize=30, pad=20)
      plt.tight_layout()
      print('dump file -> ', ofname)
      os.makedirs(os.path.dirname(ofname), exist_ok=True)
      plt.savefig(ofname)
      plt.close()

   if (subtype.find("qaqp")!=-1):

      print("\n -> Plotting QAQP ...\n")

      # get user parameters
      # eg: PLOT['qaqp'] = ['xgb/v3/qaqp.png','rho1',9,'rhor',0.15,0.6]
      # or: PLOT['qaqp'] = 'plot_fname=xgb/v3/qaqp.png rho_name=rho1 rho_thr=9 rho_label=rhor qfactor=0.15 qoffset=0.6 qa_sup=6.0 qp_sup=10.0' 
      #
      # rho_name          rho name (default = 'rho0') used to select the background entries (rho_name>rho_thr)
      # rho_thr           rho threshold (default = 8) used to select the background entries (rho_name>rho_thr)
      # rho_label         rho label (default = 'rho0') used in the legend
      # qfactor           qfactor value (default = 0.15) used to draw the dashed line Qa = qfactor/(Qp-qoffset)
      # qoffset           qoffset value (default = 0.8)  used to draw the dashed line Qa = qfactor/(Qp-qoffset)

      try:
        PLOT['qaqp']
      except:
        print("\nError: PLOT['qaqp'] must be provided in the user report config file\n");exit(1)

      qa_sup = 6.0
      qp_sup = 10.0
      if(type(PLOT['qaqp'])==list):	# old format
        ofname    = PLOT['qaqp'][0]
        rho_name  = PLOT['qaqp'][1]
        rho_thr   = PLOT['qaqp'][2]
        rho_label = PLOT['qaqp'][3]
        try: 
          qfactor = PLOT['qaqp'][4] 
        except:
          qfactor = 0.15
        try: 
          qoffset = PLOT['qaqp'][5] 
        except:
          qoffset = 0.8
      elif(type(PLOT['qaqp'])==str):	# new format
        ofname    = cwb.getopt(PLOT['qaqp'],'plot_fname','')
        rho_thr   = float(cwb.getopt(PLOT['qaqp'],'rho_thr',0.0))
        rho_name  = cwb.getopt(PLOT['qaqp'],'rho_name','')
        rho_label = cwb.getopt(PLOT['qaqp'],'rho_label','')
        qfactor   = float(cwb.getopt(PLOT['qaqp'],'qfactor',0.15))
        qoffset   = float(cwb.getopt(PLOT['qaqp'],'qoffset',0.8))
        qa_sup    = float(cwb.getopt(PLOT['qaqp'],'qa_sup',6))
        qp_sup    = float(cwb.getopt(PLOT['qaqp'],'qp_sup',10))
      else: print("\nError: wrong PLOT['qaqp'] format\n");exit(1)

      bkg_marker_color = 'red'
      bkg_marker_size = 15
      sim_bin_size = 0.1
      fcuts = None
      if(len(QAQP)!=2): print("\nError: wrong QAQP length, must be 2\n");exit(1)
      for qaqp_name, params in QAQP.items():
         if(type(QAQP[qaqp_name])==list):	# old format
           ifname = params[0]
         elif(type(QAQP[qaqp_name])==str):	# new format
           # eg: QAQP['sim'] = 'wave_fname=sim_rhor_fname wave_cuts=type[1]>=4&&type[1]<=4 bin_size=0.01'
           # eg: QAQP['bkg'] = 'wave_fname=bkg_rhor_fname marker_color=black marker_size=100'
           ifname           = cwb.getopt(QAQP[qaqp_name],'wave_fname','')
           fcuts            = cwb.getopt(QAQP[qaqp_name],'wave_cuts',None)
           if(qaqp_name=='bkg'):
             bkg_marker_color = cwb.getopt(QAQP[qaqp_name],'marker_color','red')
             bkg_marker_size  = float(cwb.getopt(QAQP[qaqp_name],'marker_size',5))
           if(qaqp_name=='sim'):
             sim_bin_size     = float(cwb.getopt(QAQP[qaqp_name],'bin_size',0.1))
         else: print("\nError: wrong QAQP['"+qaqp_name+"'] format\n");exit(1)
         # read input root sim wave file
         if not os.path.exists(ifname): print("\nerror, file ",ifname," not exist\n"); exit(1)
         if(qaqp_name=='bkg'):
           npd=cwb.readfile(ifname,1,1,{},ML_options,verbose,True,False,fcuts)
         if(qaqp_name=='sim'):
           spd=cwb.readfile(ifname,1,1,{},ML_options,verbose,False,False,fcuts)

      try:
         type(npd)
      except:
         print('QaQp plot error: bkg file not provided\n'); exit(1)

      try:
         type(spd)
      except:
         print('QaQp plot error: sim file not provided\n'); exit(1)

      # save Qa vs Qp bkg/sim plot
      print('dump file -> ', ofname)
      os.makedirs(os.path.dirname(ofname), exist_ok=True)
      cwb.plot_QaQp(ofname,npd,spd,rho_name,rho_thr,rho_label,qfactor,qoffset,bkg_marker_color,bkg_marker_size,qa_sup,qp_sup,sim_bin_size)

