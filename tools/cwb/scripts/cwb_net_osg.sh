#!/usr/bin/env bash

# Modify to make sure we source the container conda environment
# First, make sure that we have setup bash; note that in general
# singularity will *not* have run any of the initialization scripts

. /etc/bashrc

conda activate cwb_conda

export WORKING_DIR="$(ls *.tgz)"
export WORKING_DIR="$(basename $WORKING_DIR .tgz)"
mkdir $WORKING_DIR
export HOME="${PWD}/${WORKING_DIR}"
mv *.tgz $HOME/
mv .rootrc $HOME/
cd $HOME/

tar -zxf *.tgz

echo "ROOTSYS = ${ROOTSYS}"
echo "CWB_PARAMETERS_FILE = ${CWB_PARAMETERS_FILE}"
echo "CWB_UPARAMETERS_FILE = ${CWB_UPARAMETERS_FILE}"
echo "CWB_MACROS = ${CWB_MACROS}"
echo "CWB_UFILE = ${CWB_UFILE}"
echo "CWB_STAGE = ${CWB_STAGE}"

${ROOTSYS}/bin/root -b -q -l ${CWB_PARAMETERS_FILE} ${CWB_UPARAMETERS_FILE} ${CWB_MACROS}/cwb_xnet.C\(\"${CWB_UFILE}\",${CWB_STAGE},\"${CWB_UPARAMETERS_FILE}\",false,false\)

