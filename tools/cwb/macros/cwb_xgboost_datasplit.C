/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


// split wave root file for datasplit/test with xgboost

#define CCAST(PAR) const_cast<char*>(PAR)

{
  CWB::Toolbox TB;

  int estat;
  Long_t id,size,flags,mt;
  char cmd[1024];

  TString cwb_xgboost_options = TString(gSystem->Getenv("CWB_XGBOOST_OPTIONS"));

  // check if cwb_xgboost_options contains '--' than cwb_xgboost_options
  // is used to extract all xgboost parameters
  // cwb_xgboost datasplit '--mlabel M1 --ifrac 0.5 --wavecuts rho[0]<100 --verbose true --ulabel rho0_lt_100'

  TString cwb_merge_label  = "";	// merge label used to select input root files
  TString scwb_xgb_ifrac   = "";	// fraction of data to be used for training (eg: 0.2 -> 20%)
  float   cwb_xgb_ifrac    = -1;
  TString scwb_xgb_seed    = "";	// seed used to init random selection for split (def=150914)
  int     cwb_xgb_seed     = 0;
  TString cwb_xgb_cfile    = "";	// input chunk list: used to tag (eg: K02) each entry with chunk
  TString cwb_xgb_net      = "";	// add network tag to each entry (eg: HL)
  TString cwb_xgb_ulabel   = "";	// user label used to tag output files
  TString cwb_xgb_wavecuts = "";	// string used for wave selection cuts
  TString cwb_xgb_mdccuts  = "";	// string used for mdc selection cuts
  TString cwb_xgb_verbose  = "";	// true/false(def) -> enable/disable verbose output
  TString cwb_xgb_mt       = "";	// true(def)/false -> enable/disable multithreading in data selection
                                        // Warning: if true the order of output entries is not fixed and training doesn't reproduce the same XGB model

  // get the cwb_xgb_ifile
  cwb_merge_label = TB.getParameter(cwb_xgboost_options,"--mlabel");
  TString cwb_xgb_ifile = "";
  if(cwb_merge_label.BeginsWith("M")) {	// if not ends with .root then it is a merge label
    cwb_xgb_ifile = TString::Format("%s/wave_%s.%s.root",merge_dir,data_label,cwb_merge_label.Data());
  } else {
    cout << "cwb_xgboost_datasplit.C : Error - (--mlabel) wrong merge label" << endl << endl;
    gSystem->Exit(1);
  }
  // get the cwb_xgb_ifrac
  scwb_xgb_ifrac = TB.getParameter(cwb_xgboost_options,"--ifrac");
  if(scwb_xgb_ifrac!="") {
    if(scwb_xgb_ifrac.IsFloat()) cwb_xgb_ifrac = scwb_xgb_ifrac.Atof();
    else {cout << "cwb_xgboost_datasplit.C : Error - (--ifrac) is not a float" << endl << endl;gSystem->Exit(1);}
  }
  // get the cwb_xgb_seed
  scwb_xgb_seed = TB.getParameter(cwb_xgboost_options,"--seed");
  if(scwb_xgb_seed!="") {
    if(scwb_xgb_seed.IsDigit()) cwb_xgb_seed = scwb_xgb_seed.Atoi();
    else {cout << "cwb_xgboost_datasplit.C : Error - (--seed) is not an integer" << endl << endl;gSystem->Exit(1);}
  } else cwb_xgb_seed = 150914;
  // get the cwb_xgb_cfile
  cwb_xgb_cfile = TB.getParameter(cwb_xgboost_options,"--cfile");
  cwb_xgb_cfile = gSystem->ExpandPathName(cwb_xgb_cfile.Data());
  // get the cwb_xgb_net
  cwb_xgb_net = TB.getParameter(cwb_xgboost_options,"--net");
  // get the cwb_xgb_ulabel
  cwb_xgb_ulabel = TB.getParameter(cwb_xgboost_options,"--ulabel");
  // get the cwb_xgb_wavecuts
  cwb_xgb_wavecuts = TB.getParameter(cwb_xgboost_options,"--wavecuts");
  // get the cwb_xgb_mdccuts
  cwb_xgb_mdccuts = TB.getParameter(cwb_xgboost_options,"--mdccuts");
  // get the cwb_xgb_verbose
  if(TB.getParameter(cwb_xgboost_options,"--verbose")=="") cwb_xgb_verbose="false";
  else cwb_xgb_verbose=TB.getParameter(cwb_xgboost_options,"--verbose");
  // get the cwb_xgb_mt
  if(TB.getParameter(cwb_xgboost_options,"--mt")=="") cwb_xgb_mt="false";
  else cwb_xgb_mt=TB.getParameter(cwb_xgboost_options,"--mt");

  // check if ifrac is in ]0,1] range
  if(cwb_xgb_ifrac>=1) {
    cout << "cwb_xgboost_datasplit.C : Error - (--ifrac) must be < 1" << endl << endl;
    gSystem->Exit(1);
  }
  // check if seed is in > 0
  if(cwb_xgb_seed<=0) {
    cout << "cwb_xgboost_datasplit.C : Error - (--seed) must be > 0 (def=150914)" << endl << endl;
    gSystem->Exit(1);
  }
  // check if input chunk list file exist
  if(cwb_xgb_cfile!="") TB.checkFile(cwb_xgb_cfile);
  // check if input bkg root file exist
  TB.checkFile(cwb_xgb_ifile);
  // check verbose
  if((cwb_xgb_verbose!="false")&&(cwb_xgb_verbose!="true")) {
    cout << "cwb_xgboost_datasplit.C : Error - (--verbose) is not defined, available options are true/false" << endl << endl;
    gSystem->Exit(1);
  }
  // check mt
  if((cwb_xgb_mt!="false")&&(cwb_xgb_mt!="true")) {
    cout << "cwb_xgboost_datasplit.C : Error - (--mt) is not defined, available options are true/false" << endl << endl;
    gSystem->Exit(1);
  }

  // print input parameters
  printf("\ninput cwb_xgboost parameters\n\n");
  printf(" input merge_label                      = %s\n", cwb_xgb_ifile.Data());
  printf(" input fraction selection               = %g\n", cwb_xgb_ifrac);
  printf(" input seed                             = %d\n", cwb_xgb_seed);
  printf(" input chunk list file                  = %s\n", cwb_xgb_cfile.Data());
  printf(" input network                          = %s\n", cwb_xgb_net.Data());
  printf(" input user label                       = %s\n", cwb_xgb_ulabel.Data());
  printf(" input wave cuts for training           = %s\n", cwb_xgb_wavecuts.Data());
  printf(" input mdc  cuts for training           = %s\n", cwb_xgb_mdccuts.Data());
  printf(" input verbose                          = %s\n", cwb_xgb_verbose.Data());
  printf(" input mt (multithreading)              = %s\n", cwb_xgb_mt.Data());
  printf("\n");

  // -----------------------------------------------------
  // start data splitting 
  // -----------------------------------------------------

  if(cwb_xgb_mt=="true") {
    ROOT::EnableThreadSafety();
    ROOT::EnableImplicitMT(); 					// Enable Multi threading
  }

  // get nifo from input root file
  int nifo=0;  
  TFile* iroot = TFile::Open(cwb_xgb_ifile);
  if((iroot==NULL) || (iroot!=NULL && !iroot->IsOpen())) {
    cout << "cwb_xgboost_datasplit.C : Error opening root file " << cwb_xgb_ifile << endl;
    gSystem->Exit(1); 
  }

  TTree* itree = (TTree *)iroot->Get("waveburst");
  if(itree) {
    itree->SetBranchAddress("ndim",&nifo);
    if(itree->GetEntries()>0) itree->GetEntry(0);	// get number of detector from the first entry
    else {
      cout << "cwb_xgboost_datasplit.C : Error no tree entries in " << cwb_xgb_ifile << endl;
      gSystem->Exit(1); 
    }
  } else {
    cout << "cwb_xgboost_datasplit.C : Error tree waveburst not present in " << cwb_xgb_ifile << endl;
    gSystem->Exit(1); 
  }
  if(nifo>0) {
    cout << "\n number of detectors = " << nifo << endl << endl;
  } else {
    cout << "cwb_xgboost_datasplit.C : Error nifo=0 in " << cwb_xgb_ifile << endl;
    gSystem->Exit(1); 
  }
  iroot->Close();

  // retreive the fraction used to split data in training & testing
  int    nevents = 0;
  double liveTime = 0;
  if(cwb_xgb_ifrac<0) {
    if(simulation==0) {		// if background and ifrac<0 then ifrac is the number of years selected for training

      cwb_xgb_ifrac = -TMath::Nint(cwb_xgb_ifrac);			// number of background years, use only multiple of year
      if(cwb_xgb_ifrac<1) cwb_xgb_ifrac=1;

      TString ilfname = TString::Format("%s/live_%s.%s.root",merge_dir,data_label,cwb_merge_label.Data());
      cout << "Get total live background time from file: " << ilfname << " ..." << endl; 
  
      ROOT::RDataFrame dlive("liveTime", ilfname.Data());        	// read root file  
      TString bkg_cut = TString::Format("!((lag[%d]==0)&&(slag[%d]==0))",nifo,nifo);
      auto dlive_bkg = dlive.Filter(bkg_cut.Data());			// exclude zero lag
      auto live_cut = [](double live) { return live>0; };
      auto live = dlive_bkg.Filter(live_cut,{"live"});			// get live leaf
      auto pliveTime = live.Sum("live");				// get total bkg live time
      liveTime = *pliveTime;

      if(cwb_xgb_ifrac>liveTime/(24.*3600.*365.)) {
        cout << "cwb_xgboost_datasplit.C : Error, requested background years (" << cwb_xgb_ifrac << ") are > available " << liveTime/(24.*3600.*365.) << endl;
        gSystem->Exit(1); 
      }
      cout << "Total background liveTime = " << long(liveTime) << " (sec)\t" << liveTime/(24.*3600.*365.) << " (years)" << endl;

      scwb_xgb_ifrac = TString::Format("%d",TMath::Nint(cwb_xgb_ifrac));

      cwb_xgb_ifrac = cwb_xgb_ifrac/(liveTime/(24.*3600.*365.));	// compute fraction to be selected

    } else {			// if simulation and ifrac<0 then ifrac is the number of events selected for training

      cwb_xgb_ifrac = -100*TMath::Nint(cwb_xgb_ifrac/100);		// number of events, consider only multiple of 100
      if(cwb_xgb_ifrac<100) cwb_xgb_ifrac=100;

      TString iwfname = TString::Format("%s/wave_%s.%s.root",merge_dir,data_label,cwb_merge_label.Data());
      cout << "Get total reconstructed events from file: " << iwfname << " ..." << endl; 
  
      ROOT::RDataFrame dwave("waveburst", iwfname.Data());        	// read root file  
      auto pnevents = dwave.Count(); 					// get total reconstructed events
      nevents = *pnevents;

      if(cwb_xgb_ifrac>nevents) {
        cout << "cwb_xgboost_datasplit.C : Error, requested number of events (" << cwb_xgb_ifrac << ") are > available " << nevents << endl;
        gSystem->Exit(1); 
      }
      cout << "Total number of reconstructed events = " << nevents << endl;

      scwb_xgb_ifrac = TString::Format("%d",TMath::Nint(cwb_xgb_ifrac/100));

      cwb_xgb_ifrac = cwb_xgb_ifrac/double(nevents);			// compute fraction to be selected
    }
  } 
  // for background/simulation the available ifrac granularity is 1% 
  TString strain = "";
  TString stest  = "";
  TString datasplit_tag = "";
  cwb_xgb_ifrac = TMath::Nint(100*cwb_xgb_ifrac)/100.;
  if(cwb_xgb_ifrac<0.01) cwb_xgb_ifrac=0.01;
  if(cwb_xgb_ifrac>0.99) cwb_xgb_ifrac=0.99;
  int ifrac = TMath::Nint(100.*cwb_xgb_ifrac);		// split fraction %
  if(simulation==0) {	// background
    float yliveTime = liveTime/(24.*3600.*365.);
    strain = liveTime>0 ? TString::Format(" = %g (years)",cwb_xgb_ifrac*yliveTime)     : ""; 
    stest  = liveTime>0 ? TString::Format(" = %g (years)",(1-cwb_xgb_ifrac)*yliveTime) : ""; 
    datasplit_tag = liveTime>0 ? TString::Format("_Y%s_T%d",scwb_xgb_ifrac.Data(),TMath::Nint(yliveTime)) : TString::Format("_F%02d",ifrac);
  } else {		// simulation
    strain = nevents>0 ? TString::Format(" = %d (events)",TMath::Nint(cwb_xgb_ifrac*nevents))     : ""; 
    stest  = nevents>0 ? TString::Format(" = %d (events)",TMath::Nint((1-cwb_xgb_ifrac)*nevents)) : ""; 
    datasplit_tag = nevents>0 ? TString::Format("_N%s_T%d",scwb_xgb_ifrac.Data(),TMath::Nint(nevents/100.)) : TString::Format("_F%02d",ifrac);
  }
  datasplit_tag.ReplaceAll(".","d");
  cout << endl;
  printf(" input fraction selection for training  = %g %s\n", 100*cwb_xgb_ifrac,strain.Data());
  printf(" input fraction selection for testing   = %g %s\n", 100*(1-cwb_xgb_ifrac),stest.Data());
  cout << endl;

  // make ofile name
  TString ofile_training,ofile_testing;
  if(cwb_xgb_ifile.EndsWith(".root")) {
    TString ctag = cwb_xgb_cfile!="" ? "K" : "";
            ctag =   cwb_xgb_net!="" ? ctag+"N" : ctag;

    // output file for training 
    TString file_tag_training = TString::Format(".X%s_Train%s",ctag.Data(),datasplit_tag.Data());
    if(cwb_xgb_ulabel!="") file_tag_training = file_tag_training+"_"+cwb_xgb_ulabel;
    ofile_training = TString(merge_dir)+"/"+gSystem->BaseName(cwb_xgb_ifile);
    ofile_training.ReplaceAll(".root",file_tag_training+".root");

    // output file for testing 
    TString file_tag_testing = TString::Format(".X%s_Test%s",ctag.Data(),datasplit_tag.Data());
    if(cwb_xgb_ulabel!="") file_tag_testing = file_tag_testing+"_"+cwb_xgb_ulabel;
    ofile_testing = TString(merge_dir)+"/"+gSystem->BaseName(cwb_xgb_ifile);
    ofile_testing.ReplaceAll(".root",file_tag_testing+".root");
  } else {
    cout << "cwb_xgboost_datasplit.C : Error - (--ifile) is not a valid name, must be a root file *.root" << endl << endl;
    gSystem->Exit(1);
  }

  // check if ofile_testing already exist
  bool overwrite1=TB.checkFile(ofile_testing,true);
  if(!overwrite1) gSystem->Exit(1);
  // check if ofile_training already exist
  bool overwrite2=TB.checkFile(ofile_training,true);
  if(!overwrite2) gSystem->Exit(1);

  TString training_cut = "";
  TString testing_cut  = "";
  if(simulation) {		// simulation
    // split data using run number
    TString tmp; tmp += GetRndRunFrac(ifrac,cwb_xgb_seed);
    training_cut = tmp;		// we use tmp otherwise it crash. why?
    testing_cut  = TString::Format("!(%s)",training_cut.Data());
  } else {			// background
    // split data using lag[nifo] number
    TString tmp; tmp += GetRndLagFrac(ifrac,nifo,cwb_xgb_seed);
    training_cut = tmp;		// we use tmp otherwise it crash. why?
    training_cut = TString::Format(" (%s) && !(lag[%d]==0 && slag[%d]==0)",tmp.Data(),nifo,nifo);	// exclude zero lag
    testing_cut  = TString::Format("!(%s) ||  (lag[%d]==0 && slag[%d]==0)",tmp.Data(),nifo,nifo);	// include zero lag
    //training_cut = TString::Format(" ((int(lag[%d])%%100)<%d) && !(lag[%d]==0 && slag[%d]==0)",nifo,ifrac,nifo,nifo);	// exclude zero lag
    //testing_cut  = TString::Format("!((int(lag[%d])%%100)<%d) ||  (lag[%d]==0 && slag[%d]==0)",nifo,ifrac,nifo,nifo);	// include zero lag
  }

  TString wave_training_cut = (cwb_xgb_wavecuts!="") ? "("+training_cut+")&&("+cwb_xgb_wavecuts+")" : training_cut;
  TString  mdc_training_cut = (cwb_xgb_mdccuts!="")  ? "("+training_cut+")&&("+cwb_xgb_mdccuts+")"  : training_cut;

  cout << endl;
  cout << " training selection cuts (wave)       : " << wave_training_cut << endl << endl;
  cout << " training selection cuts (mdc)        : " << mdc_training_cut << endl << endl;
  cout << " testing  selection cuts (wave & mdc) : " << testing_cut << endl << endl;
  cout << endl;

  ROOT::RDataFrame dwave("waveburst", cwb_xgb_ifile.Data());		// read root file

  auto dtraining = dwave.Filter(wave_training_cut.Data());
  auto dtesting  = dwave.Filter(testing_cut.Data());

  // if chunk list is provided then the chunk id is added to dtraining/dtesting data frames
  if(cwb_xgb_cfile!="") {

    // read chunk list 
    vector<int> chunk;
    vector<double> start;
    vector<double> stop;
    int csize = ReadChunkList(cwb_xgb_cfile, chunk, start, stop);
    // make the selection chunk string
    TString chunk_sel  = "int chunk = 0; ";
    for(int i=0;i<csize;i++) {
       chunk_sel += TString::Format("if(time[0]>=%10f && time[0]<%10f) chunk=%d;",start[i],stop[i],chunk[i]);
    }
    chunk_sel += "return chunk;";
    cout << chunk_sel << endl;
    // define chunk leaf in training data set
    dtraining = dtraining.Define("chunk", chunk_sel.Data());
    // define chunk leaf in testing data set
    dtesting = dtesting.Define("chunk", chunk_sel.Data());
  }

  // if cwb_xgb_net!="" then the network id is added to dtraining/dtesting data frames
  if(cwb_xgb_net!="") {
    int net_id = GetNetID(cwb_xgb_net);
    TString snet_id = TString::Format("return %d;",net_id);
    // define chunk leaf in training data set
    dtraining = dtraining.Define("net", snet_id.Data());
    // define chunk leaf in testing data set
    dtesting = dtesting.Define("net", snet_id.Data());
  }

  cout << "\nWrite output training/testing root files ..." << endl << endl;
  dtraining.Snapshot("waveburst", ofile_training.Data());		// dump training root file
  cout << " output wave file for training : " << ofile_training << endl; 
  dtesting.Snapshot("waveburst", ofile_testing.Data());			// dump testing  root file
  cout << " output wave file for testing  : " << ofile_testing  << endl; 
  cout << endl;

  // if cwb_xgb_ifile is a merge label then we add history and create link files for post-processing (only for ofile_testing)

  // add xgboost history to output root xgb file
  TFile ifroot(cwb_xgb_ifile);
  if (ifroot.IsZombie()) {
    cout << "cwb_xgboost_prediction.C - Error opening file " << cwb_xgb_ifile << endl;
    gSystem->Exit(1);
  }
  // create xgboost history 
  CWB::History* history = (CWB::History*)ifroot.Get("history");
  if(history==NULL) history=new CWB::History();
  if(history!=NULL) {
    TList* stageList = history->GetStageNames();    // get stage list
    TString pcuts="";
    for(int i=0;i<stageList->GetSize();i++) {       // get previous cuts 
      TObjString* stageObjString = (TObjString*)stageList->At(i);
      TString stageName = stageObjString->GetString();
      char* stage = const_cast<char*>(stageName.Data());
      if(stageName=="DSPLIT") pcuts=history->GetHistory(stage,const_cast<char*>("PARAMETERS"));
    }
    // update history
    history->SetHistoryModify(true);
    if(!history->StageAlreadyPresent(CCAST("DSPLIT"))) history->AddStage(CCAST("DSPLIT"));
    if(!history->TypeAllowed(CCAST("PARAMETERS"))) history->AddType(CCAST("PARAMETERS"));
    if(!history->TypeAllowed(CCAST("WORKDIR"))) history->AddType(CCAST("WORKDIR"));
    if(!history->TypeAllowed(CCAST("SEED"))) history->AddType(CCAST("SEED"));
    if(!history->TypeAllowed(CCAST("MT"))) history->AddType(CCAST("MT"));
    char work_dir[1024]="";
    sprintf(work_dir,"%s",gSystem->WorkingDirectory());
    history->AddHistory(CCAST("DSPLIT"), CCAST("WORKDIR"), work_dir);
    TString cuts = (pcuts!="") ? pcuts+" ( XGBoost: "+cwb_xgboost_options+" )" : cwb_xgboost_options;
    history->AddHistory(CCAST("DSPLIT"), CCAST("PARAMETERS"), CCAST(cuts.Data()));
    char logmsg[2048]; sprintf(logmsg,"Apply XGBoost datasplit : %s",cwb_xgboost_options.Data());
    history->AddLog(CCAST("DSPLIT"), CCAST(logmsg));
    history->AddHistory(CCAST("DSPLIT"), CCAST("SEED"), CCAST(TString::Format("%d",cwb_xgb_seed).Data()));
    history->AddHistory(CCAST("DSPLIT"), CCAST("MT"), CCAST(cwb_xgb_mt.Data()));
  }
  // write xgboost history to merge+cuts file
  if(history!=NULL) {
    TFile ofroot(ofile_testing,"UPDATE");
    history->Write("history");
    ofroot.Close();
    delete history;
  }

  // add detectors to tree user info
  itree = (TTree *) ifroot.Get("waveburst"); 	// get waveburst input tree
  if(itree!=NULL) {
    // get detector list
    TList* list = itree->GetUserInfo();
    int nifo = list->GetSize();				// retreive nifo
    if(nifo>0) {
      TFile ofroot(ofile_testing,"UPDATE");
      TTree* otree = (TTree *) ofroot.Get("waveburst");	// get waveburst output tree
      if(otree!=NULL) {
        for(int k=0;k<nifo;k++) {
          detector* pD = new detector(*((detector*)list->At(k))); 
          otree->GetUserInfo()->Add(pD);
        }
        otree->Write();
        ofroot.Close();
      } else {
        cout << "cwb_xgboost_datasplit.C - WARNING : testing wave tree is empty !!!" << endl << endl;
      }
    } else {cout << "cwb_xgboost_datasplit.C - Error : no ifos present in the tree !!!" << endl;exit(1);}
  } else {cout << "cwb_xgboost_datasplit.C - Error : tree waveburst not present !!!" << endl;exit(1);}

  // close input root file
  ifroot.Close();

  // create a dummy merge*.lst file name
  TString olstfname_testing = ofile_testing;
  olstfname_testing.ReplaceAll("wave_","merge_");
  olstfname_testing.ReplaceAll(".root",".lst");
  sprintf(cmd," touch %s",olstfname_testing.Data());
  cout << cmd << endl;
  gSystem->Exec(cmd);
  TString olstfname_training = ofile_training;
  olstfname_training.ReplaceAll("wave_","merge_");
  olstfname_training.ReplaceAll(".root",".lst");
  sprintf(cmd," touch %s",olstfname_training.Data());
  cout << cmd << endl;
  gSystem->Exec(cmd);

  // for simulation!=0 apply cuts to mdc root file
  if(simulation) {
    char ilfname[1024];
    sprintf(ilfname,"%s/mdc_%s.%s.root",merge_dir,data_label,cwb_merge_label.Data());
    TString olfname_training = ofile_training;
    olfname_training.ReplaceAll("wave_","mdc_");
    TString olfname_testing = ofile_testing;
    olfname_testing.ReplaceAll("wave_","mdc_");
    estat = gSystem->GetPathInfo(ilfname,&id,&size,&flags,&mt);
    if(estat==0) {
      ROOT::RDataFrame dmdc("mdc", ilfname);			// read root file
      auto dtraining = dmdc.Filter(mdc_training_cut.Data());
      dtraining.Snapshot("mdc", olfname_training.Data());	// dump training root file
      cout << "\n output mdc  file for training  : " << olfname_training; 
      auto dtesting  = dmdc.Filter(testing_cut.Data());
      dtesting.Snapshot("mdc", olfname_testing.Data());		// dump testing  root file
      cout << "\n output mdc  file for testing   : " << olfname_testing << endl << endl; 
    }
  }

  // for simulation=0 apply cuts to live root file
  if(!simulation) {
    char ilfname[1024];
    sprintf(ilfname,"%s/live_%s.%s.root",merge_dir,data_label,cwb_merge_label.Data());
    TString olfname_training = ofile_training;
    olfname_training.ReplaceAll("wave_","live_");
    TString olfname_testing = ofile_testing;
    olfname_testing.ReplaceAll("wave_","live_");
    estat = gSystem->GetPathInfo(ilfname,&id,&size,&flags,&mt);
    if(estat==0) {
      ROOT::RDataFrame dlive("liveTime", ilfname);		// read root file
      auto dtraining = dlive.Filter(training_cut.Data());
      dtraining.Snapshot("liveTime", olfname_training.Data());	// dump training root file
      cout << "\n output live file for training  : " << olfname_training; 
      auto dtesting  = dlive.Filter(testing_cut.Data());
      dtesting.Snapshot("liveTime", olfname_testing.Data());	// dump testing  root file
      cout << "\n output live file for testing   : " << olfname_testing << endl << endl; 
    }
  }

  exit(0);
}

int ReadChunkList(TString ifile, vector<int>& chunk, vector<double>& start, vector<double>& stop) {

  // Open chunk list
  ifstream in;
  in.open(ifile.Data(),ios::in);
  if (!in.good()) {cout << "Error Opening File : " << ifile << endl;exit(1);}

  cout.precision(10);

  int size=0;
  char irun[256];
  int ichunk;
  double istart,istop;
  cout << endl << " -> chunk list" << endl << endl;
  cout << "\t" << "chunk" << "\t" << "start" << "\t\t" << "stop" << endl << endl;
  while(true) {
    in >> irun >> ichunk >> istart >> istop;
    if(!in.good()) break;
    cout << "\t" << ichunk << "\t" << istart << "\t" << istop << endl;
    chunk.push_back(ichunk);
    start.push_back(istart);
    stop.push_back(istop);
    size++;
  }
  cout << endl;
  in.close();

  return size;
}

TString GetRndRunFrac(int cnt, int seed) {

  gRandom->SetSeed(seed);

  cnt = abs(cnt)%100;

  vector<int> rnd;
  for(int i=0;i<100000;i++) {
    int RND = gRandom->Integer(100);
    bool unique=true;
    for(int j=0;j<rnd.size();j++) if(rnd[j]==RND) unique=false;
    if(unique) rnd.push_back(RND);
    if(rnd.size()==cnt) break;
  }
  TString rnd_frac="";
  for(int j=0;j<rnd.size();j++) {
    rnd_frac += TString::Format("((run%%100)==%d)",rnd[j]);
    if(j<rnd.size()-1) rnd_frac += "||";
  }

  return rnd_frac;
}

TString GetRndLagFrac(int cnt, int nifo, int seed) {

  gRandom->SetSeed(seed);

  cnt = abs(cnt)%100;

  vector<int> rnd;
  for(int i=0;i<100000;i++) {
    int RND = gRandom->Integer(100);
    bool unique=true;
    for(int j=0;j<rnd.size();j++) if(rnd[j]==RND) unique=false;
    if(unique) rnd.push_back(RND);
    if(rnd.size()==cnt) break;
  }
  TString rnd_frac="";
  for(int j=0;j<rnd.size();j++) {
    rnd_frac += TString::Format("((int(lag[%d])%%100)==%d)",nifo,rnd[j]);
    if(j<rnd.size()-1) rnd_frac += "||";
  }

  return rnd_frac;
}

int GetNetID(TString net) {

  // convert network string into a unique number: 'HL' -> 44
  // 1 -> convert to upper case
  // 2 -> sort characters
  // 3 -> convert to a unique number according to the ascii values

  if(!net.IsAlpha()) {
     cout << endl << "GetNetId - Error: net = '" << net << "' is not alphabetic" << endl << endl;
     exit(1);
  }

  net.ToUpper();        // convert to upper case

  // sort characters
  std::string snet = net.Data();
  std::sort(snet.begin(), snet.end());
  net = snet.c_str();

  //cout << net << endl;

  int iA = (int)'A';
  int iZ = (int)'Z';
  int iAZ = iZ-iA+1;

  int inet=0;
  for(int i=0;i<net.Sizeof()-1;i++) inet += i*iAZ+((int)net[i]-iA);

  //cout << inet << endl;

  return inet;
}

