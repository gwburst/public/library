#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago, Igor Yakushin, Sergey Klimenko
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess, sys, os, glob
import os.path
from ligo.segments import segment, segmentlist
from ligo.segments.utils import tosegwizard, fromsegwizard
from cwb_online.run_utils import get_time, get_date_from_gps, get_start_of_the_day_gps, getnumberfromstring
from cwb_online.detector import detector
from cwb_online.root_python import AdjustSlag, getpar
from collections import deque
import getpass

"""def get_request(cWB_conf):
    req_disk=""
    req_mem=""
    lines=[]
    if (hasattr(cWB_conf,"request_disk")):
        req_disk=cWB_conf.request_disk
    else:
        lines=open(os.environ['CWB_PARAMETERS_FILE']).readlines()
        line=list(filter(lambda x: x.find("request_disk")!=-1, lines))[0]
        req_disk=line.split(";")[0].split("=")[-1]
    if (hasattr(cWB_conf,"request_memory")):
        req_mem=cWB_conf.request_memory
    else:
        if (len(lines)==0):
            lines=open(os.environ['CWB_PARAMETERS_FILE']).readlines()
        line=list(filter(lambda x: x.find("request_memory")!=-1, lines))[0]
        req_mem=line.split(";")[0].split("=")[-1]
    return getnumberfromstring(req_disk),getnumberfromstring(req_mem)
"""

class JOB:
    def __init__(self,start,end,basedir):
        self.start=start
        self.end=end
        self.basedir=basedir
        self.dir="%s/%s/%d-%d"%(self.basedir, get_date_from_gps(str(self.start)),self.start, self.end)
        print(self.dir)
        a=subprocess.getstatusoutput("mkdir -p %s"%(self.dir))
        self.missing_data=[]
    def framelist(self,cWB_conf,dets):
        input_dir="%s/input"%(self.dir)
        for ifo,det in zip(cWB_conf.ifos,dets):
            fn="%s/%s_burst.in"%(input_dir,ifo)
            f=open(fn,"w")
            print("%d %d"%(int(self.start)-cWB_conf.job_offset, int(self.end)+cWB_conf.job_offset), file=f)
            f.close()
            fn="%s/%s.frames"%(input_dir,ifo)
            f=open(fn,"w")
            pattern=det.getframelist(self.start - cWB_conf.job_offset,self.end+cWB_conf.job_offset)
            print("\n".join(pattern), file=f)
            f.close()
            for fn in pattern:
                 if(not os.path.exists(fn)):
                    self.missing_data.append(fn)
            if(len(self.missing_data)>0):
               f=open("%s/missing_data.txt"%input_dir,"w")
               print("\n".join(self.missing_data), file=f)
               f.close()
    def setup(self,cWB_conf,dets,superlag=""):
        try:
            condor_requirements_lines=open(cWB_conf.condor_requirements_file).readlines()
            condor_requirements="".join(condor_requirements_lines)
        except:
            condor_requirements=""
        try:
            accounting_group_user="accounting_group_user=%s"%(cWB_conf.accounting_group_user)
        except:
            accounting_group_user=""
        os.chdir(self.dir)
        a=subprocess.getstatusoutput("mkdir -p input condor output tmp log")
        if (superlag==""):
            self.framelist(cWB_conf,dets)
            superlag="0"
            for j in range(1,len(cWB_conf.ifos)):
                superlag+=",0"
        f=open("condor/%s.sub"%(cWB_conf.label),"w")
        lagsize=cWB_conf.bkg_nlags
        lagoff=1;
        tmpfile="%s/user_parameters_bkg.C"%(cWB_conf.config_dir)
        tmpfile=tmpfile.replace(".C","_split.C")
        #req_disk,req_mem=get_request(cWB_conf)
        req_disk=getpar("request_disk","%s/user_parameters.C"%(cWB_conf.config_dir))
        req_mem=getpar("request_memory","%s/user_parameters.C"%(cWB_conf.config_dir))
        if (cWB_conf.bkg_split>1):
          environment="CWB_JOBID=$(JOB);CWB_UFILE=$(CWB_UFILE);CWB_STAGE=$(CWB_STAGE);SLAG_SHIFT=$(SLAG_SHIFT)"
        else:
          environment="PID=1;CWB_JOBID=1;CWB_UFILE=%s;CWB_STAGE=CWB_STAGE_FULL;SLAG_SHIFT=%s"%(tmpfile,superlag)
        if (os.environ['SITE_CLUSTER']=="CASCINA"):
          environment+="\nInitialdir = %s/log"%self.dir
        if (os.environ['SITE_CLUSTER']=="CIEMAT"):
          environment+=";Initialdir=%s"%self.dir
        sub="""universe = vanilla
getenv = true
request_memory = %i MB
request_disk = %i MB
executable = %s/bin/online_net_bkg
environment = %s
output = $(PID)_%s_%d_%d.out
error = $(PID)_%s_%d_%d.err
log = %s_%d_%d.log
notification = never
rank=memory
accounting_group = %s
%s
%s
queue
""" % (req_mem,req_disk,os.environ['HOME_WAT_INSTALL'], environment, cWB_conf.label, self.start, self.end, cWB_conf.label, self.start, self.end, cWB_conf.label, self.start, self.end,cWB_conf.accounting_group,accounting_group_user,condor_requirements)
        print(sub, file=f)
        f.flush()
        f.close()
        if (cWB_conf.bkg_split>1):
          f=open("condor/%s.dag"%(cWB_conf.label),"w")
          for l in range(1,cWB_conf.bkg_split+1):
            lagsize=cWB_conf.bkg_nlags/cWB_conf.bkg_split
            lagoff=lagsize*(l-1)+1
            print("""JOB A%d %s/condor/%s.sub
VARS A%d PID="%d" JOB="%d" CWB_UFILE="%s" CWB_STAGE="CWB_STAGE_FULL" SLAG_SHIFT="%s"
RETRY A%d 3000"""%(l,self.dir,cWB_conf.label,l,l,1,tmpfile,superlag,l), file=f)
          f.flush()
          f.close()
    def launch(self,cWB_conf):
        if(os.path.exists("%s/input/missing_data.txt"%self.dir)):
           return
        os.chdir("%s/condor"%self.dir)
        if (cWB_conf.bkg_split>1):
          com="condor_submit_dag %s.dag"%(cWB_conf.label)
        else:
          com="condor_submit %s.sub"%(cWB_conf.label)
        a=subprocess.getstatusoutput(com)
        com="touch ../start"
        a=subprocess.getstatusoutput(com)
class TS:
    def __init__(self,cWB_conf,bkg_superlag_index,bkg_superlag_list):
        self.bkg_superlag_index=bkg_superlag_index
        self.bkg_superlag_list=bkg_superlag_list
        self.considered_segments=segmentlist([])
        self.processed_segments=segmentlist([])
        self.running_segments=segmentlist([])
        self.missing_segments=segmentlist([])
        self.job_segments=segmentlist([])
        self.run_segments=segmentlist([])
        self.job_queue=[]
        self.detectors=[]
        for ifo,ifile in zip(cWB_conf.ifos,cWB_conf.ifo_files):
          self.detectors.append(detector(ifo,"%s/%s"%(cWB_conf.config_dir,ifile.split("/")[-1]),True))
    def update_considered(self,cWB_conf):
        found=0
        try:
            f=open("%s/%s/considered.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir))
            old=fromsegwizard(f)
            f.close()
            found+=1
        except Exception as e:
            old=segmentlist([])                        
        try:
            f=open("%s/%s/seg_global.txt"%(cWB_conf.run_dir,cWB_conf.seg_dir))
            extra=fromsegwizard(f)
            f.close()
            found+=1
        except:
            extra=segmentlist([])
        if (found>0):
          considered=old | extra; considered.coalesce()
          f=open("%s/%s/considered.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir),"w")
          tosegwizard(f,considered)
          f.close()
          self.considered_segments=considered
    def update_processed(self,cWB_conf):
        a=glob.glob("%s/%s*/????-??-??/*"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir))
        processed=[]
        for b in a:
            if(os.path.exists("%s/start"%b) and not os.path.exists("%s/finished"%b)):
              c=glob.glob("%s/output/*.root"%b)
              if(len(c)>=cWB_conf.bkg_split):
                subprocess.getstatusoutput("touch %s/finished"%b)
                subprocess.getstatusoutput("gzip %s/condor/*.out; gzip %s/condor/*.err"%(b,b))
            if(os.path.exists("%s/start"%b) and os.path.exists("%s/finished"%b)):
                processed.append(b)
        try:
            self.processed_segments=segmentlist([segment(list(map(int,x.split("/")[-1].split("-")))) for x in processed])
            self.processed_segments.coalesce()
        except Exception as e:
            print(e)
            self.processed_segments=segmentlist([])
        f=open("%s/%s/processed.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir),"w")
        tosegwizard(f,self.processed_segments)
        f.close()
    def update_missing(self,cWB_conf):
        try:
            f=open("%s/%s/missing.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir))
            self.missing_segments=fromsegwizard(f)
            f.close()
        except Exception as e:
            print("cannot read missing segments file %s\n%s"%("%s/%s/missing.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir),e))
            self.missing_segments=segmentlist([])                                
    def update_running(self,cWB_conf):
        a=glob.glob("%s/%s*/????-??-??/*"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir))
        running=[]
        for b in a:
            if(os.path.exists("%s/start"%b) and not os.path.exists("%s/finished"%b)):
                running.append(b)
        try:
            self.running_segments=segmentlist([segment(list(map(int,x.split("/")[-1].split("-")))) for x in running])
            self.running_segments.coalesce()
        except Exception as e:
            print(e)
            print(len(running))
            self.running_segments=segmentlist([])
        f=open("%s/%s/running.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir),"w")
        tosegwizard(f,self.running_segments)
        f.close()
    def break_seg(self,cWB_conf):
        f=open("%s/%s/run.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir))
        lines = f.readlines()
        f.close()
        f=open("%s/%s/jobs.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir),"w")
        for i_line in range(len(lines)):
           line=lines[i_line]
           if(line.find("#")==-1):
               seg=line.split("\t")
               start=int(seg[1])+cWB_conf.job_offset
               stop=int(seg[2])-cWB_conf.job_offset
               n = int(int(stop-start)/cWB_conf.bkg_job_duration)
               s=start
               e=stop
               if (n>1):
                   for i in range(0,n-1):
                       s = start+ i*cWB_conf.bkg_job_duration
                       e = start+(i+1)*cWB_conf.bkg_job_duration
                       print("%d %d"%(s,e), file=f)
                   s = e
                   e = stop
               if (i_line<len(lines)-1):
                 n = int(int(e-s)/cWB_conf.bkg_job_duration)
                 if (n>1):
                   print("error", file=f)
                 #if (n==0):
                 #  if (e-s>cWB_conf.bkg_job_minimum):
                 #      print("%d %d"%(s,e), file=f)
                 if (n==1):
                   #if (cWB_conf.bkg_job_minimum==cWB_conf.bkg_job_duration):
                       e = s+ cWB_conf.bkg_job_duration
                       print("%d %d"%(s,e), file=f)
                   #else:
                   #    half = (e-s)/2
                   #    if (half>cWB_conf.bkg_job_minimum):
                   #         e = s+ half
                   #         print("%d %d"%(s,e), file=f)
                   #         s = e
                   #         e = s+ half
                   #         print("%d %d"%(s,e), file=f)
                   #    else:
                   #         e = s+cWB_conf.bkg_job_duration
                   #         print("%d %d"%(s,e), file=f)
        f.close()
    def compute_job_segments(self,cWB_conf):
        #rs = self.considered_segments - self.processed_segments; rs.coalesce();
        #rs = rs - self.running_segments; rs.coalesce()
        #rs = rs - self.missing_segments; rs.coalesce()
        #if(abs(rs)==0):
        #    return
        current_time=get_time()
        self.run_segments=segmentlist(self.considered_segments)
        #self.run_segments=segmentlist(rs)
        #self.run_segments.coalesce()
        self.run_segments = segmentlist([x for x in self.run_segments if abs(x)>=cWB_conf.bkg_job_duration+2*cWB_conf.job_offset])
        self.run_segments = segmentlist([x for x in self.run_segments if x[1] >= get_start_of_the_day_gps(current_time)])
        f=open("%s/%s/run.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir),"w")
        tosegwizard(f,self.run_segments)
        f.close()
        self.break_seg(cWB_conf)
        f=open("%s/%s/jobs.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir))
        self.job_segments=[list(map(int, x.strip().split(" "))) for x in f.readlines()]
        f.close()
        self.job_queue=[]
        for j in self.job_segments:
            if (not os.path.exists("%s/%s/%s/%s-%s"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir,get_date_from_gps(str(j[0])),j[0], j[1]))):
                self.job_queue.append(JOB(j[0],j[1],"%s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir)))
        missing_dirs=glob.glob("%s/%s/*/*/input/missing_data.txt"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir))
        for m in missing_dirs:
           mis_files=open(m).readlines()
           leng=len(mis_files)
           test=0
           for l in mis_files:
             if (os.path.exists(l.replace("\n",""))):
               test+=1
           if (len(mis_files)==test):
              self.job_queue.append(JOB(int(m.split("/")[-3].split("-")[0]),int(m.split("/")[-3].split("-")[1]),"%s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir)))
              subprocess.getstatusoutput("rm %s"%(m))
    def launch(self,cWB_conf):
        missing_jobs=[]
        for job in self.job_queue:
            job.setup(cWB_conf,self.detectors)
            if(len(job.missing_data)>0):
                missing_jobs.append(job)
            else:
                job.launch(cWB_conf)
        if(len(missing_jobs)>0):
            missing_segments=segmentlist([segment(x.start - cWB_conf.job_offset, x.end + cWB_conf.job_offset) for x in missing_jobs]); self.missing_segments.coalesce()
            try:
                f=open("%s/%s/missing.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir))
                seg=fromsegwizard(f)
                f.close()
            except Exception as e:
                print(e)
                seg=segmentlist([])
            missing_segments = missing_segments | seg; missing_segments.coalesce()
            f=open("%s/%s/missing.txt"%(cWB_conf.bkg_run_dir,cWB_conf.seg_dir),"w")
            tosegwizard(f,missing_segments)
            f.close()
    def superlag(self,cWB_conf):
        date=get_date_from_gps(get_time())
        started_dir=glob.glob("%s/%s/????-??-??/*/start"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir))
        started_dir.sort()
        for d in range(0,len(started_dir)):
         #if (started_dir[d].find(date)!=-1):
          for i in range(1,len(self.bkg_superlag_index)):
            ok=True
            for j in range(0,len(cWB_conf.ifos)):
               if (d+self.bkg_superlag_list[j][i]<0 or d+self.bkg_superlag_list[j][i]>=len(started_dir)):
                 ok=False
            if (ok==True):
               second_start_job=int("/".join(started_dir[d+self.bkg_superlag_list[0][i]].split("/")[-2:-1]).split("-")[0])
               second_end_job=int("/".join(started_dir[d+self.bkg_superlag_list[0][i]].split("/")[-2:-1]).split("-")[1])
               second_dir="%s"%("/".join(started_dir[d+self.bkg_superlag_list[0][i]].split("/")[-3:-1]))
               new_dir="%s/%s_%i"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir,i)
               #superlag_string="0"
               #slag_string="%s"%(self.bkg_superlag_list[0][i])
               for j in range(0,len(cWB_conf.ifos)):
                  temp_start_job=int("/".join(started_dir[d+self.bkg_superlag_list[j][i]].split("/")[-2:-1]).split("-")[0])
                  if (j==0):
                    superlag_string=",%i"%(second_start_job-temp_start_job)
                    slag_string=",%i"%(self.bkg_superlag_list[j][i])
                  else:
                    superlag_string+=",%i"%(second_start_job-temp_start_job)
                    slag_string+=",%i"%(self.bkg_superlag_list[j][i])
               slag_string+=",%i"%(self.bkg_superlag_index[i])
               if (not os.path.exists("%s/%s/start"%(new_dir,second_dir))):
                  sjob=JOB(second_start_job,second_end_job,new_dir)
                  sjob.setup(cWB_conf,self.detectors,"%s"%(superlag_string))
                  for j in range(0,len(cWB_conf.ifos)):
                     temp_dir="/".join(started_dir[d+self.bkg_superlag_list[j][i]].split("/")[:-1])
                     com="ln -s %s/input/%s.frames %s/%s/input/."%(temp_dir,cWB_conf.ifos[j],new_dir,second_dir)
                     subprocess.getstatusoutput(com)
                     com="ln -s %s/input/%s_burst.in %s/%s/input/."%(temp_dir,cWB_conf.ifos[j],new_dir,second_dir)
                     subprocess.getstatusoutput(com)
                  sjob.launch(cWB_conf)
               if (os.path.exists("%s/%s/finished"%(new_dir,second_dir)) and not os.path.exists("%s/%s/copied"%(new_dir,second_dir))):
                  files=glob.glob("%s/%s/output/*.root"%(new_dir,second_dir))
                  if(len(files)==cWB_conf.bkg_split):
                    for file in files:
                       startfile=file.split("/")[len(file.split("/"))-1]
                       subfile=startfile.replace("slag0","slag%i"%i)
                       AdjustSlag("%s/%s/output/%s"%(new_dir,second_dir,startfile),"%s/%s/output/%s"%("%s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir),second_dir,subfile), slag_string, superlag_string)
                       com="rm  %s/%s/output/%s"%(new_dir,second_dir,startfile)
                       subprocess.getstatusoutput(com)
                    subprocess.getstatusoutput("touch %s/%s/copied"%(new_dir,second_dir))
    def cycle(self,cWB_conf):
        print("================================================ Cycle %d ================================================="%get_time())
        self.release()
        self.update_running(cWB_conf)
        self.update_processed(cWB_conf)
        self.update_missing(cWB_conf)
        self.update_considered(cWB_conf)
        self.compute_job_segments(cWB_conf)
        self.launch(cWB_conf)
        self.superlag(cWB_conf)
    def release(self):
        user=getpass.getuser()
        com="condor_q %s -nobatch | grep 'H ' "%user
        a=subprocess.getstatusoutput(com)
        jobs=a[1].split("\n")
        if jobs[0]!='':
         for job in jobs:
           j_id=job.split(" ")[0]
           com="condor_q -long %s | grep -w 'HoldReason ='"%j_id
           b=subprocess.getstatusoutput(com)
           #if (b[1].find("Job has gone over memory limit of")!=-1):
           if (b[1].find("MemoryUsage")!=-1 and b[1].find(" > RequestMemory")!=-1):
              memorylimit=b[1].split(" ")[len(b[1].split(" "))-2]
              mem=int(memorylimit)+1000
              com="condor_qedit %s RequestMemory %s"%(j_id,mem)
              subprocess.getstatusoutput(com)
           else:
              print("Job %s in held not for memory limit"%j_id)
           com="condor_release %s"%j_id
           subprocess.getstatusoutput(com)
    def run(self,cWB_conf):
        stop=False
        while(True and not stop):
            try:
                start_loop=get_time()
                self.cycle(cWB_conf)
                os.system("sleep %d"%(10*60))
            except Exception as e:
                print("Crashed cycle")
                print(e)
                sys.stdout.flush()
                sys.stderr.flush()
            stop=os.path.exists("%s/sHuTdOwN"%cWB_conf.bkg_run_dir)
        
def run_ts(cWB_conf):
    f=open("%s/superlaglist.txt"%cWB_conf.config_dir)
    lines=[x.replace("\n","") for x in [x for x in f.readlines() if x.find("#")!=0]]
    f.close()
    bkg_superlag_index=deque([])
    bkg_superlag_list={}
    for i in range(len(cWB_conf.ifos)):
        bkg_superlag_list[i]=deque([])
    for line in lines:
      arrays=line.split(" ")
      first=False
      index=0
      for array in arrays:
        if (array!=""):
           if (first==False):
             bkg_superlag_index.append(int(array))
             first=True
           else:
             bkg_superlag_list[index].append(int(array))
             index=index+1
    print(repr(bkg_superlag_index))
    for i in range(len(cWB_conf.ifos)):
      print(repr(bkg_superlag_list[i]))
   
    ts=TS(cWB_conf,bkg_superlag_index,bkg_superlag_list)
    del bkg_superlag_list
    ts.run(cWB_conf)

