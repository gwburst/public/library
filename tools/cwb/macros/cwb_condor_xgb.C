/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


// creates dag/sub files under the condor dir : used by the cwb_condor command

{
  #include <vector>

  CWB::Toolbox TB;

  TB.checkFile(gSystem->Getenv("CWB_ROOTLOGON_FILE"));
  TB.checkFile(gSystem->Getenv("CWB_PARAMETERS_FILE"));
  TB.checkFile(gSystem->Getenv("CWB_UPARAMETERS_FILE"));

  if(TString(condor_tag)=="") {
    cout << endl;
    cout << "cwb_condor_xgb.C : Error - the accounting_group is not defined !!!" << endl;
    cout << "The accounting_group must be defined in the user_parameters.C file" << endl;
    cout << "See the following link:" << endl;
    cout <<" https://ldas-gridmon.ligo.caltech.edu/accounting/condor_groups/determine_condor_account_group.html" << endl;
    cout << "Examples : " << endl;
    cout << "strcpy(condor_tag,\"ligo.dev.o2.burst.allsky.cwboffline\");" << endl;
    cout << "strcpy(condor_tag,\"ligo.prod.o2.burst.allsky.cwboffline\");" << endl;
    cout << "If you don't need it set : strcpy(condor_tag,\"disabled\");" << endl << endl;
    exit(1);
  }
  if(TString(condor_tag)=="disabled") strcpy(condor_tag,"");

  char full_condor_dir[1024];
  char full_condor_out_dir[1024];
  char full_condor_err_dir[1024];

  sprintf(full_condor_dir,"%s/%s",work_dir,condor_dir);
  sprintf(full_condor_out_dir,"%s/%s",work_dir,log_dir);
  sprintf(full_condor_err_dir,"%s/%s",work_dir,log_dir);

  TString xgb_ulabel = TString(gSystem->Getenv("CWB_XGB_ULABEL"));
  if(xgb_ulabel!="") xgb_ulabel="_"+xgb_ulabel;

  char dagfile[1024];
  sprintf(dagfile,"%s/%s%s.dag",condor_dir,data_label,xgb_ulabel.Data());

  // Check if dag file already exist
  Long_t id,size,flags,mt;
  int estat = gSystem->GetPathInfo(dagfile,&id,&size,&flags,&mt);
  if (estat==0) {
    char answer[256];
    strcpy(answer,"");
    do {
      cout << "File \"" << dagfile << "\" already exist" << endl;
      cout << "Do you want to overwrite the file ? (y/n) ";
      cin >> answer;
      cout << endl << endl;
    } while ((strcmp(answer,"y")!=0)&&(strcmp(answer,"n")!=0));
    if (strcmp(answer,"n")==0) {
      exit(0);
    }
  }

  // all tuning array parameters must have size>0
  int size_check=0;
  if(pp_xgb_learning_rate.size()==0)    size_check++;
  if(pp_xgb_max_depth.size()==0)        size_check++;
  if(pp_xgb_min_child_weight.size()==0) size_check++;
  if(pp_xgb_gamma.size()==0)            size_check++;
  if(pp_xgb_colsample_bytree.size()==0) size_check++;
  if(pp_xgb_subsample.size()==0)        size_check++;
  if(pp_xgb_scale_pos_weight.size()==0) size_check++;
  if(pp_xgb_balance_slope.size()==0)    size_check++;
  if(pp_xgb_balance_balance.size()==0)  size_check++;
  if(pp_xgb_caps_rho0.size()==0)        size_check++;
  if(size_check>0) {
    cout << endl << "cwb_condor_xgb.C : Error - all tuning parameters must have array size > 0 " << endl << endl;
    gSystem->Exit(1);
  }

  // generate xgb parameters to be used for tuning
  vector<TString> vpp_xgb_options;
  int xid = 0;
  for (int k1=0;k1<pp_xgb_learning_rate.size();k1++) {
   for (int k2=0;k2<pp_xgb_max_depth.size();k2++) {
    for (int k3=0;k3<pp_xgb_min_child_weight.size();k3++) {
     for (int k4=0;k4<pp_xgb_gamma.size();k4++) {
      for (int k5=0;k5<pp_xgb_colsample_bytree.size();k5++) {
       for (int k6=0;k6<pp_xgb_subsample.size();k6++) {
        for (int k7=0;k7<pp_xgb_balance_slope.size();k7++) {
         for (int k8=0;k8<pp_xgb_balance_balance.size();k8++) {
          for (int k9=0;k9<pp_xgb_caps_rho0.size();k9++) {
           for (int k10=0;k10<pp_xgb_scale_pos_weight.size();k10++) {

         char pp_xgb_params[1024]="";
         sprintf(pp_xgb_params,"--learning-rate %g --max-depth %d --min-child-weight %g --gamma %g --colsample-bytree %g --subsample %g --scale_pos_weight %g --balance-slope %s --balance-balance %s --caps-rho0 %g",
                 pp_xgb_learning_rate[k1],pp_xgb_max_depth[k2],pp_xgb_min_child_weight[k3],pp_xgb_gamma[k4],
                 pp_xgb_colsample_bytree[k5],pp_xgb_subsample[k6],pp_xgb_scale_pos_weight[k10],pp_xgb_balance_slope[k7].Data(),pp_xgb_balance_balance[k8].Data(),pp_xgb_caps_rho0[k9]);
         //cout << pp_xgb_params << endl;

         char pp_xgb_ofile[1024];
         sprintf(pp_xgb_ofile,"%s/%s%s_%d.out",output_dir,data_label,xgb_ulabel.Data(),xid++);

         char pp_xgb_options[2048]="";
         sprintf(pp_xgb_options,"--nfile %s --sfile %s --ofile %s --nfrac %g --sfrac %g --search %s --config %s %s",
                 pp_xgb_nfile.Data(), pp_xgb_sfile.Data(), pp_xgb_ofile, pp_xgb_nfrac, pp_xgb_sfrac, pp_xgb_search.Data(), pp_xgb_config.Data(), pp_xgb_params);
         if(pp_xgb_fgetrhor!="") {
           char pp_xgb_options_tmp[2048]="";
           strcpy(pp_xgb_options_tmp,pp_xgb_options);
           sprintf(pp_xgb_options_tmp," %s  --fgetrhor %s",pp_xgb_options, pp_xgb_fgetrhor.Data());
           strcpy(pp_xgb_options,pp_xgb_options_tmp);
         }
         //cout << pp_xgb_options << endl;

         vpp_xgb_options.push_back(pp_xgb_options);

           }
          }
         }
        }
       }
      }
     }
    }
   }
  }

  // Create dag file
  char ofile_dag[1024];
  sprintf(ofile_dag,"%s/%s%s.dag",condor_dir,data_label,xgb_ulabel.Data());
  ofstream out;
  out.open(ofile_dag,ios::out);

  int jID=0;
  for(int n=0;n<vpp_xgb_options.size();n++) {
    //cout << n << " " << vpp_xgb_options[i] << endl;
    jID = n+1;
    char ostring[1024];
    sprintf(ostring,"JOB A%i %s/%s%s.sub",jID,full_condor_dir,data_label,xgb_ulabel.Data());
    out << ostring << endl;
    sprintf(ostring,"VARS A%i PID=\"%i\" CWB_XGBOOST_OPTIONS=\"%s\"",jID,jID,vpp_xgb_options[n].Data());
    out << ostring << endl;
    sprintf(ostring,"RETRY A%i 3000",jID);
    out << ostring << endl;
  }
  out.close();

  // Create sub file
  char ofile_sub[1024];
  sprintf(ofile_sub,"%s/%s%s.sub",full_condor_dir,data_label,xgb_ulabel.Data());

  FILE *fP=NULL;
  if((fP = fopen(ofile_sub, "w")) == NULL) {
    cout << "cwb_condor_xgb.C:createSubFile : Error - cannot open file " << ofile_sub << endl;
    gSystem->Exit(1);
  }

  fprintf(fP,"universe = vanilla\n");
  fprintf(fP,"getenv = true\n");
  fprintf(fP,"priority = $(PRI)\n");
  fprintf(fP,"on_exit_hold = ( ExitCode != 0 )\n");
  fprintf(fP,"request_memory = %d MB\n", request_memory);
  fprintf(fP,"request_disk = %d MB\n", request_disk);
  fprintf(fP,"executable = xgb.sh\n");
  fprintf(fP,"job_machine_attrs = Machine\n");
  fprintf(fP,"job_machine_attrs_history_length = 5\n");
  fprintf(fP,"requirements = target.machine =!= MachineAttrMachine1 && target.machine =!= MachineAttrMachine2 && target.machine =!= MachineAttrMachine3 && target.machine =!= MachineAttrMachine4 && target.machine =!= MachineAttrMachine5\n");
  fprintf(fP,"environment = CWB_JOBID=$(PID);CWB_XGBOOST_OPTIONS=$(CWB_XGBOOST_OPTIONS)\n");
  if(TString(condor_tag)!="") fprintf(fP,"accounting_group = %s\n",condor_tag);
  fprintf(fP,"output = %s/$(PID)_%s%s.out\n",full_condor_out_dir,data_label,xgb_ulabel.Data());
  fprintf(fP,"error = %s/$(PID)_%s%s.err\n",full_condor_err_dir,data_label,xgb_ulabel.Data());
  fprintf(fP,"log = %s/%s%s.log\n",condor_log,data_label,xgb_ulabel.Data());
  fprintf(fP,"notification = never\n");
  fprintf(fP,"rank=memory\n");
  fprintf(fP,"queue\n");

  fclose(fP);

  cout << endl;
  cout << "Created dag file for XGB tuning with " << vpp_xgb_options.size() << " jobs" << endl;
  cout << endl;
  cout << "To submit condor jobs, type : \n\n cwb_condor submit " << ofile_dag << endl;
  cout << endl;

  gSystem->Exit(0);
}
