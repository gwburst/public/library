# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Gabriele Vedovato, Mishra Tanmaya, Sergey Klimenko 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import ctypes
import ROOT
import numpy as np
import pickle
import xgboost as xgb
import pandas as pd
#from ROOT import *
from ROOT import gSystem
from ctypes import *
import string
import time
import cwb_xgboost as cwb

# load wavelet library
if(os.getenv('_USE_CMAKE')==None):
  gSystem.Load("wavelet.so")
else:
  gSystem.Load("%s/lib64/cwb.so"%os.environ["HOME_WAT_INSTALL"])

# python3 cwb_xgboost.py ifile=iwave.root ofile=owave.root model=model.dat/json ulabel=tst1

def getopts(arguments):
   """
   function purpose:
      get command line input options
   """

   ulabel=""
   ifile=""
   ofile=""
   model=""
   config=""            # input xgb parameters
   search=""            # input search type (bbh/imbhb/blf/bhf/bld)
   fgetrhor=""
   rthr=0
   verbose=False
   model_ext=".dat"     # model file name default extension, for xgb_version>=1.7.1 is setup as '.json'
   how2use="\n how to use -> python3 cwb_xgboost_prediction.py ifile=wave.root model=model.dat/json ulabel=tst1\n"

   for s in arguments:
    if (s.find("ulabel=")!=-1):
      ulabel=s.replace("ulabel=","")
    elif (s.find("ifile=")!=-1):
      ifile=s.replace("ifile=","")
    elif (s.find("ofile=")!=-1):
      ofile=s.replace("ofile=","")
    elif (s.find("model=")!=-1):
      model=s.replace("model=","")
    elif (s.find("config=")!=-1):
      config=s.replace("config=","")
    elif (s.find("search=")!=-1):
      search=s.replace("search=","")
    elif (s.find("fgetrhor=")!=-1):
      fgetrhor=s.replace("fgetrhor=","")
    elif (s.find("verbose")!=-1):
      verbose=True
    elif (s.find("rthr=")!=-1):
      srthr=s.replace("rthr=","")
      rthr=float(srthr)
   if (len(ifile)==0):
     print("Error, no input file specified")
     print(how2use)
     exit(1)
   if (len(ofile)==0):
     print("Error, no output file specified")
     print(how2use)
     exit(1)
   if (len(model)==0):
     print("Error, no input model specified")
     print(how2use)
     exit(1)
   if (search!='bbh' and search!='imbhb' and search!='blf' and search!='bhf' and search!='bld'):
     print("Error, no input search specified, available options are: bbh/imbhb/blf/bhf/bld")
     print(how2use)
     exit(1)
   if (ifile.find(".root")==-1):
     print("Error, wrong input file extension, must be .root")
     print(how2use)
     exit(1)
   if (ofile.find(".root")==-1):
     print("Error, wrong output file extension, must be .root")
     print(how2use)
     exit(1)
   xgb_version = xgb.__version__
   tokens = xgb_version.split('.')
   if((int(tokens[0])>=1)&(int(tokens[1])>=7)&(int(tokens[2])>=1)): model_ext = ".json"
   if (model.find(model_ext)==-1):
     print("Error, xgboost version = "+xgb_version+" -> wrong output model file name extension, must be "+model_ext)
     print(how2use)
     exit(1)
   if (len(config)!=0):
     if (config.find(".py")==-1):
       print("Error, wrong input config extension, must be .py")
       print(how2use)
       exit(1)
   if (fgetrhor!=""):
     if (fgetrhor.find(".py")==-1):
       print("Error, wrong input fgetrhor extension, must be .py")
       print(how2use)
       exit(1)

   print("\ninput cwb_xgboost parameters\n")
   print(" input wave root file                   = ", ifile)
   print(" output wave root file                  = ", ofile)
   print(" xgb model                              = ", model)
   print(" xgb config                             = ", config)
   print(" search type                            = ", search)
   print(" getrhor function                       = ", fgetrhor)
   print(" rho_r threshold (select output events) = ", rthr)
   print(" ulabel                                 = ", ulabel)
   print(" verbose                                = ", verbose)
   print("\n")

   return ifile,ofile,model,config,search,fgetrhor,rthr,ulabel,verbose

def writefile(ofile,ifile,xpd,cuts,save_MLstat,verbose):
   """
   function purpose:
      write a new output wave root file where the old rho[1] is subsituted with the new XGB rhor statistic
      and save XGBoost weight in rho[2]

   input params:
      ifile:      input wave root file name
      xpd:        panda frame containig the the new rhor statistic, and the xgboost weight W
      cuts:       root cuts used for input ifile
      save_MLstat:if Thrue MLstat is saved into rho[2]
      verbose:    False/True -> disable/enable verbose output

   output params:
      ofile:      output wave root file name
   """

   # we use C++ to speed up the writing to root file
   ROOT.gInterpreter.Declare("  								\
     void dump_to_root(char* ifile, char* ofile, double* rhor, float rthr, char* cuts, double* W) {        \
       cout << ifile << endl;                                   				\
       cout << ofile << endl;                                   				\
       TFile iroot(ifile);                                      				\
       TTree *itree = (TTree*)iroot.Get(\"waveburst\");         				\
       int isize = itree->GetEntries();                         				\
       cout << isize << endl;                                   				\
       TTreeFormula* treeformula=NULL;								\
       TString scuts=cuts; scuts.ReplaceAll(\"(float)\",\"\");					\
       if(scuts!=\"\") {									\
         treeformula = new TTreeFormula(\"cuts\", scuts.Data(), itree);				\
         int err = treeformula->Compile(scuts.Data());						\
         if(err) {										\
           cout << \"cwb_xgboost_prediction: writefile - wrong input cuts \" << scuts << endl;	\
           exit(-1);										\
         }											\
       }											\
       float* rho = new float[3];                                                               \
       itree->SetBranchAddress(\"rho\",rho);							\
       TFile oroot(ofile,\"recreate\");                         				\
       TTree *otree = (TTree*)itree->CloneTree(0);             					\
       int osize=0;										\
       for(int i=0;i<isize;i++) {                              			 		\
         if(i%100000==0) cout << \"Write Progress ...\\t : \" << i << \" / \" << isize << endl; \
         if(rhor[i] >= rthr) {									\
           itree->GetEntry(i);                                    				\
           if(treeformula!=NULL) if(treeformula->EvalInstance()==0) continue;			\
           rho[1]=rhor[i];                                                                      \
           if(W!=NULL) rho[2]=W[i]; else rho[2]=-1;                                             \
           otree->Fill();                                         				\
           osize += 1;										\
         }											\
       }                                                        				\
       otree->Write();                                          				\
       return;											\
     }                                                          				\
   ")

   # write output file 
   print("Start writing the output ROOT file ... \n\n ", ofile, "\n")
   print("input cuts                         ... \n\n ", cuts, "\n")
   start = time.time()

   # Convert column with rhor statistic to numpy array
   xsize=len(xpd.columns)
   rhor = xpd.to_numpy()[:,xsize-1]                      #modified rhor
   if(save_MLstat): W = xpd.to_numpy()[:,xpd.columns.get_loc('MLstat')]   # xgboost weight
   else:            W = POINTER(c_double)()
   ROOT.dump_to_root(ifile,ofile,rhor,rthr,cuts,W)

   end = time.time()
   print("\nEnd of writing the output ROOT file. Elapsed time = ", end-start, " (sec)\n")



if __name__ == "__main__":

   #ROOT.ROOT.EnableImplicitMT()         #### BEWARE -> reshuffle entries -> the results are not reproducible !!!
					 ####           it is commented because we need to read/write entries with the same sequence

   # get input user parameters
   ifile,ofile,model,config,search,fgetrhor,rthr,ulabel,verbose=getopts(sys.argv[1:])
   #exit()

   # load default xgb_config function
   cwb.execfile(os.environ['CWB_SCRIPTS']+'/cwb_xgboost_config.py',globals(),locals())

   # user getrhor function
   if(fgetrhor!=""):
     cwb.execfile(fgetrhor,globals(),locals())

   # get input root wave file
   nifo = cwb.getnifo(ifile) 							# get number of detectors
   xgb_params,ML_list,ML_caps,ML_balance,ML_options=xgb_config(search,nifo) 	# get ML_caps params
   if (len(config)!=0):
     # load user xgb_config function, overwrite the default configuration
     cwb.execfile(config,globals(),locals())
   cwb.print_config("","",ML_caps,"",ML_options)

   # read input wave/mdc root file
   xpd=cwb.readfile(ifile,1,1,ML_caps,ML_options,verbose,False,False)

   # load trained XGBoost model
   print("Loading xgb model: " + model + "\n")
   XGB_clf = pickle.load(open(model, 'rb'))
   # set nthread
   XGB_clf._Booster.set_param('nthread', ML_options['nthread(prediction)'])
   # print XGB hyper parameters
   print(XGB_clf.get_xgb_params())
   #get the list of cWB parameters used in training
   ML_list = XGB_clf.get_booster().feature_names
   print("ML xgb parameters:\n")
   print(" ", ML_list, "\n")

   #calculate MLstat (p_xgb) ... output from XGBoost
   print("Start calculating MLstat ... ")
   start = time.time()
   split_loops_prediction=ML_options['split_loops(prediction)'] 
   if(split_loops_prediction==1):
     xpd['MLstat'] = XGB_clf.predict_proba(xpd[ML_list], iteration_range=None)[:,1]
   else:
     # This case is implemented to reduce memory requirements
     if (split_loops_prediction<=0 or split_loops_prediction>xpd.shape[0]):
       print("cwb_xgboost_prediction - error: split_loops(prediction) must be >0 and < size data frame -> ",xpd.shape[0])
       exit(1)
     zpd = np.array_split(xpd, split_loops_prediction)
     for i in range(split_loops_prediction):
       print("split prediction loop = ",i+1,"/",split_loops_prediction)
       zpd[i]['MLstat'] = XGB_clf.predict_proba(zpd[i][ML_list], iteration_range=None)[:,1]
     xpd = pd.concat(zpd)

   if(verbose): 
     columnsNamesArr = xpd.columns.values
     listOfColumnNames = list(columnsNamesArr)
     print("List Of Column Names" , listOfColumnNames, sep='\n')
     print( xpd )
   end = time.time()
   print("End calculating MLstat. Elapsed time = ", end-start, " (sec)\n")

   #calculate new combined detection statistic
   print("Calculating ML rho_r statistic ...")
   if(fgetrhor!=""):
     xpd = getrhor(xpd,search)		# user defined getrhor function
   else:
     xpd = cwb.getrhor(xpd,search)
   if(verbose): 
     columnsNamesArr = xpd.columns.values
     listOfColumnNames = list(columnsNamesArr)
     print("List Of Column Names" , listOfColumnNames, sep='\n')
     print( xpd )

   # write output wave root file
   writefile(ofile,ifile,xpd,ML_options['cuts(prediction)'],ML_options['save_MLstat(prediction)'],verbose)

   print("\n... end\n")
