#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago, Igor Yakushin, Sergey Klimenko
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess, os, sys, glob
from functools import reduce
from ligo.segments import segment, segmentlist
from ligo.segments.utils import tosegwizard, fromsegwizard
from collections import deque
import pickle
import framel
from cwb_online.Trigger import Trigger, root_dump_2_triggers
from cwb_online.detector import detector
from cwb_online.frame import FRAME
from cwb_online.root_python import extractlive
from cwb_online.run_utils import get_time, get_date_from_gps, tconvert, add_rootrc
from ligo.gracedb.rest import GraceDb, HTTPError
from lal import LIGOTimeGPS

class JOB:
    def __init__(self,cWB_conf,start,end,run_offset=0,launch_time=-1,status=0):
        self.run_offset=run_offset
        self.dir="%s/%s/%s/%d-%d"%(cWB_conf.run_dir,cWB_conf.jobs_dir,get_date_from_gps(str(start)),start,end)
        self.link="%s/%s/%s/%d-%d"%(cWB_conf.web_link,cWB_conf.jobs_dir,get_date_from_gps(str(start)),start,end)
        print(self.dir)
        if (status!=2):
            self.make_dirs()
        self.start=start
        self.end=end
        self.back=start
        self.launch_time=launch_time
        self.completion_time=-1
        self.triggers_dump_time=-1
        self.triggers_send_time=-1        
        self.status=status # 0 - not launched yet, 1 running, 2 finished successfully, 3 - failed, 4 - missing frames
        self.delay_start_ready=-1
        self.delay_start_launch=-1
        self.delay_launch_completion=-1
        self.delay_start_send=-1
        self.triggers_all=[]
        if (status!=2):
           self.dump_job()
        self.cats={}
    def __repr__(self):
        msg="""
        dir=%s, start=%d, end=%d, launch_time=%d, completion_time=%d, status=%s
        """ % (self.dir,self.start,self.end,self.launch_time,self.completion_time,self.interpret_status())
        return msg
    def interpret_status(self):
        if(self.status==0):
            return "Not launched yet"
        if(self.status==1):
            return "Running since %d"%(self.launch_time)
        if(self.status==2):
            return "Completed successfully at %d"%(self.completion_time)
        if(self.status==3):
            return "Failed at %d"%(self.completion_time)
        if(self.status==4):
            return "After trigger extration"
    def make_dirs(self):
        subdirs=["input","OUTPUT","tmp","config"]
        for subdir in subdirs:
           com="mkdir -p %s/%s"%(self.dir,subdir)
           a=subprocess.getstatusoutput(com)
        if (os.environ['SITE_CLUSTER']=="CASCINA"):
           add_rootrc(self.dir) 
    def generate_inputs(self,cWB_conf,detectors):
        ifo_to_index={}
        for i in range(len(cWB_conf.ifos)):
           ifo_to_index[cWB_conf.ifos[i]]=i
        cat1=segmentlist([segment(int(self.back)-cWB_conf.job_offset,int(self.end)+cWB_conf.job_offset)]);cat1.coalesce()
        self.cats["cat1"]=cat1
        for ifo in cWB_conf.ifos:
            infile="%s/input/%s.frames"%(self.dir,ifo)

            ifiles=detectors[ifo_to_index[ifo]].getframelist(self.back-cWB_conf.job_offset,self.end+cWB_conf.job_offset+2)
            f=open(infile,"w")
            print("\n".join(ifiles), file=f)
            f.flush()
            f.close()
            f=open("%s/input/%s_cat2.in"%(self.dir,ifo),"w")
            cat2_temp_seg=detectors[ifo_to_index[ifo]].getcat("cat2")
            if (abs(cat2_temp_seg)>0):
                 cat2_temp_seg&=cat1
                 self.cats["cat2_%s"%ifo]=cat2_temp_seg
                 for s in cat2_temp_seg:
                   print("%s %s"%(s[0],s[1]), file=f)
            else:
                 print("%s %s"%(int(self.back)-cWB_conf.job_offset,int(self.end)+cWB_conf.job_offset), file=f)
                 self.cats["cat2_%s"%ifo]=cat1
            f.close()
        f=open("%s/input/burst.in"%(self.dir),"w")
        print("%s %s"%(int(self.back)-cWB_conf.job_offset,int(self.end)+cWB_conf.job_offset), file=f)
        f.close()
        del ifo_to_index

    def launch(self,cWB_conf):
        print("In launch: dir=%s"%(self.dir))
        os.chdir(self.dir)
        com="%s/bin/online_net_zero %s"%(os.environ['HOME_WAT_INSTALL'],"%s/user_parameters.C"%(cWB_conf.config_dir))
        a=subprocess.getstatusoutput(com)
        if(cWB_conf.debug==1):
            print("Job launched")
            print(a)
    def publish(self,cWB_conf,segs,jobs_published,dets):
        print("Before dump_triggers()"); sys.stdout.flush()
        self.dump_triggers(cWB_conf,dets)
        self.check_triggers(cWB_conf,jobs_published)
        self.send_to_external_collaborations(cWB_conf)
        print("before extract_live()"); sys.stdout.flush()        
        self.extract_live()
        print("before web_page()"); sys.stdout.flush()        
        self.web_page(cWB_conf)
        print("after web_page()"); sys.stdout.flush()        
        self.status=4
        self.dump_job()
    def extract_live(self):
        root_files=glob.glob("%s/OUTPUT/wave*.root"%self.dir)
        print("%s/OUTPUT/*.root root_files: %i"%(self.dir,len(root_files)))
        live_file="%s/OUTPUT.merged/live.root"%self.dir
        print("live")
        if (not os.path.exists(live_file)):
             extractlive(root_files[0],live_file,self.start,self.end)
    def dump_triggers(self,cWB_conf,dets):
        subprocess.getstatusoutput("mkdir -p %s/OUTPUT.merged"%(self.dir))
        os.chdir("%s/OUTPUT.merged"%(self.dir))
        subprocess.getstatusoutput("mkdir -p TRIGGERS")
        rfiles=glob.glob("%s/OUTPUT/wave*.root"%(self.dir))
        print("1");sys.stdout.flush()
        for rfile in rfiles:
            print("rfile=%s"%rfile)
            self.triggers_all+=root_dump_2_triggers(cWB_conf,rfile,self,dets,self.run_offset) 
            print("-->len(triggers_all)=%d"%len(self.triggers_all));sys.stdout.flush()
        print("2");sys.stdout.flush()
        self.triggers_all=sorted(self.triggers_all,key=lambda x: x.time[1], reverse=True)
        print("3");sys.stdout.flush()
        self.triggers_dump_time=get_time()
        self.delay_start_dump=self.triggers_dump_time-self.start #time between the start time of the processed segment and the moment triggers are dumped
        self.delay_start_launch=self.launch_time-self.start #time between the start time of the processed segment and the moment cWB job was launched: h(t) generation + h(t) transfer + h(t) discovery
        self.delay_launch_completion=self.completion_time - self.launch_time #time it took to run cWB trigger production part
    def send_to_external_collaborations(self,cWB_conf):
        if(cWB_conf.debug==1):
            print("In send_to_external_collaborations")
        self.triggers_send_time=get_time()
        for trigger in self.triggers_all:
          if (trigger.selected>=3 and trigger.topublish==True):
            if(trigger.send_time>0):
                print("trigger %s has been already sent"%(repr(trigger.time[0])))
                continue
            if (hasattr(cWB_conf,"followup")):
                trigger.followup_skymap(cWB_conf,self.triggers_send_time)
            else:
                trigger.to_external_collaborations(cWB_conf,self.triggers_send_time)
    def check_triggers(self,cWB_conf,jobs_list):
      for trigger in self.triggers_all:
        coinc_jobs=[x for x in jobs_list if float(trigger.time[0])>int(x.back) and float(trigger.time[0])<int(x.end) and x!=self]
        tseg=segment(float(trigger.start[0]),float(trigger.stop[0]))
        tband=segment(float(trigger.left[0]),float(trigger.right[0]))
        for jj in coinc_jobs:
           for trigger2 in jj.triggers_all:
               if (tseg.intersects(segment(float(trigger2.start[0]),float(trigger2.stop[0]))) and tband.intersects(segment(float(trigger2.left[0]),float(trigger2.right[0]))) and trigger2.topublish==True):
                      trigger.topublish=False
                      if (len(trigger2.graceid)>0):
                         trigger.graceid=trigger2.graceid
                      else:
                          if (hasattr(cWB_conf,"followup")):
                              trigger.topublish=True
        self.dump_job()
    def dump_job(self):
        f=open("%s/job.pickle"%self.dir,"wb")
        pickle.dump(self,f,protocol=2)
        f.close()
    def web_page(self,cWB_conf):
        title="%s, cWB, all-sky, unmodeled, <br>job %d - %d (%s - %s),<br>generated at %s"%(cWB_conf.label, self.start, self.end, tconvert(self.start),tconvert(self.end),tconvert(get_time()))
        statistics_table=\
        """
        <table border=1>
        <tr>
        <th>Launch time</th><td>%d<td>%s</td>
        </tr>
        <tr>
        <th>Launch time - start of the interval</th><td>%d</td><td></td>
        </tr>
        <tr>
        <th>Trigger generation completion time</th><td>%d</td><td>%s</td>
        </tr>
        <tr>
        <th>Completion time - launch time</th><td>%d</td><td></td>
        </tr>
        <tr>
        <th>Triggers send time</th><td>%d</td><td>%s</td>
        </tr>
        <tr>
        <th>Total: send time - start of the interval</th><td>%d</d>
        </tr>
        <tr>
        <th>Log (zipped)</th><td><a href=\"log.zip\">log</a></td>
        </tr>
        <tr>
        <th>Job dir</th><td><a href=\".\">dir</a></td>
        </tr>
        </table>

        """ % (self.launch_time,tconvert(self.launch_time),self.delay_start_launch,self.completion_time,tconvert(self.completion_time),\
               self.delay_launch_completion, self.triggers_send_time,tconvert(self.triggers_send_time), self.delay_start_send)

        p_triggers=[x for x in self.triggers_all if x.topublish==True]
        if(len(p_triggers)>0):
            trigger_table_rows="\n".join([x[1].to_html_table_row(cWB_conf,x[0]) for x in zip(list(range(1,len(p_triggers)+1)),p_triggers)])
            trigger_table_header=p_triggers[0].table_header(cWB_conf,True)
        else:
            trigger_table_rows=""
            trigger_table_header="<tr><th>No triggers found</th></tr>"
        trigger_table="""
        <table border=1>
        %s
        %s
        </table>
        """%(trigger_table_header, trigger_table_rows)

        nop_triggers=[x for x in self.triggers_all if x.topublish==False]
        if(len(nop_triggers)>0):
            trigger_table_rows="\n".join([x[1].to_html_table_row(cWB_conf,x[0]) for x in zip(list(range(1,len(nop_triggers)+1)),nop_triggers)])
            trigger_table_header=nop_triggers[0].table_header(cWB_conf,True)
            trigger_table+="""
        <h3 align="center"> Triggers in this job but not reported because more significant in other job</h3>
        <table border=1>
        %s
        %s
        </table>
            """%(trigger_table_header, trigger_table_rows)

        selection_explaination=\
        """
        <table>
        <tr><th>0</th><td>no postproduction cuts;</td></tr>
        <tr><th>1</th><td>only coherent triggers selected but no cut on rho or DQ;</td></tr>
        <tr><th>2</th><td>rho cut for internal LSC follow up with qscans, ced, etc.;</td></tr>
        <tr><th>3</th><td>rho cut for external collaborations;</td><tr>
        <tr><th>4</th><td>rho cut and DQ and vetoes for external collaborations;</td></tr>
        <tr><th>5</th><td>rho cut and DQ and vetoes that would most likely be chosen for offline papaper.</td></tr>
        </table>
        """
        msg=\
        """
        <html>
        <title>%s</title>
        <body>
        <h1 align=center>%s</h1>
        %s
        <h2 align=center>Triggers</h2>
        %s
        <h3>Explaining selection values from the trigger table:</h3>
        %s
        </body>
        </html>

        """ % (title,title,statistics_table, trigger_table, selection_explaination)
        fn="%s/job.html"%(self.dir)
        f=open(fn,"w")
        print(msg, file=f)
        f.close()
    def zip_files(self):
       com="gzip %s/log"%(self.dir) 
       subprocess.getstatusoutput(com)
       com="gzip %s/input/*"%(self.dir) 
       subprocess.getstatusoutput(com)
       com="mv %s/OUTPUT/job*.root %s/input/."%(self.dir,self.dir)
       subprocess.getstatusoutput(com)
    def backup_dir(self,cWB_conf):
       new_dir=self.dir.replace(cWB_conf.run_dir,cWB_conf.backup_dir)
       subprocess.getstatusoutput("mkdir -p %s"%new_dir)
       subprocess.getstatusoutput("mkdir %s/input %s/tmp"%(new_dir,new_dir))
       subprocess.getstatusoutput("cp %s/log.gz %s/."%(self.dir,new_dir))
       subprocess.getstatusoutput("cp %s/job.html %s/."%(self.dir,new_dir))
       subprocess.getstatusoutput("cp -r %s/OUTPUT %s/."%(self.dir,new_dir))
       subprocess.getstatusoutput("cp -r %s/OUTPUT.merged %s/."%(self.dir,new_dir))
       self.dir=self.dir.replace(cWB_conf.run_dir,cWB_conf.backup_dir)
       for t in self.triggers_all:
           t.dir=t.dir.replace(cWB_conf.run_dir,cWB_conf.backup_dir)
           t.path=t.path.replace(cWB_conf.run_dir,cWB_conf.backup_dir)
           t.root_file=t.root_file.replace(cWB_conf.run_dir,cWB_conf.backup_dir)
           t.job.dir=t.job.dir.replace(cWB_conf.run_dir,cWB_conf.backup_dir)
       self.dump_job()
    def backup_web(self,cWB_conf):
       new_dir=self.link.replace(cWB_conf.web_link,cWB_conf.web_dir)
       subprocess.getstatusoutput("mkdir -p %s"%new_dir)
       subprocess.getstatusoutput("cp %s/job.html %s/."%(self.dir,new_dir))
       for t in self.triggers_all:
           t_dir=t.dir.replace(self.dir,new_dir)
           subprocess.getstatusoutput("mkdir -p %s"%t_dir)
           subprocess.getstatusoutput("cp %s %s"%(t.path,t.path.replace(self.dir,new_dir)))

class Tofollowup:
    def __init__(self,trigger):
        self.graceid=trigger.graceid
        self.time=float(trigger.time[0])
        self.seg=segment(float(trigger.start[0]),float(trigger.stop[0]))
        self.status=0 #0: not in coincidence time, 1: running, 2: done, -1: waiting
        self.dir=[]
        self.running_dir=[]
    def checkinside(self,i_triggers):
        if (len(i_triggers)==0):
           return False
        else:
           o_triggers=list(filter(lambda x: x.graceid==self.graceid,i_triggers))
           if (len(o_triggers)==0):
               return False
           else:
               return True
    def finddirs(self,i_dirs,cWB_conf):
        back=cWB_conf.seg_duration-cWB_conf.moving_step
        self.dir=list(filter(lambda x: int(x.split("/")[-1].split("-")[0])-back<self.time and int(x.split("/")[-1].split("-")[1])>self.time,i_dirs))
        if (len(self.dir)>0):
            self.status=-1
            self.dir.sort()
    def getnextdir(self):
        nexts=[x for x in self.dir if x not in self.running_dir]
        nexts.sort()
        if (len(nexts)>0):
            if (not os.path.exists("%s/log"%nexts[0])):
                return nexts[0]
            else:
                return None
        else:
            return None
    def runnext(self,offset,cWB_conf):
        dd=self.getnextdir()
        if(dd != None):
            jstart=int(dd.split("/")[-1].split("-")[0])
            jend=int(dd.split("/")[-1].split("-")[1])
            f=open("%s/job.pickle"%dd,"rb")
            job=pickle.load(f)
            f.close()
            #job=JOB(cWB_conf,jstart,jend,offset,get_time(),2)
            #back=cWB_conf.seg_duration-cWB_conf.moving_step
            #temp_back=jstart-back
            N_missing_files=0
            for ifo,ifile in zip(cWB_conf.ifos,cWB_conf.ifo_files):
                t_det=detector(ifo,"%s/%s"%(cWB_conf.config_dir,ifile.split("/")[-1]),True)
                f=open("%s/input/%s.frames"%(dd,ifo),"w")
                pattern=t_det.getframelist(job.back-cWB_conf.job_offset,job.end+cWB_conf.job_offset)
                print("\n".join(pattern), file=f)
                f.close()
                for fn in pattern:
                    if(not os.path.exists(fn)):
                        N_missing_files+=1
            if (N_missing_files==0):
                job.launch(cWB_conf)
                self.status=1
                job.dump_job()
                self.running_dir.append(dd)
            else:
                job.status=-1
                self.running_dir.append(dd)
        else:
            self.status=0

class cWB:
    def __init__(self,cWB_conf, run_offset=0, run_end=-1):
        self.run_offset=run_offset
        self.start_loop = get_time(self.run_offset)
        self.jobs_completed=deque([])
        self.jobs_running=deque([])
        self.jobs_failed=deque([])
        self.jobs_published=deque([])
        self.detectors=[]
        for ifo,ifile in zip(cWB_conf.ifos,cWB_conf.ifo_files):
         self.detectors.append(detector(ifo,"%s/%s"%(cWB_conf.config_dir,ifile.split("/")[-1])))
         self.detectors[-1].setcats(cWB_conf)
        self.run_end=run_end
        self.last_launch=self.detectors[-1].start_read
        self.segs_global=segmentlist([])
        if (hasattr(cWB_conf,"followup")):
           self.trigger_to_follow=[] 
    def schedule(self,cWB_conf):
        current_time=get_time(self.run_offset)
        not_done=segmentlist([segment(self.last_launch,current_time)])
        new_segs = not_done & self.segs_global; new_segs.coalesce()
        print("new_segs=%s"%(repr(new_segs)))
        self.scheduling_policy_simplest(cWB_conf,new_segs)
    def scheduling_policy_simplest(self,cWB_conf,segs):
        if(len(segs)==0 or max([abs(x) for x in segs])<cWB_conf.moving_step+cWB_conf.job_offset):
            return
        if (max([abs(x) for x in self.segs_global])<cWB_conf.seg_duration+2*cWB_conf.job_offset):
            return
        for seg in segs:
            if(abs(seg)<cWB_conf.moving_step+cWB_conf.job_offset):
                continue
            a=segmentlist([segment(seg[0]-cWB_conf.job_offset,seg[0])])
            a = a & self.segs_global
            if(len(a)==1 and a[0][1]==seg[0]):
                jstart=a[0][0]+cWB_conf.job_offset
            else:
                jstart=seg[0]+cWB_conf.job_offset
            jend=jstart+cWB_conf.moving_step
            back=cWB_conf.seg_duration-cWB_conf.moving_step
            temp_back=jstart-back
            while(jend + cWB_conf.job_offset <= seg[1]):
                whiteseg=segmentlist([segment(temp_back-cWB_conf.job_offset,jend+cWB_conf.job_offset)])
                whiteseg = whiteseg & self.segs_global
                if (abs(whiteseg)<cWB_conf.seg_duration+2*cWB_conf.job_offset):
                    jend=jend+cWB_conf.moving_step
                    temp_back += cWB_conf.moving_step
                else:
                    job=JOB(cWB_conf,jstart,jend,self.run_offset)
                    job.back=temp_back
                    if(job.launch_time<0):
                       job.status=1
                       job.generate_inputs(cWB_conf,self.detectors)
                       if (not hasattr(cWB_conf,"followup")):
                         job.launch_time = get_time()
                         job.launch(cWB_conf)
                         self.jobs_running.append(job)
                       job.dump_job()
                       self.last_launch=job.end
                       self.update_segments(cWB_conf)
                    jstart=jend
                    jend=jstart+cWB_conf.moving_step
                    temp_back=jstart-back
                    del job
    def followup(self,cWB_conf):
        l_end=get_time()
        l_start=l_end-cWB_conf.look_back
        date_e=get_date_from_gps(str(l_end))
        date_s=get_date_from_gps(str(l_start))
        i_dirs=glob.glob("%s/RUN_cWB/JOBS/%s/*"%(cWB_conf.followup,date_e))
        o_dirs=glob.glob("%s/JOBS/%s/*"%(cWB_conf.run_dir,date_e))
        if (date_e!=date_s):
            i_dirs+=glob.glob("%s/RUN_cWB/JOBS/%s/*"%(cWB_conf.followup,date_s))
            o_dirs+=glob.glob("%s/JOBS/%s/*"%(cWB_conf.run_dir,date_s))
        for d in i_dirs:
            triggers=glob.glob("%s/OUTPUT.merged/TRIGGERS/*/trigger.txt"%d)
            if (len(triggers)>0):
                f=open("%s/job.pickle"%d,"rb")
                i_job=pickle.load(f)
                f.close()
                for t in i_job.triggers_all:
                    if (len(t.graceid)>0 and t.topublish==True):
                        temp=Tofollowup(t)
                        if (not temp.checkinside(self.trigger_to_follow)):
                           temp.finddirs(o_dirs,cWB_conf)
                           temp.runnext(self.run_offset,cWB_conf)
                           self.trigger_to_follow.append(temp) 

    def check_follow(self,cWB_conf):
        for t in self.trigger_to_follow:
          if (t.status==-1):
            N_missing_files=0
            for ifo,ifile in zip(cWB_conf.ifos,cWB_conf.ifo_files):
                pattern=open("%s/input/%s.frames"%(t.running_dir[-1],ifo)).readlines()
                for fn in pattern:
                    if(not os.path.exists(fn.replace("\n",""))):
                         N_missing_files=+1
            if (N_missing_files==0):
                f=open("%s/job.pickle"%t.running_dir[-1],"rb")
                i_job=pickle.load(f)
                f.close()
                i_job.launch(cWB_conf)
                t.status=1
                i_job.dump_job()
          if (t.status==1):
            d=t.running_dir[-1]
            f=open("%s/job.pickle"%d,"rb")
            job=pickle.load(f)
            f.close()
            if (os.path.exists(job.dir+"/OUTPUT/finished")):
               job.status=2
               job.completion_time=get_time()
               job.dump_triggers(cWB_conf,self.detectors)
               if (len(job.triggers_all)>0):
                   trigger=list(filter(lambda x: abs(float(t.time)-float(x.time[0]))<.1, job.triggers_all))
                   if (len(trigger)==1):
                     trigger[0].graceid=t.graceid
                     trigger[0].followup_skymap(cWB_conf,get_time())
                     t.status=2
               else:
                   t.runnext(self.run_offset,cWB_conf)
               job.dump_job()
    def check(self,cWB_conf):
        c=get_time()
        print("c=%s"%c)
        if(cWB_conf.debug==1):
           print("In check()")
           print("n=%d"%(len(self.jobs_running)))
        aa=deque([])
        while(len(self.jobs_running)>0):
            job=self.jobs_running.pop()
            if (os.path.exists(job.dir+"/OUTPUT/finished")):
                job.status=2
                job.completion_time=c
                job.publish(cWB_conf,self.segs_global,self.jobs_published,self.detectors)
                self.jobs_published.append(job)
                job.dump_job()
                job.zip_files()
                if (hasattr(cWB_conf,"backup_dir")):
                   job.backup_dir(cWB_conf)
                if (hasattr(cWB_conf,"copy_web")):
                   job.backup_web(cWB_conf)
            elif(c-job.launch_time>cWB_conf.job_timeout):
                job.status=3
                self.jobs_failed.append(job)
                job.completion_time=c
            else:
                aa.append(job)
            try:
                job.dump_job()
            except Exception as e:
                print("job.dump failed")
                print(e)
        self.jobs_running=aa

        if(cWB_conf.debug==1):
            print("In check:")
            print("*"*10)            
            print("Jobs running: %d"%(len(self.jobs_running)))
            print("*"*10)
            print(("-"*5+"\n").join(map(repr,self.jobs_running)))
            print("*"*10)                                    
            print("Jobs completed: %d"%(len(self.jobs_completed)))
            print("*"*10)
            print(("-"*5+"\n").join(map(repr,self.jobs_completed)))
            print("*"*10)                        
            print("Jobs failed: %d"%(len(self.jobs_failed)))
            print("*"*10)
            print(("-"*5+"\n").join(map(repr,self.jobs_failed)))
            print("*"*10)
            print("Jobs published: %d"%(len(self.jobs_published)))
            print("*"*10)
            print(("-"*5+"\n").join(map(repr,self.jobs_published)))
            print("*"*10)                                    
        self.jobs_failed=[x for x in self.jobs_failed if segment(x.start-cWB_conf.job_offset, x.end+cWB_conf.job_offset) in \
                                segment(self.cycle_start-cWB_conf.job_timeout, self.cycle_start)]
    def publish(self,cWB_conf):
        try:
            #job=self.jobs_completed.popleft()
            job=self.jobs_published.popleft()
        except:
            print("Why am I here: len(jobs_completed)=%d"%(len(self.jobs_completed)))
            return
        while(True):
            #job.publish(cWB_conf,self.segs_global,self.jobs_published,self.detectors)
            #self.jobs_published.append(job)
            job.dump_job()
            job.zip_files()
            if (hasattr(cWB_conf,"backup_dir")):
                job.backup_dir(cWB_conf)
            try:
                #job=self.jobs_completed.popleft()
                job=self.jobs_published.popleft()
            except:
                print("Why am I here: len(jobs_completed)=%d"%(len(self.jobs_completed)))                
                break
        self.jobs_published=[x for x in self.jobs_published if segment(x.start-cWB_conf.job_offset, x.end+cWB_conf.job_offset) in \
                            segment(self.cycle_start-5*cWB_conf.job_timeout, self.cycle_start)]
    def run(self,cWB_conf):
        while(True):
            try:
                self.cycle_start=get_time(self.run_offset)
                if(self.run_end>0 and self.cycle_start > self.run_end):
                    print("Run is finished, run_end=%s, current_time=%s"%(self.run_end, self.cycle_start))
                    return
                self.cycle(cWB_conf)
            except Exception as e:
                print("The cycle broke due to the exception %s"%(repr(e)))
                print("Restarting ...")
                sys.exit(1)
            sys.stdout.flush()
            sys.stderr.flush()
    def copy(self,cWB_conf):
      fn_combined_global="%s/%s/seg_global.txt"%(cWB_conf.run_dir,cWB_conf.seg_dir)
      try:
        f=open(fn_combined_global)
        self.segs_global=fromsegwizard(f)
        f.close()
      except Exception as e:
        self.segs_global=segmentlist([])
      if (abs(self.segs_global)>0):
        started_dirs=glob.glob("%s/%s/????-??-??"%(cWB_conf.run_dir.replace(cWB_conf.online_dir,cWB_conf.production_dir),cWB_conf.jobs_dir))
        for d in started_dirs:
          dirs_orig=glob.glob("%s/*"%d) 
          dirs_copied=glob.glob("%s/*"%d.replace(cWB_conf.production_dir,cWB_conf.online_dir))
          print("%s %i %i"%(d,len(dirs_orig),len(dirs_copied)))
          if (len(dirs_orig)>len(dirs_copied)):
            for o in dirs_orig:
             c=o.replace(cWB_conf.production_dir,cWB_conf.online_dir)
             if (os.path.exists("%s/OUTPUT/finished"%o) and not os.path.exists("%s/OUTPUT/finished"%c)):
              try:
               f=open("%s/job.pickle"%o,"rb")
               j=pickle.load(f)
               f.close()
               if (j.status==2):
                start=j.start
                stop=j.end
                job=JOB(cWB_conf,start,stop)
                job.back=j.back
                job.launch_time=j.launch_time
                job.completion_time=j.completion_time
                job.status=j.status # 0 - not launched yet, 1 running, 2 finished successfully, 3 - failed, 4 - missing frames
                job.delay_start_ready=j.delay_start_ready
                job.delay_start_launch=j.delay_start_launch
                job.dump_job()
                com="cp -r %s/OUTPUT %s/."%(o,c)
                print(com)
                subprocess.getstatusoutput(com)
                com="cp -r %s/input/* %s/input/."%(o,c)
                print(com)
                subprocess.getstatusoutput(com)
                #self.jobs_completed.append(job)
                job.publish(cWB_conf,self.segs_global,self.jobs_published,self.detectors)
                self.jobs_published.append(job)
                job.dump_job()
                job.zip_files()
                if (hasattr(cWB_conf,"backup_dir")):
                   job.backup_dir(cWB_conf)
                del job
              except Exception as e:
               print(e)
      print(repr(self.segs_global))
      print(len(self.jobs_completed))
    def cycle(self,cWB_conf):
      if(cWB_conf.debug==1):
            print("="*20 + "Cycle start time=%d"%(self.cycle_start) + "="*20)
      if (hasattr(cWB_conf,"production_dir")):
        if(cWB_conf.debug==1):
           print("In cycle before copy"); sys.stdout.flush()                
        self.copy(cWB_conf)
      else:
        if(cWB_conf.debug==1):
           print("In cycle before discover_segments"); sys.stdout.flush()
        self.discover_segments(cWB_conf)
        if(cWB_conf.debug==1):
           print("In cycle before schedule"); sys.stdout.flush()        
        self.schedule(cWB_conf)
        if(cWB_conf.debug==1):
           print("In cycle before check"); sys.stdout.flush()                
        if (hasattr(cWB_conf,"followup")):
           self.followup(cWB_conf)
           self.check_follow(cWB_conf)
        else:
           self.check(cWB_conf)
      #print("In cycle before publish"); sys.stdout.flush()                
      #self.publish(cWB_conf)
      if(cWB_conf.debug==1):
        print("In cycle at the end"); sys.stdout.flush()                
    def update_segments(self,cWB_conf):
        fn_combined_global="%s/%s/seg_global.txt"%(cWB_conf.run_dir,cWB_conf.seg_dir)
        self.segs_global=self.detectors[0].updatefile(self.segs_global,fn_combined_global,False,True)
        for ifo in range(len(cWB_conf.ifos)):
           self.detectors[ifo].update_segments(cWB_conf)
    def discover_segments(self,cWB_conf):
        s={}
        frames_seg={}
        for ifo in range(len(cWB_conf.ifos)):
            frames_seg[ifo]=self.detectors[ifo].findsegment(cWB_conf)
            s[ifo]=self.detectors[ifo].getcat("cat1")
        try:
            combined_global=reduce(lambda x,y: (x & y).coalesce(), list(s.values()))
        except Exception as e:
            print("Is it here: e=%s"%(repr(e)))
            combined_global=segmentlist([])
        self.segs_global|=combined_global
        del s
        del frames_seg

def run(cWB_conf):
    current_time=get_time()
    print("current_time=%d"%(current_time))
    if (hasattr(cWB_conf,"run_start")):
        run_start=int(cWB_conf.run_start)
    else:
        run_start=current_time
    print("run_start=%d"%(run_start))
    run_offset=current_time - run_start
    print("run_offset=%d"%(run_offset))
    if (hasattr(cWB_conf,"run_end")):
        run_end=int(cWB_conf.run_end)
    else:
        run_end=-1
    print("run_end=%d"%(run_end))

    cwb=cWB(cWB_conf,run_offset,run_end)
    cwb.run(cWB_conf)

