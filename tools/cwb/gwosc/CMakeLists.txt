install(
        DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/
        DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}/cwb/gwosc
)

#Get the full path to the rootlogon file
get_filename_component(CWB_MAKEFILE_GWOSC
                       ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_SYSCONFDIR}/cwb/gwosc/Makefile.gwosc
                       ABSOLUTE
                       BASE_DIR ${CMAKE_BINARY_DIR})

file(
    GENERATE OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/cwb_gwosc
    CONTENT "#!/usr/bin/env bash\nmake -f ${CWB_MAKEFILE_GWOSC} $@"
)

install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/cwb_gwosc
    TYPE BIN
    PERMISSIONS OWNER_WRITE
                OWNER_READ GROUP_READ WORLD_READ
                OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE
)