#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys,os
import cwb_online.run as run
from cwb_online.run import JOB
import cwb_online.run_ts as run_ts
import cwb_online.web_pages as web_pages
import cwb_online.directory as directory
import cwb_online.findtrigger as findtrigger
import cwb_online.check_restart as check_restart

def main():
   if (os.path.exists(sys.argv[1])):
     import importlib.util
     spec = importlib.util.spec_from_file_location('what.ever', sys.argv[1])
     cWB_conf = importlib.util.module_from_spec(spec)
     spec.loader.exec_module(cWB_conf)
     cWB_conf.jobs_dir="JOBS"
     cWB_conf.seg_dir="SEGMENTS"
     cWB_conf.summaries_dir="SUMMARIES"
     cWB_conf.postprod_dir="POSTPRODUCTION"
     if (hasattr(cWB_conf,'xgboost')):
         if (not hasattr(cWB_conf,'id_rho')):
             cWB_conf.id_rho=1
   elif (sys.argv[1]!="trigger" and sys.argv[1]!="restart"):
     print(sys.argv[1],"does not exists, please check path")
     exit()

   if (sys.argv[2]=="clean"):
      directory.directory(cWB_conf,sys.argv[1],True)
   elif (sys.argv[2]=="update"):
      directory.directory(cWB_conf,sys.argv[1],False,False)
   elif (sys.argv[2]=="xgboost"):
      directory.directory(cWB_conf,sys.argv[1],False,False,True)
   elif  (sys.argv[2]=="create"): 
      directory.directory(cWB_conf,sys.argv[1],False,True)
   elif  (sys.argv[1]=="trigger"): 
      try:
        import importlib.util
        spec = importlib.util.spec_from_file_location('what.ever', "%s/cWB_conf.py"%sys.argv[3])
        cWB_conf = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(cWB_conf)
        if (hasattr(cWB_conf,'xgboost')):
          cWB_conf.id_rho=1
        findtrigger.findtrigger(cWB_conf,float(sys.argv[2]),sys.argv[3])
      except:
        import importlib.util
        spec = importlib.util.spec_from_file_location('what.ever', "%s/cWB_conf.py"%os.getcwd())
        cWB_conf = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(cWB_conf)
        if (hasattr(cWB_conf,'xgboost')):
          cWB_conf.id_rho=1
        findtrigger.findtrigger(cWB_conf,float(sys.argv[2]))
   elif (sys.argv[1]=='restart'):
       check_restart.check_restart(sys.argv[2],sys.argv[3],sys.argv[4])
   elif (sys.argv[2]=='zero'):
       run.run(cWB_conf)
   elif (sys.argv[2]=='bkg'):
       run_ts.run_ts(cWB_conf)
   else:
       if (sys.argv[2].find("loop-")!=-1):
         web_pages.web_pages(sys.argv[2].replace("loop-",""),True,cWB_conf)
       else:
         web_pages.web_pages(sys.argv[2],False,cWB_conf)

if(__name__=="__main__"):
    sys.exit(main())
