# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Gabriele Vedovato, Mishra Tanmaya, Sergey Klimenko 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import io
import gc
import sys
import os
import ctypes
import warnings
import ROOT
import numpy as np
import pickle
import xgboost as xgb
import pandas as pd
#from ROOT import *
from ROOT import gSystem, TTree, TFile
from ctypes import *
import string
import time
import matplotlib
matplotlib.use('Agg')
from matplotlib import rcParams
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from sklearn.model_selection import train_test_split
import matplotlib.ticker as ticker
from matplotlib.ticker import MultipleLocator
from matplotlib.ticker import AutoMinorLocator, FormatStrFormatter
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import f1_score
from sklearn.metrics import auc
import statsmodels.api as sm
import uproot
import copy

#plotting options
rcParams["text.usetex"] = True
rcParams["font.serif"] = "Times New Roman"
rcParams["font.family"] = "Serif"
rcParams["xtick.labelsize"]=28
rcParams["ytick.labelsize"]=28
rcParams["legend.fontsize"]=22
rcParams["axes.labelsize"]=28

# load wavelet library
if(os.getenv('_USE_CMAKE')==None):
  gSystem.Load("wavelet.so")
else:
  gSystem.Load("%s/lib64/cwb.so"%os.environ["HOME_WAT_INSTALL"])

# cwb xgboost library

def convert2list(ipd, ikey=""):
    """
    function purpose:
       convert panda data series to lists
       this function is used when uproot version is >= 5
       ex: convert rho array into lists rho0, rho1, rho2

    input params:
       ipd:      input data frame
       ikey:     if not "" only ikey array is converted
    output params:
       if ikey=="" then return converted data frame else ipd is converted
    """

    opd = pd.DataFrame()
    for key,dtype in zip(ipd.keys(),ipd.dtypes):
      if (dtype=="awkward" and ikey=="") or (dtype=="awkward" and key==ikey):
        N = len(ipd[key].iloc[0])
        xvars = []
        for n in range(0,N): xvars.append(key+str(n))
        opd[xvars] = ipd[key].tolist()
        xvars.clear()
      else:
        opd[key] = ipd[key]
      ipd.drop(key, axis=1, inplace=True)
    return opd

def execfile(filepath, globals=None, locals=None):
    """
    function purpose:
       execute python script

    input params:
       filepath: python script path
       globals:  global variables
       locals:   local variables
    """

    if globals is None:
        globals = {}
    globals.update({
        "__file__": filepath,
        "__name__": "__main__",
    })
    with open(filepath, 'rb') as file:
        exec(compile(file.read(), filepath, 'exec'), globals, locals)

def getnifo(ifile):
   """
   function purpose:
      get number of detectors from the first tree wave entry

   input params:
      ifile: wave root file path
   """

   if (ifile.find(".py")==-1):
     ifname = ifile
   else:
     # load xfile list from python xfile
     execfile(ifile,globals())
     ifname = flist[0]
     if type(ifname) is list: ifname = ifname[0]

   if type(ifname) is not str:
     print("\nError:\n",ifile,"\nthe entry\n",ifname,"\nmust be a string or a list")
     print("Format = ['fname','cut',fraction] and fraction must be a number in the range [0,1]\n")
     exit(1)

   iroot = TFile(ifname)
   itree = iroot.Get("waveburst")
   isize = itree.GetEntries()
   if(isize>0):
     itree.GetEntry(0)
     nifo = itree.ndim
   else:
     print('\nError: no entries in the wave input file\n')
     print(' '+ ifname+'\n')
     exit(0)

   print('\nnumber of detectors = '+str(nifo)+'\n')

   return nifo

def getcapname(feature,cap):
   """
   function purpose:
      return formatted cap name

   input params:
      feature: feature name (eg: rho0, Qa, Qp)
      cap:     cap value (eg: 8)

   return:
      formatted cap name (eg: rho0,8 -> rho0_8d0)
      only the first digit is used: (eg. rho0,8.12 -> rho_8d1)
   """

   return (feature+'_'+str("{:.1f}".format(cap))).replace(".","d")

def update_ML_list(ML_list,ML_defcaps,ML_caps):
   """
   function purpose:
      update the default ML_caps in the ML_list

   input params:
      ML_list:    feature list (defined in cwb_xgboost_config.py)
      ML_defcaps: default MLcaps (defined in cwb_xgboost_config.py)
      ML_caps:    updated ML_caps list

   output:
      ML_list:    updated feature list
   """

   # remove from ML_list the default caps (ML_defcaps) features
   for cap_name, cap_value in ML_defcaps.items():
     if(cap_value>0):
        full_cap_name = getcapname(cap_name,cap_value)
     elif(cap_value==0):
        full_cap_name = cap_name
     for feature in ML_list:
        if(feature==full_cap_name): ML_list.remove(feature)

   # updat ML_list with new ML_caps
   for cap_name, cap_value in ML_caps.items():
     if(cap_value>0):
        ML_list.append(getcapname(cap_name,cap_value))
     elif(cap_value==0):
        ML_list.append(cap_name)

def readfile(xfile,classifier,xfrac,ML_caps,ML_options,verbose,xzl,checkifar=False,data_cuts=None):
   """
   function purpose:
      read feature list (defined in ML_list) from the input tree root file and return a default list of features in a panda frame

   input params:
      xfile:      input wave root file path
      classifier: 0/1 -> bkg/sim
      xfrac:      [0,1] -> fraction of entries randomly selected from the input tree root file
      ML_caps:    ML caps list
      ML_options: ML options list
      verbose:    False/True -> disable/enable verbose output
      xzl:        False/True -> disable/enable zero lag entries
      checkifar:  False/True -> disable/enable check if ifar is contained in the tree root list
      data_cuts:  data cuts, python syntax used to cut pandas dataframes using pandas query option, use xvars to cut root data tree. Eg: 'rho[0]>7&&netcc[0]>0.7' -> 'rho0>7 & netcc0>0.7'

   return:
      return a default list of features in a panda frame
   """

   # retreive random seed
   seed=ML_options['readfile(seed)']
   np.random.seed(seed=seed)
   print("\nreadfile(seed) = ",seed,"\n")

   # ---> variables used by xgboost

   # retreive readfile parameters from ML_options
   xvars=copy.deepcopy(ML_options['readfile(vars)'])   # deepcopy is required to make xvars independent from the original 'readfile(vars)' configuration parameter
   print("\nreadfile(vars) = \n\n",xvars,"\n")

   # add mandatory parameters
   if('rho'     not in xvars): xvars.append('rho')         # used in the new definition of rho0
   if('penalty' not in xvars): xvars.append('penalty')     # used in the new definition of rho0
   if('ecor'    not in xvars): xvars.append('ecor')        # used in the new definition of rho0 and Qp
   if('Qveto'   not in xvars): xvars.append('Qveto')       # used in Qa and Qp

   """
   # add parameters used to select sim events in cwb_xgboost_report_prediction.py
   if(classifier==1):
     if('factor' not in xvars): xvars.append('factor')
     if('type'   not in xvars): xvars.append('type')
   """

   # add parameters used in zero lag selection
   if(xzl):
     if('lag'  not in xvars): xvars.append('lag')
     if('slag' not in xvars): xvars.append('slag')

   print("Start reading the input ROOT file ... \n\n ", xfile, "\n")
   start = time.time()

   # Convert inputs to format readable by machine learning tools
   nfiles = 0
   FILE   = []       # array of input file names
   TREE   = []       # array of input trees
   DFRAME = []       # array of data frames
   CUT    = []       # array of input cuts
   FRAC   = []       # array of input fraction values
   if (xfile.find(".py")==-1):
     events = uproot.open("%s:waveburst"%xfile)      # read wave root file
     colNames = events.keys()
   else:
     # load xfile list from python xfile
     execfile(xfile,globals())
     print('\nInput file list\n')
   
     for element in flist:
       print(element)
       cut = ''
       frac = 1.0
       if type(element) is list:
          ifname = element[0]
          if(len(element)==2): cut = element[1]
          if(len(element)==3):
            cut = element[1]
            if type(element[2]) is float or type(element[2]) is int:
              if(element[2]<0 or element[2]>1):
                print("\nError:\n",xfile,"\nthe third entry of\n",element,"\nmust be a number in the range [0,1]\n")
                exit(1)
              else: frac = element[2]
            else:
              print("\nError:\n",xfile,"\nthe third entry of\n",element,"\nis not a number.")
              print("Format = ['fname','cut',fraction] and fraction must be a number in the range [0,1]\n")
              exit(1)
          if(len(element)>3):
            print("\nError:\n",xfile,"\nnumber of entries of\n",element,"\nmust be <=3.")
            print("Format = ['fname','cut',fraction] and fraction must be a number in the range [0,1]\n")
            exit(1)
       if type(element) is set:
          print("\nError:\n",xfile,"\nthe entry\n",element,"\nmust be a string or a list")
          print("Format = ['fname','cut',fraction] and fraction must be a number in the range [0,1]\n")
          exit(1)
       if type(element) is str:
          ifname = element
       nfiles += 1
       FILE.append(ifname)
       CUT.append(cut)
       FRAC.append(frac)
       TREE.append(uproot.open("%s:waveburst"%ifname))     # read each wave root file in file list
     colNames = TREE[0].keys()
     print('\n')
   
   if(verbose): print("Colnames:\n{}\n".format(colNames))

   nifo = getnifo(xfile) 			# get number of detectors

   # check if 'ifar' & 'chunk are in colNames'
   bifar=False
   bchunk=False
   bnet=False
   bae=False
   for var in colNames:
     if(var=='ifar'):
       bifar=True
       if('ifar'  not in xvars): xvars.append('ifar')
     if(var=='chunk'):
       bchunk=True
       if('chunk' not in xvars): xvars.append('chunk')
     if(var=='net'):
       bnet=True
       if('net'   not in xvars): xvars.append('net')
     if(var=='ae'):
       bae=True
       if('ae'    not in xvars): xvars.append('ae')

   if((checkifar==True) and (bifar==False)):
     print('\nError: ifar is not present in tree data file\n')
     exit(1)

   # check targeted searches option
   targeted=ML_options['targeted']
   if(targeted==True):  # add features used for targeted searches
     if('erA'   not in xvars): xvars.append('erA')
     if('phi'   not in xvars): xvars.append('phi')
     if('theta' not in xvars): xvars.append('theta')
     if('bp'    not in xvars): xvars.append('bp')
     if('bx'    not in xvars): xvars.append('bx')

   print("\nxvars = \n\n",xvars,"\n")

   if nfiles != 0:
     for i in range(nfiles):
       print('read file =\n',FILE[i],"\n")
       start_read_file = time.time()
       #TREE[i].show()     # print input tree parameters list
       ipd = TREE[i].arrays(xvars, library="pd")       #convert all branches (defined in xvars above) of the waveburst tree into pandas dataframe
       if(float(uproot.__version__[0])>=5.):
         ipd = convert2list(ipd,"rho")
       else:
         ipd.rename(columns=lambda x: x.replace("[","").replace("]",""), inplace=True)    #change column names from rho[0] -> rho0 for all columns
       if(FRAC[i]<1.0):
         print('apply input random fraction selection =',FRAC[i],"\n")
         print('entries before input random fraction selection =',len(ipd))
         ipd = ipd.sample(frac=FRAC[i])
         print('entries after  input random fraction selection =',len(ipd),"\n")
       if(len(CUT[i])>0):
         print('apply input data cuts =',CUT[i],"\n")
         print('entries before input data_cuts =',len(ipd))
         ipd.query(CUT[i], inplace=True)
         print('entries after  input data_cuts =',len(ipd),"\n")
       if not ((FRAC[i]<1.0) or (len(CUT[i])>0)):
         print('entries input data             =',len(ipd),"\n")
       if(float(uproot.__version__[0])>=5.):
         ipd = convert2list(ipd)
       DFRAME.append(ipd)
       del ipd
       DFRAME[i].reset_index(inplace=True, drop=True)
       end_read_file = time.time()
       print("End of reading the input ROOT file. Elapsed time = ", end_read_file-start_read_file, " (sec)\n")
     xpd=pd.concat(DFRAME)
     gc.collect()
   else:
     xpd=events.arrays(xvars,library="pd")          #convert all branches (defined in xvars above) of the waveburst tree into pandas dataframe
     if(float(uproot.__version__[0])>=5.):
       xpd = convert2list(xpd)
     else:
       xpd.rename(columns=lambda x: x.replace("[","").replace("]",""), inplace=True)      #change column names from rho[0] -> rho0 for all columns

   # Convert inputs to format readable by machine learning tools
   xpd.rename(columns={'rho0': 'rho0_std'}, inplace=True) 
   if(verbose):
     columnsNamesArr = xpd.columns.values
     listOfColumnNames = list(columnsNamesArr)
     print("List Of Column Names" , listOfColumnNames, sep='\n')
     print( xpd )

   # The 'derived variables' stuff shows the message: PerformanceWarning: DataFrame is highly fragmented.
   # It seems that there's no compelling reason to display this warning, as it seems to be a legacy artifact from earlier versions of pandas.
   # -> The PerformanceWarning message is silenced
   warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)

   # define new derived variables
   if(bchunk==False): xpd['chunk']=0
   xpd['Qa']=np.sqrt(xpd['Qveto0'])
   if 'ecor' in xvars and 'likelihood' in xvars:
     xpd['ecor/likelihood'] = xpd['ecor']/xpd['likelihood']
   if 'duration' in xvars and 'bandwidth' in xvars:
     xpd['duration0*bandwidth0'] = xpd['duration0']*xpd['bandwidth0']
     xpd['bandwidth0/duration0'] = xpd['bandwidth0']/xpd['duration0']
   # minimum sSNR
   if 'sSNR' in xvars and 'likelihood' in xvars:
     xpd['mSNR'] = np.minimum(xpd['sSNR0'],xpd['sSNR1'])
     for n in range(2,nifo): xpd['mSNR'] = np.minimum(xpd['mSNR'],xpd['sSNR'+str(n)])
     xpd['mSNR/likelihood'] = xpd['mSNR']/xpd['likelihood']
     for n in range(0,nifo): xpd['sSNR'+str(n)+'/likelihood'] = xpd['sSNR'+str(n)]/xpd['likelihood']
   # noise
   if 'noise' in xvars:
     xpd['noise'] = 1/(xpd['noise0']*xpd['noise0'])
     for n in range(1,nifo): xpd['noise'] = xpd['noise']+1/(xpd['noise'+str(n)]*xpd['noise'+str(n)])
     xpd['noise'] = np.sqrt(1/xpd['noise'])

   if(targeted==True):  # add derived features used for targeted searches
     # source antenna pattern sensitivity in theta1/phi1
     xpd['netSens1'] =                 (xpd['bp'+str(nifo)]*xpd['bp'+str(nifo)])/np.power(xpd['noise0'],2)
     xpd['netSens1'] = xpd['netSens1']+(xpd['bx'+str(nifo)]*xpd['bx'+str(nifo)])/np.power(xpd['noise0'],2)
     for n in range(1,nifo):
       xpd['netSens1'] = xpd['netSens1']+(xpd['bp'+str(nifo+n)]*xpd['bp'+str(nifo+n)])/np.power(xpd['noise'+str(n)],2)
       xpd['netSens1'] = xpd['netSens1']+(xpd['bx'+str(nifo+n)]*xpd['bx'+str(nifo+n)])/np.power(xpd['noise'+str(n)],2)
     xpd['netSens1'] = xpd['netSens1']*np.power(xpd['noise'],2)

     # sky distance (theta0/1, phi0/1 are in cWB coordinates: theta=[0,180], phi=[0,360])
     sind = lambda degrees: np.sin(np.deg2rad(degrees))
     cosd = lambda degrees: np.cos(np.deg2rad(degrees))
     xpd['skyDist01']=np.arccos(cosd(xpd['theta0'])*cosd(xpd['theta1'])+sind(xpd['theta0'])*sind(xpd['theta1'])*cosd(xpd['phi0']-xpd['phi1']))*180./np.pi
     xpd['phi01']=xpd['phi0']-xpd['phi1']
     xpd['theta01']=xpd['theta0']-xpd['theta1']

   # check ML_options
   for option, value in ML_options.items():
     if(option=='Qp(index)'):
       # add Qp feature to xpd
       if (value!=1) and (value!=2) and (value!=3):
          print('\nError: ML_options(''Qp(index)'') must be 1 or 2 or 3\n')
          exit(1)
       xpd['Qp'] = xpd['Qveto'+str(value)]/(2*np.sqrt(np.log10(np.minimum(200,xpd['ecor']))))
     if(option=='rho0(define)'):
       # select rho0 definition
       if (value!=0) and (value!=1):
          print('\nError: ML_options(''rho0(define)'') must be 0(std) or 1(new)\n')
          exit(1)
       if (value==0):	# standard -> rho0 = rho[0]
          xpd['rho0'] = xpd['rho0_std']
       if (value==1):   # new -> rho0 = sqrt(ecor/(1+penalty*max(1,penalty)-1)
          xpd['rho0'] = np.sqrt(xpd['ecor']/(1+xpd['penalty']*(np.maximum(1,xpd['penalty'])-1)))

   #xpd = xpd[(xpd['veto_hveto_H1'] == 0) & (xpd['veto_hveto_L1'] == 0)]
   if(bifar==True): xpd['far'] = 1/(xpd['ifar']/(24*3600*365))

   # add ML_caps features to xpd
   for feature, cap in ML_caps.items():
     if(cap>0):
       feature_cap = getcapname(feature,cap)
       xpd[feature_cap] = xpd[feature]
       xpd.loc[xpd[feature_cap]>cap, feature_cap] = cap

   # add classifier
   xpd['classifier']=classifier
 
   # apply data cuts
   if(data_cuts!=None):
     print('size before data_cuts =',len(xpd))
     print('data_cuts = ',data_cuts)
     xpd.query(data_cuts, inplace=True)
     #xpd = xpd.loc[eval(f"{data_cuts}")]
     print('size after data_cuts =',len(xpd))

   # remove zero lag events
   if(xzl):
     print('size before zero lag cuts =',len(xpd))
     xpd = xpd.loc[~((xpd['lag'+str(nifo)]==0) & (xpd['slag'+str(nifo)]==0))]
     print('size after zero lag cuts =',len(xpd))

   # random event selection using fraction=xfrac
   if(xfrac<1):
     print('size before random selection =',len(xpd))
     xmsk = np.random.rand(len(xpd)) < (xfrac)
     xpd = xpd[xmsk]
     print('size after random selection =',len(xpd))

   # retreive and exec ucode
   ucode=ML_options['readfile(ucode)']
   if(ucode!=None):
      print("\nreadfile(ucode) = ",ucode,"\n")
      ucode_err = False
      if type(ucode) is str:
         print('exec ucode = ',ucode,'\n')
         if(ucode!='none'): exec(ucode)
      elif type(ucode) is list:
         ucode_list=ucode
         for ucode in ucode_list:
            if type(ucode) is str:
               print('exec ucode = ',ucode,'\n')
               exec(ucode)
            else:
               ucode_err=True
      else:
         ucode_err=True

      if(ucode_err==True):
         print("\nError ucode ",ucode," definition:\n")
         print("Format : ML_options['readfile(ucode)']=\"ucode\"\n")
         print("or\n")
         print("Format : ML_options['readfile(ucode)']=[\"ucode_1\",\"ucode_2\",...,\"ucode_n\"]\n")
         exit(1)
      else:
         print('size after ucode =',len(xpd),"\n")

   if(verbose):
     columnsNamesArr = xpd.columns.values
     listOfColumnNames = list(columnsNamesArr)
     print("List Of Column Names" , listOfColumnNames, sep='\n')
     print( xpd )

   end = time.time()
   print("End of reading the input ROOT file. Elapsed time (full readfile) = ", end-start, " (sec)\n")

   """
   # debug code -> dump input data to csv file
   exvars = xvars
   exvars.extend(['rho8','chunk','Qa','Qp','ecor/likelihood','sSNR0/likelihood','sSNR1/likelihood','classifier'])
   print(exvars)
   print(" -> dump csv file = "+'dump_data_classifier_'+str(classifier)+'.csv')
   xpd[exvars].to_csv('dump_data_classifier_'+str(classifier)+'.csv')
   #exit()
   """
   return xpd 

def getrhor(xdp, search):
   """
   function purpose:
      return panda frame with the new rhor statistic. This function can be overwritten by a user defined function.
        The ML_stat is first tranformed into Wc (the ranking remain the same)
          then a penalization is applied in Qa,Qp plane (blip glitch penalization)
          finally the statistic is multiplied by sqrt(ecor/chi), where chi = 1 +penalty∗(max(penalty,1)−1)

   input params:
      xdp:        input panda frame containig the ML_stat
      search:     search used: bbh/imbhb/blf/bhf/bld

   return:
      panda frame with the new rhor statistic.
   """

   # compute rhor statistic target searches
   if((search=='bhf') or (search=='bld') or (search=='bbh')):
     xdp['Wc'] = -np.log(1.0 - 0.995*np.sqrt(xdp['MLstat']))/5.3
     xdp['rhor'] = np.sqrt(xdp['ecor'])*xdp['Wc']/np.sqrt(1+xdp['penalty']*(np.maximum(1,xdp['penalty'])-1))
   elif((search=='imbhb') or (search=='blf')): 
     a=0.15
     xdp['MLstat_cor'] = xdp['MLstat']-a+xdp['Qa']*(xdp['Qp']-0.8)
     xdp.loc[(xdp.Qa*(xdp.Qp - 0.8) > 0.15),'MLstat_cor'] =xdp['MLstat']
     xdp.loc[(xdp.MLstat_cor < 0.),'MLstat_cor'] = 0
     xdp['Wc'] = -np.log(1.0 - 0.995*np.sqrt(xdp['MLstat_cor']))/5.3
     xdp['rhor'] = np.sqrt(xdp['ecor'])*xdp['Wc']/np.sqrt(1+xdp['penalty']*(np.maximum(1,xdp['penalty'])-1))
   else:
     print('\ncwb_xgboost.py - wrong search type, available options are: bbh/imbhb/blf/bhf/bld\n')
     exit(1)

   return xdp

def print_config(xgb_params,ML_list,ML_caps,ML_balance,ML_options):
   """
   function purpose:
      print ML parameters
   """

   print("XGB Config --->\n")
   if(xgb_params!=None): print(xgb_params,'\n')
   if(ML_list!=None):    print(ML_list,'\n')
   if(ML_caps!=None):    print(ML_caps,'\n')
   if(ML_balance!=None): print(ML_balance,'\n')
   if(ML_options!=None): print(ML_options,'\n')
   print("<---\n")

def plot_hist_rho(ofile,bkg,sim,xrho):
   """
   function purpose:
      plot rho bkg vs sim histograms

   input params:
      bkg:        bkg panda frame containig the rho+str(xrho) name
      sim:        sim panda frame containig the rho+str(xrho) name
      xrho:       0/1 -> used to identify the entry rho+str(xrho)

   output params:
      ofile:      output plot file name
   """

   print('\nSave plot -> '+ofile+' ...\n')
   binhist = np.linspace(0,40.0,180)
   fig = plt.figure(figsize=(10,7))
   ax = fig.add_subplot(111)
   ax.hist(bkg['rho'+str(xrho)],bins=binhist,alpha=0.5,label='bkg',log=True,color='blue')
   ax.hist(sim['rho'+str(xrho)],bins=binhist,alpha=0.5,label='sim',log=True,color='green')
   ax.set_xlabel(r'$\rho_'+str(xrho)+'$')
   ax.set_ylabel('Number of events')
   ax.legend(loc='upper right')
   plt.title("Detection Statistic Distributions", fontsize=30, pad=20)
   plt.tight_layout()
   fig.savefig(ofile)
   plt.close()
   return

def plot_hist_mchirp(ofile,bkg,sim):
   """
   function purpose:
      plot the reconstructed mchirp (chirp1) bkg vs sim histograms

   input params:
      bkg:        bkg panda frame containig the chirp1 name
      sim:        sim panda frame containig the chirp1 name

   output params:
      ofile:      output plot file name
   """

   print('\nSave plot -> '+ofile+' ...\n')
   binhist = np.linspace(0,100.0,200)
   fig = plt.figure(figsize=(10,7))
   ax = fig.add_subplot(111)
   ax.hist(bkg['chirp1'],bins=binhist,alpha=0.5,label='bkg',log=True,color='blue')
   ax.hist(sim['chirp1'],bins=binhist,alpha=0.5,label='sim',log=True,color='green')
   ax.set_xlabel(r'$mchirp$')
   ax.set_ylabel('Number of events')
   ax.legend(loc='upper right')
   plt.title("Chirp Distributions", fontsize=30, pad=20)
   plt.tight_layout()
   fig.savefig(ofile)
   plt.close()
   return

def plot_hist_freq(ofile,bkg,sim):
   """
   function purpose:
      plot the reconstructed frequency (frequency0) bkg vs sim histograms

   input params:
      bkg:        bkg panda frame containig the frequency0 name
      sim:        sim panda frame containig the frequency0 name

   output params:
      ofile:      output plot file name
   """

   print('\nSave plot -> '+ofile+' ...\n')
   #freq events BKG + SIM distribution
   binhist  = 250
   fig = plt.figure(figsize=(14,7))
   ax = fig.add_subplot(111)
   ax.hist(bkg['frequency0'], bins = binhist, alpha = 0.5, label ='bkg',log=True, color = 'blue')
   ax.hist(sim['frequency0'], bins = binhist, alpha = 0.5, label = 'sim',log=True, color = 'green')
   ax.set_xlabel('Central frequency (Hz)')
   plt.grid()
   ax.set_ylabel('Number of events in testing set')
   ax.legend(loc='upper right')
   plt.title("Central Frequency Distributions", fontsize=35, pad=20)
   plt.tight_layout()
   fig.savefig(ofile)
   plt.close()
   return

def plot1d(par,ofile,bkg,sim,inf=None,sup=None,bins=None,bkg_color='blue',sim_color='green',xlog=False,ylog=True):
   """
   function purpose:
      plot bkg vs sim xpar histogram plot

   input params:
      xpar		feature used for xaxis
      bkg:              bkg panda frame containig the xpar,ypar names
      sim:              sim panda frame containig the xpar,ypar names
      inf/sup           hist inf/sup limits
      bins              hist bins number
      xlog              False/True -> disable/enable xlog hist
      ylog              False/True -> disable/enable ylog hist

   output params:
      ofile:            output plot file name
   """

   print('\nSave plot -> '+ofile+' ...\n')

   data_min = min(float(np.amin(bkg[par])),float(np.amin(sim[par])))
   data_max = max(float(np.amax(bkg[par])),float(np.amax(sim[par])))

   if(inf==None): inf=data_min
   if(sup==None): sup=data_max

   if((bins==None)|(bins<=0)): bins=200

   if(inf<data_min): inf=data_min
   if(sup>data_max): sup=data_max

   # par bkg & sim distribution
   fig = plt.figure(figsize=(14,7))
   ax = fig.add_subplot(111)
   ax.hist(bkg[(bkg[par]>inf)&(bkg[par]<sup)][par], bins=int(bins), alpha=0.5, label='bkg',log=ylog, color=bkg_color)
   ax.hist(sim[(sim[par]>inf)&(sim[par]<sup)][par], bins=int(bins), alpha=0.5, label='sim',log=ylog, color=sim_color)
   xlabel = par.replace('_','\_')	# tex format
   ax.set_xlabel(r'$\textrm{'+xlabel+'}$', fontsize=30)
   ax.set_xlim(inf, sup)
   plt.grid()
   if(xlog==True): plt.xscale('log')
   ax.set_ylabel(r'$\textrm{Number of events}$', fontsize=30)
   ax.legend(loc='upper right', fontsize=25)
   plt.title(r'$\textrm{'+xlabel+'}$', fontsize=40, pad=20)
   plt.tight_layout()
   fig.savefig(ofile)
   plt.close()

   return

def mplot1d(bkg,sim,odir=".",utag=None,goptions=None):
   """
   function purpose:
      multi plot 1d - dump 1d plots features using the plot1d function

   input params:
      bkg:              bkg panda frame name
      sim:              sim panda frame name
      odir:             plot output dir
      utag:             user tag for plot output name: ofname=odir+"/"+utag+"_bkg_sim_"+xfeature+"_"+yfeature+"_plot.png"
      goptions:         plot options dictionary

   output params:
      ofile:            output plot file name
   """

   if(goptions==None):
     print("mplot1d - Error: goptions empty!")
     exit(1)

   _goptions=goptions.copy()	# makes a copy to avoid modification of input parameters

   # if feature(mplot*d) and feature(mplot1d) are both presents then feature(mplot*d) is removed from _goptions
   # if feature(mplot*d) is present but not feature(mplot1d) then feature(mplot*d) is renamed to feature(mplot1d)
   for index, (feature, options) in enumerate(_goptions.copy().items()):
     if '(mplot*d)' in feature:
       _feature=feature
       _feature=_feature.replace('*','1')
       if(_goptions.get(_feature)==None): _goptions[_feature]=options
       _goptions.pop(feature)

   # dump 1d plots features
   if not(os.path.exists(odir)): os.makedirs(odir, exist_ok=True)	# creates the output dir
   bkg_color = _goptions['bkg(mplot1d)'].get('color')
   sim_color = _goptions['sim(mplot1d)'].get('color')
   for index, (feature, options) in enumerate(_goptions.items()):
     if '(mplot1d)' in feature:
       _feature=feature
       _feature=_feature.replace('(mplot1d)','')
       _feature=_feature.replace(' ','')
       if((_feature!='bkg')&(_feature!='sim')):
         enabled = _goptions[feature].get('enable')
         inf     = _goptions[feature].get('inf')
         sup     = _goptions[feature].get('sup')
         bins    = _goptions[feature].get('bins')
         if(enabled): 
           ofname  = "bkg_sim_"+_feature+"_plot.png"
           ofname  = ofname.replace("/","S")
           if(utag!=None):
             ofname = odir+"/"+utag+"_"+ofname
           else:
             ofname = odir+"/"+ofname
           if(os.path.exists(ofname)): os.remove(ofname)    # remove existing file
           plot1d(_feature, ofname, bkg, sim, inf=inf, sup=sup, bins=bins, bkg_color=bkg_color, sim_color=sim_color)

   return

def plot2d(xpar,ypar,ofile,bkg,sim,sim_cmap=None,bkg_rho_name=None,bkg_rho_thr=None,bkg_rho_label=None,bkg_marker_color=None,bkg_marker_size=None,sim_xinf=None,sim_xsup=None,sim_xbins=None,sim_yinf=None,sim_ysup=None,sim_ybins=None):
   """
   function purpose:
      plot bkg vs sim xpar,ypar scatter plot

   input params:
      xpar		feature used for xaxis
      ypar		feature used for yaxis
      bkg:              bkg panda frame containig the xpar,ypar names
      sim:              sim panda frame containig the xpar,ypar names
      sim_cmap		sim hist2d colormap
      bkg_rho_name      rho name (default = 'rho0') used to select the background entries (bkg_rho_name>bkg_rho_thr)
      bkg_rho_thr       rho threshold (default = 8) used to select the background entries (bkg_rho_name>bkg_rho_thr)
      bkg_rho_label     rho label (default = 'rho0') used in the legend
      bkg_marker_color  marker color (default = 'black') used for the bkg scatter plot
      bkg_marker_size   marker size (default = 22) used for the bkg scatter plot
      sim_xinf/xsup     sim hist2d xpar inf/sup limits
      sim_yinf/ysup     sim hist2d ypar inf/sup limits
      sim_xbins/ybins   sim hist2d x/y bins number

   output params:
      ofile:            output plot file name
   """

   # ignore message: 'UserWarning: Legend does not support handles for QuadMesh instances' in 'legend = plt.legend(fontsize=15, ...'
   warnings.filterwarnings(action='ignore', category=UserWarning)

   print('\nSave plot -> '+ofile+' ...\n')

   if(sim_xinf==None): sim_xinf=float(np.amin(sim[xpar]))
   if(sim_xsup==None): sim_xsup=float(np.amax(sim[xpar]))

   if(sim_yinf==None): sim_yinf=float(np.amin(sim[ypar]))
   if(sim_ysup==None): sim_ysup=float(np.amax(sim[ypar]))

   if((sim_xbins==None)|(sim_xbins<=0)): sim_xbins=200
   if((sim_ybins==None)|(sim_ybins<=0)): sim_ybins=200

   if(sim_cmap==None):         sim_cmap='rainbow'

   if(bkg_rho_name==None):     bkg_rho_name='rho0'
   if(bkg_rho_thr==None):      bkg_rho_thr=8
   if(bkg_rho_label==None):    bkg_rho_label='rho0'
   if(bkg_marker_color==None): bkg_marker_color='black'
   if(bkg_marker_size==None) : bkg_marker_size=22

   if(sim_xinf<float(np.amin(sim[xpar]))): sim_xinf=float(np.amin(sim[xpar]))
   if(sim_yinf<float(np.amin(sim[ypar]))): sim_yinf=float(np.amin(sim[ypar]))
   if(sim_xsup>float(np.amax(sim[xpar]))): sim_xsup=float(np.amax(sim[xpar]))
   if(sim_ysup>float(np.amax(sim[ypar]))): sim_ysup=float(np.amax(sim[ypar]))

   fig, ax = plt.subplots(figsize=(10, 7))
   sim_selection = (sim[xpar]>sim_xinf)&(sim[xpar]<sim_xsup)&(sim[ypar]>sim_yinf)&(sim[ypar]<sim_ysup)
   h = ax.hist2d(sim[sim_selection][xpar], sim[sim_selection][ypar], bins=[sim_xbins,sim_ybins],cmap = plt.get_cmap(sim_cmap), norm=LogNorm(), label='sim')
   fig.colorbar(h[3], ax=ax)
   rho_selection = 'bkg ('+bkg_rho_label+'$>$'+str(bkg_rho_thr)+')' 
   ax.scatter(bkg[bkg[bkg_rho_name]>bkg_rho_thr][xpar], bkg[bkg[bkg_rho_name]>bkg_rho_thr][ypar], marker=".", color=bkg_marker_color, label=rho_selection, s=bkg_marker_size)
   xlabel = xpar.replace('_','\_')	# tex format
   ax.set_xlabel(r'$\textrm{'+xlabel+'}$', fontsize=20)
   ax.set_xlim(sim_xinf, sim_xsup)
   ylabel = ypar.replace('_','\_')	# tex format
   ax.set_ylabel(r'$\textrm{'+ylabel+'}$', fontsize=20)
   ax.set_ylim(sim_yinf, sim_ysup)
   #legend = plt.legend(fontsize=15, loc='upper left', edgecolor="black")
   legend = plt.legend(fontsize=15, edgecolor="black", loc='lower right', bbox_to_anchor=(1, 0.95))
   legend.get_frame().set_alpha(None)
   plt.title(r'$\textrm{'+ylabel+' vs '+xlabel+'}$', fontsize=30, pad=20)
   plt.tight_layout()
   fig.savefig(ofile)
   plt.close()
   return

def mplot2d(bkg,sim,odir=".",utag=None,goptions=None):
   """
   function purpose:
      multi plot 2d - dump 2d plots features using the plot2d function

   input params:
      bkg:              bkg panda frame name
      sim:              sim panda frame name
      odir:             plot output dir
      utag:             user tag for plot output name: ofname=odir+"/"+utag+"_bkg_sim_"+xfeature+"_"+yfeature+"_plot.png"
      goptions:          plot options dictionary

   output params:
      ofile:            output plot file name
   """

   if(goptions==None):
     print("mplot2d - Error: goptions empty!")
     exit(1)

   _goptions=goptions.copy()	# makes a copy to avoid modification of input parameters

   # if feature(mplot*d) and feature(mplot2d) are both presents then feature(mplot*d) is removed from _goptions
   # if feature(mplot*d) is present but not feature(mplot2d) then feature(mplot*d) is renamed to feature(mplot2d)
   for index, (feature, options) in enumerate(_goptions.copy().items()):
     if '(mplot*d)' in feature:
       _feature=feature
       _feature=_feature.replace('*','2')
       if(_goptions.get(_feature)==None): _goptions[_feature]=options
       _goptions.pop(feature)

   # dump 2d plots features
   if not(os.path.exists(odir)): os.makedirs(odir, exist_ok=True)	# creates the output dir
   bkg_rho_name     = _goptions['bkg(mplot2d)'].get('rho_name')
   bkg_rho_thr      = _goptions['bkg(mplot2d)'].get('rho_thr')
   bkg_rho_label    = _goptions['bkg(mplot2d)'].get('rho_label')
   bkg_marker_color = _goptions['bkg(mplot2d)'].get('marker_color')
   bkg_marker_size  = _goptions['bkg(mplot2d)'].get('marker_size')
   sim_cmap         = _goptions['sim(mplot2d)'].get('cmap')
   for index1, (feature1, goptions1) in enumerate(_goptions.items()):
     if '(mplot2d)' in feature1:
       _feature1=feature1
       _feature1=_feature1.replace('(mplot2d)','')
       _feature1=_feature1.replace(' ','')
       if((_feature1!='bkg')&(_feature1!='sim')):
         sim_xenable = _goptions[feature1].get('enable')
         sim_xinf    = _goptions[feature1].get('inf')
         sim_xsup    = _goptions[feature1].get('sup')
         sim_xbins   = _goptions[feature1].get('bins')
         for index2, (feature2, goptions2) in enumerate(_goptions.items()):
           if(index2>index1):
             if '(mplot2d)' in feature2:
               _feature2=feature2
               _feature2=_feature2.replace('(mplot2d)','')
               _feature2=_feature2.replace(' ','')
               if((_feature2!='bkg')&(_feature2!='sim')):
                 ofname = "bkg_sim_"+_feature1+"_"+_feature2+"_plot.png"
                 ofname = ofname.replace("/","S")
                 if(utag!=None):
                   ofname = odir+"/"+utag+"_"+ofname
                 else:
                   ofname = odir+"/"+ofname
                 if(os.path.exists(ofname)): os.remove(ofname)    # remove existing file
                 sim_yenable = _goptions[feature2].get('enable')
                 sim_yinf    = _goptions[feature2].get('inf')
                 sim_ysup    = _goptions[feature2].get('sup')
                 sim_ybins   = _goptions[feature2].get('bins')

                 if(sim_xenable & sim_yenable):
                    plot2d(_feature1, _feature2,
                           ofname, bkg, sim, sim_cmap=sim_cmap,
                           bkg_rho_name=bkg_rho_name, bkg_rho_thr=bkg_rho_thr,
                           bkg_marker_color=bkg_marker_color, bkg_marker_size=bkg_marker_size,
                           sim_xinf=sim_xinf, sim_xsup=sim_xsup, sim_xbins=sim_xbins,
                           sim_yinf=sim_yinf, sim_ysup=sim_ysup, sim_ybins=sim_ybins)

   return

def plot_QaQp(ofile,bkg,sim,rho_name='rho0',rho_thr=8,rho_label='rho0',qfactor=0.15,qoffset=0.8,bkg_marker_color='black',bkg_marker_size=22,qa_sup=6.0,qp_sup=10.0,sim_bin_size=0.1):
   """
   function purpose:
      plot bkg vs sim Qa,Qp scatter plot (Qa = sqrt(Qveto0), Qp ~ Qveto(1/2/3)/Pi)

   input params:
      bkg:              bkg panda frame containig the Qa,Qp names
      sim:              sim panda frame containig the Qa,Qp names
      rho_name          rho name (default = 'rho0') used to select the background entries (rho_name>rho_thr)
      rho_thr           rho threshold (default = 8) used to select the background entries (rho_name>rho_thr)
      rho_label         rho label (default = 'rho0') used in the legend
      qfactor           qfactor value (default = 0.15) used to draw the dashed line Qa = qfactor/(Qp-qoffset)
      qoffset           qoffset value (default = 0.8)  used to draw the dashed line Qa = qfactor/(Qp-qoffset)
      bkg_marker_color  marker color (default = 'black') used for the bkg scatter plot
      bkg_marker_size   marker size (default = 22) used for the bkg scatter plot
      qa_sup            Qa sup limit
      qp_sup            Qp sup limit
      sim_bin_size      sim scatter plot bin size

   output params:
      ofile:            output plot file name
   """

   # ignore message: 'UserWarning: Legend does not support handles for QuadMesh instances' in 'ax.legend(loc='lower right')'
   warnings.filterwarnings(action='ignore', category=UserWarning)

   print('\nSave plot -> '+ofile+' ...\n')
   if(sim_bin_size<=0): sim_bin_size=0.1
   nxbins = int(np.amax(sim['Qp']/sim_bin_size))
   nybins = int(np.amax(sim['Qa']/sim_bin_size))
   g, ax = plt.subplots(figsize=(10, 7))
   h = ax.hist2d(sim['Qp'], sim['Qa'], bins=[nxbins,nybins],cmap = plt.get_cmap("rainbow"), norm=LogNorm(), label='sim')
   g.colorbar(h[3], ax=ax)
   rho_selection = 'bkg ('+rho_label+'$>$'+str(rho_thr)+')' 
   ax.scatter(bkg[bkg[rho_name]>rho_thr]['Qp'], bkg[bkg[rho_name]>rho_thr]['Qa'], marker=".", color=bkg_marker_color, label=rho_selection, s=bkg_marker_size)
   ax.set_xlabel('Qp')
   ax.set_xlim(-0.1, qp_sup)
   ax.set_ylabel('Qa')
   ax.set_ylim(-0.1, qa_sup)
   x = np.arange(qoffset*1.01, qp_sup, 0.01)
   ax.plot(x, qfactor/(x-qoffset), linestyle='dashed', label='Qa='+str(qfactor)+'/(Qp-'+str(qoffset)+')', color='red')
   ax.legend(loc='lower right')
   plt.title(r'$\textrm{Qa vs Qp Distributions}$', fontsize=30, pad=20)
   plt.tight_layout()
   g.savefig(ofile)
   plt.close()
   return

   """
   print('\nSave plot -> '+ofile+' ...\n')
   nbins = 500
   g, ax = plt.subplots(figsize=(10, 7))
   ax.set_yscale('symlog')
   ax.set_xscale('symlog')
   ax.scatter(np.log10(np.maximum(0.1,bkg[bkg['rho0']>8]['Qa'])), np.log10(np.maximum(0.1,bkg[bkg['rho0']>8]['Qp'])), marker = ".", color = 'black')
   h = ax.hist2d(np.log10(np.maximum(0.1,sim['Qa'])), np.log10(np.maximum(0.1,sim['Qp'])), bins=nbins,cmap = 'jet', norm=LogNorm())
   g.colorbar(h[3], ax=ax)
   ax.set_xlabel('log10(Qp)')
   ax.set_ylabel(r'log10(Qa)')
   plt.tight_layout()
   g.savefig(ofile)
   plt.close()
   return
   """

def get_balanced_tail(tpd,ML_caps,seed):
   """
   function purpose:
      balance rho0 tail for training
        Sampling high rho0 SIM and BKG events to achieve required class balancing for (rho0 >= rho0_capvalue) events
        -> num(sim) = num(bkg)

   input params:
      tpd:           panda training set containig rho0+cap and the classifier 0/1
      ML_caps:       ML cap list containig the rho0 cap
      seed:          seed for sim tail random down/up sampling

   return:
      panda training set with balanced tail
   """

   print('\n -> start tail balance ...')

   # retreive from ML_caps the key feature (rho0 cap) for balancing
   rho0_capvalue = -1
   for feature, cap in ML_caps.items():
     if (feature.find("rho0")!=-1):
       rho0_capname  = getcapname(feature,cap)
       rho0_capvalue = cap
   if(rho0_capvalue<0):
     print('\nError - rho0 not found in the ML_caps, must be defined\n')
     exit(1)
   print('\n rho0_capname = '+rho0_capname+' - rho0_capvalue = '+str(rho0_capvalue))

   # Sampling SIM and BKG events with (rho0 >= rho0_capvalue)
   ssample = tpd.loc[(tpd[rho0_capname] >= rho0_capvalue) & (tpd.classifier == 1)]
   nsample = tpd.loc[(tpd[rho0_capname] >= rho0_capvalue) & (tpd.classifier == 0)]
   ncount = nsample.shape[0]
   scount = ssample.shape[0]
   print("\nbalance(1) selected events (rho0>="+str(rho0_capvalue)+")    -> size(sim)/size(bkg): "
                     + str(scount) + "/" + str(ncount), end='')
   if(ncount>0): print(" = " + str(float(scount)/float(ncount)))

   # if (nSIM >or< nBKG) then downsampled/upsampled SIM events with (rho0>=rho0_capvalue)
   if ssample.shape[0] > nsample.shape[0]:
      print("\n  -> sim(bin0>="+str(rho0_capvalue)+") downsampled to match BKG bin"
                                        +str(rho0_capvalue)+" with no duplicates.")
      ssample = ssample.sample(nsample.shape[0], replace = False, random_state = seed)
   else:
      print("\n  -> sim(bin0>="+str(rho0_capvalue)+") upsampled to match BKG bin"
                                        +str(rho0_capvalue)+" with replacement.")
      ssample = ssample.sample(nsample.shape[0], replace = True, random_state = seed)
   ncount = nsample.shape[0]
   scount = ssample.shape[0]
   print("\nbalance(2) balanced events (rho0>="+str(rho0_capvalue)+")    -> size(sim)/size(bkg): " + str(scount) + "/" + str(ncount), end='')
   if(ncount>0): print(" = " + str(float(scount)/float(ncount)))

   # remove SIM events with (rho0>=rho0_capvalue) from tpd
   tpd.drop(tpd[(tpd[rho0_capname] >= rho0_capvalue) & (tpd.classifier == 1)].index, inplace=True)
   ncount = tpd.loc[(tpd.classifier == 0)].shape[0]
   scount = tpd.loc[(tpd.classifier == 1)].shape[0]
   print("\nbalance(3) balanced events (sim rho0<"+str(rho0_capvalue)+") -> size(sim)/size(bkg): " + str(scount) + "/" + str(ncount), end='')
   if(scount>0): print(" = " + str(float(scount)/float(ncount)))

   # merge tpd with balanced SIM events with (rho0>=rho0_capvalue)
   tpd = pd.concat([tpd, ssample])
   tpd.reset_index(inplace=True, drop=True)
   ncount = tpd.loc[(tpd.classifier == 0)].shape[0]
   scount = tpd.loc[(tpd.classifier == 1)].shape[0]
   print("\nbalance(4) balanced events              -> size(sim)/size(bkg): " + str(scount)
                            + "/" + str(ncount) + " = " + str(float(scount)/float(ncount)))

   print('\n <- end tail balance')

   return tpd

def get_balanced_bulk(X_train,ML_caps,ML_balance,balance_type,dump,ofile_tag):
   """
   function purpose:
      balance rho0 bulk for training
        Sampling low rho0 SIM and BKG events to achieve required class balancing for (rho0 < rho0_capvalue) events

   input params:
      X_train:       panda training set containig rho0+cap and the classifier 0/1
      ML_caps:       ML cap list containig the rho0 cap
      ML_balance:    ML balance options
      balance_type   tuning/training
      dump:          True/False enable dump plots: 'ofile_tag'_balance_weight.png, 'ofile_tag'_balance_hist.png
      ofile_tag:     tag for output file name

   output:
      if dump=True -> dump plots 'ofile_tag'_balance_weight.png, 'ofile_tag'_balance_hist.png

   return:
      panda training set with [weight1] and weights
   """

   print('\n -> start bulk balance ...')

   if (balance_type!='tuning') and (balance_type!='training'):
      print('get_balanced_bulk - wrong balance_type, available options are tuning/training')
      exit(1)

   # retreive from ML_balance -> binning.nbins,balance,slope,smoothing
   binning = ML_balance['binning('+balance_type+')']
   nbins   = ML_balance['nbins('+balance_type+')']
   balance = ML_balance['balance('+balance_type+')']          #BKG/SIM Ratio in the first bin while applying weights
   slope   = ML_balance['slope('+balance_type+')']
   smooth  = ML_balance['smoothing('+balance_type+')']

   if (slope.find("q=")!=-1):
      sq=slope.replace("q=","")
      q=float(sq)
   else:
      print('get_balanced_bulk - error: slope parameter is not defined')
      exit(1)

   if (balance.find("A=")!=-1):
      sA=balance.replace("A=","")
      A=float(sA)
   else:
      print('get_balanced_bulk - error: balance parameter is not defined')
      exit(1)

   # retreive from ML_caps the key feature (rho0 cap) for balancing
   rho0_capvalue = -1
   for feature, cap in ML_caps.items():
     if (feature.find("rho0")!=-1):
       rho0_capname  = getcapname(feature,cap)
       rho0_capvalue = cap
   if(rho0_capvalue<0):
     print('\nError - rho0 not found in the ML_caps, must be defined\n')
     exit(1)
   print('\n rho0_capname = '+rho0_capname+' - rho0_capvalue = '+str(rho0_capvalue))

   # BKG binned sample weight
   X_train['weight1'] = 1
   #print("\ntotal = ",X_train['weight1'].sum())

   # select events with rho0 < rho0_capvalue
   if ((binning=='bkg(percentiles)') or (binning=='linear')):
     classifier = 0
   elif (binning=='sim(percentiles)'):
     classifier = 1
   else:
     print('\nget_balanced_bulk: Error - binning not allowedget_balanced_bulk. Allowed valued: bkg(percentiles) / sim(percentiles) \n')
     exit(1)

   rho0 = X_train.loc[(X_train[rho0_capname]<rho0_capvalue) & (X_train.classifier == classifier), rho0_capname]
   if ((binning=='bkg(percentiles)') or (binning=='sim(percentiles)')):	# percentiles binning
     rho0 = np.sort(rho0)
     perc = np.linspace(0,100*0.99999,nbins+1)
     bins = np.percentile(rho0, perc)
   elif (binning=='linear'):						# linear binning
     bins = np.linspace(min(rho0),rho0_capvalue*0.99999,nbins+1)


   hist_b, bins_b=np.histogram(X_train.loc[X_train.classifier == 0, rho0_capname], bins=bins)
   hist_s, bins_s=np.histogram(X_train.loc[X_train.classifier == 1, rho0_capname], bins=bins)

   # compute weights
   W = []
   for i in np.arange(0,nbins):
     W.append((hist_s[i]/hist_b[i])*np.exp(np.log(A)*(1-i/(nbins-1))**q))

   # smooth weights
   if (smooth==True):
     lowess = sm.nonparametric.lowess(W, bins[:-1], frac=0.1)
     W = lowess[:, 1]

   for i in np.arange(0,nbins):
      X_train.loc[(X_train[rho0_capname] >= bins_b[i]) & (X_train[rho0_capname] <= bins_b[i+1]) & (X_train.classifier == 0),'weight1'] = W[i]

   # save balance weight plot
   if(dump):
     ofile = ofile_tag+"_balance_weight.png"
     plot_balance_weight(ofile,bins,W)
     ofile = ofile_tag+"_balance_hist.png"
     plot_balance_hist(ofile,X_train,rho0_capname,bins)
     ofile = ofile_tag+"_balance_weighted_hist.png"
     plot_balance_weight_hist(ofile,X_train,rho0_capname,bins)

   # Storing sample weight
   weight = X_train['weight1']
   #print(weight) 

   print('\n <- end bulk balance')

   return X_train,weight

def plot_balance_weight_hist(ofile,X_train,rho0_capname,bins):
   """
   function purpose:
      plot rho0_capname weighted bkg vs sim histograms using the 'bins' binning

   input params:
      X_train:       trained set containig the classifier 0/1
      rho0_capname:  cap name of rho0 used by the histogram (eg: rho0_0d8)
      bins:          binning used by the histogram
      weight:        array of weights

   output params:
      ofile:         output plot file name
   """

   print('\nSave plot -> '+ofile+' ...\n')
   fig = plt.figure(figsize=(10,7))
   ax = fig.add_subplot(111)
   ax.hist(X_train.loc[X_train.classifier == 0, rho0_capname],bins=bins,weights=X_train.loc[X_train.classifier == 0, 'weight1'],alpha=0.5,label='bkg',log=True,color='blue')
   ax.hist(X_train.loc[X_train.classifier == 1, rho0_capname],bins=bins,alpha=0.5,label='sim',log=True,color='green')
   rho0_capname=rho0_capname.replace('_',"\\_")
   ax.set_xlabel(r'$\textrm{'+rho0_capname+'}$')
   ax.set_ylabel(r'$\textrm{Number of events}$')
   ax.legend(loc='upper right')
   plt.title("Background/Simulation Balanced Distributions", fontsize=30, pad=20)
   plt.tight_layout()
   fig.savefig(ofile)
   plt.close()
   return


def plot_balance_hist(ofile,X_train,rho0_capname,bins):
   """
   function purpose:
      plot rho0_capname bkg vs sim histograms using the 'bins' binning

   input params:
      X_train:       trained set containig the classifier 0/1
      rho0_capname:  cap name of rho0 used by the histogram (eg: rho0_0d8)
      bins:          binning used by the histogram

   output params:
      ofile:         output plot file name
   """

   print('\nSave plot -> '+ofile+' ...\n')
   fig = plt.figure(figsize=(10,7))
   ax = fig.add_subplot(111)
   ax.hist(X_train.loc[X_train.classifier == 0, rho0_capname],bins=bins,alpha=0.5,label='bkg',log=True,color='blue')
   ax.hist(X_train.loc[X_train.classifier == 1, rho0_capname],bins=bins,alpha=0.5,label='sim',log=True,color='green')
   rho0_capname=rho0_capname.replace('_',"\\_")
   ax.set_xlabel(r'$\textrm{'+rho0_capname+'}$')
   ax.set_ylabel(r'$\textrm{Number of events}$')
   ax.legend(loc='upper right')
   plt.title("Background/Simulation Distributions", fontsize=30, pad=20)
   plt.tight_layout()
   fig.savefig(ofile)
   plt.close()
   return

def plot_balance_weight(ofile,bins,weight):
   """
   function purpose:
      plot the weights used fot the bulk balance

   input params:
      bins:          binning used by the histograms
      weight:        array of weights

   output params:
      ofile:         output plot file name
   """

   print('\nSave plot -> '+ofile+' ...\n')
   fig, ax = plt.subplots()
   bins_x = []
   bins_y = []
   nbins = len(bins)-1
   for i in np.arange(0,nbins):
      # step function
      bins_x.append(bins[i])
      bins_y.append(weight[i])
      bins_x.append(bins[i+1])
      bins_y.append(weight[i])
   bins_x.append(bins[nbins])
   bins_y.append(weight[i-1])
   ax.plot(bins_x,bins_y,marker='',linestyle='-')
   ax.semilogy(bins_x,bins_y)
   ax.set_xlabel(r'$\rho_0$')
   plt.grid()
   rcParams["text.usetex"] = False
   plt.title("Bulk Balance Weights", fontsize=20, pad=20)
   ax.set_ylabel('balance weight ( sim / bkg )')
   plt.tight_layout()
   fig.savefig(ofile)
   plt.close()
   rcParams["text.usetex"] = True

def plot_roc(ofile,model,X_eval,y_eval):
   """
   function purpose:
      plot the roc curve of X,y evaluation sets
        'True Positive Rate' vs 'False Positive Rate'
          True Positive Rate = Sensitivity = True Positives / (True Positives + False Negatives)
          False Positive Rate = False Positives / (False Positives + True Negatives)
          see https://machinelearningmastery.com/roc-curves-and-precision-recall-curves-for-classification-in-python/

   input params:
      model:         XGBoost model
      X_eval:        X evaluation set
      y_eval:        y evaluation set

   output params:
      ofile:         output plot file name
   """

   print('\nSave plot -> '+ofile+' ...\n')
   fig, ax = plt.subplots()
   # generate a no skill prediction (majority class)
   ns_probs = [0 for _ in range(len(y_eval))]
   # predict probabilities
   lr_probs = model.predict_proba(X_eval)
   # keep probabilities for the positive outcome only
   lr_probs = lr_probs[:, 1]
   # calculate scores
   ns_auc = roc_auc_score(y_eval, ns_probs)
   lr_auc = roc_auc_score(y_eval, lr_probs)
   # summarize scores
   print('No Skill: ROC AUC=%.3f' % (ns_auc))
   print('Skill   : ROC AUC=%.3f' % (lr_auc))
   # calculate roc curves
   ns_fpr, ns_tpr, _ = roc_curve(y_eval, ns_probs)
   lr_fpr, lr_tpr, _ = roc_curve(y_eval, lr_probs)
   # plot the roc curve for the model
   ax.plot(ns_fpr, ns_tpr, linestyle='--', label='No Skill')
   ax.plot(lr_fpr, lr_tpr, marker='.', label='Skill')
   # axis labels
   ax.set_xlabel('False Positive Rate')
   ax.set_ylabel('True Positive Rate')
   # show the legend
   ax.legend(loc='lower right')
   plt.title("ROC", fontsize=20, pad=20)
   # save the plot
   fig.savefig(ofile)
   plt.close()

def plot_pr(ofile,model,X_eval,y_eval):
   """
   function purpose:
      plot the precision-recall curve of X,y evaluation sets
        Precision vs Recall
          Precision = True Positives / (True Positives + False Positives)
          Recall = True Positives / (True Positives + False Negatives) = Sensitivity
          see https://machinelearningmastery.com/roc-curves-and-precision-recall-curves-for-classification-in-python/

   input params:
      model:         XGBoost model
      X_eval:        X evaluation set
      y_eval:        y evaluation set

   output params:
      ofile:         output plot file name
   """

   print('\nSave plot -> '+ofile+' ...\n')
   fig, ax = plt.subplots()

   # predict probabilities
   lr_probs = model.predict_proba(X_eval)
   # keep probabilities for the positive outcome only
   lr_probs = lr_probs[:, 1]
   # predict class values
   yhat = model.predict(X_eval)
   lr_precision, lr_recall, _ = precision_recall_curve(y_eval, lr_probs)
   lr_f1, lr_auc = f1_score(y_eval, yhat), auc(lr_recall, lr_precision)
   # summarize scores
   print('Skill: f1=%.3f auc=%.3f' % (lr_f1, lr_auc))
   # plot the precision-recall curves
   no_skill = len(y_eval.loc[y_eval.classifier == 1]) / len(y_eval)
   ax.plot([0, 1], [no_skill, no_skill], linestyle='--', label='No Skill')
   ax.plot(lr_recall, lr_precision, marker='.', label='Skill')
   # axis labels
   ax.set_xlabel('Recall')
   ax.set_ylabel('Precision')
   # show the legend
   ax.legend(loc='lower left')
   # save the plot
   plt.title("Precision-Recall Plot", fontsize=20, pad=20)
   fig.savefig(ofile)
   plt.close()

def rhor_metric_scores(model,R_eval,ugetrhor,search,plpath=None,pltag=''):
   """
   function purpose:
      compute the rhor metric scores (based on rhor statistic):

      aucpr_rhor_score       area under the precision/recall curve
      auc_rhor_score         area under the roc curve
      f1_rhor_score          f1 score
      aucelf_rhor_score      area under the curve: efficiency vs log10(far) 

   input params:
      model:         XGBoost model
      R_eval:        X evaluation set + ecor,penalty,Qa,Qp features used to compute the final cWB statistic rho_r
      ugetrhor:      user getrho function
      search:        search used: bbh/imbhb/blf/bhf/bld
      plpath:        output plot path if!=None is used as base name to dump plots 
      pltag:         used to tag output plot name. eg: plpath/pltag_eff_far.png 

   return:
      aucpr_rhor_score
      auc_rhor_score
      f1_rhor_score
      aucelf_rhor_score

   notes:
      aucelf =  area under the curve: efficiency vs log10(far) 

      auc    = area under the roc curve
               roc curve = 'True Positive Rate' vs 'False Positive Rate'
               'True Positive Rate' = TP/(TP+FN)
               'False Positive Rate' = FP/(FP+TN)

      aucpr  = area under the pr curve
               pr curve = precision vs recall

      f1     = 2 * (precision * recall) / (precision + recall)
               recall = TP/(TP+FN)
               precision = TP/(TP+FP)

      TP = True  Positive
      FP = False Positive
      TN = True  Negative
      FN = False Negative
   """

   #get the list of cWB parameters used in training
   ML_list = model.get_booster().feature_names

   # predict probabilities
   R_eval['MLstat'] = model.predict_proba(R_eval[ML_list], iteration_range=None)[:,1]

   # get the final rhor ranking statistic
   if(ugetrhor==None):
     R_eval = getrhor(R_eval, search)	# default getrhor
   else:
     R_eval = ugetrhor(R_eval, search)	# user getrhor

   # check if getrhor return the rhor parameter
   try:
     R_eval['rhor']
   except:
     print("\nError in rhor_metric_scores: getrhor doesn't return the rhor parameter, check the user getrhor python code\n")
     exit(1)

   # compute aucelf
   if(pltag!=''): pltag=pltag+'_' 
   if(plpath!=None):
     ofile_far_rhor = plpath+'/'+pltag+'far_rhor.png'
     ofile_eff_far  = plpath+'/'+pltag+'eff_far.png'
   else:
     ofile_far_rhor = None
     ofile_eff_far  = None

   bkg = R_eval.loc[R_eval.classifier == 0]
   rhor_rate = far_rho(bkg, bkg_years=1, ts='rhor', step_size = 0.01, plname=ofile_far_rhor, color='bo')
   sim = R_eval[R_eval['classifier']==1]
   # force sim('rhor') dtype to be the the same as rhor_rate('rhor')
   sim['rhor'] = sim['rhor'].astype(np.float32)
   rhor_rate['rhor'] = rhor_rate['rhor'].astype(np.float32)
   # merge sim & rhor_rate
   sim = pd.merge_asof(sim.sort_values('rhor'), rhor_rate.sort_values('rhor'), on="rhor", direction="nearest")
   _, aucelf_rhor_score = eff_far(sim, 'rate_rhor', tot_num_events=sim.shape[0], far_loglow = 0.0, far_loghigh = np.log10(bkg.shape[0]),
                                  num_steps=10000, color='red', plname=ofile_eff_far)

   # dump rhor bkg/sim histogram
   if(plpath!=None):
     ofile = plpath+'/'+pltag+'bkg_sim_rhor_hist.png'
     plot_hist_rho(ofile,bkg,sim,'r')

   """
   # the following code shows how AUC is computed
   roc_fpr, roc_tpr, _ = roc_curve(R_eval['classifier'],R_eval['rhor'])
   AUC=0
   pfpr=0	# previous false positive rate
   ptpr=0	# previous true  positive rate
   for fpr, tpr in zip(roc_fpr, roc_tpr):
     if(pfpr>0):
       AUC+=(tpr+ptpr)*(fpr-pfpr)/2
     pfpr=fpr
     ptpr=tpr
   print('---------------------------> AUC = ',AUC)
   """

   # aucpr_rhor_score
   precision, recall, _ = precision_recall_curve(R_eval['classifier'],R_eval['rhor'])
   aucpr_rhor_score = auc(recall, precision)

   # auc_rhor_score
   auc_rhor_score   = roc_auc_score(R_eval['classifier'],R_eval['rhor'])

   # f1_rhor_score
   f1_rhor_score    = f1_score(R_eval['classifier'],model.predict(R_eval[ML_list]))

   return aucpr_rhor_score,auc_rhor_score,f1_rhor_score,aucelf_rhor_score


def far_rho(bkg_test_df, bkg_years, ts='rho1', step_size = 0.01, plname=None, color='bo', verbose=True):
    """
    function purpose:
       calculate the rate of background events per year as a function of the test statistic (e.g. rho1). 
       This is the main measure of noise events.

    input params:
       bkg_test_df: input pandas.DataFrame used to calculate "rate_vs_rho", should include only background noise events
       ts:          test statistic used for "rate_vs_rho" analysis, should be a column name in bkg_test_df
       bkg_years:   the amount of background generated (in units of Years)
       num_steps:   number of steps in 'rho' used for "rate_vs_rho". for faster analysis, use fewer steps; for more accurate analysis, use more steps

       plname:      if!=None then eff vs far plot is saved with name plname
       color:       color of plot line

    returns:        
       a pandas.DataFrame object which contains two columns: test statistic and rate (at a given test statistic)
    """

    bkg_ts_df = bkg_test_df[ts]
    rho_min   = np.amin(bkg_ts_df)
    rho_max   = np.amax(bkg_ts_df)
    rho_thresh  = np.arange(rho_min, rho_max, step_size)
    rate = np.zeros(len(rho_thresh))
    rate_err = np.zeros(len(rho_thresh))
    if(verbose): print("Generating rate vs rho ... this may take some time")
    
    for idx, rho in enumerate(rho_thresh):
       num_events = bkg_ts_df[bkg_ts_df >= rho].shape[0]
       rate[idx] = ( num_events / bkg_years )
       rate_err[idx] = ( np.sqrt(num_events) / bkg_years )

    if(plname != None):
       plt.plot(rho_thresh, rate, color)
       plt.ylabel('Background rate per year')
       plt.xlabel('Detection statistic (' + ts + ')')
       plt.yscale('log')
       plt.grid()
       plt.title("Background Rate vs Detection Statistic", fontsize=30, pad=20)
       plt.savefig(plname)
       plt.close()

    rate_df = pd.DataFrame({ts: rho_thresh, 'rate_'+ts: rate, 'rate_'+ts+'_err': rate_err})
    return rate_df


def eff_far(sim_test_df, model_far_label, tot_num_events, far_loglow = -3.0, far_loghigh = 1.0, num_steps=5000, plname=None, color='bo'):
    """
    function purpose:
       to calculate the detection efficiency at different FARs for a given model,
       and aucelf =  area under the curve: efficiency vs log10(far) 
       NOTE: one should first call Calc_FAR to calculate the FAR for a list of events.
    
    input params:
       sim_test_df:     input pandas.DataFrame which contains the detected simulated (or zerolag) events
       rate_df:         input pandas.DataFrame which contains two columns: test statistic and rate (output from Rate_vs_Rho function)
       model_far_label: label of the FAR column in 'sim_test_df' used to calculate detection efficiency
       far_loglow:      min log10 far used to compute the detection efficiency
       far_loghigh:     max log10 far used to compute the detection efficiency
       num_steps:       number of bins used in the [min,max] range

       plname:          if!=None then eff vs far plot is saved with name plname
       color:           color of plot line

    returns: 
       a pandas.Series object with a single column of 'far' values
       aucelf
    """

    far_thresh = np.logspace(far_loglow, far_loghigh, num_steps)
    frac = np.zeros(num_steps)
    sim_far_df = sim_test_df[model_far_label]
    for idx, far in enumerate(far_thresh):
       num_events = sim_far_df[sim_far_df <= far].shape[0]
       frac[idx] = float(num_events) / float(tot_num_events)

    frac_df = pd.DataFrame({'far': far_thresh, 'frac': frac})

    if(plname != None):
      xinf = pow(10,far_loglow)
      xsup = pow(10,far_loghigh)
      plt.figure(figsize=(10,7))
      plt.plot(frac_df['far'], frac_df['frac'], c=color, label='rhor')
      plt.legend(loc='lower right')
      plt.ylabel('Detection efficiency')
      plt.xlabel('False alarm rate (yr$^{-1}$)')
      plt.axis([xinf,xsup,0.0,1.0])
      plt.xscale('log')
      plt.title("Detection Efficiency vs FAR", fontsize=30, pad=20)
      plt.tight_layout()
      plt.grid()
      plt.savefig(plname)
      plt.close()

    # compute aucelf, area under the curve: efficiency vs log10(far) 
    aucelf=0
    pfar=0	# previous false alarm rate
    peff=0	# previous efficiency
    for far, eff in zip(far_thresh, frac):
      #print(far,eff)
      #if(far>0.8):
      #  break
      if(pfar>0):
        aucelf+=(eff+peff)*(np.log10(far)-np.log10(pfar))/2
      pfar=far
      peff=eff

    aucelf = aucelf/far_loghigh 	# the log10 far range is normalized to 1

    return frac_df, aucelf


def getopt(arguments,option,default,separator=" "):
   """
   function purpose:
      return the option value found in the arguments string, if not found ir returns the default values 

   input params:
      arguments:   list of options (option1=xx option2=yy ...)
      option:      option name (eg: option1)
      default:     option default value (used when oprion is not present in arguments)
      separator:   used to split options (eg: 'option1=xx:option2=yy')
   """

   split_arguments = arguments.split(separator)

   value=default
   for s in split_arguments:
     if (s.find(option+'=')!=-1):
       s=s.replace("\t","")             # remove tabs
       s=s.replace(" ","")              # remove white spaces
       value=s.replace(option+'=',"")
       break

   if(value==''):
     print('\nError: missing parameter -> '+option+'\n')
     exit(1)

   return value


def aucelf_rhor(preds, dtrain):
   """
   function purpose:
      return the aucelf_rhor_score: area under the curve: efficiency vs log10(far)

   notes:
      aucelf_rhor is a custom metric based on cWB rhor statistic -> preliminary tests show that is equivalent to AUCPR metric
      this metric can be used in tuning/prediction setting eval_metric=cwb.aucelf_rhor where cwb is 'import cwb_xgboost as cwb'

   input params:
      preds:       prediction statistic by xgboost
      dtrain:      data training set. Is contains the features parameters in DMatrix format and classifier (0=noise, 1=sim)
   """

   # contains classifier (0=noise, 1=sim)
   labels = dtrain.get_label()
   #print("labels",labels)
   #print("labels len = ",labels.shape[0])

   # extrat feature_names list
   feature_names=dtrain.feature_names
   #print("feature_names",feature_names)

   # get data (csr_matrix) from dtrain (DMatrix)
   dtrain_data=dtrain.get_data()

   # extract rho0 index
   for feature in feature_names:
     if 'rho0' in str(feature):
       rho0_index=feature_names.index(feature)
       print("rho0 = ",feature," index = ",feature_names.index(feature))
   #print("rho0_index = ",rho0_index)

   # get rho0 data from dtrain
   rho0=dtrain_data.getcol(rho0_index)
   rho0=np.transpose(rho0)
   rho0=rho0.toarray()
   rho0=rho0[0]
   #print("rho0 = ",rho0)
   #print("rho0 len = ",rho0.shape[0])

   # extract penalty/Qa/Qp index
   penalty_index=feature_names.index("penalty")
   Qa_index=feature_names.index("Qa")
   Qp_index=feature_names.index("Qp")

   # get penalty/Qa/Qp data from dtrain
   penalty=dtrain_data.getcol(penalty_index)
   penalty=np.transpose(penalty)
   penalty=penalty.toarray()
   penalty=penalty[0]
   Qa=dtrain_data.getcol(Qa_index)
   Qa=np.transpose(Qa)
   Qa=Qa.toarray()
   Qa=Qa[0]
   Qp=dtrain_data.getcol(Qp_index)
   Qp=np.transpose(Qp)
   Qp=Qp.toarray()
   Qp=Qp[0]

   # convert to final rhor statistic

   df = pd.DataFrame({'rho0':rho0 , 'classifier':labels, 'Qa':Qa, 'Qp':Qp, 'penalty':penalty, 'MLstat':preds})
   df = df.astype('float64')

   qoffset=0.4
   a=0.15
   df['ecor'] = np.power(df['rho0']*np.sqrt((1+df['penalty']*(np.maximum(1,df['penalty'])-1))),2)
   df['MLstat_cor'] = df['MLstat']+(np.log10(df['ecor'])/2*np.sqrt((1+df['penalty']*(np.maximum(1,df['penalty'])-1))))*(-a+df['Qa']*(df['Qp']-qoffset))
   df.loc[(df.Qa*(df.Qp - qoffset) > a),'MLstat_cor'] = df['MLstat']
   df.loc[(df.MLstat_cor < 0.),'MLstat_cor'] = 0
   df['Wc'] = -np.log(1.0 - 0.995*np.sqrt(df['MLstat_cor']))/5.3
   df['rhor'] = np.sqrt(df['ecor'])*df['Wc']/np.sqrt(1+df['penalty']*(np.maximum(1,df['penalty'])-1))

   # compute aucelf_rhor_score

   bkg = df.loc[df.classifier == 0]
   #print("bkg = ",bkg)
   #print("bkg events = ",bkg.shape[0])
   rhor_rate = far_rho(bkg, bkg_years=1, ts='rhor', step_size = 0.01, plname=None, color='bo', verbose=False)
   sim = df[df['classifier']==1]
   #print("sim",sim)
   #print("sim.dtypes",sim.dtypes)
   #print("sim events = ",sim.shape[0])
   sim = pd.merge_asof(sim.sort_values('rhor'), rhor_rate.sort_values('rhor'), on="rhor", direction="nearest")
   _, aucelf_rhor_score = eff_far(sim, 'rate_rhor', tot_num_events=sim.shape[0], far_loglow = 0.0, far_loghigh = np.log10(bkg.shape[0]),
                                 num_steps=10000, color='red', plname=None)

   #auc   = roc_auc_score(labels, preds)
   #aucpr = precision_recall_curve(labels, preds)

   # release memory
   del df
   del rho0
   del penalty
   del Qa
   del Qp
   del bkg
   del sim
   gc.collect()    # garbage collector

   return 'aucelf_rhor', aucelf_rhor_score
   #return [('auc', auc), ('aucpr', aucpr), ('aucelf_rhor', aucelf_rhor_score)]

