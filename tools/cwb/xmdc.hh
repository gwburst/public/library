/*
# Copyright (C) 2024 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*****************************************************************
 * Package:      xmdc Class Library (eXtended MDC)
 * File name:    xmdc.cc
 * Author:       Gabriele Vedovato (vedovato@lnl.infn.it)
 *****************************************************************/

// This class is used to generate injections distributed uniformely in volume

#ifndef XMDC_HH
#define XMDC_HH

#include "TTree.h"
#include "TString.h"
#include "CWB_Plugin.h"
#include "wavearray.hh"
#include "History.hh"
#include <vector>
#include "mdc.hh"

using namespace std;

#define XMDC_OBJECT_NAME         "xmdc"
#define XMDC_TREE_INJ_NAME       "injections"
#define XMDC_TREE_WAVE_NAME      "waveforms"

namespace CWB {

class xmdc : public TNamed {

#define XMDC_VERSION	5			// WARNING: version number must change when xmdc.hh sructure change !!!
#define XMDC_STAGE	"XMDC"			// user in the history class

#define XMDC_SUBDIR_DATA         "data"		// preproduction data output dir
#define XMDC_SUBDIR_REPORT       "report"	// preproduction report output dir
#define XMDC_SUBDIR_CONFIG       "config"	// preproduction config output dir
#define XMDC_SUBDIR_SUMMARY      "summary"	// preproduction summary output dir

public:

  struct injection {				// XMDC injection structure
    double 		snr_min;		// network snr min thresholds 
    double  		fiducial_hrss;		// fiducial hrss values (size = inj_nid)
  };

  struct config {				// XMDC configuration structure
    vector<TString> 	detector_name;		// network detector names
    vector<TString> 	psd_fname;		// psd file names
    TString 	  	umacro_path_name="";	// user macro path name
    TString             umacro_source_code="";  // user macro source code
    TString             umacro_func_name="";    // macro InitMDC(builtin)/getWaveforms(external) user config function name
    TString             umacro_inj_type="";     // injection type [builtin(defined in mdc)/external(eg: CCSN)]
    TString             sky_distribution="";    // sky distribution [uv(uniform_in_volume)/poly.../mngd(Miyamoto-Nagai)/mmgd(McMillan Galactic Disk Model)]

    vector<injection>	inj;			// injection stucture
    int		  	inj_size=-1;		// input number of injections to be generated
    int		  	inj_max_trials=0;	// max number of testing trials, if 0 there are no limits
    int 	  	inj_len=-1;		// injection buffer lenght (integer even number of sec)
    double 	  	inj_srate=-1;		// injection sample rate (Hz)
    double 	  	inj_resample=-1;	// injection resample rate (Hz)
    double 	  	inj_flow=-1;		// injection low frequency (Hz)
    double 	  	inj_fhigh=-1;		// injection high frequency (Hz)
    double 	  	inj_distance=-1;	// distance (Mpc) associated to the input waveforms (Ex: CCSN read from file),
						// used in GetInjections to compute the fiducial distance. If 0 then the fiducial_hrss is used 
    double 	  	inj_fiducial_fraction=-1;	// is the fiducial fraction between selected and total number of events inside the fiducial volume
						// is used only to dump the set of fiducial_hrss related to inj_fiducial_fraction value
    double 	  	inj_step=-1;		// injection time interval (sec)
    double 	  	inj_jitter=-1;		// injection time jitter (sec)
    double 	  	inj_offset=-1;		// injection time offset (sec)

    double		inj_iota=-1; 		// iota = 0  : the line of sight is perpendicular to the orbit plane
						// iota = 90 : the line of sight has 0 deg inclination angle respect to the orbit plane 
						// iota = -1 : iota is random

    bool		qveto_enable=false;	// true/false -> enable/disable qveto estimation (disable -> save execution time)

    int		  	seed=150914;		// seed used to initialize the random number generator
    TString 	  	tag="";			// tag string. Ex: CCSN, StdInj 
  };

  struct rconfig {				// XMDC report configuration structure
    TString		xmdc_fname;		// xmdc file name
    vector<TString> 	gid_name;		// used by ComputeVisibleVolume
    vector<vector<int>> gid;			// used by ComputeVisibleVolume
    double          	visibleV_min;		// TGraphError visible volume min (Mpc^3)
    double          	visibleV_max;		// TGraphError visible volume max (Mpc^3)
    vector<double> 	visibleV_ifar;		// used by the ReportVisibleVolume method
    double  		hrss_at_1Mpc;		// hrss @ 1 Mpc 
    TString		distance_units;		// Kpc, Mpc
  };

  struct jsummary {				// summary structure -> contains data produced by the MakeInjections function
    int 		ID;			// waveform ID
    TString 		name;			// waveform name
    int 		nsel;			// number of selected injections above the snr threshold
    int 		ntst;			// number of injections with hrss > fiducial hrss
    double		fiducial_distance;	// (Mpc) is > 0 when inj_distance>0. Is computed in GetInjections
						// if>0 it is used to compute the visible volume otherwise must be used fiducial_hrss
  };

  struct summary {				// final summary structure for all injection types
    vector<jsummary>	data;			// summary injection data
  };
 
  xmdc();					// default constructor
  xmdc(config ucfg);				// constructor with input user XMDC config
  xmdc(TString jfname);				// constructor with input XMDC config from XMDC file

  ~xmdc();
 
  // preproduction 
  TString MakeInjections(TString odir, bool oplot, TTree* GetWaveforms());
  
  // postproduction 
  void ReportGDDistributions(TString wfname, rconfig rcfg, TString odir);
  void ReportVisibleVolume(TString wfname, rconfig rcfg, TString odir);
  void ReportDistributions(TString wfname, vector<double> ifar, rconfig rcfg, TString odir);
  void ReportDistributions(TString wfname, double ifar, rconfig rcfg, TString odir) {
         vector<double> vifar = {ifar}; ReportDistributions(wfname, vifar, rcfg, odir);
       }
  static config GetConfig(TString jfname);		// get config from XMDC jroot file name
  static config GetConfig(TFile* jroot);		// get config from XMDC jroot file
  config GetConfig() {return cfg;}			// get config from XMDC object
  static void  CheckConfig(config ucfg);		// check input user config
  void 	 CheckConfig() {CheckConfig(this->cfg);}	// check XMDC config
  static void  PrintConfig(config ucfg, TString ofname="");	// print input user config
  void 	 PrintConfig(TString ofname="") { 			// print XMDC config
	   PrintConfig(this->cfg, ofname);
	 }

  static void PrintReportConfig(rconfig rcfg);		// print XMDC report rconfig
  static void CheckReportConfig(rconfig rcfg);		// check XMDC report rconfig

  static int  GetVersion(TString jfname);		// get XMDC version from XMDC jroot file
  static int  GetVersion() {return XMDC_VERSION;}	// get XMDC version from XMDC object
  static void CheckVersion(TString jfname);		// check XMDC version: XMDC object version vs XMDC version in jfname

  static CWB::History* GetHistory(TFile* jroot);	// get history from XMDC jroot file

  static TString GetStageName() {return XMDC_STAGE;}	// get XMDC history stage name

  vector<wavearray<double>> GetPSD(TString jfname);	// get psd from XMDC jroot file name

  static summary GetSummary(TString jfname);		// get summary from XMDC jroot file name
  static summary GetSummary(TFile* jroot);		// get summary from XMDC jroot file
  summary GetSummary() {return sdata;}			// get XMDC summary data
  void 	  PrintSummary(TString ofname="");		// print XMDC summary data

  static void InitMDC(TFile* jroot, TString tmpFile);	// used to add waveforms to MDC from waveform tree
  static void Exec(TString command, bool verbose=true);	// exec command line (used for cwb_mkhtml)
  static void ConvertPS2PNG(TString idir);		// convert all ps files contained in idir to png
  static void GetQveto(wavearray<double>* wf, float &Qveto, float &Qfactor);	// computr Qveto

  // write report infos file
  static void WriteReportInfos(TString type, TString infos, TString odir, TString umacro_path_name="", bool add_run_infos=true);
 
private:

  void InitNetwork();					// init jNET object
  void InitMDC(TFile* inj_froot);			// init jMDC object
  vector<wavearray<double>> GetPSD(); 			// read PSD input files (cfg.psd_fname)

  void InitConfig();					// init XMDC config

  TF1* InitPOLY(TString sky_distribution);		// define distance distribution -> p0 + p1*dist^1 + p2*dist^2 [0,1]

  void WriteHistory();					// write history to output root file

  void CheckMD5(TString wfname);			// check md5 xmdc file stored in the wfname history vs gJNAME md5

  // fill the injections list  
  TTree* GetInjections(vector<wavearray<double>> psd, bool oplot, TString odir);
  
  // report injection infos, used by MakeInjections
  void ReportDistributions(TString ofname);
  void ReportTypeDistributions(TString type, int ID, TString jname, TString ofname);
  void ReportThetaPhiDistributions(TString type, int ID, TString jname, TString ofname);
  void ReportGDDistributions(TString wfname, TString view, int ID, TString jname, TString ofname);
  void ReportWaveforms(vector<wavearray<double>> psd, TString odir);
  void ReportWaveform(int inj_id, vector<wavearray<double>> psd, TString ofname);

  // print production plots, used by ReportDistributions
  void ReportDistributions(TString plot_type, TString wfname, double ifar, rconfig rcfg, TString odir);
  void PrintFiducialHrssAtFixedFiducialFraction(TString ofname="");

  config  cfg;						// user input XMDC configuration
  summary sdata;					// output summary data
  vector<wavearray<double>> psd;			// psd arrays 

  // global variables
  CWB::mdc* gMDC;	//! mdc
  network*  gNET;	//! network
  TTree*    gTREE;	//! injection tree used for output plots
  TString   gJFNAME;	//! use by the constructor 'CWB::xmdc::xmdc(TString jfname)' to save the injection file name
                        // the methods ReportVisibleVolume & ReportDistributions can be used only if it is != ""

  ClassDef(xmdc,XMDC_VERSION)
};
  
} // end namespace
  
#endif
  
