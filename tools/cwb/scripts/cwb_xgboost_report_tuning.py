import os
from pathlib import Path
import numpy as np
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rcParams
import math


def getopts(arguments):

   idir=""		# input directory for output jobs
   flabel=""		# data label
   ulabel=""		# user label
   ntrials=""		# number of xgb trials
   odir=""	        # output xgb
   verbose=False
   how2use="\n how to use -> python3 cwb_xgboost_report_tuning.py idir=... flabel=... ulabel=... ntrials=... odir=... verbose\n"

   for s in arguments:
    if (s.find("idir=")!=-1):
      idir=s.replace("idir=","")
    elif (s.find("flabel=")!=-1):
      flabel=s.replace("flabel=","")
    elif (s.find("ulabel=")!=-1):
      ulabel=s.replace("ulabel=","")
    elif (s.find("odir=")!=-1):
      odir=s.replace("odir=","")
    elif (s.find("verbose")!=-1):
      verbose=True
    elif (s.find("ntrials=")!=-1):
      sntrials=s.replace("ntrials=","")
      ntrials=int(sntrials)
   if (len(idir)==0):
     print("Error, no idir directory specified")
     print(how2use)
     exit()
   if (len(flabel)==0):
     print("Error, no flabel specified")
     print(how2use)
     exit()
   if (len(odir)==0):
     print("Error, no odir directory specified")
     print(how2use)
     exit()
   if (ntrials <= 0):
     print("Error, wrong input ntrials, must be > 0")
     print(how2use)
     exit()

   print("\ninput cwb_xgboost_report_tuning parameters\n")
   print(" input idir                             = ", idir)
   print(" input flabel                           = ", flabel)
   print(" input ulabel                           = ", ulabel)
   print(" input ntrials                          = ", ntrials)
   print(" output odir                            = ", odir)
   print(" verbose                                = ", verbose)
   print("\n")

   return idir,flabel,ulabel,ntrials,odir,verbose

def plot_gridsearch_results(auc_score,aucpr_score,fbeta_score,brier_score,ntrials):

   #plotting options
   rcParams["text.usetex"] = True
   rcParams["font.serif"] = "Times New Roman"
   rcParams["font.family"] = "Serif"
   rcParams["xtick.labelsize"]=28
   rcParams["ytick.labelsize"]=28
   rcParams["legend.fontsize"]=22
   rcParams["axes.labelsize"]=28

   brier_loss_score = 1 - brier_score

   if (len(ulabel)==0):
     ofile = odir+'/gridsearch_results.png'
   else:
     ofile = odir+'/gridsearch_results_'+ulabel+'.png'

   #y_min = np.min(fbeta_score) - 0.1
   y_min = np.min(aucpr_score) - 0.1
   y_max = 1.0
   x_min = 0
   x_max = ntrials
   plt.figure(figsize=(10,7))
   plt.vlines(auc_i, y_min, y_max, colors='forestgreen')
   plt.vlines(aucpr_i, y_min, y_max, colors='royalblue')
   plt.vlines(fbeta_i, y_min, y_max, colors='cyan')
   plt.vlines(brier_i, y_min, y_max, colors='darkmagenta')
   plt.plot(np.arange(ntrials), auc_score, 'g', markersize=2, label = 'auc')
   plt.plot(np.arange(ntrials), aucpr_score, 'b', markersize=2, label = 'aucpr')
   plt.plot(np.arange(ntrials), fbeta_score, 'c', markersize=2, label = 'fbeta0.5')
   plt.plot(np.arange(ntrials), brier_loss_score, 'm', markersize=2, label = 'brier loss')
   plt.ylim(y_min, y_max)
   plt.xlim(x_min, x_max)
   plt.legend()
   plt.xlabel("hyper-parameter grid points")
   plt.ylabel("metric scores")
   plt.tight_layout()
   plt.savefig(ofile)
   plt.close()

   print('\n Write file: '+ofile)

def plot_gridsearch_rhor(aucelf_rhor_score,auc_rhor_score,aucpr_rhor_score,f1_rhor_score,ntrials):

   #plotting options
   rcParams["text.usetex"] = True
   rcParams["font.serif"] = "Times New Roman"
   rcParams["font.family"] = "Serif"
   rcParams["xtick.labelsize"]=28
   rcParams["ytick.labelsize"]=28
   rcParams["legend.fontsize"]=22
   rcParams["axes.labelsize"]=28

   brier_loss_score = 1 - brier_score

   if (len(ulabel)==0):
     ofile = odir+'/gridsearch_rhor.png'
   else:
     ofile = odir+'/gridsearch_rhor_'+ulabel+'.png'

   y_min = np.min(aucpr_rhor_score) - 0.1
   y_max = 1.0
   x_min = 0
   x_max = ntrials
   plt.figure(figsize=(10,7))
   plt.vlines(aucelf_rhor_i, y_min, y_max, colors='darkmagenta')
   plt.vlines(aucpr_rhor_i, y_min, y_max, colors='royalblue')
   plt.vlines(auc_rhor_i, y_min, y_max, colors='forestgreen')
   plt.vlines(f1_rhor_i, y_min, y_max, colors='cyan')
   plt.plot(np.arange(ntrials), aucelf_rhor_score, 'm', markersize=2, label = 'aucelf rhor')
   plt.plot(np.arange(ntrials), aucpr_rhor_score, 'b', markersize=2, label = 'aucpr rhor')
   plt.plot(np.arange(ntrials), auc_rhor_score, 'g', markersize=2, label = 'auc rhor')
   plt.plot(np.arange(ntrials), f1_rhor_score, 'c', markersize=2, label = 'f1 rhor')
   plt.ylim(y_min, y_max)
   plt.xlim(x_min, x_max)
   plt.legend()
   plt.xlabel("hyper-parameter grid points")
   plt.ylabel("rhor metric scores")
   plt.tight_layout()
   plt.savefig(ofile)
   plt.close()

   print('\n Write file: '+ofile)


if __name__ == "__main__":

   # get input user parameters
   idir,flabel,ulabel,ntrials,odir,verbose=getopts(sys.argv[1:])
   if (len(ulabel)!=0): flabel = flabel+"_"+ulabel
   #exit()

   aucelf_rhor_score  = np.zeros(ntrials);
   aucpr_rhor_score   = np.zeros(ntrials)
   auc_rhor_score     = np.zeros(ntrials)
   f1_rhor_score      = np.zeros(ntrials)
   aucpr_score        = np.zeros(ntrials)
   brier_score        = np.empty(ntrials); brier_score.fill(1)
   fbeta_score        = np.zeros(ntrials)
   auc_score          = np.zeros(ntrials)
   logloss_score      = np.empty(ntrials); logloss_score.fill(1)
   mae_score          = np.empty(ntrials); mae_score.fill(1)
   xgb_params         = [None] * ntrials
   ML_list            = [None] * ntrials
   ML_caps            = [None] * ntrials
   ML_balance         = [None] * ntrials
   ML_options         = [None] * ntrials

   print('\ninput files ...\n')
   nfiles = 0
   for i in range(ntrials):
     my_file = Path(idir+'/'+flabel+'_' + str(i) + '.out')
     #if(verbose): print(my_file)
     if my_file.is_file():
       if os.stat(str(my_file)).st_size != 0:
         if(verbose): print(' ',i,' -> ',my_file)
         nfiles += 1
         with open(str(my_file), 'r') as f:
           #print(i)
           temp_file = f.readlines()
           #if(verbose): print(temp_file[-1])
           for line in temp_file:
             if (line.find("AUCELF_rhor_score=")!=-1):
               aucelf_rhor_score[i]=float(line.replace("AUCELF_rhor_score=",""))
             if (line.find("AUCPR_rhor_score=")!=-1):
               aucpr_rhor_score[i]=float(line.replace("AUCPR_rhor_score=",""))
             if (line.find("AUC_rhor_score=")!=-1):
               auc_rhor_score[i]=float(line.replace("AUC_rhor_score=",""))
             if (line.find("f1_rhor_score=")!=-1):
               f1_rhor_score[i]=float(line.replace("f1_rhor_score=",""))
             if (line.find("mae_score=")!=-1):
               mae_score[i]=float(line.replace("mae_score=",""))
             if (line.find("logloss_score=")!=-1):
               logloss_score[i]=float(line.replace("logloss_score=",""))
             if (line.find("brier_score=")!=-1):
               brier_score[i]=float(line.replace("brier_score=",""))
             if (line.find("fbeta_score=")!=-1):
               fbeta_score[i]=float(line.replace("fbeta_score=",""))
             if (line.find("AUC_score=")!=-1):
               auc_score[i]=float(line.replace("AUC_score=",""))
             if (line.find("AUCPR_score=")!=-1):
               aucpr_score[i]=float(line.replace("AUCPR_score=",""))
             if (line.find("xgb_params=")!=-1):
               xgb_params[i]=line.replace("xgb_params=","")
             if (line.find("ML_list=")!=-1):
               ML_list[i]=line.replace("ML_list=","")
             if (line.find("ML_caps=")!=-1):
               ML_caps[i]=line.replace("ML_caps=","")
             if (line.find("ML_balance=")!=-1):
               ML_balance[i]=line.replace("ML_balance=","")
             if (line.find("ML_options=")!=-1):
               ML_options[i]=line.replace("ML_options=","")


   if(nfiles==0):
     print('\nNo files found with tag = '+idir+'/'+flabel+'_*.out\n')
     exit(0)

   aucelf_rhor_i  = np.argmax(aucelf_rhor_score)
   aucpr_rhor_i   = np.argmax(aucpr_rhor_score)
   auc_rhor_i     = np.argmax(auc_rhor_score)
   f1_rhor_i      = np.argmax(f1_rhor_score)
   aucpr_i        = np.argmax(aucpr_score)
   brier_i        = np.argmin(brier_score)
   fbeta_i        = np.argmax(fbeta_score)
   auc_i          = np.argmax(auc_score)
   logloss_i      = np.argmin(logloss_score)
   mae_i          = np.argmin(mae_score)

   plot_gridsearch_results(auc_score,aucpr_score,fbeta_score,brier_score,ntrials)
   plot_gridsearch_rhor(aucelf_rhor_score,auc_rhor_score,aucpr_rhor_score,f1_rhor_score,ntrials)

   if (len(ulabel)==0):
     ofile = odir+'/gridsearch_results.txt'
   else:
     ofile = odir+'/gridsearch_results_'+ulabel+'.txt'

   print('\noutput file\n\n',ofile,'\n')

   stdoutOrigin=sys.stdout
   with open(ofile, 'w') as f:
     sys.stdout = f # Change the standard output to the file we created.
     print('')

     print("----------------------------------------------------------------------")
     print("Best parameters based on AUC score: " + str(auc_score[auc_i]))
     print("----------------------------------------------------------------------\n")
     print("Input file: "+idir+'/'+flabel+'_'+str(auc_i)+'.out'+'\n')
     print("xgb_params: " + str(xgb_params[auc_i]))
     print("ML_list: " + str(ML_list[auc_i]))
     print("ML_caps: " + str(ML_caps[auc_i]))
     print("ML_balance: " + str(ML_balance[auc_i]))
     print("ML_options: " + str(ML_options[auc_i]))
     print('')

     print("----------------------------------------------------------------------")
     print("Best parameters based on AUC PR score: " + str(aucpr_score[aucpr_i]))
     print("----------------------------------------------------------------------\n")
     print("Input file: "+idir+'/'+flabel+'_'+str(aucpr_i)+'.out'+'\n')
     print("xgb_params: " + str(xgb_params[aucpr_i]))
     print("ML_list: " + str(ML_list[aucpr_i]))
     print("ML_caps: " + str(ML_caps[aucpr_i]))
     print("ML_balance: " + str(ML_balance[aucpr_i]))
     print("ML_options: " + str(ML_options[aucpr_i]))
     print('')
     
     print("----------------------------------------------------------------------")
     print("Best parameters based on f0.5 score: " + str(fbeta_score[fbeta_i]))
     print("----------------------------------------------------------------------\n")
     print("Input file: "+idir+'/'+flabel+'_'+str(fbeta_i)+'.out'+'\n')
     print("xgb_params: " + str(xgb_params[fbeta_i]))
     print("ML_list: " + str(ML_list[fbeta_i]))
     print("ML_caps: " + str(ML_caps[fbeta_i]))
     print("ML_balance: " + str(ML_balance[fbeta_i]))
     print("ML_options: " + str(ML_options[fbeta_i]))
     print('')
     
     print("----------------------------------------------------------------------")
     print("Best parameters based on brier score loss: " + str(brier_score[brier_i]))
     print("----------------------------------------------------------------------\n")
     print("Input file: "+idir+'/'+flabel+'_'+str(brier_i)+'.out'+'\n')
     print("xgb_params: " + str(xgb_params[brier_i]))
     print("ML_list: " + str(ML_list[brier_i]))
     print("ML_caps: " + str(ML_caps[brier_i]))
     print("ML_balance: " + str(ML_balance[brier_i]))
     print("ML_options: " + str(ML_options[brier_i]))
     print('')
     
     print("----------------------------------------------------------------------")
     print("Best parameters based on MAE score: " + str(mae_score[mae_i]))
     print("----------------------------------------------------------------------\n")
     print("Input file: "+idir+'/'+flabel+'_'+str(mae_i)+'.out'+'\n')
     print("xgb_params: " + str(xgb_params[mae_i]))
     print("ML_list: " + str(ML_list[mae_i]))
     print("ML_caps: " + str(ML_caps[mae_i]))
     print("ML_balance: " + str(ML_balance[mae_i]))
     print("ML_options: " + str(ML_options[mae_i]))
     print('')
     
     print("----------------------------------------------------------------------")
     print("Best parameters based on aucelf_rhor score: " + str(aucelf_rhor_score[aucelf_rhor_i]))
     print("----------------------------------------------------------------------\n")
     print("Input file: "+idir+'/'+flabel+'_'+str(aucelf_rhor_i)+'.out'+'\n')
     print("xgb_params: " + str(xgb_params[aucelf_rhor_i]))
     print("ML_list: " + str(ML_list[aucelf_rhor_i]))
     print("ML_caps: " + str(ML_caps[aucelf_rhor_i]))
     print("ML_balance: " + str(ML_balance[aucelf_rhor_i]))
     print("ML_options: " + str(ML_options[aucelf_rhor_i]))
     
     print("----------------------------------------------------------------------")
     print("Best parameters based on aucpr_rhor score: " + str(aucpr_rhor_score[aucpr_rhor_i]))
     print("----------------------------------------------------------------------\n")
     print("Input file: "+idir+'/'+flabel+'_'+str(aucpr_rhor_i)+'.out'+'\n')
     print("xgb_params: " + str(xgb_params[aucpr_rhor_i]))
     print("ML_list: " + str(ML_list[aucpr_rhor_i]))
     print("ML_caps: " + str(ML_caps[aucpr_rhor_i]))
     print("ML_balance: " + str(ML_balance[aucpr_rhor_i]))
     print("ML_options: " + str(ML_options[aucpr_rhor_i]))
     
     print("----------------------------------------------------------------------")
     print("Best parameters based on auc_rhor score: " + str(auc_rhor_score[auc_rhor_i]))
     print("----------------------------------------------------------------------\n")
     print("Input file: "+idir+'/'+flabel+'_'+str(auc_rhor_i)+'.out'+'\n')
     print("xgb_params: " + str(xgb_params[auc_rhor_i]))
     print("ML_list: " + str(ML_list[auc_rhor_i]))
     print("ML_caps: " + str(ML_caps[auc_rhor_i]))
     print("ML_balance: " + str(ML_balance[auc_rhor_i]))
     print("ML_options: " + str(ML_options[auc_rhor_i]))
     
     print("----------------------------------------------------------------------")
     print("Best parameters based on f1_rhor score: " + str(f1_rhor_score[f1_rhor_i]))
     print("----------------------------------------------------------------------\n")
     print("Input file: "+idir+'/'+flabel+'_'+str(f1_rhor_i)+'.out'+'\n')
     print("xgb_params: " + str(xgb_params[f1_rhor_i]))
     print("ML_list: " + str(ML_list[f1_rhor_i]))
     print("ML_caps: " + str(ML_caps[f1_rhor_i]))
     print("ML_balance: " + str(ML_balance[f1_rhor_i]))
     print("ML_options: " + str(ML_options[f1_rhor_i]))
     print('')

   aucelf_rhor_score  = np.zeros(ntrials)
   aucpr_rhor_score   = np.zeros(ntrials)
   auc_rhor_score     = np.zeros(ntrials)
   f1_rhor_score      = np.zeros(ntrials)
   aucpr_score        = np.zeros(ntrials)
   brier_score        = np.zeros(ntrials)
   fbeta_score        = np.zeros(ntrials)
   auc_score          = np.zeros(ntrials)
   logloss_score      = np.zeros(ntrials)
   mae_score          = np.zeros(ntrials)

