#!/usr/bin/env tcsh

onintr irq_ctrlc

if ( $1 == '') then
  $CWB_SCRIPTS/cwb_help.csh cwb_compile
  exit
endif

setenv CWB_COMPILE_MACRO_NAME $1
setenv CWB_COMPILE_MACRO_OPTIONS "$2"

root -n -l -b ${CWB_ROOTLOGON_FILE} ${CWB_PARAMETERS_FILE} ${CWB_EPARAMETERS_FILE} ${CWB_MACROS}/cwb_compile.C

unsetenv CWB_COMPILE_MACRO_NAME
unsetenv CWB_COMPILE_MACRO_OPTIONS

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1

