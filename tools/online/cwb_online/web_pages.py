#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago, Igor Yakushin, Sergey Klimenko
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from cwb_online.run_utils import get_time, get_date_from_gps, get_start_of_the_day_string, time_convert
import subprocess, os, sys, glob
import cwb_online.summary_page as summary_page
import cwb_online.run_calendar as run_calendar
from cwb_online.run import JOB
import cwb_online.checkstatus as checkstatus
from ligo.segments import segment, segmentlist
from ligo.segments.utils import tosegwizard, fromsegwizard

def web_page(choice,cWB_conf):
  if (not os.path.exists("%s/%s/seg_global.txt"%(cWB_conf.run_dir,cWB_conf.seg_dir))):
    return

  f = open("%s/%s/seg_global.txt"%(cWB_conf.run_dir,cWB_conf.seg_dir))
  segments = fromsegwizard(f)
  f.close()

  if (abs(segments)==0):
    return

  current_time=get_time()
  collection_jobs_dir=cWB_conf.run_dir
  if (hasattr(cWB_conf,"backup_dir")): 
     collection_jobs_dir=cWB_conf.backup_dir

  if (choice.find("-")!=-1):
    if (os.path.exists("%s/%s/%s"%(collection_jobs_dir,cWB_conf.jobs_dir,choice))):
      rootdir="%s/%s/daily"%(collection_jobs_dir,cWB_conf.summaries_dir)
      date=choice
      page_rootdir="%s/%s"%(rootdir,date)
      b=time_convert("-f \"%D\" "+str(get_start_of_the_day_string(date)))
      cdate=b[1].strip()
      print("Date",date,cdate)
      summary_page.summary_page(cWB_conf,date,page_rootdir,True,True,"day.html","%s"%(cdate))
      dirs=glob.glob("%s/%s/daily/????-??-??"%(collection_jobs_dir,cWB_conf.summaries_dir))
      days=list(map(lambda x: x.split("/")[-1], dirs))
      run_calendar.run_calendar(cWB_conf,days)
    else:
      print("%s/%s/%s does not exist, check if it is a valid date or it has not been processed"%(collection_jobs_dir,cWB_conf.jobs_dir,choice))

  if (choice=="daily"):
   print("-"*50)
   a=time_convert("-f \"%D\" now")
   date=get_date_from_gps(a[1])
   rootdir="%s/%s/daily"%(collection_jobs_dir,cWB_conf.summaries_dir)
   b=time_convert("-f \"%D\" "+str(get_start_of_the_day_string(date)))
   cdate=b[1].strip()
   print("Today",get_date_from_gps(a[1]),cdate)
   page_rootdir="%s/%s"%(rootdir,date)
   summary_page.summary_page(cWB_conf,date,page_rootdir,True,True,"day.html","%s"%(cdate))    

   time=get_start_of_the_day_string(date)
   date=get_date_from_gps(time-86400)
   rootdir="%s/%s/daily"%(collection_jobs_dir,cWB_conf.summaries_dir)
   b=time_convert("-f \"%D\" "+str(get_start_of_the_day_string(date)))
   cdate=b[1].strip()
   print("Yesterday",get_date_from_gps(a[1]),cdate)
   page_rootdir="%s/%s"%(rootdir,date)
   summary_page.summary_page(cWB_conf,date,page_rootdir,True,True,"day.html","%s"%(cdate))    

   dirs=glob.glob("%s/%s/daily/????-??-??"%(collection_jobs_dir,cWB_conf.summaries_dir))
   days=list(map(lambda x: x.split("/")[-1], dirs))
   run_calendar.run_calendar(cWB_conf,days)

  if (choice=="all" or choice=="days"):
   print("-"*50)
   print("Each day")
   dirs=glob.glob("%s/%s/????-??-??"%(collection_jobs_dir,cWB_conf.jobs_dir))
   dirs.sort()
   print(dirs)
   rootdir="%s/%s/daily"%(collection_jobs_dir,cWB_conf.summaries_dir)
   a=time_convert("-f \"%D\" now")
   today=get_date_from_gps(a[1])
   days=[]
   if (len(dirs)>0):
     start_days=dirs[0].split("/")[-1]
     start_time=get_start_of_the_day_string(start_days)
     time=get_start_of_the_day_string(today) 
     while(start_time<=time):
       days.append(get_date_from_gps(time))
       time-=86400
   for date in days:
      print("day=",date)
      page_rootdir="%s/%s"%(rootdir,date)
      print("page_rootdir=%s"%page_rootdir)
      a=time_convert("-f \"%D\" "+str(get_start_of_the_day_string(date)))
      cdate=a[1].strip()
      summary_page.summary_page(cWB_conf,date,page_rootdir,True,True,"day.html","%s"%(cdate))    
      print("="*80)
   run_calendar.run_calendar(cWB_conf,days)

  #----------------------------------------------------------
  if (choice=="all" or choice=="day"):
   print("-"*50)
   print("Last day")
   page_rootdir="%s/%s/last_day"%(collection_jobs_dir,cWB_conf.summaries_dir)
   summary_page.summary_page(cWB_conf,1,page_rootdir,True,True,"last_day.html","Last day")

  #----------------------------------------------------------
  if (choice=="all" or choice=="week"):
   print("-"*50)
   print("Last week")
   page_rootdir="%s/%s/last_week"%(collection_jobs_dir,cWB_conf.summaries_dir)
   summary_page.summary_page(cWB_conf,7,page_rootdir,True,True,"last_week.html","Last week")

  #----------------------------------------------------------
  if (choice=="all" or choice=="run"):
   print("-"*50)
   print("All run")
   page_rootdir="%s/%s/the_whole_run"%(collection_jobs_dir,cWB_conf.summaries_dir)
   summary_page.summary_page(cWB_conf,0,page_rootdir,True,True,"the_whole_run.html","The whole run")
  
  #----------------------------------------------------------
  if (choice.find("all")!=-1 or choice.find("check")!=-1):
    checkstatus.check_status(cWB_conf)

def web_pages(s_choice,loop,cWB_conf):
    choices=s_choice.split(",")
    if (loop==True):
        while(1):
          for choice in choices:
            web_page(choice,cWB_conf)
          os.system("sleep %d"%(10*60))
    else:
      for choice in choices:
        web_page(choice,cWB_conf)
