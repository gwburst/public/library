#!/usr/bin/env tcsh

onintr irq_ctrlc

set cmd_line="$0 $argv"

if ((($1 == '') || ("$2" == '')) && ($1 != 'list')) then
  $CWB_SCRIPTS/cwb_help.csh cwb_settrials
  exit
endif

if ($1 == 'list') then
  root -n -l -b ${CWB_PARMS_FILES} ${CWB_MACROS}/cwb_dump_merge_dir.C
  exit
endif

setenv CWB_MERGE_LABEL       $1
setenv CWB_SETTRIALS_FACTOR "$2"

root -n -l -b ${CWB_PPARMS_FILES} ${CWB_MACROS}/cwb_settrials.C
if ( $? != 0) exit 1

# create cWB_analysis.log file
make -f $CWB_SCRIPTS/Makefile.log CMD_LINE="$cmd_line" git >& /dev/null

unsetenv CWB_MERGE_LABEL
unsetenv CWB_SETTRIALS_FACTOR

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1

