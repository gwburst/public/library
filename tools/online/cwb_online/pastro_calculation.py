#!/usr/bin/env python3

# Copyright (C) 2019 Marek Szczepanczyk, Marco Drago
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse, h5py
import numpy as np
import bisect

#import pastro_rates_functions as ratef
import cwb_online.pastro_fgmc_laguerre as fgmcl

def log_rho_bg(trigs, bins, counts):
        """
        trigs: zerolag event statistic values
        bins: bin edges of the background histogram
        counts: background histogram

        Returns:
        log of background PDF at the zerolag statistic values,
        fractional uncertainty due to Poisson count (set to 100% for empty bins)
        """
        trigs = np.atleast_1d(trigs)
        if len(trigs) == 0: # corner case
            return np.array([]), np.array([])

        assert np.all(trigs >= np.min(bins)), 'cannot have triggers smaller than bin lower limit'

        N = sum(counts)
        # If any zerolag triggers that are louder than the max bin, put one
        # fictitious count in a bin that extends from the limits of the slide triggers
        # out to the loudest trigger.
        if np.any(trigs >= np.max(bins)):
            N = N + 1

        log_rhos = []
        fracerr = []
        for t in trigs:
            if t >= np.max(bins):
                # For a trigger louder than the max bin, put one fictitious count in
                # a bin that extends from the limits of the slide triggers out to the
                # loudest trigger.  Fractional error is 100%
                log_rhos.append(-np.log(N) - np.log(np.max(trigs) - bins[-1]))
                fracerr.append(1.)
            else:
                i = bisect.bisect(bins, t) - 1
                # If there are no counts for a foreground trigger put a fictitious count
                # in the background bin
                if counts[i] == 0:
                    counts[i] = 1
                log_rhos.append(np.log(counts[i]) - np.log(bins[i+1] - bins[i]) - np.log(N))
                fracerr.append(counts[i] ** -0.5)
        return np.array(log_rhos), np.array(fracerr)

class getargs:
 def __init__(self,txt_zerolag,txt_timelag,txt_background_ratio,injection_files):
   self.txt_zerolag = txt_zerolag
   self.txt_timelag = txt_timelag
   self.txt_background_ratio = txt_background_ratio
   self.injection_files = injection_files
   self.power_law_prior = -0.5
   self.th = 0.01
   self.ntop = 5
   self.plot_max_stat = 15
   self.background_log_bin_width = 0.015
   self.inj_log_bin_width = 0.0333
   self.laguerre_degree = 20
   self.p_astro_txt = "p_astro_data.txt"
   self.plot_dir = "plots/"
   self.plot_tag = "BBH_XP"
   self.verbose = False
   self.plot_limits = None

####### FUNCTIONS ########

def read_zerolag_timelag(args):

    #print(zltuple)
    zl = np.loadtxt(args.txt_zerolag, delimiter=',', ndmin=2)
    full_dict = {}
    if zl.size == 0:# corner case with no events
        full_dict['zerolagstat'] = np.array([])
        full_dict['zerolagtime'] = np.array([])
        full_dict['zerolagifar'] = np.array([])
    else:
        zlstat = zl[:,0]
        zltime = zl[:,1]
        zlifar = zl[:,2]
        full_dict['zerolagstat'] = zlstat[zlstat > args.th]
        full_dict['zerolagtime'] = zltime[zlstat > args.th]
        full_dict['zerolagifar'] = zlifar[zlstat > args.th]
    lag = np.loadtxt(args.txt_timelag)
    lag = lag[lag > args.th]
    full_dict['cstat_back_exc'] = lag
    full_dict['dec_factors'] = np.ones_like(lag)
    ratio = np.loadtxt(args.txt_background_ratio)
    exp_bg = float(len(lag)) * ratio

    return full_dict, exp_bg

# weighted histogram for non-volume distributed injections
def log_rho_fg_weighted(trigs, injstat, bins, weights=None):
    """
    trigs: zerolag event statistic values
    injstat: injection event statistic values
    bins: histogram bins

    Returns:
    log of signal PDF at the zerolag statistic value,
    fractional uncertainty from Poisson count
    """
    trigs = np.atleast_1d(trigs)
    if len(trigs) == 0: # corner case
        return np.array([]), np.array([])

    assert np.min(trigs) >= np.min(bins)
    assert np.max(trigs) < np.max(bins)

    counts, bins = np.histogram(injstat, bins, weights=weights)
    N = sum(counts)
    dens = counts / np.diff(bins) / float(N)
    fracerr = np.zeros(len(counts)) # fracerr is not used, but shows an error while runtime

    tinds = np.searchsorted(bins, trigs) - 1
    return np.log(dens[tinds]), fracerr

######## CODE ##############

#>>> Background PDF and zero lag

def get_pastro(txt_zerolag,txt_timelag,txt_background_ratio,injection_files):
    # read zerolag/time slide data chunk by chunk
    args=getargs(txt_zerolag,txt_timelag,txt_background_ratio,injection_files)
    zlstat = {}
    zltime = {}
    zlifar = {}
    bgpdf = {}
    #tot_exp_bg = 0

    inputtuple = (args.txt_zerolag, args.txt_timelag, args.txt_background_ratio)
    full_dict, tot_exp_bg = read_zerolag_timelag(args)
    allstat = full_dict['zerolagstat']
    alltime = full_dict['zerolagtime']
    allifar = full_dict['zerolagifar']
    max_bg_stat = np.max(full_dict['cstat_back_exc'])
    n_bg_bins = np.log(max_bg_stat / args.th) / args.background_log_bin_width
    #bg_bins = np.logspace(np.log10(args.th) - 0.0001, np.log10(max_bg_stat), n_bg_bins + 1)
    bg_bins = np.logspace(np.log10(args.th), np.log10(max_bg_stat), int(n_bg_bins) + 1)
    bg_counts, bedges = np.histogram(full_dict['cstat_back_exc'],
                    weights=full_dict['dec_factors'],  bins=bg_bins)

    bgpdf[0], _ = log_rho_bg(allstat, bg_bins, bg_counts)

    #>>> Foreground PDF

    injstats = {}
    injweights = {}
    fgpdf = {}

    # Read injections:
    injstats = np.loadtxt(args.injection_files)
    injstats = injstats[injstats > args.th]
    injweights = np.ones_like(injstats)

    # evaluate foreground PDF separately for each chunk
    # get normalization of fg histogram by taking bins out to max injection stat
    binmax = injstats.max() * 1.01
    n_fg_bins = np.log(binmax / args.th) / args.inj_log_bin_width
    fg_bins = np.logspace(np.log10(args.th), np.log10(binmax), int(n_fg_bins) + 1)
    fgpdf, _ = log_rho_fg_weighted(allstat, injstats, fg_bins, weights=injweights)

    #>>> Calculations
    # assemble the zerolag triggers
    allfgpdf = fgpdf[0]
    allbgpdf = bgpdf[0]
    allfgbg = allfgpdf - allbgpdf
    foreground_count_distribution = \
         fgmcl.count_posterior(allfgbg, laguerre_n=args.laguerre_degree,
                            Lambda0=tot_exp_bg, name='foreground count posterior',
                            prior=args.power_law_prior)

    p_b = foreground_count_distribution.p_bg(allfgbg)
    return p_b
