#!/usr/bin/env -S tcsh -f

set man="\n- cwb_online create python_file\n \
create directory from filename\n \
- cwb_online update\n \
update changes, must be done in the directory\n \
- cwb_online start option\n \
start running, options: zero, bkg, run, week, day, cal, all\n \
- cwb_online stop option\n \
stop running, options: zero, bkg, run, week, day, cal, all\n \
- cwb_online test option\n \
test running, options: zero, bkg, run, week, day, cal, all \n \
- cwb_online trigger GPS (dir)\n \
check if trigger with gps=GPS has been found by analysis dir\n \"

#set opt="- zero: zero lag\n\
#- bkg: background\n\
#- run: web page production of all run\n \
#- week: web page production of previous 7 days\n \
#- day: web page production of previous day\n \
#- cal: web page production day by day"

if ( $1 == '' ) then
  $CWB_SCRIPTS/cwb_help.csh cwb_online
  exit
endif

if ( ($1 != "create") && ($1 != "update") && ($1 != "start") && ($1 != "stop") && ($1 != "test") && ($1 != "clean") && ($1 != "trigger") && ($1 != "restart")  && ($1 != "xgboost")) then
  echo "Wrong options:"
  echo $man
  exit
endif

if ( ($1 == "create") && ($2 == "") ) then
  echo "Specify path of configuration file:"
endif

if ( ($1 == "start") && ($2 == "") ) then
  echo "Specify option:"
  #echo $opt
  #echo "- all: web page production all previous cases"
endif

if ( ($1 == "stop") && ($2 == "") ) then
  echo "Specify option:"
  echo $opt
  echo "- all: web page production all previous cases"
endif

if ( ($1 == "test") && ($2 == "") ) then
  echo "Specify option:"
  echo $opt
endif

if ( ($1 == "trigger") && ($2 == "") ) then
  echo "Specify trigger GPS"
endif

if  ( $1 == "create" ) then
  cwb_online_exec $2 $1
endif

if ( ( $1 == "update" ) || ( $1 == "clean" ) || ( $1 == "xgboost" )) then
  cwb_online_exec cWB_conf.py $1
endif

if  ( $1 == "trigger" ) then
  cwb_online_exec $1 $2 $3
endif

if  ( $1 == "restart" ) then
  cwb_online_exec $1 $2 $3 $4
endif

if  ( ( $1 == "start" ) ) then
 if  ( ( $2 == "zero" ) || ( $2 == "all" ) ) then
   echo nohup cwb_online_exec "$PWD"/cWB_conf.py zero ">> log/run.log 2>&1 &"
 endif
 if  ( ( $2 == "bkg" ) || ( $2 == "all" ) ) then
   echo nohup cwb_online_exec "$PWD"/cWB_conf.py bkg" >> log/run_ts.log 2>&1 &"
 endif
 #if  ( ( $2 == "check" ) || ( $2 == "all" ) ) then
 if  ( ( $2 == "check" ) ) then
   echo nohup cwb_online_exec "$PWD"/cWB_conf.py loop-check " >> log/web_pages_check.log  2>&1 &"
 endif
 if  ( ( $2 == "week" ) || ( $2 == "all" ) ) then
   echo nohup cwb_online_exec "$PWD"/cWB_conf.py loop-week " >> log/web_pages_week.log  2>&1 &"
 endif
 #if  ( ( $2 == "day" ) || ( $2 == "all" ) ) then
 if  ( ( $2 == "day" ) ) then
   echo nohup cwb_online_exec "$PWD"/cWB_conf.py loop-day " >> log/web_pages_day.log  2>&1 &"
 endif
 if  ( ( $2 == "days" ) ) then
   echo nohup cwb_online_exec "$PWD"/cWB_conf.py loop-days " >> log/web_pages_days.log  2>&1 &"
 endif
 #if  ( ( $2 == "daily" ) || ( $2 == "all" ) ) then
 if  ( ( $2 == "daily" ) ) then
   echo nohup cwb_online_exec "$PWD"/cWB_conf.py loop-daily " >> log/web_pages_daily.log  2>&1 &"
 endif
 if  ( ( $2 == "daily,day,check" ) || ( $2 == "all" ) ) then
   echo nohup cwb_online_exec "$PWD"/cWB_conf.py loop-daily,day,check " >> log/web_pages_dailydaycheck.log  2>&1 &"
 endif
endif

if  ( ( $1 == "test" ) ) then
   cwb_online_exec cWB_conf.py $2
endif

if  ( ( $1 == "stop" ) ) then
 if  ( ( $2 == "zero" ) || ( $2 == "all" ) ) then
   fuser -k log/run.log
 endif
 if  ( ( $2 == "bkg" ) || ( $2 == "all" ) ) then
   fuser -k log/run_ts.log
 endif
 if  ( ( $2 == "check" ) || ( $2 == "all" ) ) then
   fuser -k log/web_pages_check.log
 endif
 if  ( ( $2 == "week" ) || ( $2 == "all" ) ) then
   fuser -k log/web_pages_week.log
 endif
 if  ( ( $2 == "day" ) || ( $2 == "all" ) ) then
   fuser -k log/web_pages_day.log
 endif
 if  ( ( $2 == "daily" ) || ( $2 == "all" ) ) then
   fuser -k log/web_pages_daily.log
 endif
endif

