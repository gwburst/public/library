/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


// compute xgboost statistic to merged root file

#define CCAST(PAR) const_cast<char*>(PAR)
#define YEAR (24.*3600.*365.)
{

  CWB::Toolbox TB;

  int estat;
  Long_t id,size,flags,mt;
  char cmd[1024];

  	  cwb_merge_label     = TString(gSystem->Getenv("CWB_MERGE_LABEL"));
  TString cwb_xgboost_options = TString(gSystem->Getenv("CWB_XGBOOST_OPTIONS"));

  TString mdir = merge_dir;

  // check if cwb_xgboost_options contains '--' than cwb_xgboost_options
  // is used to extract all xgboost parameters
  // eg: cwb_xgboost M1 '--model xgboost_model_path/xgb/v1/xgb_model_v1.dat --ulabel v1 --rthr 3 --search blf  \
                         --fgetrhor config/user_getrhor.py --config config/user_xgboost_config.py --dfar true'

  TString cwb_xgb_model    = "";	// xgboost model path
  TString cwb_xgb_config   = "";	// xgboost configuration file. If declared, it overwrites the default options defined in cwb_xgboost_config.py file
  TString cwb_xgb_search   = "";	// search option: blf,bld,bhf,bbh,imbhb
  TString cwb_xgb_fgetrhor = "";	// user function getrhor. If declared, it replaces the default getrhor defined in cwb_xgboost.py file
  TString cwb_xgb_ulabel   = "";	// user label used to tag output files
  TString scwb_xgb_rthr    = "0";	// rhor threshold, only events with rhor>scwb_xgb_rthr are written to root output file
  float   cwb_xgb_rthr     = 0;
  TString cwb_xgb_verbose  = "";	// true/false -> (enable/disable(def)) verbose output
  TString cwb_xgb_dump     = "";	// dummy option (to be implemented)
  TString cwb_xgb_dfar     = ""; 	// true/false -> (enable/disable(def)) dump the far_rho.txt,efar_rho.txt files
					// into the xgb/cwb_xgb_ulabel/cwb_merge_label directory

  // get the cwb_xgb_model
  cwb_xgb_model = TB.getParameter(cwb_xgboost_options,"--model");
  cwb_xgb_model = gSystem->ExpandPathName(cwb_xgb_model.Data());
  if(cwb_xgb_model=="") {
    if(pp_xgb_model!="") {
      cwb_xgb_model = gSystem->ExpandPathName(pp_xgb_model.Data());;
    } else {
      cout << "\ncwb_xgboost_training.C : Error - model not defined" << endl << endl;
      cout << "- must be defined from command line (--model = ...) or in user_pparameterc.C (pp_xgb_model = ...)" << endl << endl;
      gSystem->Exit(1);
    }
  }
  bool bcwb_xgb_config = true;	// cwb_xgb_config is user defined
  cwb_xgb_config = TB.getParameter(cwb_xgboost_options,"--config");
  cwb_xgb_config = gSystem->ExpandPathName(cwb_xgb_config.Data());
  if(cwb_xgb_config=="") {      // if config s not provided from command line then 
    if(pp_xgb_config!="") {     // reads xgb_config from cwb_pparametrs.C file, if is not presents then the default one is used
      cwb_xgb_config = gSystem->ExpandPathName(pp_xgb_config.Data());
      bcwb_xgb_config = false;
    }
  }
  // get the cwb_xgb_search
  cwb_xgb_search = TB.getParameter(cwb_xgboost_options,"--search");
  if(cwb_xgb_search=="") {
    if(pp_xgb_search!="") {
      cwb_xgb_search = pp_xgb_search;
    } else {
      cout << "\ncwb_xgboost_training.C : Error - search not defined" << endl << endl;
      cout << "- must be defined from command line (--search = ...) or in user_pparameterc.C (pp_xgb_search = ...)" << endl << endl;
      gSystem->Exit(1);
    }
  }
  // get the cwb_xgb_fgetrhor
  cwb_xgb_fgetrhor = TB.getParameter(cwb_xgboost_options,"--fgetrhor");
  cwb_xgb_fgetrhor = gSystem->ExpandPathName(cwb_xgb_fgetrhor.Data());
  if(cwb_xgb_fgetrhor=="") {
    if(pp_xgb_fgetrhor!="") {
      cwb_xgb_fgetrhor = gSystem->ExpandPathName(pp_xgb_fgetrhor.Data());;
    }
  }
  // get the cwb_xgb_ulabel
  cwb_xgb_ulabel = TB.getParameter(cwb_xgboost_options,"--ulabel");
  // get the cwb_xgb_rthr
  scwb_xgb_rthr = TB.getParameter(cwb_xgboost_options,"--rthr");
  if(scwb_xgb_rthr!="") {
    cwb_xgb_rthr = scwb_xgb_rthr.Atof();
  } else {
      if(pp_xgb_rthr!=-1) {
      cwb_xgb_rthr = pp_xgb_rthr;
    } else {
      cout << "\ncwb_xgboost_training.C : Error - rthr not defined" << endl << endl;
      cout << "- must be defined from command line (--rthr = ...) or in user_pparameterc.C (pp_xgb_rthr = ...)" << endl << endl;
      gSystem->Exit(1);
    }
  }
  if(cwb_xgb_rthr<=0) {cwb_xgb_rthr=0.;scwb_xgb_rthr="0";} 
  // get the cwb_xgb_verbose
  if(TB.getParameter(cwb_xgboost_options,"--verbose")=="") cwb_xgb_verbose="false";
  else cwb_xgb_verbose=TB.getParameter(cwb_xgboost_options,"--verbose");
  // get the cwb_xgb_dump
  if(TB.getParameter(cwb_xgboost_options,"--dump")=="") cwb_xgb_dump="false";
  else cwb_xgb_dump=TB.getParameter(cwb_xgboost_options,"--dump");
  // get the cwb_xgb_dfar
  if(TB.getParameter(cwb_xgboost_options,"--dfar")=="") cwb_xgb_dfar="false";
  else cwb_xgb_dfar=TB.getParameter(cwb_xgboost_options,"--dfar");

  // check if xgb file config exist
  if(cwb_xgb_config!="") TB.checkFile(cwb_xgb_config);
  // check if cwb_xgb_ulabel contains '.'
  if(cwb_xgb_ulabel.Contains(".")) {
    cout << "cwb_xgboost_prediction.C : Error - cwb_xgb_ulabel " << cwb_xgb_ulabel 
         << " can not contains '.'" << endl << endl;
    gSystem->Exit(1);
  }
  if(cwb_xgb_model=="") {
    cout << "cwb_xgboost_prediction.C : Error - (--model) is not defined !!!" << endl << endl;
    gSystem->Exit(1);
  } else {
    // check if xgb file model exist
    TB.checkFile(cwb_xgb_model);
  }
  if((cwb_xgb_search!="bbh")&&(cwb_xgb_search!="imbhb")&&(cwb_xgb_search!="blf")&&(cwb_xgb_search!="bhf")&&(cwb_xgb_search!="bld")) {
    cout << "cwb_xgboost_prediction.C : Error - (--search) is not defined, available options are bbh/imbhb/blf/bhf/bld" << endl << endl;
    gSystem->Exit(1);
  }
  if(cwb_xgb_fgetrhor!="") {
    // check if xgb file fgetrhor exist
    TB.checkFile(cwb_xgb_fgetrhor);
  }

  // save config to xgb_dir
  TString ucwb_xgb_config = bcwb_xgb_config ? cwb_xgb_config : "";
  dump_config(xgb_dir,cwb_xgb_ulabel,ucwb_xgb_config,cwb_xgb_fgetrhor);

  // create input wave root xgb file name
  char iwfname[1024];
  sprintf(iwfname,"wave_%s.%s.root",data_label,cwb_merge_label.Data());

  // create output wave root xgb file name
  TString owfname = mdir+"/"+iwfname;
  scwb_xgb_rthr.ReplaceAll(".","d");
  if(cwb_xgb_ulabel!="") owfname.ReplaceAll(".root",TString(".XGB_rthr")+scwb_xgb_rthr+"_"+cwb_xgb_ulabel+".root");
  else                   owfname.ReplaceAll(".root",TString(".XGB_rthr")+scwb_xgb_rthr+".root");

  // check if output file already exist
  bool overwrite=TB.checkFile(owfname,true);
  if(!overwrite) gSystem->Exit(1);

  // apply xgboost model
  TString verbose = cwb_xgb_verbose=="true" ? "verbose" : "";
  TString dump    = cwb_xgb_dump=="true" ? "dump" : "";
  TString config  = cwb_xgb_config!="" ? "config="+cwb_xgb_config : "";
  sprintf(cmd,"python3 -B %s ifile=%s/%s ofile=%s model=%s search=%s fgetrhor=%s ulabel=%s rthr=%f %s %s %s", 
          gSystem->ExpandPathName("$CWB_SCRIPTS/cwb_xgboost_prediction.py"),
          mdir.Data(), iwfname, owfname.Data(), cwb_xgb_model.Data(), cwb_xgb_search.Data(), cwb_xgb_fgetrhor.Data(),
          cwb_xgb_ulabel.Data(), cwb_xgb_rthr, config.Data(), verbose.Data(), dump.Data());
  cout << cmd << endl;
  int ret=gSystem->Exec(cmd);
  if(ret) {cout << endl << "Error  while executing python cwb_xgboost_prediction.py script !!!" << endl << endl;exit(1);}

  // add xgboost history to output root xgb file
  TFile ifroot(mdir+"/"+iwfname);
  if (ifroot.IsZombie()) {
    cout << "cwb_xgboost_prediction.C - Error opening file " << mdir+"/"+iwfname << endl;
    gSystem->Exit(1);
  }
  // create xgboost history 
  CWB::History* history = (CWB::History*)ifroot.Get("history");
  if(history==NULL) history=new CWB::History();
  if(history!=NULL) {
    TList* stageList = history->GetStageNames();    // get stage list
    TString phcuts="";
    for(int i=0;i<stageList->GetSize();i++) {       // get previous xgb history
      TObjString* stageObjString = (TObjString*)stageList->At(i);
      TString stageName = stageObjString->GetString();
      char* stage = const_cast<char*>(stageName.Data());
      if(stageName=="XGB") phcuts=history->GetHistory(stage,const_cast<char*>("PARAMETERS"));
    }
    // update history
    history->SetHistoryModify(true);
    if(!history->StageAlreadyPresent(CCAST("XGB"))) history->AddStage(CCAST("XGB"));
    if(!history->TypeAllowed(CCAST("MODEL_MD5"))) history->AddType(CCAST("MODEL_MD5"));
    if(!history->TypeAllowed(CCAST("FGETRHOR_MD5"))) history->AddType(CCAST("FGETRHOR_MD5"));
    if(!history->TypeAllowed(CCAST("CONFIG_MD5"))) history->AddType(CCAST("CONFIG_MD5"));
    if(!history->TypeAllowed(CCAST("PARAMETERS"))) history->AddType(CCAST("PARAMETERS"));
    if(!history->TypeAllowed(CCAST("WORKDIR"))) history->AddType(CCAST("WORKDIR"));
    char work_dir[1024]="";
    sprintf(work_dir,"%s",gSystem->WorkingDirectory());
    history->AddHistory(CCAST("XGB"), CCAST("WORKDIR"), work_dir);

    TString hxgb = (phcuts!="") ? phcuts+" ( XGBoost: "+cwb_xgboost_options+" )" : cwb_xgboost_options;
    history->AddHistory(CCAST("XGB"), CCAST("PARAMETERS"), CCAST(hxgb.Data()));

    TString xgb_model_md5 = (TMD5::FileChecksum(cwb_xgb_model.Data()))->AsString();
    history->AddHistory(CCAST("XGB"), CCAST("MODEL_MD5"), CCAST(xgb_model_md5.Data()));

    TString xgb_fgetrhor_md5 = (TMD5::FileChecksum(cwb_xgb_fgetrhor.Data()))->AsString();
    history->AddHistory(CCAST("XGB"), CCAST("FGETRHOR_MD5"), CCAST(xgb_fgetrhor_md5.Data()));

    TString xgb_config_md5 = (TMD5::FileChecksum(cwb_xgb_config.Data()))->AsString();
    history->AddHistory(CCAST("XGB"), CCAST("CONFIG_MD5"), CCAST(xgb_config_md5.Data()));
  }
  // write xgboost history to merge+xgb file
  if(history!=NULL) {
    TFile ofroot(owfname,"UPDATE");
    history->Write("history");
    ofroot.Close();
    delete history;
  }
  // close input root file
  ifroot.Close();

  // create a merge*.lst file name & xgb
  vector<int> jobList;
  char ilstfname[1024];  
  sprintf(ilstfname,"merge_%s.%s.lst",data_label,cwb_merge_label.Data());
  TString olstfname = owfname;
  olstfname.ReplaceAll("wave_","merge_");
  olstfname.ReplaceAll(".root",".lst");
  olstfname.Remove(0,olstfname.Last('/')+1);	// strip path
  cout << olstfname << endl;
  estat = gSystem->GetPathInfo(mdir+"/"+ilstfname,&id,&size,&flags,&mt);
  if (estat==0) {
    sprintf(cmd,"cd %s;ln -sf %s %s",mdir.Data(),ilstfname,olstfname.Data());
    cout << cmd << endl;
    gSystem->Exec(cmd);
  }

  // for simulation!=0 create a symbolic link to mdc*.root file name
  if(simulation) {
    char ilfname[1024];
    sprintf(ilfname,"mdc_%s.%s.root",data_label,cwb_merge_label.Data());
    TString olfname = owfname;
    olfname.ReplaceAll("wave_","mdc_");
    olfname.Remove(0,olfname.Last('/')+1);	// strip path
    cout << olfname << endl;
    estat = gSystem->GetPathInfo(mdir+"/"+ilfname,&id,&size,&flags,&mt);
    if(estat==0) {
      sprintf(cmd,"cd %s;ln -sf %s %s",mdir.Data(),ilfname,olfname.Data());
      cout << cmd << endl;
      gSystem->Exec(cmd);
    }
  }

  // for simulation=0 create a symbolic link to live*.root file name
  if(!simulation) {		// only for background
    char ilfname[1024];
    sprintf(ilfname,"live_%s.%s.root",data_label,cwb_merge_label.Data());
    TString olfname = owfname;
    olfname.ReplaceAll("wave_","live_");
    olfname.Remove(0,olfname.Last('/')+1);	// strip path
    cout << olfname << endl;
    estat = gSystem->GetPathInfo(mdir+"/"+ilfname,&id,&size,&flags,&mt);
    if(estat==0) {
      sprintf(cmd,"cd %s;ln -sf %s %s",mdir.Data(),ilfname,olfname.Data());
      cout << cmd << endl;
      gSystem->Exec(cmd);
    }

    // generate file far_rho.txt, it is the same generated by the post-production report
    if(cwb_xgb_dfar=="true") {			// WARNING!!! this is still an experimental option, use output from standart report 
      // create output model dir 
      TString cwb_xgb_file_tag = "";
      if(cwb_xgb_ulabel!="") cwb_xgb_file_tag = TString(".XGB_rthr")+scwb_xgb_rthr+"_"+cwb_xgb_ulabel;
      else                   cwb_xgb_file_tag = TString(".XGB_rthr")+scwb_xgb_rthr;
      TString odir = TString::Format("%s/%s/%s%s",xgb_dir,cwb_xgb_ulabel.Data(),cwb_merge_label.Data(),cwb_xgb_file_tag.Data());
      gSystem->mkdir(odir,kTRUE);
      CWB::Toolbox::checkFile(odir);        	// check if odir has been created
      cout << "\n -> Dump far vs rho file ...\n" << endl;
      TString fRhoFar; fRhoFar += dumpRhoFar(mdir+"/"+olfname, owfname, pp_irho, T_out, odir); 
      plotRhoFar(fRhoFar, cwb_xgb_rthr, odir);		// plot far vs rho
    }
  }

  exit(0);
}

void dump_config(TString xgb_dir,TString cwb_xgb_ulabel,TString cwb_xgb_config,TString cwb_xgb_fgetrhor) {

  char cmd[1024];

  // create output dir
  TString odir = TString::Format("%s/%s",xgb_dir.Data(),cwb_xgb_ulabel.Data());
  gSystem->mkdir(odir,kTRUE);
  CWB::Toolbox::checkFile(odir);        // check if odir has been created

  // dump user config
  if(cwb_xgb_config!="") {
    sprintf(cmd,"cp %s %s/user_xgboost_config.py",cwb_xgb_config.Data(),odir.Data());
    cout << cmd << endl;
    int ret=gSystem->Exec(cmd);
  }

  // dump user fgetrhor
  if(cwb_xgb_fgetrhor!="") {
    sprintf(cmd,"cp %s %s/user_xgboost_fgetrhor.py",cwb_xgb_fgetrhor.Data(),odir.Data());
    cout << cmd << endl;
    int ret=gSystem->Exec(cmd);
  }

}

int getNifo(TString owfname) {

  // get nifo from input root file
  int nifo=0;
  TFile* iroot = TFile::Open(owfname.Data());
  if((iroot==NULL) || (iroot!=NULL && !iroot->IsOpen())) {
    cout << "cwb_xgboost_prediction.C : Error opening root file " << owfname << endl;
    gSystem->Exit(1);
  }

  TTree* itree = (TTree *)iroot->Get("waveburst");
  if(itree) {
    itree->SetBranchAddress("ndim",&nifo);
    if(itree->GetEntries()>0) itree->GetEntry(0);       // get number of detector from the first entry
    else {
      cout << "cwb_xgboost_prediction.C : Error no tree entries in " << owfname << endl;
      gSystem->Exit(1);
    }
  } else {
    cout << "cwb_xgboost_prediction.C : Error tree waveburst not present in " << owfname << endl;
    gSystem->Exit(1);
  }
  if(nifo>0) {
    cout << "\n number of detectors = " << nifo << endl << endl;
  } else {
    cout << "cwb_xgboost_prediction.C : Error nifo=0 in " << owfname << endl;
    gSystem->Exit(1);
  }
  iroot->Close();

  return nifo;
}

double getLiveTime(TString olfname, TString owfname) {

//  MT is temporarily disabled due to the following error:
//  Error in <TReentrantRWLock::WriteUnLock>: Write lock already released for 0xa413fc8
//  moreover ROOT must be built with the compilation flag imt=ON for this feature to be available.

//  ROOT::EnableImplicitMT();					// Enable Multi threading
//  ROOT::EnableThreadSafety();

  cout << "\n\nGetLiveTime from file: " << olfname << " ..." << endl << endl;

  int nifo = getNifo(owfname);

  ROOT::RDataFrame dlive("liveTime", olfname.Data());		// read root file  
  TString bkg_cut = TString::Format("!((lag[%d]==0)&&(slag[%d]==0))",nifo,nifo);
  auto dlive_bkg = dlive.Filter(bkg_cut.Data());		// exclude zero lag
  auto live_cut = [](double live) { return live>0; };
  auto live = dlive_bkg.Filter(live_cut,{"live"});		// get live leaf
  auto pliveTime = live.Sum("live");				// get total bkg live time
  double liveTime = *pliveTime;

//  ROOT::DisableImplicitMT();					// Disable Multi threading

  return liveTime;
}

TString dumpRhoFar(TString olfname, TString owfname, int pp_irho, float T_out, TString odir) {

  double liveTime = getLiveTime(olfname, owfname);	// get bkg livetime
  cout << "\nTotal background liveTime = " << long(liveTime) << " (sec)\t" << liveTime/(24.*3600.*365.) << " (years)\n" << endl;

  // open background file
  TFile* iroot = TFile::Open(owfname.Data());
  if((iroot==NULL) || (iroot!=NULL && !iroot->IsOpen())) {
    cout << "cwb_xgboost_prediction.C : Error opening root file " << owfname << endl;
    gSystem->Exit(1);
  }

  TTree* itree = (TTree *)iroot->Get("waveburst");
  char fname[1024],efname[1024];
  if(itree) {
    int nifo=0;
    itree->SetBranchAddress("ndim",&nifo);
    if(itree->GetEntries()>0) itree->GetEntry(0);       // get number of detectors from the first entry
    else {
      cout << "cwb_xgboost_prediction.C : Error no tree entries in " << owfname << endl;
      gSystem->Exit(1);
    }
    TString sel = TString::Format("rho[%d]",pp_irho);	// select rho[pp_irho]
    TString bkg_cut = TString::Format("!(lag[%d]==0 && slag[%d]==0) && rho[%d]>%f",nifo,nifo,pp_irho,T_out);   // exclude zero lag
    int nsize = itree->GetEntries();
    itree->SetEstimate(nsize);
    itree->Draw(sel,bkg_cut,"goff");
    int nevt = itree->GetSelectedRows();		// get number of background events
    double* rho = itree->GetV1();

    // find the min and max in the tree and set rho limits for far_rho.txt file
    double rho_min = TMath::MinElement(nevt, rho);
    double rho_max = TMath::MaxElement(nevt, rho);

    double far_rho_min = TMath::Min(pp_rho_min,(double)TMath::Nint(rho_min-0.5));
    double far_rho_max = TMath::Max(pp_rho_max,(double)TMath::Nint(rho_max+0.5));
    double far_rho_wbin = pp_drho;
    int far_rho_bin = double(far_rho_max-far_rho_min)/far_rho_wbin;
    double far_drho = double(far_rho_max-far_rho_min)/double(far_rho_bin);

    wavearray<double> Xfar(far_rho_bin);
    wavearray<double> Yfar(far_rho_bin); Yfar = 0.;

    // fill Xfar,Yfar
    for(int j=0; j<Xfar.size(); j++) Xfar.data[j]=far_rho_min+j*far_drho;
    for(int i=0; i<nevt; i++) {
      if(rho[i]>far_rho_min) {
        double r = ((rho[i]-far_rho_min)/far_drho);
        int j = fmod(r,1)==0 ? int(r)-1 : int(r);
        Yfar.data[j] += 1.;
      }
    }
    for(int j=Xfar.size()-2; j>=0; j--) Yfar.data[j]+=Yfar.data[j+1];

    // write rho statistic distribution
    ofstream out_far;
    sprintf(fname,"%s/far_rho.txt",odir.Data());
    out_far.open(fname,ios::out);
    if(!out_far.good()) {cout << "Error Opening File " << fname << endl;exit(1);}
    ofstream out_efar;
    sprintf(efname,"%s/efar_rho.txt",odir.Data());
    out_efar.open(efname,ios::out);
    if(!out_efar.good()) {cout << "Error Opening File " << efname << endl;exit(1);}
    for(int i=0;i<Xfar.size(); i++) if (Yfar[i]>0) {
      double x  = Xfar[i];
      double ex = 0;
      double y  = Yfar[i]/liveTime;
      double ey = sqrt(Yfar[i])/liveTime;
      if(y>=1) out_far << x << "\t\t" << y << endl;
      if(y<1)  out_far << x << "\t\t" << y << endl;
      //cout << i << " " << x << " " << y << " " << ex << " " << ey << endl;
      if(y>=1) out_efar << x << "\t\t" << y << "\t\t" << ex << "\t" << ey << endl;
      if(y<1)  out_efar << x << "\t\t" << y << "\t" << ex << "\t" << ey << endl;
    }
    out_far.close();
    out_efar.close();

    cout << "\ndump file: " << fname << endl << endl; 
    cout << "\ndump file: " << efname << endl << endl; 

  } else {
    cout << "cwb_xgboost_prediction.C : Error tree waveburst not present in " << owfname << endl;
    gSystem->Exit(1);
  }
  iroot->Close();

  return efname;
}

void plotRhoFar(TString ifRhoFar, double rthr, TString odir) {

  // Open ifRhoFar file
  ifstream in;
  in.open(ifRhoFar.Data(),ios::in);
  if (!in.good()) {cout << "Error Opening File : " << ifRhoFar << endl;exit(1);}
  double max_rho=0.,min_rho=1e20;
  double max_far=0.,min_far=1e20;
  double rho,far,erho,efar;
  vector<double> vrho,vfar,verho,vefar;
  while(true) {
    in >> rho >> far >> erho >> efar;
    if(!in.good()) break;
    vrho.push_back(rho);
    vfar.push_back(far*YEAR);
    verho.push_back(erho);
    vefar.push_back(efar*YEAR);
    if(rho>max_rho) max_rho=rho;
    if(rho<min_rho) min_rho=rho;
    if(far*YEAR>max_far) max_far=far*YEAR;
    if(far*YEAR<min_far) min_far=far*YEAR;
  }
  if(rthr>min_rho) min_rho=rthr;
  in.close();

  TGraph* gr = new TGraph(vrho.size(),vrho.data(),vfar.data());
  gr->SetLineWidth(2);
  gr->SetLineColor(TColor::GetColor(255, 128,  14));

  TGraphAsymmErrors* egr = new TGraphAsymmErrors(vrho.size(),vrho.data(),vfar.data(),verho.data(),verho.data(),vefar.data(),vefar.data());
  egr->SetLineColor(17);
  egr->SetFillStyle(1001);
  egr->SetFillColorAlpha(17, 0.6);
  egr->GetXaxis()->SetTitle("rhor");
  egr->GetYaxis()->SetTitle("rate (1/year)");
  egr->SetTitle(title);
  egr->GetXaxis()->SetTitleFont(42);
  egr->GetXaxis()->SetLabelFont(42);
  egr->GetXaxis()->SetLabelOffset(0.012);
  egr->GetXaxis()->SetLabelSize(0.05);
  egr->GetXaxis()->SetTitleOffset(0.85);
  egr->GetXaxis()->SetTitleSize(0.05);
  egr->GetXaxis()->SetMoreLogLabels();
  egr->GetYaxis()->SetTitleFont(42);
  egr->GetYaxis()->SetLabelFont(42);
  egr->GetYaxis()->SetLabelOffset(0.01);
  egr->GetYaxis()->SetLabelSize(0.05);
  egr->GetYaxis()->SetTitleOffset(0.95);
  egr->GetYaxis()->SetTitleSize(0.05);
  egr->GetXaxis()->SetRangeUser(min_rho, 2*max_rho);
  egr->GetYaxis()->SetRangeUser(min_far/2, 2*max_far);

  TCanvas* canvas = new TCanvas("canvas","canvas",200,20,800,500);
  canvas->cd();
  canvas->SetGridx();
  canvas->SetGridy();
  canvas->SetLogx(kTRUE);
  canvas->SetLogy(kTRUE);

  gStyle->SetGridWidth(1);
  gStyle->SetGridStyle(3);
  gStyle->SetGridColor(TColor::GetColor(207, 207, 207));

  // draw
  egr->Draw("A3");
  gr->Draw("Lsame");

  // save
  char ofname[1024];
  sprintf(ofname,"%s/efar_rho.png",odir.Data());
  canvas->Update(); canvas->Print(ofname);

  // release resouces
  delete canvas;
  delete egr;
  delete gr;
}
