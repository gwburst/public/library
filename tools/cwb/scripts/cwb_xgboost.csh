#!/usr/bin/env tcsh

onintr irq_ctrlc

set cmd_line="$0 $1 '$2'"

if (($1 == '') || ((($1 == 'tuning')||($1 == 'report')) && (("$2" == '')||("$2" == 'help')))) then
  #$CWB_SCRIPTS/cwb_help.csh cwb_xgboost
  echo ""
  echo "cwb_xgboost xgb_type 'xgb_options'"
  echo ""
  echo " - xgb_type = datasplit/tuning/training/mlabel(prediction)/report"
  echo ""
  echo " - datasplit"
  echo "   - xgb_options = '--mlabel merge_label --ifrac (trainig[ifrac]/testing[1-ifrac]) (--ulabel user_label --verbose false/true)'"
  echo "                    (--cfile chunk list --wavecuts training selection cuts (wave)) (--mdccuts training selection cuts (mdc))"
  echo ""
  echo ""
  echo " - tuning"
  echo "   - xgb_options = '--nfile wave_bkg.root/.py --sfile wave_sim.root /.py --ofile output.txt --search bbh/imbhb/blf/bhf/bld"
  echo "                    --nfrac blg fraction_selection --sfrac sim fraction_selection (--config xgb_config.py --verbose false/true)"
  echo "                    --learning-rate 0.03 --max-depth 8 --min-child-weight 5.0 --gamma 1 --colsample-bytree 0.9 --subsample 0.6'"
  echo ""
  echo " - training"
  echo "   - xgb_options = '--nfile wave_bkg.root/.py --sfile wave_sim.root/.py --model xgb_model.dat --search bbh/imbhb/blf/bhf/bld"
  echo "                    --nfrac blg fraction_selection --sfrac sim fraction_selection (--config xgb_config.py --verbose false/true)'"
  echo "                    --dump false/true)'"
  echo ""
  echo " - mlabel (prediction)"
  echo "   - xgb_options = '--model xgb_model.dat --ulabel user_label --rthr output_rhor_threshold'"
  echo "                    --search bbh/imbhb/blf/bhf/bld (--fgetrhor user_getrhor --verbose false/true --config xgb_config.py)'"
  echo ""
  echo " - report"
  echo "   - xgb_options = '--type tuning/prediction(training not yet implemented) (--ulabel user_label --verbose false/true --config config.py)'"
  echo "                   note: --config option is used only for prediction. To get more help type: 'cwb_xgboost report help'"
  echo "                   eg: cwb_xgboost report  '--type prediction --subtype hrho/lrho/hchirp/qaqp/roc/efreq --config report_config.py'"
  echo "                   eg: cwb_xgboost report  '--type tuning --ulabel r2 --verbose true'"
  echo ""

  if (($1 == 'report') && ("$2" == 'help')) then
    less ${CWB_SCRIPTS}/cwb_xgboost_report_prediction_help.py
    exit
  endif

  exit
endif

setenv CWB_XGBOOST_OPTIONS  "$2"

if ($1 == 'datasplit') then							# datasplit
  setenv CWB_MERGE_LABEL "DUMMY"
  root -n -l -b ${CWB_PARMS_FILES} ${CWB_PPARAMETERS_FILE} ${CWB_UPPARAMETERS_FILE} ${CWB_MACROS}/cwb_xgboost_datasplit.C
  if ( $? != 0) exit 1
else if ($1 == 'tuning') then							# tuning
  setenv CWB_MERGE_LABEL "DUMMY"
  root -n -l -b ${CWB_PARMS_FILES} ${CWB_PPARAMETERS_FILE} ${CWB_UPPARAMETERS_FILE} ${CWB_MACROS}/cwb_xgboost_tuning.C
  if ( $? != 0) exit 1
else if ($1 == 'report') then							# report
  setenv CWB_MERGE_LABEL "DUMMY"
  root -n -l -b ${CWB_PARMS_FILES} ${CWB_PPARAMETERS_FILE} ${CWB_UPPARAMETERS_FILE} ${CWB_MACROS}/cwb_xgboost_report.C
  if ( $? != 0) exit 1
else if ($1 == 'training') then							# training
  setenv CWB_MERGE_LABEL "DUMMY"
  root -n -l -b ${CWB_PARMS_FILES} ${CWB_PPARAMETERS_FILE} ${CWB_UPPARAMETERS_FILE} ${CWB_MACROS}/cwb_xgboost_training.C
  if ( $? != 0) exit 1
else 										# prediction
  setenv CWB_MERGE_LABEL $1
  root -n -l -b ${CWB_PPARMS_FILES} ${CWB_MACROS}/cwb_xgboost_prediction.C
  unsetenv CWB_MERGE_LABEL
  if ( $? != 0) exit 1
endif

# create cWB_analysis.log file
make -f $CWB_SCRIPTS/Makefile.log CMD_LINE="$cmd_line" git >& /dev/null

unsetenv CWB_XGBOOST_OPTIONS

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1

