#!/usr/bin/env tcsh

onintr irq_ctrlc

set cmd_line="$0 $1 '$2'"

if (($1 == '') || ((`echo $1 | grep -c '[_A-Za-z]'`)&&(("$2" == '')||("$2" == 'help')))) then
  #$CWB_SCRIPTS/cwb_help.csh cwb_xmdc
  echo ""
  echo "cwb_xmdc xmdc_type 'xmdc_options'"
  echo ""
  echo " - xmdc_type = xmdc_user_config_macro_name.C / job_id / merge_label"
  echo ""
  echo " - make injections"
  echo ""
  echo " - xmdc_user_config_macro_name.C"
  echo "   - xmdc_options = '--ninj number of injections --seed seed (def=150914) --tag tag_name --odir dir_name (def=xmdc) --rtype (def=all, none-> no reports))'"
  echo "                   eg: cwb_xmdc XMDC_Config_BurstLF_CCSN.C '--ninj 10 --seed 1 --rtype all'"
  echo ""
  echo " - make frame"
  echo ""
  echo " - job_id"
  echo ""
  echo " - postproduction reports"
  echo ""
  echo " - merge_label"
  echo "   - xmdc_options = '--xmdc_fname xmdc file name --rttype (visible_volume/hrss50/dist50/snr_distance/gd/eff2vv/eff2hrss50/eff2dist50) --ifar ifar_years)'"
  echo "                   eg: cwb_xmdc M1.C_U.XGB_rthr0_run1_v3.S_ifar '--xmdc_fname xmdc_file_name --rtype snr_distance --ifar 100'"
  echo "                   eg: cwb_xmdc M1.C_U.XGB_rthr0_run1_v3.S_ifar '--xmdc_fname xmdc_file_name --rtype visible_volume --compare_dir idir --compare_label label'"
  echo "                   eg: cwb_xmdc M1.C_U.XGB_rthr0_run1_v3.S_ifar '--xmdc_fname xmdc_file_name --rtype gd'"
  echo "                   eg: cwb_xmdc M1.C_U.XGB_rthr0_run1_v3.S_ifar '--rtype eff2vv --ifar 100 --xeff_min 1e-23 --xeff_max 1e-21 --hrss_1mpc 1e-22'"
  echo "                       Note: --xeff_min (def=1e-23) --xeff_max (def=1e-21) --hrss_1mpc (def=1e-22) are optionals"
  echo ""
  exit
endif

setenv CWB_XMDC_OPTIONS  "$2"

if ($1 =~ "*.C") then							# make injections
  setenv CWB_MERGE_LABEL "DUMMY"
  if ($1 =~ "/*") then
    setenv CWB_XMDC_UMACRO_PATH_NAME  $1
  else
    setenv CWB_XMDC_UMACRO_PATH_NAME  $PWD/$1
  endif
  if ( ! -e $CWB_XMDC_UMACRO_PATH_NAME ) then
    echo "\nError: file $CWB_XMDC_UMACRO_PATH_NAME doesn't exist\n"
    exit 1
  endif
  ${ROOTSYS}/bin/root -n -l -b ${CWB_ROOTLOGON_FILE} ${CWB_PARAMETERS_FILE} $1 ${CWB_MACROS}/cwb_xmdc_make.C
  if ( $? != 0) exit 1
  unsetenv CWB_XMDC_UMACRO_PATH_NAME
  unsetenv CWB_MERGE_LABEL
else if !(`echo $1 | grep -c '[_A-Za-z]'`) then				# make frame
  setenv CWB_JOBID $1 
  ${ROOTSYS}/bin/root -b -q -l ${CWB_PARAMETERS_FILE} ${CWB_UPARAMETERS_FILE} ${CWB_MACROS}/cwb_xmdc_frame.C
  if ( $? != 0) exit 1
else 									# report
  setenv CWB_MERGE_LABEL $1
  ${ROOTSYS}/bin/root -n -l -b ${CWB_PPARMS_FILES} ${CWB_MACROS}/cwb_xmdc_report.C
  if ( $? != 0) exit 1
  unsetenv CWB_MERGE_LABEL
endif

# create cWB_analysis.log file
make -f $CWB_SCRIPTS/Makefile.log CMD_LINE="$cmd_line" git >& /dev/null

unsetenv CWB_XMDC_OPTIONS

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1

