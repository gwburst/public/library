#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os

class FRAME:
    def __init__(self,path):
        self.pfn = path.strip()
        self.lfn = os.path.basename(self.pfn)
        a = self.lfn.split("-")
        self.ifos = a[0]
        self.type = a[1]
        self.start = int(a[-2])
        self.duration = int(a[-1].split(".")[0])
        self.end = self.start+self.duration
    def __cmp__(self,other):
        return cmp(self.start,other.start)
    def __repr__(self):
        msg="""
        ======= frame ======
        pfn = %s
        lfn = %s
        ifos = %s
        type = %s
        start = %d
        end = %d
        duration = %d
        ====================
        """ % (self.pfn, self.lfn, self.ifos, self.type, self.start, self.end, self.duration)
        return msg
