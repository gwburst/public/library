#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import subprocess
import cwb_online.run_utils as run_utils

class xgbinfo:
  def __init__(self,istring):
   self.source=""
   self.config=""
   self.getrhor=""
   self.models=[]
   self.labels=[]
   self.freqs=[]
   self.cuts=[]
   self.addfiles=[]
   self.add=""
   self.link=""
   lines=istring.split("\n")
   self.getparameters(lines)
  def getparameters(self,lines):
   for l in lines:
     l=l.replace("https://","LINK")
     if (l.find(":")!=-1):
       setattr(self,l.split(":")[0],l.split(":")[1])
     else:
       if (len(l)>0):
         self.addmodels(l)
   if (len(self.add)>0):
       split=self.add.split(",")
       for i in split:
            if (i.find("LINK")==-1):
                self.addfiles.append(i)
            else:
                self.link=i.replace("LINK","https://")
       del self.add
  def addmodels(self,line):     
    s=line.split(" ")
    for v in s:
      if (v.endswith(".dat") or v.endswith(".txt") or v.endswith(".json")):
        self.models.append(v)
      elif (v.find(",")!=-1):
        tcuts=[]
        tfreq=[]
        ss=v.split(",")
        if (ss[0]!="-"):
            tcuts.append("(frequency[0]>%s)"%ss[0])
            tfreq.append(run_utils.getnumberfromstring(ss[0]))
        else:
            tfreq.append(0)
        if (ss[1]!="-"):
            tcuts.append("(frequency[0]<=%s)"%ss[1])
            tfreq.append(run_utils.getnumberfromstring(ss[1]))
        else:
            tfreq.append(16384)
        self.cuts.append("(%s)"%"&&".join(tcuts))
        self.freqs.append(tfreq)
      else:
        self.labels.append(s[0])
  def fixmissing(self,cWB_conf):
    if (len(self.getrhor)>0):
       self.getrhor="%s/MODELS/preferred/xgb_getrhor.py"%(cWB_conf.config_dir)
    self.config="%s/MODELS/preferred/xgb_config.py"%(cWB_conf.config_dir)
    if (len(self.freqs)==0):
      self.freqs.append([0,16384])
  def check(self):
   ok=True
   error=""
   if (len(self.config)==0):
     error+="WARNING: config missing\n"
   if (len(self.getrhor)==0):
     error+="WARNING: getrhor missing\n"
   if (len(self.addfiles)==0):
     error+="WARNING: No additional files\n"
   if (len(self.link)==0):
     error+="WARNING: No html link\n"
   if (len(self.models)==0):
     error+="ERROR: no module found\n"
     ok=False
   if (len(self.labels)==0):
     error+="ERROR: no labels defined\n"
     ok=False
   if (len(self.freqs)==0):
     if (len(self.models)>1):
       error+="ERROR: no all freqs defined\n"
       ok=False
     else:
       error+="WARNING: no all freqs defined\n"
       ok=True
   return ok,error
  
def apply_xgboost(xgb,ifile,ofile,config_dir,label,search):
   model="%s/MODELS/preferred/xgb_model_%s.json"%(config_dir,label)
   com="""python3 -B %s/cwb_xgboost_prediction.py ifile=%s ofile=%s model=%s ulabel=%s search=%s fgetrhor="%s" rthr=0.000000 config=%s;"""%(os.environ['CWB_SCRIPTS'],ifile,ofile,model,label,search,xgb.getrhor,xgb.config)
   subprocess.getstatusoutput(com)
