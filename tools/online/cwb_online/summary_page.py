#!/usr/bin/env python3

# Copyright (C) 2019 Marco Drago, Igor Yakushin, Sergey Klimenko
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess, os, sys, glob
import pickle
from collections import deque
from cwb_online.run import JOB
from cwb_online.Trigger import Trigger
from cwb_online.detector import detector
import cwb_online.draw_bkg as draw_bkg
from cwb_online.root_python import merge_trees, addhistory
from ligo.segments import segment, segmentlist
from ligo.segments.utils import tosegwizard, fromsegwizard
from cwb_online.run_utils import tconvert, get_time, get_time, get_start_of_the_day_string, setparse, getnumberfromstring, getext, add_rootrc
from ligo.gracedb.rest import GraceDb
import ligo.skymap.tool.ligo_skymap_plot as skp

import matplotlib
matplotlib.use('Agg')
import pylab

class statistic:
  def __init__(self,j):
    self.delay_launch_completion=j.completion_time - j.launch_time
    self.delay_launch_start=j.launch_time - j.end
    self.delay_start_completion=j.completion_time - j.end
    self.delay_start_send=j.delay_start_send
    self.pickle="%s/job.pickle"%j.dir
    p_triggers=[x for x in j.triggers_all if x.topublish==True]
    if (len(j.triggers_all)-len(p_triggers) > 0):
      s_tr="    [%s]"%(len(j.triggers_all)-len(p_triggers))
    else:
      s_tr=""
    self.jobs_str="<tr><td>%s</td><td>%d</td><td>%d</td><td>%d</td><td>%d</td><td>%d</td><td>%d</td><td>%d%s</td></tr>"%\
                  (tconvert(j.start),j.start,j.end,j.launch_time,j.completion_time,j.completion_time - j.launch_time,j.launch_time-j.end, len(p_triggers),s_tr)

def trigger_2_segment(t,segs):
    for s in segs:
        if(t.time[0]>=s[0] and t.time[0]<=s[1]):
            return s
    return None

def make_empty_page(cWB_conf,start,end,rootdir,output_file_name,title,roothtml,current_time,ifar_flag):
        fn_tmp="%s.tmp"%(roothtml)
        ff=open("%s.tmp"%(roothtml),"w")
        print("<html>", file=ff)
        print("<title>%s</title>"%(title), file=ff)
        print("<body>", file=ff)
        print("""<h1 align=center><font size=10 face="Courier" color="#DF013A">%s</font></h1>"""%(title), file=ff)
        t=get_time()
        print("<center><i>The page generated at %d : %s</i></center><p>"%(t,tconvert(t)), file=ff)
        print("<hr size=3 noshade color=\"blue\">", file=ff)
        print("<h2 align=center>Run time statistics</h2>", file=ff)
        print("<center>No jobs were running during the specified time interval</center><p>", file=ff)

        f = open("%s/%s/seg_global.txt"%(cWB_conf.run_dir,cWB_conf.seg_dir))
        segments = fromsegwizard(f)
        f.close()

        segments &= segmentlist([segment(start,end)]); segments.coalesce()

        segments_fnr="science_segments.txt"
        segments_fn="%s/%s"%(rootdir,segments_fnr)
        fff=open(segments_fn,"w")
        tosegwizard(fff,segments)
        fff.close()
    
        single_segments={}
        segments_fnr={}
        segments_fn={}
        for ifo in cWB_conf.ifos:
            f=open("%s/%s/%s_cat1.txt"%(cWB_conf.run_dir,cWB_conf.seg_dir,ifo))
            single_segments[ifo]=fromsegwizard(f)
            f.close()
            single_segments[ifo] &=segmentlist([segment(start,end)]);
            single_segments[ifo].coalesce()
            segments_fnr[ifo]="%s_segments.txt"%ifo
            segments_fn[ifo]="%s/%s"%(rootdir,segments_fnr[ifo])
            fff=open(segments_fn[ifo],"w")
            tosegwizard(fff,single_segments[ifo])
            fff.close()

        print("<center><table border=1>", file=ff)
        print("<tr><th align=left>current time</th><td>%d : %s</td></tr>"%(current_time,tconvert(current_time)), file=ff)
        print("<tr><th align=left>start</th><td>%d : %s</td></tr>"%(start,tconvert(start)), file=ff)
        print("<tr><th align=left>end</th><td>%d : %s</td></tr>"%(end,tconvert(end)), file=ff)
        print("</table></center>", file=ff)
        print("<h2 align=center>Livetime</h2>", file=ff)
        print("<center><table border=1 cellpadding=5 bgcolor=\"yellow\">", file=ff)
        print("<tr><th align=left><a href=\"%s\">Science segments</a></th><td>%d</td></tr>"%("science_segments.txt",abs(segments)), file=ff)
        for ifo in cWB_conf.ifos:
          print("<tr><th align=left><a href=\"%s\">%s</a></th>"%(segments_fnr[ifo],ifo), file=ff)
          print("<td>%i</td></tr>"%(abs(single_segments[ifo])), file=ff)
        print("</table></center>", file=ff)

        ff.flush()
        ff.close()
        a=subprocess.getstatusoutput("mv %s %s"%(fn_tmp,roothtml))
        del single_segments
        del segments_fnr
        del segments_fn


def summary_page(cWB_conf,day,rootdir,qPrintJobs,qPrintTriggers,output_file_name,title):
    inj_saved=False
    for ifo,ifile in zip(cWB_conf.ifos,cWB_conf.ifo_files):
         t_det=detector(ifo,"%s/%s"%(cWB_conf.config_dir,ifile.split("/")[-1]))
         if (hasattr(t_det,"inj_name")):
            inj_saved=True
    print("INJ:",inj_saved)

    subprocess.getstatusoutput("mkdir -p %s"%rootdir)
    roothtml="%s/%s"%(rootdir,output_file_name)
    job_dirs=[]

    dodir=1
    if (isinstance(day,str)):
      if (hasattr(cWB_conf,"backup_dir")):
         t_job_dirs=glob.glob(cWB_conf.backup_dir+"/"+cWB_conf.jobs_dir+"/%s/*"%day)
      else:
         t_job_dirs=glob.glob(cWB_conf.run_dir+"/"+cWB_conf.jobs_dir+"/%s/*"%day)
      if (len(t_job_dirs)>0):
        nn=0
        for d in t_job_dirs:
          if (os.path.exists("%s/job.html"%d)):
            nn+=1
            job_dirs.append(d)
      start=get_start_of_the_day_string(day)
      end=start+86400
      list_job_segments_fn="%s/list_job_segments.txt"%rootdir
      if (not os.path.exists(list_job_segments_fn)):
        dodir=1
      else:
        linesfile=len(open(list_job_segments_fn).readlines())
        print("linesfile:%s %i, jobs:%i"%(list_job_segments_fn,linesfile,nn))
        if (nn<linesfile):
           dodir=0
    else:
      if (hasattr(cWB_conf,"backup_dir")):
         job_dirs=glob.glob("%s/%s/daily/????-??-??/day.html"%(cWB_conf.backup_dir,cWB_conf.summaries_dir))
      else:
         job_dirs=glob.glob("%s/%s/daily/????-??-??/day.html"%(cWB_conf.run_dir,cWB_conf.summaries_dir))
      job_dirs.sort()
      if (day>0):
        job_dirs=job_dirs[-day:]
      if (len(job_dirs)>0):
        start=get_start_of_the_day_string(job_dirs[0].split("/")[-2])
        end=get_start_of_the_day_string(job_dirs[-1].split("/")[-2])+86400
      else:
        end=get_time()
        if (day>0):
           start=end-day*86400.
        else:
           start=end-day
         
      
    job_dirs.sort()
    print("In summary_pages: job_dirs=%i"%(len(job_dirs)))

    bkg_postprod_dir="%s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.postprod_dir)
    bkg_input_dir="%s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.jobs_dir)
    if (isinstance(day,int)):
        ifar_flag="FOM_%i_0_0"%day
        draw_bkg.last_N(cWB_conf,setparse("-N %i -recompute 1 -idir %s -odir %s"%(day,bkg_input_dir,bkg_postprod_dir)))
    else:
        ifar_flag="FOM_daily_%s"%(day)
        draw_bkg.query_rate(cWB_conf,setparse("-day %s -rho 6 -idir %s -odir %s/FOM_daily_%s"%(day,bkg_input_dir,bkg_postprod_dir,day)))

    current_time=get_time()
    if(len(job_dirs)==0):
        make_empty_page(cWB_conf,start,end,rootdir,output_file_name,title,roothtml,current_time,ifar_flag)
        if (hasattr(cWB_conf,"copy_web")):
           copy_web(cWB_conf,rootdir,ifar_flag,False)
        return
    if (dodir==1):
      if (isinstance(day,str)):
        JOBS,triggers,job_segments=get_daily_info(cWB_conf,job_dirs,rootdir,qPrintTriggers)
        dump_daily_info(rootdir,job_segments,JOBS,triggers)
      else:
        JOBS=[]
        triggers=[]
        job_segments=segmentlist([])
        for r in job_dirs:
          t_job_segments,t_JOBS,t_triggers=read_daily_info(r.replace("/day.html",""))
          job_segments|=t_job_segments
          JOBS+=t_JOBS
          triggers+=t_triggers
        plot_FOMs(cWB_conf,job_dirs,[],[],rootdir,True)
      job_segments.coalesce()
    else:
      job_segments,JOBS,triggers=read_daily_info(rootdir)
      for t in triggers:
        if (t.selected>=3):
            launch_ced(t,cWB_conf)
            if (hasattr(cWB_conf,"copy_web")):
               png_file_cwb="%s/cWB.png"%(t.dir)
               if (not os.path.exists(png_file_cwb)):
                  skp.main([fit_file,"-o",png_file_cwb,'--annotate', '--contour', '50', '90'])
               png_file_cwb2="%s/cWB.png"%(t.dir.replace(t.job.dir,t.job.link.replace(cWB_conf.web_link,cWB_conf.web_dir)))
               if (not os.path.exists(png_file_cwb2)):
                  subprocess.getstatusoutput("cp %s %s"%(png_file_cwb,png_file_cwb2))
               new_ced=t.compute_ced(cWB_conf,"dir").replace(t.job.dir,t.job.link.replace(cWB_conf.web_link,cWB_conf.web_dir))
               if (not os.path.exists(new_ced) and os.path.exists(t.compute_ced(cWB_conf,"dir"))):
                  subprocess.getstatusoutput("gzip %s/*.dat;mkdir -p %s; cp %s/* %s/."%(t.compute_ced(cWB_conf,"dir"),new_ced,t.compute_ced(cWB_conf,"dir"),new_ced))

    if(abs(job_segments)==0):
        make_empty_page(cWB_conf,start,end,rootdir,output_file_name,title,roothtml,current_time,ifar_flag)
        if (hasattr(cWB_conf,"copy_web")):
           copy_web(cWB_conf,rootdir,ifar_flag)
        return
            
    htmls=glob.glob("%s/FOMs/plot*/body.html"%rootdir)
    pickles=list(map(lambda x: x.pickle,JOBS))
    for html in htmls:
        substitute_ced(cWB_conf,html,pickles,dodir)
    summary_page_next(cWB_conf,JOBS,triggers,roothtml,rootdir,title,ifar_flag,job_segments,qPrintJobs,start,end,output_file_name,qPrintTriggers,bkg_postprod_dir,inj_saved)
    del JOBS[:]
    del triggers[:]
    if (hasattr(cWB_conf,"copy_web")):
       copy_web(cWB_conf,rootdir,ifar_flag)

def substitute_hard_link(cWB_conf,new_dir,rootdir):
   if (not cWB_conf.web_dir in new_dir):
      print("Error,",cWB_conf.web_dir," is not in ",new_dir)
      exit()
   if (cWB_conf.web_dir.endswith("/")):
      cWB_conf.web_dir=cWB_conf.web_dir[:-1]
   if (new_dir.endswith("/")):
      new_dir=new_dir[:-1]
   subdir_number=new_dir.replace(cWB_conf.web_dir,"").count("/")
   new_string="../"*subdir_number
   html_files=glob.glob("%s/*.html"%rootdir)
   for html in html_files:
      fin = open(html, "rt")
      data = fin.read()
      data = data.replace(cWB_conf.web_link, new_string)
      fin.close()
      fin = open(html.replace(rootdir,new_dir), "wt")
      fin.write(data)
      fin.close()

def copy_web(cWB_conf,rootdir,ifar_flag,copybkg=True):
    if (copybkg==True):
       old_dir="%s/%s/%s"%(cWB_conf.bkg_run_dir,cWB_conf.postprod_dir,ifar_flag)
       new_dir="%s/%s/%s"%(cWB_conf.web_dir,cWB_conf.postprod_dir,ifar_flag)
       subprocess.getstatusoutput("rm -rf %s;mkdir -p %s/merge"%(new_dir,new_dir))
       subprocess.getstatusoutput("cp %s/*.txt %s/."%(old_dir,new_dir))
       subprocess.getstatusoutput("cp -r %s/plot* %s/."%(old_dir,new_dir))
       subprocess.getstatusoutput("cp -L %s/merge/wave_%s.M1.root %s/merge/."%(old_dir,ifar_flag,new_dir))
    if (hasattr(cWB_conf,"backup_dir")):
          new_dir=rootdir.replace(cWB_conf.backup_dir,cWB_conf.web_dir)
    else:
          new_dir=rootdir.replace(cWB_conf.run_dir,cWB_conf.web_dir)
    subprocess.getstatusoutput("rm -rf %s;mkdir -p %s;cp -r %s/* %s/."%(new_dir,new_dir,rootdir,new_dir))
    substitute_hard_link(cWB_conf,new_dir,rootdir)


def dump_daily_info(rootdir,job_segments,JOBS,triggers):
      fff=open("%s/list_job_segments.txt"%rootdir,"w")
      tosegwizard(fff,job_segments)
      fff.close()
      job_segments.coalesce()
      f=open("%s/jobs_temp.pickle"%rootdir,"wb")
      pickle.dump(JOBS,f,protocol=2)
      f.close()
      subprocess.getstatusoutput("mv %s/jobs_temp.pickle %s/jobs.pickle"%(rootdir,rootdir))
      f=open("%s/triggers_temp.pickle"%rootdir,"wb")
      pickle.dump(triggers,f,protocol=2)
      f.close()
      subprocess.getstatusoutput("mv %s/triggers_temp.pickle %s/triggers.pickle"%(rootdir,rootdir))

def read_daily_info(rootdir):
      job_s=segmentlist([])
      JOBs=[]
      Triggers=[]
      if (os.path.exists("%s/list_job_segments.txt"%rootdir)):
        fff=open("%s/list_job_segments.txt"%rootdir,"r")
        job_s=fromsegwizard(fff)
        fff.close()
        job_s.coalesce()
      if (os.path.exists("%s/jobs.pickle"%rootdir)):
        f=open("%s/jobs.pickle"%rootdir,"rb")
        JOBs=pickle.load(f)
        f.close()
      if (os.path.exists("%s/triggers.pickle"%rootdir)):
        f=open("%s/triggers.pickle"%rootdir,"rb")
        Triggers=pickle.load(f)
        f.close()
      return job_s,JOBs,Triggers
      return job_segments,JOBS,triggers

def get_daily_info(cWB_conf,job_dirs,rootdir,qPrintTriggers):

    jobs=[x+"/job.pickle" for x in job_dirs]
    job_dirs=None

    JOBS=[]
    rootfiles=[]
    wavefiles=[]
    livefiles=[]
    triggers=[]
    job_segments=segmentlist([])

    n=0

    for job in jobs:
        f=open(job,"rb")
        j=pickle.load(f)
        f.close()
        n+=len(j.triggers_all)
        j.jdir=cWB_conf.web_link+"/"+"/".join(j.dir.split("/")[-3:])
        if(j.status==4):
         JOBS.append(statistic(j))
         job_segments.append(segment(j.start,j.end))
         for t in j.triggers_all:
          if (t.topublish==True):
            triggers.append(t)
            wavefiles.append(t.root_file)
            fit_file="%s/OUTPUT/skymap_%.3f.fits"%(t.job.dir,t.time[0])
            png_file_cwb="%s/cWB.png"%(t.dir)
            if (t.selected>=3):
               launch_ced(t,cWB_conf)
               if (not os.path.exists(png_file_cwb)):
                 skp.main([fit_file,"-o",png_file_cwb,'--annotate', '--contour', '50', '90'])
               if (hasattr(cWB_conf,"copy_web")):
                   png_file_cwb2="%s/cWB.png"%(t.dir.replace(t.job.dir,t.job.link.replace(cWB_conf.web_link,cWB_conf.web_dir)))
                   if (not os.path.exists(png_file_cwb2)):
                      subprocess.getstatusoutput("cp %s %s"%(png_file_cwb,png_file_cwb2))
                   new_ced=t.compute_ced(cWB_conf,"dir").replace(t.job.dir,t.job.link.replace(cWB_conf.web_link,cWB_conf.web_dir))
                   if (not os.path.exists(new_ced) and os.path.exists(t.compute_ced(cWB_conf,"dir"))):
                      subprocess.getstatusoutput("gzip %s/*.dat;mkdir -p %s; cp %s/* %s/."%(t.compute_ced(cWB_conf,"dir"),new_ced,t.compute_ced(cWB_conf,"dir"),new_ced))
        rfiles=glob.glob("%s/OUTPUT/w*.root"%j.dir)
        rootfiles+=rfiles
        rfiles=glob.glob("%s/OUTPUT.merged/live.root"%j.dir)
        livefiles+=rfiles

    if(qPrintTriggers and n > 0):
        print("before FOMs")
        plot_FOMs(cWB_conf,rootfiles,wavefiles,livefiles,rootdir)
        print("after FOMs")
        olddir=os.getcwd()
        os.chdir("%s/FOMs/merge"%rootdir)
        for itype in ["wave","live"]:
          subprocess.getstatusoutput("ln -s %s_cWB_online.M1.root %s_FOMs.M1.root"%(itype,itype))
        os.chdir(olddir)
     
    del rootfiles[:]
    del wavefiles[:]
    del livefiles[:]
    return JOBS,triggers,job_segments

def summary_page_next(cWB_conf,JOBS,triggers,roothtml,rootdir,title,ifar_flag,job_segments,qPrintJobs,start,end,output_file_name,qPrintTriggers,bkg_postprod_dir,inj_saved):
    fn_tmp="%s.tmp"%(roothtml)
    ff=open("%s.tmp"%(roothtml),"w")
    print("<html>", file=ff)
    print("""<head>
<link rel="stylesheet" type="text/css" href="/~vedovato/waveburst/ced-1.0/scripts/shadowbox-3.0.3/shadowbox.css">
<script type="text/javascript" src="/~vedovato/waveburst/ced-1.0/scripts/shadowbox-3.0.3/shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.init();
</script>
<script type="text/javascript" src="%s/tabber.js"></script>                                                         
<link rel="stylesheet" href="%s/tabber.css" TYPE="text/css" MEDIA="screen">
</head>"""%(cWB_conf.web_link,cWB_conf.web_link), file=ff)
    print("<title>%s</title>"%(title), file=ff)
    print("<body>", file=ff)
    print("""<h1 align=center><font size=10 face="Courier" color="#DF013A">%s</font></h1>"""%(title), file=ff)
    t=get_time()
    print("<center><i>The page generated at %d : %s</i></center><p>"%(t,tconvert(t)), file=ff)

    print("<hr size=3 noshade color=\"blue\">", file=ff)

    delay_launch_completion=list(map(lambda x: x.delay_launch_completion,JOBS))
    max_delay_launch_completion=-1
    min_delay_launch_completion=-1
    avg_delay_launch_completion=-1
    if(len(delay_launch_completion)>0):
        max_delay_launch_completion=max(delay_launch_completion)
        tmp_delay_launch_completion=[x for x in delay_launch_completion if x>0]
        try:
            min_delay_launch_completion=min(tmp_delay_launch_completion)
            avg_delay_launch_completion=sum(tmp_delay_launch_completion)/float(len(tmp_delay_launch_completion))
        except Exception as e:
            print("Got exception %s"%(repr(e)))
            print("tmp_delay_launch_completion=%s"%(repr(tmp_delay_launch_completion)))
            min_delay_launch_completion=-1
            avg_delay_launch_completion=-1

    run_statistics_fn="%s/run_statistics.html"%(rootdir) # is that the right directory?
    run_statistics_fnr="run_statistics.html"

    if (hasattr(cWB_conf,"Cuts_list")):
      nclass=len(cWB_conf.Cuts_list)
      if (nclass>1):
         dir_bkg="OR_cut"
      else:
         dir_bkg=cWB_conf.Cuts_list[0]
    elif (hasattr(cWB_conf,"xgboost")):
      dir_bkg="xgb"
    else:
      dir_bkg=""
    if (os.path.exists("%s/POSTPRODUCTION/%s/plot%s/index.html"%(cWB_conf.bkg_run_dir,ifar_flag,dir_bkg))):
      bkgready=True
      imgext=getext("%s/POSTPRODUCTION/%s/plot%s/data/rho_frequency"%(cWB_conf.bkg_run_dir,ifar_flag,dir_bkg))
    else:
      bkgready=False

    if (bkgready==True):
      stat_bkg_title="<th>Background</th>"
      stat_bkg_content="""
    <td>
    <a href="%s/POSTPRODUCTION/%s/segments.txt" target=_new>Segment list</a><br>
    <a href="%s/POSTPRODUCTION/%s/plot%s/data/job_status.html" target=_new>Running time</a>
    </td>"""%(cWB_conf.web_link,ifar_flag,cWB_conf.web_link,ifar_flag,dir_bkg)
    else:
      stat_bkg_title=""
      stat_bkg_content=""
    stat_table="""
    <center>
    <table border=1 cellpadding=5 bgcolor=\"yellow\">
    <tr>
    <th>Zero lag jobs</th>
    <th>Zero lag run time (s)</th>
    %s
    </tr>
    <tr align=\"center\">
    <td><table>
    <tr><td align="left"><a href=\"%s\" target=_new>Job list</a></td><td align="right">%d</td>
    <tr><td align="left"><a href=\"%s\" target=_new>Segment list</a></td><td align="right">%d s</td>
    <tr><td align="left"><a href=\"%s\" target=_new>More details</a></td></tr>
    </table></td>
    <td><table>
    <tr><td align=\"left\">Maximum</td><td align=\"right\">%d</td></tr>
    <tr><td align=\"left\">Minimum</td><td align=\"right\">%d</td></tr>
    <tr><td align=\"left\">Average</td><td align=\"right\">%.2f</td></tr>
    </table></td>
    %s
    </tr>
    </table>
    </center><p>
    """%(stat_bkg_title,"list_job_segments.txt",len(delay_launch_completion),"job_segments.txt",abs(job_segments),run_statistics_fnr,max_delay_launch_completion,min_delay_launch_completion,avg_delay_launch_completion,stat_bkg_content)
    print("<h3 align=center>General information</h3>", file=ff)
    print(stat_table, file=ff)
    del delay_launch_completion[:]

    run_statistics(cWB_conf,JOBS,stat_table,rootdir,start,end, run_statistics_fn, qPrintJobs, title, output_file_name, job_segments, qPrintTriggers)

    seg_table="""
    <table border=1>
    <tr><th>Network</th><th>Segment type</th><th>Livetime, hours</th><th>Duty cycle, \%</th></tr>
    <tr><td>L1H1</td><td>Processed by cWB</td><td><a href=\"\">N</a></td><td>N</td></tr>
    <tr><td>L1H1</td><td>Up</td><td><a href=\"\">N</a></td><td>N</td></tr>
    <tr><td>L1H1</td><td>cWB - UP</td><td><a href=\"\">N hours</a></td><td>N</td></tr>
    </table>
    """

    injection_triggers=[x for x in triggers if x.inj_found==True]
    print(len(injection_triggers))
    if (inj_saved==True):
        triggers=[x for x in triggers if x.inj_found==False]

    string_miss=""
    miss_file="%s/%s/missing.txt"%(bkg_postprod_dir,ifar_flag)
    if(os.path.exists(miss_file)):
        miss=open(miss_file)
        missing_segments = fromsegwizard(miss); missing_segments.coalesce()
        miss.close()
        if(len(missing_segments)>0):
            string_miss="<br><font color=\"red\"><a href=\"%s/POSTPRODUCTION/%s/missing.txt\" target=_new>Some data have still not processed</a></font>"%(cWB_conf.web_link,ifar_flag)

    if(qPrintTriggers and (len(triggers)+len(injection_triggers)) > 0):

        gw_candidates=[x for x in triggers if x.selected==4]
        lumin_triggers=[x for x in triggers if x.selected>=3]
        coherent_triggers=[x for x in triggers if x.selected>=1]
        for ifo in cWB_conf.ifos:
            for category in range(0,4):
                try:
                    lumin_triggers=[x for x in lumin_triggers if x.cat_passed[category][ifo]]
                except:
                    pass
        if(len(gw_candidates)>0):
            trigger_table_gw_fn="%s/triggers_gw.html"%(rootdir)
            trigger_table_gw_fnr="triggers_gw.html"
            trigger_table(cWB_conf,trigger_table_gw_fn, gw_candidates, "GW event candidates",injection_triggers)        
            gw_candidates=sorted(gw_candidates,key=lambda x: x.rho[cWB_conf.id_rho], reverse=True)
            trigger_table_gw_r_fn="%s/triggers_gw_r.html"%(rootdir)
            trigger_table_gw_r_fnr="triggers_gw_r.html"
            trigger_table(cWB_conf,trigger_table_gw_r_fn, gw_candidates, "GW event candidates sorted by rho",injection_triggers)

        if(len(lumin_triggers)>0):
            trigger_table_lumin_fn="%s/triggers_lumin.html"%(rootdir)
            trigger_table_lumin_fnr="triggers_lumin.html"
            trigger_table(cWB_conf,trigger_table_lumin_fn, lumin_triggers, "GraceDB triggers after DQs and vetoes",injection_triggers)
            lumin_triggers=sorted(lumin_triggers,key=lambda x: x.rho[cWB_conf.id_rho], reverse=True)
            trigger_table_lumin_r_fn="%s/triggers_lumin_r.html"%(rootdir)
            trigger_table_lumin_r_fnr="triggers_lumin_r.html"
            trigger_table(cWB_conf,trigger_table_lumin_r_fn, lumin_triggers, "GraceDB triggers sorted by rho",injection_triggers)

        if(len(injection_triggers)>0):
            trigger_table_injection_fn="%s/triggers_injection.html"%(rootdir)
            trigger_table_injection_fnr="triggers_injection.html"
            trigger_table(cWB_conf,trigger_table_injection_fn, injection_triggers, "Hardware injections",injection_triggers)

        if(len(triggers)>0):
            trigger_table_t_fn="%s/triggers_t.html"%(rootdir)
            trigger_table_t_fnr="triggers_t.html"
            trigger_table(cWB_conf,trigger_table_t_fn, triggers, "Triggers sorted by time",injection_triggers,True)
            triggers=sorted(triggers,key=lambda x: x.rho[cWB_conf.id_rho], reverse=True)
            trigger_table_r_fn="%s/triggers_r.html"%(rootdir)
            trigger_table_r_fnr="triggers_r.html"
            trigger_table(cWB_conf,trigger_table_r_fn, triggers, "Triggers sorted by rho",injection_triggers)

        if(len(coherent_triggers)>0):
            coherent_triggers=sorted(coherent_triggers,key=lambda x: x.time[0], reverse=False)
            coherent_table_t_fn="%s/coherent_t.html"%(rootdir)
            coherent_table_t_fnr="coherent_t.html"
            trigger_table(cWB_conf,coherent_table_t_fn, coherent_triggers, "Triggers sorted by time",injection_triggers,True)
            coherent_triggers=sorted(coherent_triggers,key=lambda x: x.rho[cWB_conf.id_rho], reverse=True)
            coherent_table_r_fn="%s/coherent_r.html"%(rootdir)
            coherent_table_r_fnr="coherent_r.html"
            trigger_table(cWB_conf,coherent_table_r_fn, coherent_triggers, "Triggers sorted by rho",injection_triggers)

        print("<center>", file=ff)
        print("<h3 align=center>Triggers</h3>", file=ff)
        print("<table border=1 cellpadding=5>", file=ff)
        print("<tr align=\"center\">", file=ff)
        print("<td><b>All triggers</b><br>No selection</td>", file=ff)
        if (not hasattr(cWB_conf,"xgboost")):
          if (hasattr(cWB_conf,"Cuts_list")):
            nclass=len(cWB_conf.Cuts_list)
            print("<td bgcolor=\"lightslategray\"><b>Selected triggers</b><br>%s</td>"%("+".join([x.replace("_cut","") for x in cWB_conf.Cuts_list])), file=ff)
          else:
            if (hasattr(cWB_conf,"th_cc")):
                th_cc=cWB_conf.th_cc
            else:
                th_cc=0.6
            print("<td bgcolor=\"lightslategray\"><b>Selected triggers</b><br>netcc>=%.1f</td>"%(th_cc), file=ff)
        print("<td bgcolor=\"red\"><b>Gracedb triggers</b><br>(Selected & FAR<=%.2e Hz)</td>"%(cWB_conf.th_far_lum), file=ff)
        print("<td bgcolor=\"chartreuse\"><b>GW candidates</b><br>(Selected & FAR<%.2e Hz)</td>"%(cWB_conf.th_far_off), file=ff)
        print("<td bgcolor=\"cyan\">Injections</td>", file=ff)
        print("</tr>", file=ff)
        print("<tr align=\"center\">", file=ff)
        print("<td><b>%d triggers</b></a><br>"%(len(triggers)), file=ff)
        if (len(triggers)>0):
          print("<a href=\"%s\" target=_new>sorted by time</a><br>"%(trigger_table_t_fnr), file=ff)
          print("<a href=\"%s\" target=_new>sorted by rho</a>"%(trigger_table_r_fnr), file=ff)
        if (not hasattr(cWB_conf,"xgboost")):
          if (len(coherent_triggers)>0):
            print("<td bgcolor=\"lightslategray\"><b>%d triggers</b></a><br>"%(len(coherent_triggers)), file=ff)
            print("<a href=\"%s\" target=_new>sorted by time</a><br>"%(coherent_table_t_fnr), file=ff)
            print("<a href=\"%s\" target=_new>sorted by rho</a>"%(coherent_table_r_fnr), file=ff)
          else:
            print("<td bgcolor=\"lightslategray\"><b>no triggers</b><br>on the given time interval</td>", file=ff)
        if(len(lumin_triggers)==0):
             print("<td bgcolor=\"red\"><b>no triggers</b><br>on the given time interval</td>", file=ff)
        else:
            print("<td bgcolor=\"red\"><b>%d triggers</b><br><a href=\"%s\" target=_new>sorted by time</a><br>"%(len(lumin_triggers),trigger_table_lumin_fnr), file=ff)
            print("<a href=\"%s\" target=_new>sorted by rho</a></td>"%(trigger_table_lumin_r_fnr), file=ff)
        if(len(gw_candidates)==0):
             print("<td bgcolor=\"chartreuse\"><b>no candidates</b><br>on the given time interval</td>", file=ff)
        else:
            print("<td bgcolor=\"chartreuse\"><b>%d candidates</b><br><a href=\"%s\" target=_new>sorted by time</a><br>"%(len(gw_candidates),trigger_table_gw_fnr), file=ff)
            print("<a href=\"%s\" target=_new>sorted by rho</a></td>"%(trigger_table_gw_r_fnr), file=ff)        
        if(len(injection_triggers)>0):
           print("<td bgcolor=\"cyan\"><b>%d</b><br><a href=%s target=_new>Hardware injections</a></td>"%(len(injection_triggers),trigger_table_injection_fnr), file=ff)
        else:
           print("<td bgcolor=\"cyan\">No Hardware injections</td>", file=ff)
        print("</tr>", file=ff)
        print("</table>", file=ff)
        print("</center>", file=ff)

        #add loudest
        if (len(triggers)>0):
          if (len(coherent_triggers)>0):
            coherent_triggers=sorted(coherent_triggers,key=lambda x: x.rho[cWB_conf.id_rho], reverse=True)
            t=coherent_triggers[0]
          else:
            t=triggers[0]
          print("<h3 align=center>Loudest event</h3>", file=ff)
          print("<center>", file=ff)
          print("<table border=1>", file=ff)
          header=t.table_header(cWB_conf,False)
          print(header, file=ff)
          jdir=cWB_conf.web_link+"/"+"/".join(t.job.dir.split("/")[-3:])
          print(t.to_html_table_row(cWB_conf,-1), file=ff)
          print("</table>", file=ff)
          print("</center>", file=ff)

        print("<h3 align=center>Standard cWB pages</h3>", file=ff)
        print("<center>", file=ff)
        print("<table border=1 cellpadding=5 bgcolor=\"#F5A9E1\">", file=ff)
        print("<tr align=\"center\">", file=ff)
        if (hasattr(cWB_conf,"Cuts_list")):
            print("""<th><a href="%s/Cuts.hh.html" target=_new>Class</a></th>"""%(cWB_conf.web_link), file=ff)
        elif (hasattr(cWB_conf,"xgboost")):
            print("""<th><a href="%s/MODELS" target=_new>All Models</a></th>"""%(cWB_conf.web_link), file=ff)
        else:
            print("<th>Class</th>", file=ff)
        print("<th bgcolor=white></th>", file=ff)
        print("<th colspan=2>Zero lag</th>", file=ff)
        if (bkgready==True):
           print("<th bgcolor=white></th>", file=ff)
           print("<th colspan=2>Background</th>", file=ff)
        print("</tr>", file=ff)
        if(len(triggers) > 0):
           if (hasattr(cWB_conf,"Cuts_list")):
             nclass=len(cWB_conf.Cuts_list)
             print("<tr bgcolor=white align=\"center\">", file=ff)
             print("<td><i>Detchar</i></td>", file=ff)
             print("<td bgcolor=white></td>", file=ff)
             print("<td><a href=\"FOMs/plot\" target=_new>Link</a></td>", file=ff)
             print("<td><a href=\"FOMs/plot/data/EVENTS.txt\" target=_new>EVENTS</a></td>", file=ff)
             if (bkgready==True):
                print("<td bgcolor=white></td>", file=ff)
                print("<td><a href=\"%s/POSTPRODUCTION/%s/plot\" target=_new>Link</a></td>"%(cWB_conf.web_link,ifar_flag), file=ff)
                print("<td><a href=\"%s/POSTPRODUCTION/%s/plot/data/EVENTS.txt\" target=_new>EVENTS</a></td>"%(cWB_conf.web_link,ifar_flag), file=ff)
             print("</tr>", file=ff)
             if (nclass>1):
               print("<tr align=\"center\">", file=ff)
               print("<td><i>%s</i></td>"%("+".join([x.replace("_cut","") for x in cWB_conf.Cuts_list])), file=ff)
               print("<td bgcolor=white></td>", file=ff)
               print("<td><a href=\"FOMs/plotOR_cut\" target=_new>Link</a></td>", file=ff)
               print("<td><a href=\"FOMs/plotOR_cut/data/EVENTS.txt\" target=_new>EVENTS</a></td>", file=ff)
               if (bkgready==True):
                 print("<td bgcolor=white></td>", file=ff)
                 print("<td><a href=\"%s/POSTPRODUCTION/%s/plotOR_cut\" target=_new>Link</a></td>"%(cWB_conf.web_link,ifar_flag), file=ff)
                 print("<td><a href=\"%s/POSTPRODUCTION/%s/plotOR_cut/data/EVENTS.txt\" target=_new>EVENTS</a></td>"%(cWB_conf.web_link,ifar_flag), file=ff)
             for i in range(len(cWB_conf.Cuts_list)):
              n=cWB_conf.Cuts_list[i]
              try:
                name=" (%s)"%(cWB_conf.Cuts_name[i])
              except:
                name=""
              print("<tr align=\"center\">", file=ff)
              print("<td><i>%s</i>%s</td>"%(n.replace("_cut",""),name), file=ff)
              print("<td bgcolor=white></td>", file=ff)
              print("<td><a href=\"FOMs/plot%s\" target=_new>Link</a></td>"%(n), file=ff)
              print("<td><a href=\"FOMs/plot%s/data/EVENTS.txt\" target=_new>EVENTS</a></td>"%(n), file=ff)
              if (bkgready==True):
                 print("<td bgcolor=white></td>", file=ff)
                 print("<td><a href=\"%s/POSTPRODUCTION/%s/plot%s\" target=_new>Link</a></td>"%(cWB_conf.web_link,ifar_flag,n), file=ff)
                 print("<td><a href=\"%s/POSTPRODUCTION/%s/plot%s/data/EVENTS.txt\" target=_new>EVENTS</a></td>"%(cWB_conf.web_link,ifar_flag,n), file=ff)
             print("</tr>", file=ff)
           else:
             ext=""
             title="<i>Selected</i>"
             if (hasattr(cWB_conf,"xgboost")):
               ext="xgb"
               last_date=os.readlink("%s/MODELS/preferred"%cWB_conf.config_dir).split("/")[-1]
               title="<a href=\"%s/MODELS/%s\" target=_new>Last</a>"%(cWB_conf.web_link,last_date)
               if (os.path.exists("%s/MODELS/preferred/LINK"%cWB_conf.config_dir)):
                   link=open("%s/MODELS/preferred/LINK"%cWB_conf.config_dir).readlines()
                   title+="-<a href=\"%s\" target=_new>Web page</a>"%link[0].strip()
             print("<tr align=\"center\">", file=ff)
             print("<td>%s</td>"%title, file=ff)
             print("<td bgcolor=white></td>", file=ff)
             print("<td><a href=\"FOMs/plot%s\" target=_new>Link</a></td>"%ext, file=ff)
             print("<td><a href=\"FOMs/plot%s/data/EVENTS.txt\" target=_new>EVENTS</a></td>"%ext, file=ff)
             if (bkgready==True):
                print("<td bgcolor=white></td>", file=ff)
                print("<td><a href=\"%s/POSTPRODUCTION/%s/plot%s\" target=_new>Link</a></td>"%(cWB_conf.web_link,ifar_flag,ext), file=ff)
                print("<td><a href=\"%s/POSTPRODUCTION/%s/plot%s/data/EVENTS.txt\" target=_new>EVENTS</a></td>"%(cWB_conf.web_link,ifar_flag,ext), file=ff)
             print("</tr>", file=ff)
        print("</table>", file=ff)
        print("</center>", file=ff)

        print("<hr size=5 noshade color=\"white\">", file=ff)
        print("<hr width=90% size=3 noshade color=\"blue\">", file=ff)
        print("<hr width=90% size=3 noshade color=\"blue\">", file=ff)

        plot_zero=True
        tabber=False
        if (hasattr(cWB_conf,"Cuts_list")):
          nclass=len(cWB_conf.Cuts_list)
          if (nclass>1):
            dir_bkg="OR_cut"
            tabber=True
          else:
            dir_bkg=cWB_conf.Cuts_list[0]
          if (len(coherent_triggers)==0):
           plot_zero=False
        elif (hasattr(cWB_conf,"xgboost")):
          dir_bkg="xgb"
        else:
          dir_bkg="" 
 
        print("<h2 align=center><font size=8 face=\"Courier\"  color=\"#DF013A\">Figures of merit</font></h2>", file=ff)
        print("<center>", file=ff)
        if (plot_zero):
          figext=getext("%s/FOMs/plot%s/data/frequency"%(rootdir,dir_bkg))
          print("<h3 align=center><font size=6 face=\"Courier\"  color=\"#01DF01\">Zero lag</font></h3>", file=ff)
          print("<table border=0 cellspacing = 20 width=60% bgcolor=\"#F2F2F2\">", file=ff)
          print("<tr><th colspan=\"2\">Ranking statistic vs time and frequency</th></tr>", file=ff)
          print("<tr>", file=ff)
          if (tabber==False):
             print("<td align=\"left\"><a href=\"FOMs/plot%s/data/rho_time.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/rho_time.%s\" width=400></a></td><td align=\"right\"><a href=\"FOMs/plot%s/data/rho_frequency.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/rho_frequency.%s\" width=400></a></td>"%(dir_bkg,figext,dir_bkg,figext,dir_bkg,figext,dir_bkg,figext), file=ff)
          else:
             print("<td align=\"center\">", file=ff)
             print("<div class=\"tabber\">", file=ff)
             print("<div class=\"tabbertab\"><h2>%s</h2><a href=\"FOMs/plot%s/data/rho_time.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/rho_time.%s\" width=400></a></div>"%("+".join([x.replace("_cut","") for x in cWB_conf.Cuts_list]),dir_bkg,figext,dir_bkg,figext), file=ff)
             for qq in range(len(cWB_conf.Cuts_list)):
               print("<div class=\"tabbertab\"><h2>%s</h2><a href=\"FOMs/plot%s/data/rho_time.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/rho_time.%s\" width=400></a></div>"%(cWB_conf.Cuts_list[qq].replace("_cut",""),cWB_conf.Cuts_list[qq],figext,cWB_conf.Cuts_list[qq],figext), file=ff)
             print("</div>", file=ff)
             print("</td>", file=ff)
             print("<td align=\"center\">", file=ff)
             print("<div class=\"tabber\">", file=ff)
             print("<div class=\"tabbertab\"><h2>%s</h2><a href=\"FOMs/plot%s/data/rho_frequency.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/rho_frequency%sf\" width=400></a></div>"%("+".join([x.replace("_cut","") for x in cWB_conf.Cuts_list]),dir_bkg,figext,dir_bkg,figext), file=ff)
             for qq in range(len(cWB_conf.Cuts_list)):
               print("<div class=\"tabbertab\"><h2>%s</h2><a href=\"FOMs/plot%s/data/rho_frequency.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/rho_frequency.%s\" width=400></a></div>"%(cWB_conf.Cuts_list[qq].replace("_cut",""),cWB_conf.Cuts_list[qq],figext,cWB_conf.Cuts_list[qq],figext), file=ff)
             print("</div>", file=ff)
             print("</td>", file=ff)
          print("</tr>", file=ff)
          print("<tr align=\"justify\"><td colspan=\"2\"><i>Ranking statistic as a function of gps time (left) and estimated frequency (right).</i></td></tr>", file=ff)
          print("</table><p>", file=ff)
          print("<table border=0 cellspacing = 20 width=60% bgcolor=\"#F2F2F2\">", file=ff)
          print("<table border=0 cellspacing = 20 width=60% bgcolor=\"#F2F2F2\">", file=ff)
          print("<tr>", file=ff)
          if (tabber==False):
              print("<td align=\"left\"><a href=\"FOMs/plot%s/data/ra_vs_dec_online.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/ra_vs_dec_online.%s\" width=400></a></td><td align=\"right\"><a href=\"FOMs/plot%s/data/phi_vs_theta_online.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/phi_vs_theta_online.%s\" width=400></a></td>"%(dir_bkg,figext,dir_bkg,figext,dir_bkg,figext,dir_bkg,figext), file=ff)
          else:
             print("<td align=\"center\">", file=ff)
             print("<div class=\"tabber\">", file=ff)
             print("<div class=\"tabbertab\"><h2>%s</h2><a href=\"FOMs/plot%s/data/ra_vs_dec_online.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/ra_vs_dec_online.%s\" width=400></a></div>"%("+".join([x.replace("_cut","") for x in cWB_conf.Cuts_list]),dir_bkg,figext,dir_bkg,figext), file=ff)
             for qq in range(len(cWB_conf.Cuts_list)):
               print("<div class=\"tabbertab\"><h2>%s</h2><a href=\"FOMs/plot%s/data/ra_vs_dec_online.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/ra_vs_dec_online.%s\" width=400></a></div>"%(cWB_conf.Cuts_list[qq].replace("_cut",""),cWB_conf.Cuts_list[qq],figext,cWB_conf.Cuts_list[qq],figext), file=ff)
             print("</div>", file=ff)
             print("</td>", file=ff)
             print("<td align=\"center\">", file=ff)
             print("<div class=\"tabber\">", file=ff)
             print("<div class=\"tabbertab\"><h2>%s</h2><a href=\"FOMs/plot%s/data/phi_vs_theta_online.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/phi_vs_theta_online.%s\" width=400></a></div>"%("+".join([x.replace("_cut","") for x in cWB_conf.Cuts_list]),dir_bkg,figext,dir_bkg,figext), file=ff)
             for qq in range(len(cWB_conf.Cuts_list)):
               print("<div class=\"tabbertab\"><h2>%s</h2><a href=\"FOMs/plot%s/data/phi_vs_theta_online.%s\" rel=\"shadowbox[gallery]\"><img src=\"FOMs/plot%s/data/phi_vs_theta_online.%s\" width=400></a></div>"%(cWB_conf.Cuts_list[qq].replace("_cut",""),cWB_conf.Cuts_list[qq],figext,cWB_conf.Cuts_list[qq],figext), file=ff)
             print("</div>", file=ff)
             print("</td>", file=ff)
          print("</tr>", file=ff)
          print("<tr align=\"justify\"><td colspan=\"2\"><i>Estimated sky coordinates for each trigger. Left: right ascension and declination, right: latitude and longitude. Colors refer to the number of events inside each bin.</i></td></tr>", file=ff)
          print("</table>", file=ff)
        if (bkgready==True):
           write_bkg(cWB_conf,ff,ifar_flag,imgext,string_miss,False)
    else:
        print("<h2 align=center>No zero lag triggers</h2>", file=ff)
        print("<center>", file=ff)
        if (bkgready==True):
           write_bkg(cWB_conf,ff,ifar_flag,imgext,string_miss)
    print("</body>", file=ff)
    print("</html>", file=ff)

    ff.flush()
    ff.close()

    a=subprocess.getstatusoutput("mv %s %s"%(fn_tmp,roothtml))

def write_bkg(cWB_conf,ff,ifar_flag,figext,string_miss="",putlink=True):
        tabber=False
        if (hasattr(cWB_conf,"Cuts_list")):
          nclass=len(cWB_conf.Cuts_list)
          if (nclass>1):
            dir_bkg="OR_cut"
            tabber=True
          else:
            dir_bkg=cWB_conf.Cuts_list[0]
        elif (hasattr(cWB_conf,"xgboost")):
          dir_bkg="xgb"
        else:
          dir_bkg=""
        print("<center>", file=ff)
        print("<h3 align=center><font size=6 face=\"Courier\"  color=\"#01DF01\">Background</font>%s</h3>"%(string_miss), file=ff)
        print("<table border=0 cellspacing = 20 width=40% bgcolor=\"#F2F2F2\">", file=ff)
        if (putlink==True):
             print("<tr><th><a href=\"%s/POSTPRODUCTION/%s/plot%s\" target=_new>Standard web page</a></th></tr>"%(cWB_conf.web_link,ifar_flag,dir_bkg), file=ff)
        print("<tr><th align=\"center\">Cumulative rate vs ranking statistic</th></tr>", file=ff)
        print("<tr><th align=\"center\">", file=ff)
        if (tabber==False):
           print("<a href=\"%s/POSTPRODUCTION/%s/plot%s/data/rate_threshold.%s\" rel=\"shadowbox[gallery]\"><img src=\"%s/POSTPRODUCTION/%s/plot%s/data/rate_threshold.%s\" width=400></a>"%(cWB_conf.web_link,ifar_flag,dir_bkg,figext,cWB_conf.web_link,ifar_flag,dir_bkg,figext), file=ff)
        else:
           print("""<div class="tabber">""", file=ff)
           print("""<div class="tabbertab"><h2>%s</h2><a href=\"%s/POSTPRODUCTION/%s/plot%s/data/rate_threshold.%s\" rel=\"shadowbox[gallery]\"><img src=\"%s/POSTPRODUCTION/%s/plot%s/data/rate_threshold.%s\" width=400></a></div>"""%("+".join([x.replace("_cut","") for x in cWB_conf.Cuts_list]),cWB_conf.web_link,ifar_flag,dir_bkg,figext,cWB_conf.web_link,ifar_flag,dir_bkg,figext), file=ff)
           for qq in range(len(cWB_conf.Cuts_list)):
              print("""<div class="tabbertab"><h2>%s</h2><a href=\"%s/POSTPRODUCTION/%s/plot%s/data/rate_threshold.%s\" rel=\"shadowbox[gallery]\"><img src=\"%s/POSTPRODUCTION/%s/plot%s/data/rate_threshold.%s\" width=400></a></div>"""%(cWB_conf.Cuts_list[qq].replace("_cut",""),cWB_conf.web_link,ifar_flag,cWB_conf.Cuts_list[qq],figext,cWB_conf.web_link,ifar_flag,cWB_conf.Cuts_list[qq],figext), file=ff)
           print("</div>", file=ff)
        print("</td></tr>", file=ff)
        print("<tr align=\"justify\"><td><i>Cumulative rate of triggers according to their amplitude distribution.</i></td></tr>", file=ff)
        print("</table><p>", file=ff)
        print("<table border=0 cellspacing = 20 width=60% bgcolor=\"#F2F2F2\">", file=ff)
        print("<tr><th colspan=\"2\">Ranking statistics vs time and frequency</th></tr>", file=ff)
        print("<tr>", file=ff)
        if (tabber==False):
           print("<td align=\"left\"><a href=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_time.%s\" rel=\"shadowbox[gallery]\"><img src=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_time.%s\" width=400></a></td><td align=\"right\"><a href=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_frequency.%s\" rel=\"shadowbox[gallery]\"><img src=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_frequency.%s\" width=400></a></td>"%(cWB_conf.web_link,ifar_flag,dir_bkg,figext,cWB_conf.web_link,ifar_flag,dir_bkg,figext,cWB_conf.web_link,ifar_flag,dir_bkg,figext,cWB_conf.web_link,ifar_flag,dir_bkg,figext), file=ff)
        else:
           print("<td align=\"center\">", file=ff)
           print("<div class=\"tabber\">", file=ff)
           print("""<div class="tabbertab"><h2>%s</h2><a href=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_time.%s\" rel=\"shadowbox[gallery]\"><img src=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_time.%s\" width=400></a></div>"""%("+".join([x.replace("_cut","") for x in cWB_conf.Cuts_list]),cWB_conf.web_link,ifar_flag,dir_bkg,figext,cWB_conf.web_link,ifar_flag,dir_bkg,figext), file=ff)
           for qq in range(len(cWB_conf.Cuts_list)):
              print("""<div class="tabbertab"><h2>%s</h2><a href=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_time.%s\" rel=\"shadowbox[gallery]\"><img src=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_time.%s\" width=400></a></div>"""%(cWB_conf.Cuts_list[qq].replace("_cut",""),cWB_conf.web_link,ifar_flag,cWB_conf.Cuts_list[qq],figext,cWB_conf.web_link,ifar_flag,cWB_conf.Cuts_list[qq],figext), file=ff)
           print("</div>", file=ff)
           print("</td>", file=ff)
           print("<td align=\"center\">", file=ff)
           print("<div class=\"tabber\">", file=ff)
           print("""<div class="tabbertab"><h2>%s</h2><a href=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_frequency.%s\" rel=\"shadowbox[gallery]\"><img src=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_frequency.%s\" width=400></a></div>"""%("+".join([x.replace("_cut","") for x in cWB_conf.Cuts_list]),cWB_conf.web_link,ifar_flag,dir_bkg,figext,cWB_conf.web_link,ifar_flag,dir_bkg,figext), file=ff)
           for qq in range(len(cWB_conf.Cuts_list)):
              print("""<div class="tabbertab"><h2>%s</h2><a href=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_frequency.%s\" rel=\"shadowbox[gallery]\"><img src=\"%s/POSTPRODUCTION/%s/plot%s/data/rho_frequency.%s\" width=400></a></div>"""%(cWB_conf.Cuts_list[qq].replace("_cut",""),cWB_conf.web_link,ifar_flag,cWB_conf.Cuts_list[qq],figext,cWB_conf.web_link,ifar_flag,cWB_conf.Cuts_list[qq],figext), file=ff)
           print("</div>", file=ff)
           print("</td>", file=ff)
        print("</tr>", file=ff)
        print("<tr align=\"justify\"><td colspan=\"2\"><i>Ranking statistic as a function of gps time (left) and estimated frequency (right).</i></td></tr>", file=ff)
        print("</table><p>", file=ff)
        print("</center>", file=ff)

def findced(cWB_conf,pickles,cedstring):
    ced=""
    trigger_time=float(cedstring.split("_")[len(cedstring.split("_"))-1].replace("\/",""))
    #if (hasattr(cWB_conf,"backup_dir")):
    #   files=glob.glob("%s/%s/????-??-??/*/job.pickle"%(cWB_conf.backup_dir,cWB_conf.jobs_dir))
    #else:
    #   files=glob.glob("%s/%s/????-??-??/*/job.pickle"%(cWB_conf.run_dir,cWB_conf.jobs_dir))
    #files=[x for x in files if int(x.split("/")[-2].split("-")[0])-cWB_conf.seg_duration<=trigger_time and int(x.split("/")[-2].split("-")[1])>trigger_time]
    files=[x for x in pickles if int(x.split("/")[-2].split("-")[0])-cWB_conf.seg_duration<=trigger_time and int(x.split("/")[-2].split("-")[1])>trigger_time]
    for file in files:
       f=open(file,"rb")
       j=pickle.load(f)
       f.close()
       if (j.back<=trigger_time and j.end>=trigger_time):
          if (len(j.triggers_all)>0):
             for t in j.triggers_all:
               if (t.topublish==True and abs(float(t.start[0])-trigger_time)<.1):
                if (os.path.exists("%s/OUTPUT/%s"%(t.job.dir,t.compute_ced_dir(cWB_conf)))):
                   ced=t.compute_ced(cWB_conf)
    return ced

def substitute_ced(cWB_conf,ifile,pickles,dodir):
    tfile=ifile.replace(".html","_temp.html")
    if (dodir):
      subprocess.getstatusoutput("mv %s %s"%(ifile,tfile))
    lines=open(tfile).readlines()
    ff=open(ifile,"w")
    for l in lines:
       if (l.find("href")!=-1 and l.find("ced")!=-1):
          numbers=l.split(">")
          for n in numbers:
             nn=list(filter(lambda x: isinstance(getnumberfromstring(x),int),n.split("<")))
             if (len(nn)==1):
               number=getnumberfromstring(nn[0])
          cedstring=l.split("\"")[1]
          newced=findced(cWB_conf,pickles,cedstring)
          if (len(newced)==0):
             print("<td>%i</td>"%number,file=ff)
          else:
             print(l.replace(cedstring,newced), file=ff)
       else:
          print(l, file=ff)
    ff.close()

def plot_FOMs(cWB_conf,rootfiles,wavefiles,livefiles,rootdir,labels=False):
    olddir=os.getcwd()
    namefile="cWB_online"
    version="M1"
    subprocess.getstatusoutput("rm -rf %s/%s;mkdir -p %s/%s"%(rootdir,namefile,rootdir,namefile))
    if (os.environ['SITE_CLUSTER']=="CASCINA"):
      add_rootrc("%s/%s"%(rootdir,namefile))
    os.chdir("%s/%s"%(rootdir,namefile))
    subprocess.getstatusoutput("mkdir -p output config condor merge report/postprod")
    subprocess.getstatusoutput("cp %s config/user_parameters.C"%"%s/user_parameters.C"%(cWB_conf.config_dir))
    subprocess.getstatusoutput("cp %s config/user_pparameters.C"%("%s/user_pparameters.C"%cWB_conf.config_dir))
    if (labels==True):
      for trootfile in rootfiles:
        rootfile=trootfile.replace("/day.html","/FOMs")
        print(rootfile)
        print("%s/bin/cwb_clonedir %s ../%s '--output merge'"%(os.environ['HOME_WAT_INSTALL'],rootfile,namefile))
        subprocess.getstatusoutput("%s/bin/cwb_clonedir %s ../%s '--output merge'"%(os.environ['HOME_WAT_INSTALL'],rootfile,namefile))
        label=rootfile.split("/")[-2]
        subprocess.getstatusoutput("mv output/wave_FOMs.M1.root output/wave_%s.M1_job%i.root; rm output/wave_FOMs.M1.lst"%(label,1))
      subprocess.getstatusoutput("""%s/bin/cwb_merge %s"""%(os.environ['HOME_WAT_INSTALL'],version))
    else:
      f=open("merge/merge_%s.%s.lst"%(namefile,version),"w")
      print("\n".join(rootfiles), file=f)
      f.close()
      f=open("merge/wave_%s.%s.lst"%(namefile,version),"w")
      print("\n".join(wavefiles), file=f)
      f.close()
      f=open("merge/live_%s.%s.lst"%(namefile,version),"w")
      print("\n".join(livefiles), file=f)
      f.close()
      for rootfile in rootfiles:
          subprocess.getstatusoutput("ln -sf %s output/"%(rootfile))
      merge_trees(wavefiles,"waveburst","merge/wave_%s.%s.root"%(namefile,version))
      addhistory(rootfiles[0],"merge/wave_%s.%s.root"%(namefile,version))
      merge_trees(livefiles,"liveTime","merge/live_%s.%s.root"%(namefile,version))
    print(subprocess.getstatusoutput("mkdir -p ../FOMs"))
    com="%s/bin/cwb_report %s create %i %i"%(os.environ['HOME_WAT_INSTALL'],version,0,0)
    print(com)
    subprocess.getstatusoutput(com)
    pp_dir=glob.glob("report/postprod/*")
    print(pp_dir)
    if (hasattr(cWB_conf,"xgboost")):
      print(subprocess.getstatusoutput("rm -rf ../FOMs/plotxgb"))
      print(subprocess.getstatusoutput("mv %s ../FOMs/plotxgb"%(pp_dir[0])))
    else:
      print(subprocess.getstatusoutput("rm -rf ../FOMs/plot"))
      print(subprocess.getstatusoutput("mv %s ../FOMs/plot"%(pp_dir[0])))
    try:
       if (len(cWB_conf.Cuts_list)>1):
          T_Cuts_list=["OR_cut"]
       else:
          T_Cuts_list=[]
       for n in cWB_conf.Cuts_list:
            T_Cuts_list.append(n)
       for n in T_Cuts_list:
         com="""%s/bin/cwb_setcuts %s '--tcuts %s --label %s' """%(os.environ['HOME_WAT_INSTALL'],version,n,n)
         subprocess.getstatusoutput(com)
         if (os.path.exists("merge/live_%s.%s.C_%s.root"%(namefile,version,n))):
           com="%s/bin/cwb_report %s.C_%s create %i %i"%(os.environ['HOME_WAT_INSTALL'],version,n,0,0)
           subprocess.getstatusoutput(com)
           pp_dir=glob.glob("report/postprod/*")
           print(pp_dir)
           print(subprocess.getstatusoutput("rm -rf ../FOMs/plot%s"%n))
           print(subprocess.getstatusoutput("mv %s ../FOMs/plot%s"%(pp_dir[0],n)))
         else:
           print("no event in this class")
    except:
       print("no class")
    print(subprocess.getstatusoutput("rm -rf ../FOMs/merge"))
    print(subprocess.getstatusoutput("mv merge ../FOMs/."))
    os.chdir("../")
    print(subprocess.getstatusoutput("rm -rf %s"%(namefile)))
    os.chdir(olddir)

def run_statistics(cWB_conf,JOBS,stat_table,rootdir,start,end,run_statistics_fn, qPrintJobs, title, output_file_name, job_segments, qPrintTriggers):
    delay_launch_completion=list(map(lambda x: x.delay_launch_completion,JOBS))
    delay_launch_start=list(map(lambda x: x.delay_launch_start,JOBS))
    delay_start_completion=list(map(lambda x: x.delay_start_completion,JOBS))
    delay_start_send=list(map(lambda x: x.delay_start_send,JOBS))
    jobs_str=list(map(lambda x: x.jobs_str,JOBS))

    fn_tmp="%s.tmp"%(run_statistics_fn)
    ff=open(fn_tmp,"w")
    print("<html>", file=ff)
    print("<title>%s</title>"%(title), file=ff)
    print("<body>", file=ff)

    print(stat_table, file=ff)
    print("<p>", file=ff)
    print("<center>", file=ff)
    print("<table>", file=ff)
    print("<tr align=\"center\">", file=ff)
    print("<td><img src=\"running_time.png\"></td>", file=ff)
    print("<td><img src=\"launch_time.png\"></td>", file=ff)
    print("<td><img src=\"completion_time.png\"></td>", file=ff)
    print("</tr>", file=ff)
    print("<tr>", file=ff)
    print("<td><i>Time analysis for each segment.</i></td>", file=ff)
    print("<td><i>Delay time between the starting of the analysis<br>and the end of the segment</i></td>", file=ff)
    print("<td><i>Difference between the end of the analysis job<br>and the end segment time</i></td>", file=ff)
    print("</tr>", file=ff)
    print("</table>", file=ff)
    print("</center>", file=ff)
    
    pylab.figure(figsize=(4,4))
    try:
        b=pylab.hist(delay_launch_completion)
    except:
        pass
    pylab.grid(True)
    pylab.xlabel("job running time, seconds")
    pylab.ylabel("events")
    pylab.title("Job running time")
    pylab.savefig("%s/running_time.png"%(rootdir))
    pylab.close()
    pylab.clf()

    pylab.figure(figsize=(4,4))
    try:
        b=pylab.hist(delay_launch_start)
    except:
        pass
    pylab.grid(True)
    pylab.xlabel("delay launch time, seconds")
    pylab.ylabel("events")
    pylab.title("Delay launch time")
    pylab.savefig("%s/launch_time.png"%(rootdir))
    pylab.close()
    pylab.clf()

    pylab.figure(figsize=(4,4))
    try:
        b=pylab.hist(delay_start_completion)
    except:
        pass
    pylab.grid(True)
    pylab.xlabel("job completion time, seconds")
    pylab.ylabel("events")
    pylab.title("Job completion time")
    pylab.savefig("%s/completion_time.png"%(rootdir))
    pylab.close()
    pylab.clf()

    current_time=get_time()

    f = open("%s/%s/seg_global.txt"%(cWB_conf.run_dir,cWB_conf.seg_dir))
    segments = fromsegwizard(f)
    f.close()

    segments &= segmentlist([segment(start,end)]); segments.coalesce()

    segments_fnr="science_segments.txt"
    segments_fn="%s/%s"%(rootdir,segments_fnr)
    fff=open(segments_fn,"w")
    tosegwizard(fff,segments)
    fff.close()

    job_segments_fnr="job_segments.txt"
    job_segments_fn="%s/%s"%(rootdir,job_segments_fnr)
    fff=open(job_segments_fn,"w")
    tosegwizard(fff,job_segments)
    fff.close()

    diff_segments_job_segments = segments - job_segments; diff_segments_job_segments.coalesce()

    diff_segments_job_segments_fnr="diff_segments_job_segments.txt"
    diff_segments_job_segments_fn="%s/%s"%(rootdir,diff_segments_job_segments_fnr)
    fff=open(diff_segments_job_segments_fn,"w")
    tosegwizard(fff,diff_segments_job_segments)
    fff.close()
    
    print("<h2 align=center>Job coverage of segments</h2>", file=ff)
    print("<table border=1>", file=ff)
    print("<tr><th align=left>Current time</th><td>%d :  %s</td></tr>"%(current_time,tconvert(current_time)), file=ff)
    print("<tr><th align=left>Start of the considered interval</th><td>%d :  %s</td></tr>"%(start,tconvert(start)), file=ff)
    print("<tr><th align=left>End of the considered interval</th><td>%d :  %s</td></tr>"%(end,tconvert(end)), file=ff)    
    print("</table>", file=ff)

    note="""
<p>
Each cWB job takes %i seconds of input data plus %i second of offset at the beginning and end.
Jobs time shift is %i seconds, so each job overlaps the previous one of %i seconds (excluding the first one).
This means that at least %i seconds of unprocessed data are required to launch a new job.
Any time segments with length less than %i seconds is not considered at the moment.
<p>

"""%(cWB_conf.seg_duration,2*cWB_conf.job_offset,cWB_conf.moving_step,cWB_conf.seg_duration-cWB_conf.moving_step,cWB_conf.moving_step,cWB_conf.seg_duration+2*cWB_conf.job_offset)

    print(note, file=ff)

    if(qPrintJobs):
        
        print("<h2 align=center>Completed jobs</h2>", file=ff)
        print("<table border=1>", file=ff)
        print("<tr><th>start date/time</th><th>start</th><th>end</th><th>launch</th><th>completion</th><th>completion-launch</th><th>launch-start</th><th>number of triggers</th></tr>", file=ff)
        for job_str in jobs_str:
           print(job_str, file=ff)
        print("</table>", file=ff)

    ff.flush()
    ff.close()
    subprocess.getstatusoutput("mv %s %s"%(fn_tmp,run_statistics_fn))

    del delay_launch_completion[:]
    del delay_launch_start[:]
    del delay_start_completion[:]
    del delay_start_send[:]
    del jobs_str

def llcache(trigger):
    start,end=list(map(int,os.getcwd().split("/")[-1].split("-")))
    print("%s %s"%(start,end))
    start=trigger.job.back-cWB_conf.job_offset

    ms=start/4*4-8
    me=end/4*4
    if(me!=end):
        me+=4
    me+=8

    for ifo,dir,fnames in zip(cWB_conf.ifos,cWB_conf.bkg_dir,cWB_conf.bkg_fnames):
        f=open("input/%s_scratch.frames"%ifo,"w")
        f1=open("input/%s_scratch.missed"%ifo,"w")
        for t in range(ms,me+4,4):
            five=str(t)[:5]
            p="%s/%s/%s%s/%s%s-4.gwf"%(dir,ifo,fnames,five,fnames,t)
            if(os.path.exists(p)):
                print(p, file=f)
            else:
                print(p, file=f1)
        f.close()
        f1.close()

def followup(trigger):
    print("In followup for trigger\n%s"%(repr(trigger)))
    olddir=os.getcwd()
    os.chdir(trigger.job.dir)
    print("dir=%s"%(os.getcwd()))
    try:
       pe_par=cWB_conf.pe_par
       do_pe=True
    except:
       do_pe=False
    try:
        print("Searching for frames")
        llcache(trigger)
        launch_ced(trigger,do_pe)
    except Exception as e:
        print("Not found frames for %f"%float(trigger.time[0]))
    os.chdir(olddir)

def launch_ced(trigger,cWB_conf):
    time=str(trigger.time[0])
    if (not os.path.exists("%s/log_ced_%s"%(trigger.job.dir,time))):
       job_file=glob.glob("%s/input/job*.root"%trigger.job.dir)
       olddir=os.getcwd()
       os.chdir(trigger.job.dir)
       if (hasattr(cWB_conf,"backup_dir")):
           subprocess.getstatusoutput("mkdir config")
       subprocess.getstatusoutput("rm config/user_parameters.C;ln -s %s/user_parameters_ced.C config/user_parameters.C"%cWB_conf.config_dir)
       if (len(job_file)==1):
        job_f=job_file[0].split("/")[-1]
        launch_ced_from_job(trigger,time,job_f,cWB_conf)
       else:
        launch_ced_from_files(trigger,time,cWB_conf)
       os.chdir(olddir)
def launch_ced_from_job(trigger,time,job_f,cWB_conf):
       subprocess.getstatusoutput("rm OUTPUT/%s;cp input/%s OUTPUT/."%(job_f,job_f))
       com="%s/bin/cwb_inet2G OUTPUT/%s LIKELIHOOD config/user_parameters.C 'ced' >& log_ced_%s &"%(os.environ['HOME_WAT_INSTALL'],job_f,time)
       subprocess.getstatusoutput(com)
def launch_ced_from_files(trigger,time,cWB_conf):
    N_missing_data=[]
    if (not os.path.exists("input/missing_frame_ced.txt")):
       f=open("input/burst.in","w")
       print("%s %s"%(trigger.job.cats["cat1"][0][0],trigger.job.cats["cat1"][0][1]), file=f)
       f.close()
       for ifo,ifile in zip(cWB_conf.ifos,cWB_conf.ifo_files):
          f=open("input/%s_cat2.in"%(ifo),"w")
          for s in trigger.job.cats["cat2_%s"%ifo]:
              print("%s %s"%(s[0],s[1]), file=f)
          f.close()
          t_det=detector(ifo,"%s/%s"%(cWB_conf.config_dir,ifile.split("/")[-1]),True)
          fn="input/%s.frames"%ifo
          f=open("input/%s.frames"%ifo,"w")
          pattern=t_det.getframelist(trigger.job.cats["cat1"][0][0],trigger.job.cats["cat1"][0][1])
          print("\n".join(pattern), file=f)
          f.close()
          for fn in pattern:
                 if(not os.path.exists(fn)):
                    N_missing_data.append(fn)
    else:
       pattern=open("input/missing_frame_ced.txt").readlines()
       for fn in pattern:
           if(not os.path.exists(fn.strip())):
               N_missing_data.append(fn.strip())
    if (len(N_missing_data)>0):
       f=open("input/missing_frame_ced.txt","w")
       print("\n".join(N_missing_data), file=f)
       f.close()
    else:
       com="%s/bin/cwb_inet 1 0 'ced' >& log_ced_%s &"%(os.environ['HOME_WAT_INSTALL'],time)
       subprocess.getstatusoutput(com)
       
def trigger_table(cWB_conf,trigger_table_fn, triggers, title, injections, firstTime=False):
    ff=open(trigger_table_fn+".tmp","w")
    print("<html>", file=ff)
    print("<title>%s</title>"%(title), file=ff)
    print("<body>", file=ff)
    print("<h1 align=center>%s</h1>"%(title), file=ff)
    print("<table border=1>", file=ff)
    header=triggers[0].table_header(cWB_conf,True)
    print(header, file=ff)
    counter=1

    for t in triggers:
        jdir=cWB_conf.web_link+"/"+"/".join(t.job.dir.split("/")[-3:])
        t.cat_passed_txt=""
        print(t.to_html_table_row(cWB_conf,counter), file=ff)
        counter+=1
    print("</table>", file=ff)
    print("</body>", file=ff)
    print("</html>", file=ff)
    ff.flush()
    ff.close()
    subprocess.getstatusoutput("mv %s.tmp %s"%(trigger_table_fn, trigger_table_fn))
