import sys, os, glob
import pickle
from cwb_online.run_utils import get_date_from_gps
import ROOT
import cppyy
from cwb_online.run import JOB
from cwb_online.Trigger import Trigger

class bcolors:
   HEADER = '\033[95m'
   OK = '\033[92m'
   WARNING = '\033[93m'
   FAIL = '\033[91m'
   ENDC = '\033[0m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'

def getstring(trigger,cWB_conf,time,status,all=True):
    if (trigger.selected>0 and all==True):
        ifar="\n\tifar = %s"%trigger.ifar
    else:
        ifar=""
    if (os.path.exists(trigger.compute_ced(cWB_conf,"dir")) and all==True):
        ced_link="\n\tCED: %s"%trigger.compute_ced(cWB_conf)
    else:
        ced_link=""
    if (all==True):
        f_status="\n\tStatus: %s"%status[trigger.selected]
    else:
        f_status=""
    return"""\tTxt file: %s%s
\ttime: %.3f (diff: %.3f s)
\trho: %g%s%s"""%(trigger.path,ced_link,trigger.time[0],(trigger.time[0]-time),trigger.rho[cWB_conf.id_rho],ifar,f_status)

def findtrigger(cWB_conf,time,idir=""):
    if (idir==""):
      idir=os.getcwd()
    if (not os.path.exists(idir)):
      print("%s%s does not exist. Please check path%s"%(bcolors.FAIL,idir,bcolors.ENDC))
      exit()
    if (not os.path.exists("%s/cWB_conf.py"%idir)):
      print("%s%s is not a standard online dir. Please check path%s"%(bcolors.WARNING,idir,bcolors.ENDC))
      exit()

    if (not idir==cWB_conf.online_dir):
        wrong_dir=cWB_conf.online_dir
        cWB_conf.online_dir=cWB_conf.online_dir.replace(wrong_dir,idir)
        cWB_conf.run_dir=cWB_conf.run_dir.replace(wrong_dir,idir)
        cWB_conf.config_dir=cWB_conf.config_dir.replace(wrong_dir,idir)
        cWB_conf.bkg_run_dir=cWB_conf.bkg_run_dir.replace(wrong_dir,idir)

    date=get_date_from_gps(int(time))

    final_res=""
    analysed=False
    found=False
    others_t=[]

    if (hasattr(cWB_conf,"backup_dir")):
       dirs=glob.glob("%s/JOBS/%s/*"%(cWB_conf.backup_dir,date))
    else:
       dirs=glob.glob("%s/JOBS/%s/*"%(cWB_conf.run_dir,date))

    sel_dirs=list(filter(lambda x: float(x.split("/")[-1].split("-")[0])-cWB_conf.seg_duration<time and float(x.split("/")[-1].split("-")[1])>time, dirs))
    sel_dirs.sort()

    status=["%sNot passed cuts%s"%(bcolors.FAIL,bcolors.ENDC),"%sPassed cuts but low rho%s"%(bcolors.WARNING,bcolors.ENDC),"","%sSubmitted to gracedb%s"%(bcolors.OK,bcolors.ENDC),"%sSubmitted to gracedb%s, low far"%(bcolors.OK,bcolors.ENDC)]

    if (len(sel_dirs)>0):
     analysed=True
     min_diff=float(sel_dirs[-1].split("/")[-1].split("-")[1])-float(sel_dirs[0].split("/")[-1].split("-")[0])
     for d in sel_dirs:
       f=open("%s/job.pickle"%d,"rb")
       j=pickle.load(f)
       f.close()
       if (len(j.triggers_all)>0):
         for t in j.triggers_all:
          if (float(t.start[0])<time and float(t.stop[0])>time and t.topublish==False):
            others_t.append(t)
          if (float(t.start[0])<time and float(t.stop[0])>time and t.topublish==True):
            found=True
            final_res+="%sTrigger has been found.\n%s%s"%(bcolors.OK,bcolors.ENDC,getstring(t,cWB_conf,time,status))
            if (t.selected==3):
               final_res+=" (%s)"%(t.graceid)
            if (t.selected==0 and hasattr(cWB_conf,"Cuts_list")):
               final_res+="\n%sAnalysis on applied Cuts:%s"%(bcolors.HEADER,bcolors.ENDC)
               result=["%snot passed%s"%(bcolors.FAIL,bcolors.ENDC),"%spassed%s"%(bcolors.OK,bcolors.ENDC)]
               iroot = ROOT.TFile(t.root_file)
               itree = iroot.Get("waveburst")
               Cuts_file="%s/Cuts.hh"%(cWB_conf.config_dir)
               before=list(ROOT.gROOT.GetListOfGlobals())
               ROOT.gROOT.LoadMacro(Cuts_file)
               after=list(ROOT.gROOT.GetListOfGlobals())
               diff = [x for x in after if x not in before]
               names=list(map(lambda x: x.GetName(), diff))
               res=[]
               for c in names:
                  g=ROOT.gROOT.GetListOfGlobals().FindObject(c)
                  cut = cppyy.bind_object(g.GetAddress(),'TCut')
                  itree.Draw("run",cut,"goff")
                  res.append(itree.GetSelectedRows())
               iroot.Close()
               for c,r in zip(names,res):
                   final_res+="\n\t%s\t%s"%(c,result[r])
            else:
                final_res+="\n\tSearch bin: %s"%t.cutclass
          elif (t.topublish==True):
              t_diff=abs(t.time[0]-time)
              if (t_diff<min_diff):
                  min_diff=t_diff
                  near_trigger=t

    print("%s%s\nResult about trigger with GPS=%s on analysis dir %s%s"%(bcolors.HEADER,"="*50,time,idir,bcolors.ENDC))
    if (analysed==False):
        print("%sThis time has not been analyzed%s"%(bcolors.FAIL,bcolors.ENDC))
    elif (found==False):
        print("%sThe time has been analyzed but no trigger has been found%s"%(bcolors.WARNING,bcolors.ENDC))
        print("\n".join(sel_dirs))
        try:
            print("%s%s\nNearest trigger%s\n%s"%(bcolors.HEADER,"-"*50,bcolors.ENDC,getstring(near_trigger,cWB_conf,time,status)))
        except:
            pass
    else:
        print(final_res)
        if (len(others_t)>0):
            print("%sSame trigger found in other jobs%s"%(bcolors.HEADER,bcolors.ENDC))
            for t in others_t:
                print("%s%s\n%s%s"%(bcolors.HEADER,"-"*50,bcolors.ENDC,getstring(t,cWB_conf,time,status,False)))
