/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


// compute xgboost statistic to merged root file

{
  CWB::Toolbox TB;

  int estat;
  Long_t id,size,flags,mt;
  char cmd[1024];

  TString cwb_xgboost_options = TString(gSystem->Getenv("CWB_XGBOOST_OPTIONS"));

  // check if cwb_xgboost_options contains '--' than cwb_xgboost_options
  // is used to extract all xgboost parameters
  // eg: cwb_xgboost tuning '--nfile nfile.py --sfile sfile.py --ofile output/file.out --nfrac 0.5 --sfrac 0.5 --search blf \
                             --config config/user_xgboost_config.py --learning-rate 0.03 --max-depth 6 --min-child-weight 5 \
                             --gamma 2 --colsample-bytree 1 --subsample 0.6 --balance-slope q=2.0 --balance-balance A=1     \
                             --caps-rho0 20  --fgetrhor config/user_getrhor.py'

  TString cwb_xgb_nfile    = "";	// python file with the list (flist=[file1,...,fileN]) of background events root files used for tuning
  TString cwb_xgb_sfile    = "";	// python file with the list (flist=[file1,...,fileN]) of simulation events root files used for tuning
  TString cwb_xgb_ofile    = "";
  TString cwb_xgb_config   = "";	// xgboost configuration file. If declared, it overwrites the default options defined in cwb_xgboost_config.py file
  TString cwb_xgb_fgetrhor = "";	// user function getrhor. If declared, it replaces the default getrhor defined in cwb_xgboost.py file
  TString cwb_xgb_search   = "";	// search option: blf,bld,bhf,bbh,imbhb
  TString scwb_xgb_nfrac   = "";	// fraction ([0,1], def=1) of data to be used for background file tuning (eg: 0.2 -> 20%)
  float   cwb_xgb_nfrac    = -1;
  TString scwb_xgb_sfrac   = "";	// fraction ([0,1], def=1) of data to be used for simulation file tuning (eg: 0.2 -> 20%)
  float   cwb_xgb_sfrac    = -1;
  TString cwb_xgb_verbose  = "";	// true/false -> (enable/disable(def)) verbose output

  // list of XGBoost input hyperparameters tested with tuning (see cwb_xgboost_config.py)
  TString cwb_xgb_learning_rate    = "";
  TString cwb_xgb_max_depth        = "";
  TString cwb_xgb_min_child_weight = "";
  TString cwb_xgb_gamma            = "";
  TString cwb_xgb_colsample_bytree = "";
  TString cwb_xgb_subsample        = "";
  TString cwb_xgb_scale_pos_weight = "";

  // list of cWB input hyperparameters tested with tuning (see cwb_xgboost_config.py)
  TString cwb_xgb_balance_slope    = "";
  TString cwb_xgb_balance_balance  = "";
  TString cwb_xgb_caps_rho0        = "";

  // get the cwb_xgb_nfile
  cwb_xgb_nfile = TB.getParameter(cwb_xgboost_options,"--nfile");
  cwb_xgb_nfile = gSystem->ExpandPathName(cwb_xgb_nfile.Data());
  if(cwb_xgb_nfile=="") {
    if(pp_xgb_nfile!="") {
      cwb_xgb_nfile = gSystem->ExpandPathName(pp_xgb_nfile.Data());;
    } else {
      cout << "\ncwb_xgboost_tuning.C : Error - nfile not defined" << endl << endl;
      cout << "- must be defined from command line (--nfile = ...) or in user_pparameterc.C (pp_xgb_nfile = ...)" << endl << endl;
      gSystem->Exit(1);
    }
  }
  // get the cwb_xgb_sfile
  cwb_xgb_sfile = TB.getParameter(cwb_xgboost_options,"--sfile");
  cwb_xgb_sfile = gSystem->ExpandPathName(cwb_xgb_sfile.Data());
  if(cwb_xgb_sfile=="") {
    if(pp_xgb_sfile!="") {
      cwb_xgb_sfile = gSystem->ExpandPathName(pp_xgb_sfile.Data());;
    } else {
      cout << "\ncwb_xgboost_tuning.C : Error - sfile not defined" << endl << endl;
      cout << "- must be defined from command line (--sfile = ...) or in user_pparameterc.C (pp_xgb_sfile = ...)" << endl << endl;
      gSystem->Exit(1);
    }
  }
  // get the cwb_xgb_ofile
  cwb_xgb_ofile = TB.getParameter(cwb_xgboost_options,"--ofile");
  cwb_xgb_ofile = gSystem->ExpandPathName(cwb_xgb_ofile.Data());
  // get the cwb_xgb_ofile
  if(cwb_xgb_ofile=="") {
    if(pp_xgb_ofile!="") {
      cwb_xgb_ofile = gSystem->ExpandPathName(pp_xgb_ofile.Data());;
    } else {
      cout << "\ncwb_xgboost_tuning.C : Error - ofile not defined" << endl << endl;
      cout << "- must be defined from command line (--ofile = ...) or in user_pparameterc.C (pp_xgb_ofile = ...)" << endl << endl;
      gSystem->Exit(1);
    }
  }
  cwb_xgb_config = TB.getParameter(cwb_xgboost_options,"--config");
  cwb_xgb_config = gSystem->ExpandPathName(cwb_xgb_config.Data());
  if(cwb_xgb_config=="") {
    if(pp_xgb_config!="") {
       cwb_xgb_config = gSystem->ExpandPathName(pp_xgb_config.Data());
    } else {
      cout << "\ncwb_xgboost_tuning.C : Error - config not defined" << endl << endl;
      cout << "- must be defined from command line (--config = ...) or in user_pparameterc.C (pp_xgb_config = ...)" << endl << endl;
      gSystem->Exit(1);
    }
  }
  // get the cwb_xgb_fgetrhor
  cwb_xgb_fgetrhor = TB.getParameter(cwb_xgboost_options,"--fgetrhor");
  cwb_xgb_fgetrhor = gSystem->ExpandPathName(cwb_xgb_fgetrhor.Data());
  if(cwb_xgb_fgetrhor=="") {
    if(pp_xgb_fgetrhor!="") {
      cwb_xgb_fgetrhor = gSystem->ExpandPathName(pp_xgb_fgetrhor.Data());;
    }
  }
  // get the cwb_xgb_search
  cwb_xgb_search = TB.getParameter(cwb_xgboost_options,"--search");
  if(cwb_xgb_search=="") {
    if(pp_xgb_search!="") {
      cwb_xgb_search = pp_xgb_search;
    } else {
      cout << "\ncwb_xgboost_tuning.C : Error - search not defined" << endl << endl;
      cout << "- must be defined from command line (--search = ...) or in user_pparameterc.C (pp_xgb_search = ...)" << endl << endl;
      gSystem->Exit(1);
    }
  }
  // get the cwb_xgb_nfrac
  scwb_xgb_nfrac = TB.getParameter(cwb_xgboost_options,"--nfrac");
  if(scwb_xgb_nfrac!="") {
    cwb_xgb_nfrac = scwb_xgb_nfrac.Atof();
  } else {
    if(pp_xgb_nfrac!=-1) {
      cwb_xgb_nfrac = pp_xgb_nfrac;
    } else {
      cout << "\ncwb_xgboost_tuning.C : Error - nfrac not defined" << endl << endl;
      cout << "- must be defined from command line (--nfrac = ...) or in user_pparameterc.C (pp_xgb_nfrac = ...)" << endl << endl;
      gSystem->Exit(1);
    }
  }
  // get the cwb_xgb_sfrac
  scwb_xgb_sfrac = TB.getParameter(cwb_xgboost_options,"--sfrac");
  if(scwb_xgb_sfrac!="") {
    cwb_xgb_sfrac = scwb_xgb_sfrac.Atof();
  } else {
    if(pp_xgb_sfrac!=-1) {
      cwb_xgb_sfrac = pp_xgb_sfrac;
    } else {
      cout << "\ncwb_xgboost_tuning.C : Error - sfrac not defined" << endl << endl;
      cout << "- must be defined from command line (--sfrac = ...) or in user_pparameterc.C (pp_xgb_sfrac = ...)" << endl << endl;
      gSystem->Exit(1);
    }
  }
  // get the cwb_xgb_verbose
  if(TB.getParameter(cwb_xgboost_options,"--verbose")=="") cwb_xgb_verbose="false";
  else cwb_xgb_verbose=TB.getParameter(cwb_xgboost_options,"--verbose");

  cwb_xgb_learning_rate    = TB.getParameter(cwb_xgboost_options,"--learning-rate");
  cwb_xgb_max_depth        = TB.getParameter(cwb_xgboost_options,"--max-depth");
  cwb_xgb_min_child_weight = TB.getParameter(cwb_xgboost_options,"--min-child-weight");
  cwb_xgb_gamma            = TB.getParameter(cwb_xgboost_options,"--gamma");
  cwb_xgb_colsample_bytree = TB.getParameter(cwb_xgboost_options,"--colsample-bytree");
  cwb_xgb_subsample        = TB.getParameter(cwb_xgboost_options,"--subsample");
  cwb_xgb_scale_pos_weight = TB.getParameter(cwb_xgboost_options,"--scale_pos_weight");
  cwb_xgb_balance_slope    = TB.getParameter(cwb_xgboost_options,"--balance-slope");
  cwb_xgb_balance_balance  = TB.getParameter(cwb_xgboost_options,"--balance-balance");
  cwb_xgb_caps_rho0        = TB.getParameter(cwb_xgboost_options,"--caps-rho0");

  // check if search is an ailable option
  if((cwb_xgb_search!="bbh")&&(cwb_xgb_search!="imbhb")&&(cwb_xgb_search!="blf")&&(cwb_xgb_search!="bhf")&&(cwb_xgb_search!="bld")) {
    cout << "cwb_xgboost_tuning.C : Error - (--search) is not defined, available options are bbh/imbhb/blf/bhf/bld" << endl << endl;
    gSystem->Exit(1);
  }
  // check if nfrac is in ]0,1] range
  if(cwb_xgb_nfrac<=0 || cwb_xgb_nfrac>1) {
    cout << "cwb_xgboost_tuning.C : Error - (--nfrac) must be in the range ]0,1]" << endl << endl;
    gSystem->Exit(1);
  }
  // check if sfrac is in [0,1] range
  if(cwb_xgb_sfrac<=0 || cwb_xgb_sfrac>1) {
    cout << "cwb_xgboost_tuning.C : Error - (--sfrac) must be in the range ]0,1]" << endl << endl;
    gSystem->Exit(1);
  }
  // check if xgb file config exist
  if(cwb_xgb_config!="") TB.checkFile(cwb_xgb_config);
  // check if xgb file fgetrhor exist
  if(cwb_xgb_fgetrhor!="") {
    TB.checkFile(cwb_xgb_fgetrhor);
  }
  // check if ofile already exist
  if(cwb_xgb_ofile=="") {
    cout << "cwb_xgboost_tuning.C : Error - (--ofile) is not defined !!!" << endl << endl;
    gSystem->Exit(1);
  } else {
    // check if ofile already exist
    bool overwrite=TB.checkFile(cwb_xgb_ofile,true);
    if(!overwrite) gSystem->Exit(1);
  }
  // check if input bkg root file exist
  TB.checkFile(cwb_xgb_nfile);
  // check if input sim root file exist
  TB.checkFile(cwb_xgb_sfile);
  // check if verbose is an ailable option
  if((cwb_xgb_verbose!="false")&&(cwb_xgb_verbose!="true")) {
    cout << "cwb_xgboost_tuning.C : Error - (--verbose) is not defined, available options are true/false" << endl << endl;
    gSystem->Exit(1);
  }

  cwb_xgboost_options    =
  " --nfile " 		 +  cwb_xgb_nfile +
  " --sfile " 		 +  cwb_xgb_sfile +
  " --ofile " 		 +  cwb_xgb_ofile +
  " --search " 		 +  cwb_xgb_search +
  " --nfrac " 		 +  TString::Format("%g",cwb_xgb_nfrac) +
  " --sfrac " 		 +  TString::Format("%g",cwb_xgb_sfrac) +
  " --verbose " 	 +  cwb_xgb_verbose +
  " --learning-rate " 	 +  cwb_xgb_learning_rate +
  " --max-depth " 	 +  cwb_xgb_max_depth +
  " --min-child-weight " +  cwb_xgb_min_child_weight +
  " --gamma " 		 +  cwb_xgb_gamma +
  " --colsample-bytree " +  cwb_xgb_colsample_bytree +
  " --subsample " 	 +  cwb_xgb_subsample +
  " --scale_pos_weight " +  cwb_xgb_scale_pos_weight +
  " --balance-slope   "  +  cwb_xgb_balance_slope +
  " --balance-balance "  +  cwb_xgb_balance_balance +
  " --caps-rho0 "        +  cwb_xgb_caps_rho0;

  if(cwb_xgb_config!="")   cwb_xgboost_options += " --config "+cwb_xgb_config;
  if(cwb_xgb_fgetrhor!="") cwb_xgboost_options += " --fgetrhor "+cwb_xgb_fgetrhor;

  // compute xgboost tuning
  cwb_xgboost_options=gSystem->ExpandPathName(cwb_xgboost_options.Data());
  sprintf(cmd,"python3 -B %s %s", gSystem->ExpandPathName("$CWB_SCRIPTS/cwb_xgboost_tuning.py"), cwb_xgboost_options.Data());
  cout << cmd << endl;
  int ret=gSystem->Exec(cmd);
  if(ret) {cout << endl << "Error  while executing python cwb_xgboost_tuning.py script !!!" << endl << endl;exit(1);}

  exit(0);
}
