# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Gabriele Vedovato, Mishra Tanmaya, Sergey Klimenko 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import io
import logging
import shutil, os
import sys
import ctypes
import ROOT
import numpy as np
import pickle
import xgboost as xgb
import pandas as pd
#from ROOT import *
from ROOT import gSystem
from ctypes import *
import string
import time
import copy
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import cwb_xgboost as cwb
from matplotlib import rcParams

#plotting options
rcParams["text.usetex"] = True
rcParams["font.serif"] = "Times New Roman"
rcParams["font.family"] = "Serif"
rcParams["xtick.labelsize"]=16
rcParams["ytick.labelsize"]=16
rcParams["legend.fontsize"]=12
rcParams["axes.labelsize"]=16

# load wavelet library
if(os.getenv('_USE_CMAKE')==None):
  gSystem.Load("wavelet.so")
else:
  gSystem.Load("%s/lib64/cwb.so"%os.environ["HOME_WAT_INSTALL"])

# python3 cwb_xgboost_training.py nfile=bkg_wave.root sfile=sim_wave.root model=output_xgb_model.dat/json

def getopts(arguments):

   nfile=""		# bkg wave file
   sfile=""		# sim wave file
   model=""		# output trained xgb model
   config=""	        # input xgb parameters
   search=""	        # input search type (bbh/imbhb/blf/bhf/bld)
   nfrac=1		# fraction of events selected from bkg wave
   sfrac=1		# fraction of events selected from sim wave
   verbose=False
   dump=False		# dump plots
   model_ext=".dat"     # model file name default extension, for xgb_version>=1.7.1 is setup as '.json'
   how2use="\n how to use -> python3 cwb_xgboost_training.py nfile=wave.root model=model.dat/json\n"

   for s in arguments:
    if (s.find("nfile=")!=-1):
      nfile=s.replace("nfile=","")
    elif (s.find("sfile=")!=-1):
      sfile=s.replace("sfile=","")
    elif (s.find("model=")!=-1):
      model=s.replace("model=","")
    elif (s.find("config=")!=-1):
      config=s.replace("config=","")
    elif (s.find("search=")!=-1):
      search=s.replace("search=","")
    elif (s.find("verbose")!=-1):
      verbose=True
    elif (s.find("dump")!=-1):
      dump=True
    elif (s.find("nfrac=")!=-1):
      snfrac=s.replace("nfrac=","")
      nfrac=float(snfrac)
    elif (s.find("sfrac=")!=-1):
      ssfrac=s.replace("sfrac=","")
      sfrac=float(ssfrac)
   if (len(nfile)==0):
     print("Error, no input file specified")
     print(how2use)
     exit(1)
   if (len(sfile)==0):
     print("Error, no output file specified")
     print(how2use)
     exit(1)
   if (len(model)==0):
     print("Error, no input model specified")
     print(how2use)
     exit(1)
   if (search!='bbh' and search!='imbhb' and search!='blf' and search!='bhf' and search!='bld'):
     print("Error, no input search specified, available options are: bbh/imbhb/blf/bhf/bld")
     print(how2use)
     exit(1)
   if ((nfile.find(".root")==-1) and (nfile.find(".py")==-1)):
     print("Error, wrong wave file extension, must be .root/.py")
     print(how2use)
     exit(1)
   if ((sfile.find(".root")==-1) and (sfile.find(".py")==-1)):
     print("Error, wrong mdc file extension, must be .root/.py")
     print(how2use)
     exit(1)
   xgb_version = xgb.__version__
   tokens = xgb_version.split('.')
   if((int(tokens[0])>=1)&(int(tokens[1])>=7)&(int(tokens[2])>=1)): model_ext = ".json"
   if (model.find(model_ext)==-1):
     print("Error, xgboost version = "+xgb_version+" -> wrong output model file name extension, must be "+model_ext)
     print(how2use)
     exit(1)
   if (len(config)!=0):
     if (config.find(".py")==-1):
       print("Error, wrong input config extension, must be .py")
       print(how2use)
       exit(1)
   if (nfrac <= 0 or nfrac > 1):
     print("Error, wrong input bkg fraction selection (nfrac), must be ]0, 1]")
     print(how2use)
     exit(1)
   if (sfrac <= 0 or sfrac > 1):
     print("Error, wrong input sim fraction selection (sfrac), must be ]0, 1]")
     print(how2use)
     exit(1)

   print("\ninput cwb_xgboost parameters\n")
   print(" input wave_root/list_wave_py file      = ", nfile)
   print(" output wave_root/list_wave_py file     = ", sfile)
   print(" xgb model                              = ", model)
   print(" xgb config                             = ", config)
   print(" search type                            = ", search)
   print(" bkg fraction used for training         = ", nfrac)
   print(" sim fraction used for training         = ", sfrac)
   print(" verbose                                = ", verbose)
   print(" dump                                   = ", dump)
   print("\n")

   return nfile,sfile,model,config,search,nfrac,sfrac,verbose,dump,model_ext

def dump_config(model, config, nfile, sfile, ML_list, ML_caps, ML_balance, ML_options, xgb_params, nfrac, sfrac, search, model_ext):

   if (model.find(model_ext)!=-1):
     ofile=model.replace(model_ext,".cfg")
   else:
     print('\nmodel file ' + model + ' with wrong extension, must be '+model_ext+'\n')
     exit(1)
 
   print('\nwrite config to file:'+ofile+'\n')
   fconfig = open(ofile, "w")

   old_stdout = sys.stdout
   new_stdout = io.StringIO()
   sys.stdout = new_stdout
 
   print('\n')
   print('----------------------------------------------------')
   print('- Training config parameters used to generate model -')
   print('----------------------------------------------------\n')
   print(' '+model)
   print('\n')
   print('-----------------')
   print('- BKG file list -')
   print('-----------------\n')
   if (nfile.find(".py")==-1):
     print(' '+nfile)
   else:
     cwb.execfile(nfile,globals())
     print(' ',flist,'\n')
   print('\n')
   print('-----------------')
   print('- SIM file list -')
   print('-----------------\n')
   if (sfile.find(".py")==-1):
     print(' '+sfile)
   else:
     cwb.execfile(sfile,globals())
     print(' ',flist,'\n')
   print('\n')
   print('-----------------')
   print('- ML_list       -')
   print('-----------------\n')
   print(' '+str(ML_list))
   print('\n\n')
   print('-----------------')
   print('- ML_caps       -')
   print('-----------------\n')
   print(' '+str(ML_caps))
   print('\n\n')
   print('-----------------')
   print('- ML_balance    -')
   print('-----------------\n')
   print(' '+str(ML_balance))
   print('\n\n')
   print('-----------------')
   print('- ML_options    -')
   print('-----------------\n')
   print(' '+str(ML_options))
   print('\n\n')
   print('-----------------')
   print('- xgb_params    -')
   print('-----------------\n')
   print(' '+str(xgb_params))
   print('\n\n')
   print('-----------------')
   print('- nfrac         -')
   print('-----------------\n')
   print(' '+str(nfrac))
   print('\n\n')
   print('-----------------')
   print('- sfrac         -')
   print('-----------------\n')
   print(' '+str(sfrac))
   print('\n\n')
   print('-----------------')
   print('- search        -')
   print('-----------------\n')
   print(' '+search)
   print('\n\n')

   output = new_stdout.getvalue()
   sys.stdout = old_stdout

   fconfig.write(output)
   fconfig.close()

   # copy user_xgboost_config.py into model dir 
   if (len(config)!=0):
     model_dirname = os.path.dirname(model)
     shutil.copy(config, model_dirname+"/user_xgboost_config.py")
     print("Copy file ",config," to ",model_dirname+"/user_xgboost_config.py") 

def dump_output(XGB_clf, model, xgb_params, model_ext):

   if (model.find(model_ext)!=-1):
     ofile=model.replace(model_ext,".out")
   else:
     print('\nmodel file ' + model + ' with wrong extension, must be '+model_ext+'\n')
     exit(1)

   print('\nXGB Training -> write output file: '+ofile+'\n')
   out = open(ofile, "w")

   old_stdout = sys.stdout
   new_stdout = io.StringIO()
   sys.stdout = new_stdout

   ML_list = XGB_clf.get_booster().feature_names
   print("\nML_list")
   print(ML_list)
   dump_list = XGB_clf.get_booster().get_dump()
   best_score = XGB_clf.best_score
   best_iteration = XGB_clf.best_iteration
   n_tree_limit = XGB_clf.best_ntree_limit
   print("\n")
   print("Best score                        = ", best_score)
   print("Best iteration                    = ", best_iteration)
   print("Number of trees                   = ", n_tree_limit)
   print("Number of leaves in tree 1        = ", dump_list[0].count('leaf'))
   print("Number of leaves in the best tree = ", dump_list[n_tree_limit - 1].count('leaf'))
   n_leaves = 0
   dump_list_limit = dump_list[:n_tree_limit]
   for tree in dump_list_limit:
        n_leaves += tree.count('leaf')
   print("Total number of leaves in the trained model = ", n_leaves)

   print("\nfeature importances:")
   print(list(zip(ML_list,XGB_clf.feature_importances_)))
   print("\nimportance gain:")
   print(XGB_clf.get_booster().get_score(importance_type="gain"))
   print("\nimportance weight:")
   print(XGB_clf.get_booster().get_score(importance_type="weight"))
   print("\nimportance cover:")
   print(XGB_clf.get_booster().get_score(importance_type="cover"))
   print("\nimportance total_gain:")
   print(XGB_clf.get_booster().get_score(importance_type="total_gain"))
   print("\nimportance total_cover:")
   print(XGB_clf.get_booster().get_score(importance_type="total_cover"))
   print("\n")
   output = new_stdout.getvalue()
   sys.stdout = old_stdout
   print('\n'+output+'\n')

   out.write(output)
   out.close()

def dump_plot(XGB_clf, model, model_ext):

   # disable message: 'findfont: Font family 'Serif' not found.'
   logging.getLogger('matplotlib.font_manager').disabled = True

   if (model.find(model_ext)!=-1):
     ofile=model.replace(model_ext,"")
   else:
     print('\nmodel file ' + model + ' with wrong extension, must be '+model_ext+'\n')
     exit(1)
 
   print('\nwrite trained model best tree graph to file:'+ofile+'.png\n')

   dump_list = XGB_clf.get_booster().get_dump()
   n_tree_limit = XGB_clf.best_ntree_limit

   graph_to_save = xgb.to_graphviz(XGB_clf, num_trees = n_tree_limit-1, rankdir='LR')
   graph_to_save.format = 'png'            
   graph_to_save.render(ofile)

   # plot feature importance
   ofile=model.replace(model_ext,"_importance.png")
   print('\nwrite trained model importance plot to file: '+ofile+'\n')

   rcParams["text.usetex"] = False
   fig, ax = plt.subplots(figsize=(18,12))
   xgb.plot_importance(XGB_clf, max_num_features=50, height=0.8, ax=ax)
   plt.savefig(ofile)
   plt.close()
   rcParams["text.usetex"] = True



if __name__ == "__main__":

   #ROOT.ROOT.EnableImplicitMT()		#### BEWARE -> reshuffle entries -> the results are not reproducible !!!

   # get input user parameters
   nfile,sfile,model,config,search,nfrac,sfrac,verbose,dump,model_ext=getopts(sys.argv[1:])

   # load default xgb_config function
   cwb.execfile(os.environ['CWB_SCRIPTS']+'/cwb_xgboost_config.py',globals(),locals())

   # get number of detectors
   nifo = cwb.getnifo(nfile)

   # get xgb params
   xgb_params,ML_list,ML_caps,ML_balance,ML_options=xgb_config(search,nifo)
   if (len(config)!=0):
     # load user xgb_config function, overwrite the default configuration
     ML_defcaps = copy.deepcopy(ML_caps)
     cwb.execfile(config,globals(),locals())
     cwb.update_ML_list(ML_list,ML_defcaps,ML_caps)

   # add parameters used in function plot_hist_freq
   if('frequency' not in ML_options['readfile(vars)']): ML_options['readfile(vars)'].append('frequency')  

   cwb.print_config(xgb_params,ML_list,ML_caps,ML_balance,ML_options)

   seed = xgb_params['seed'];

   # get ML_list_weight
   ML_list_weight = list()
   ML_list_weight.extend(ML_list)
   ML_list_weight.append('classifier')
   if 'penalty' not in ML_list_weight: ML_list_weight.append('penalty')
   if 'ecor' not in ML_list_weight:    ML_list_weight.append('ecor')
   if((ML_caps['Qa']>=0) and ('Qa' not in ML_list_weight)): ML_list_weight.append('Qa')
   if((ML_caps['Qp']>=0) and ('Qp' not in ML_list_weight)): ML_list_weight.append('Qp')
   print(ML_list_weight,"\n")

   # read input root bkg wave file
   npd=cwb.readfile(nfile,0,nfrac,ML_caps,ML_options,verbose,True,False,ML_balance['cuts(training)'])

   # read input root sim wave file
   spd=cwb.readfile(sfile,1,sfrac,ML_caps,ML_options,verbose,False,False,ML_balance['cuts(training)'])

   # save rho0 bkg/sim histogram
   ofile = model.replace(model_ext,"_bkg_sim_rho0_hist.png")
   if(dump): cwb.plot_hist_rho(ofile,npd,spd,'0')

   # save chirp1 bkg/sim histogram
   if (search=='bbh') or (search=='imbhb'):
     ofile = model.replace(model_ext,"_bkg_sim_chirp_hist.png")
     if(dump): cwb.plot_hist_mchirp(ofile,npd,spd)

   # save frequency0 bkg/sim histogram
   ofile = model.replace(model_ext,"_bkg_sim_frequency0_hist.png")
   if(dump): cwb.plot_hist_freq(ofile,npd,spd)

   # retreive from ML_caps the key feature (rho0 cap) for balancing
   rho0_capvalue = -1
   for feature, cap in ML_caps.items():
     if (feature.find("rho0")!=-1):
       rho0_capname  = cwb.getcapname(feature,cap)
       rho0_capvalue = cap
   if(rho0_capvalue<0):
     print('\nError - rho0 not found in the ML_caps, must be defined\n')
     exit(1)
   print('\n rho0_capname = '+rho0_capname+' - rho0_capvalue = '+str(rho0_capvalue))

   # save Qa vs Qp bkg/sim plot
   ofile = model.replace(model_ext,"_bkg_sim_QaQp_plot.png")
   if(dump): cwb.plot_QaQp(ofile,npd,spd,rho_name=rho0_capname,rho_thr=rho0_capvalue)

   # dump 1d plots features
   odir = os.path.dirname(model)+"/mplot1d"
   utag = os.path.basename(model)
   utag = utag.replace(model_ext,"")
   print(odir,"   ",utag )
   if(dump): cwb.mplot1d(npd,spd,odir=odir,utag=utag,goptions=ML_options)

   # dump 2d plots features
   odir = os.path.dirname(model)+"/mplot2d"
   utag = os.path.basename(model)
   utag = utag.replace(model_ext,"")
   print(odir,"   ",utag )
   if(dump): cwb.mplot2d(npd,spd,odir=odir,utag=utag,goptions=ML_options)

   # merge training set BKG+SIM
   tpd = pd.concat([spd, npd], ignore_index = True)
   ncount = tpd[tpd['classifier']==0].shape[0]
   scount = tpd[tpd['classifier']==1].shape[0]
   print("\nrandom(1)  selected events -> size(sim)/size(bkg): " + str(scount) + "/" + str(ncount) 
                                                       + " = " + str(float(scount)/float(ncount)))
   # Sampling high rho0 SIM and BKG events to achieve required class balancing for (rho0 >= rho0_capvalue) events
   # -> num(sim) = num(bkg)
   if(ML_balance['tail(training)']): tpd = cwb.get_balanced_tail(tpd,ML_caps,seed)

   # Splitting dataset into training (90%) and validation (10%) set
   # X_train, X_eval are the input  (train/test) data -> contains the ML_list features
   # y_train, y_eval are the output (train/test) data -> contains the classifier feature
   X_train, X_eval, y_train, y_eval = train_test_split(tpd[ML_list_weight], tpd[['classifier']], test_size=0.10, random_state=seed)
   print("\nsplitted(1) selected events -> size(X_train)/size(X_eval): " + str(X_train.shape[0]) + "/" + str(X_eval.shape[0]))
   print("\nsplitted(1) selected events -> size(y_train)/size(y_eval): " + str(y_train.shape[0]) + "/" + str(y_eval.shape[0]))

   if(ML_balance['bulk(training)']): 
     # Sampling low rho0 SIM and BKG events to achieve required class balancing for (rho0 < rho0_capvalue) events
     ofile_tag = model.replace(model_ext,"")
     X_train,weight = cwb.get_balanced_bulk(X_train,ML_caps,ML_balance,'training',dump,ofile_tag)
   else:
     # BKG binned sample weight
     X_train['weight1'] = 1
     # Storing sample weight
     weight = X_train['weight1']

   # R_eval used to compute the rhor metric scores
   R_eval  = X_eval
   # Using ML_list for training and validation
   X_train = X_train[ML_list]
   X_eval  = X_eval[ML_list]

   # Calling XGBoost Classifier
   XGB_clf = xgb.XGBClassifier(**xgb_params)

   # Training XGBoost model
   print("\nStart Training ...\n")
   start = time.time()
   XGB_clf.fit(X_train, y_train, sample_weight = weight, eval_set=[(X_eval, y_eval)],
               eval_metric=['logloss','auc','aucpr'], verbose=verbose, early_stopping_rounds=50)
   end = time.time()
   print("\nEnd Training. Elapsed time = ", end-start, " (sec)\n")

   """
   # The following code is temporarily commented out because it uses only rhor returned by the default function cwb.getrhor, 
   # must be implemented the definition of the user getrhor function.
   # This code is used only as infos and does not affect the training !!!

   # compute rhor metric scores (see description in the rhor_metric_scores function definition)
   # note: in the training is used only to output infos
   model_dirname  = os.path.dirname(model)
   model_basename = os.path.basename(model)
   model_basename = model_basename.replace(model_ext,'')
   aucpr_rhor_score,auc_rhor_score,f1_rhor_score,aucelf_rhor_score = cwb.rhor_metric_scores(XGB_clf,R_eval,None,search,model_dirname,model_basename)
   print('\n---> rhor metric scores\n')
   print('  -> aucelf_rhor_score  = ',round(aucelf_rhor_score,4))
   print('  -> aucpr_rhor_score   = ',round(aucpr_rhor_score,4))
   print('  -> auc_rhor_score     = ',round(auc_rhor_score,4))
   print('  -> f1_rhor_score      = ',round(f1_rhor_score,4))
   """

   # write the trained XGBoost model
   pickle.dump(XGB_clf, open(model, 'wb'))

   # write the config used to train XGBoost model
   dump_config(model, config, nfile, sfile, ML_list, ML_caps, ML_balance, ML_options, xgb_params, nfrac, sfrac, search, model_ext)

   # write plot XGBoost model
   if(dump): dump_plot(XGB_clf, model, model_ext)

   # output final statistic
   dump_output(XGB_clf, model, xgb_params, model_ext)

   # dump roc
   ofile = model.replace(model_ext,"_roc.png")
   if(dump): cwb.plot_roc(ofile,XGB_clf,X_eval,y_eval)

   # dump pr
   ofile = model.replace(model_ext,"_pr.png")
   if(dump): cwb.plot_pr(ofile,XGB_clf,X_eval,y_eval)

