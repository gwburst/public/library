#  -----------------------------------------------------------------
#  WAT env for CIT CLUSTER
#  -----------------------------------------------------------------

  setenv LINUX_DISTRO	     "RL8"				# Roky Linux 8 distribution
  #setenv LINUX_DISTRO	     "SL7"				# obsolete, mantained only for testing, works only in ldas-pcdev4
  
  unsetenv _USE_CMAKE           # use make

  if (! $?LD_LIBRARY_PATH ) then
    setenv LD_LIBRARY_PATH ""
  endif

  setenv SITE_CLUSTER	     "CIT"
  setenv BATCH_SYSTEM        "CONDOR"

  if ("$LINUX_DISTRO" == RL8) then
    unsetenv _USE_ICC		# disable ICC			(mandatory for RL8)
  else
    setenv   _USE_ICC 1		# enable ICC			(mandatory for SL7)
  endif

  #unsetenv _USE_CPP		# disable C++17
  if ("$LINUX_DISTRO" == RL8) then
    setenv _USE_CPP 17		# enable C++17			(mandatory for RL8)
  else
    setenv _USE_CPP 11		# enable C++11			(mandatory for SL7)
  endif

  #unsetenv _USE_ROOT6		# disable ROOT6
  setenv _USE_ROOT6 1		# enable ROOT6			(mandatory: only ROOT6 is currently enabled !!!)

  unsetenv _USE_PEGASUS		#				(mandatory: pegasus is not longer used !!!)

  if ("$LINUX_DISTRO" == RL8) then
    setenv _USE_CONDA 1		# cWB uses CONDA packages 	(mandatory for RL8)
  else
    unsetenv _USE_CONDA		#				(mandatory for SL7)
  endif

  if (! $?_USE_CONDA ) then
    source /home/waveburst/virtualenv/xgboost/bin/activate	# activate xgboost
  else
    setenv HOME_CONDA "/home/waveburst/.conda/envs/cwb-env" 	
    setenv CPLUS_INCLUDE_PATH "${HOME_CONDA}/include"
    conda activate ${HOME_CONDA}				# activate conda
  endif

#  -----------------------------------------------------------------
#  this section mandatory to compile the WAT libraries
#  -----------------------------------------------------------------

  # LIBRARIES PATH
  setenv HOME_LIBS           "/home/waveburst/SOFT/"		# (SL7 library path)

  # ROOT
  if (! $?_USE_CONDA ) then
    setenv ROOT_VERSION      "root_v6.22.06_icc"
    setenv ROOTSYS 	     "${HOME_LIBS}/ROOT/${ROOT_VERSION}"
  else
    setenv ROOTSYS 	     "${HOME_CONDA}"
  endif

  # FRAMELIB
  if (! $?_USE_CONDA ) then
    setenv HOME_FRLIB        "${HOME_LIBS}/FRAMELIB/libframe-8.41"
  else
    setenv HOME_FRLIB 	     "${HOME_CONDA}"
  endif

  # HEALPIX
  setenv _USE_HEALPIX 1      # enable HEALPix in CWB 		(mandatory !!!)
  if (! $?_USE_CONDA ) then
    setenv HOME_HEALPIX	     "${HOME_LIBS}/HEALPix/Healpix_3.40"
    setenv HOME_CFITSIO      "${HOME_LIBS}/CFITSIO/cfitsio-3.45"
  else
    setenv HOME_HEALPIX      "${HOME_CONDA}"
    setenv HOME_CFITSIO      "${HOME_CONDA}"
  endif

  # LAL
  setenv _USE_LAL 1          # enable LAL in CWB
  if (! $?_USE_CONDA ) then
    #unsetenv _USE_LAL        # disable LAL in CWB
    setenv HOME_LAL          "${HOME_LIBS}/LAL/lalsuite-v6.79"
  else
    setenv HOME_LAL          "${HOME_CONDA}"
  endif
  setenv LAL_INC             "${HOME_LAL}/include"
  setenv LALINSPINJ_EXEC     "${HOME_LAL}/bin/lalapps_inspinj"

  # EBBH/CVODE
  unsetenv _USE_EBBH         # disable EBBH in CWB 	(mandatory for RL8, must be fixed)
  #setenv _USE_EBBH 1        # enable EBBH in CWB	(SL7)
  if (! $?_USE_CONDA ) then
    setenv HOME_CVODE      "${HOME_LIBS}/CVODE/sundials-2.7.0/dist"
  else
    setenv HOME_CVODE        "${HOME_CONDA}"
  endif

#  -----------------------------------------------------------------
#  this section is specific for the CWB pipeline 
#  -----------------------------------------------------------------

  setenv CWB_ANALYSIS        "2G"      # 2G  analysis

  setenv CWB_CONFIG          "${HOME_LIBS}/cWB/tags/config/cWB-OfflineO3-v9-O3a"   # Location of CWB Configuration files, DQ files, FRAMES files (O3a)
  #setenv CWB_CONFIG          "${HOME_LIBS}/cWB/tags/config/cWB-OfflineO3-v9-O3b"   # Location of CWB Configuration files, DQ files, FRAMES files (O3a)

  setenv HOME_WWW            "~waveburst/waveburst/WWW"
  setenv HOME_CED_WWW        "~waveburst/waveburst/WWW/ced"
  setenv HOME_CED_PATH       "/home/waveburst/public_html/waveburst/WWW/ced"
  setenv CWB_DOC_URL         "https://ldas-jobs.ligo.caltech.edu/~waveburst/waveburst/WWW/doc"
  setenv CWB_REP_URL         "https://ldas-jobs.ligo.caltech.edu/~waveburst/waveburst/WWW/reports"
  setenv CWB_GIT_URL         "https://git.ligo.org/cWB"

  setenv HOME_WAT_FILTERS    "${CWB_CONFIG}/XTALKS"
  setenv HOME_BAUDLINE       "${HOME_LIBS}/BAUDLINE/baudline_1.08_linux_x86_64"
  setenv HOME_ALADIN         "${HOME_LIBS}/ALADIN/Aladin.v7.533"

  setenv HOME_SKYMAP_LIB     "${HOME_LIBS}/SKYMAP/skymap_statistics"
  setenv CWB_USER_URL        "https://ldas-jobs.ligo.caltech.edu/~${USER}/reports"

  # select watenv script to be executed remote site : used when _USE_PEGASUS is enabled
  setenv CWB_PEGASUS_WATENV  "/cvmfs/virgo.infn.it/waveburst/SOFT/WAT/trunk/cnaf_watenv.sh"
  setenv CWB_PEGASUS_SITE    "creamce_cnaf"

#  -----------------------------------------------------------------
#  DO NOT MODIFY !!!
#
#  1) In this section the HOME_WAT env is automatically initialized 
#  2) Init Default cWB library 
#  3) Init Default cWB config  
#  -----------------------------------------------------------------

  # HOME_WAT is initalized with directory path name of the this script
  set MYSHELL=`readlink /proc/$$/exe`
  if ( "$MYSHELL" =~ "*tcsh" ) then
    echo "\nEntering in TCSH section..."
    set CWB_CALLED=($_)
    if ( $#CWB_CALLED == 2 ) then       # alias
      set CWB_CALLED=`alias $CWB_CALLED`
    endif
    if ( "$CWB_CALLED" != "" ) then     # called by source 
      set WATENV_SCRIPT=`readlink -f $CWB_CALLED[2]`
    else                                # called by direct excution of the script
      echo "\nError: script must be executed with source command"
      exit 1
    endif
    set script_dir=`dirname $WATENV_SCRIPT`
  endif
  if ( "$MYSHELL" =~ "*bash" ) then
    echo ""
    echo "Entering in BASH section..."
    script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) #_SKIP_CSH2SH_
  endif
  if ( "$MYSHELL" =~ "*zsh" ) then
    echo "\nEntering in ZSH section..."
    script_dir=$( cd "$( dirname "${(%):-%N}" )" && pwd )        #_SKIP_CSH2SH_
  endif
  setenv HOME_WAT           $script_dir
  setenv HOME_WAT_INSTALL   "${HOME_WAT}/tools/install" # define the wat lib/inc

  source $HOME_WAT/tools/config.csh     # init default cWB library
  source $CWB_CONFIG/setup.csh          # init default cWB config
  source $CWB_SCRIPTS/cwb_watenv.sh     # print the cWB enviroment variables

  # use modern style for cWB report banner
  setenv CWB_HTML_INDEX        "${CWB_MACROS}/html_templates/html_index_template_modern.txt"
  
