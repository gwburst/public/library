# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Gabriele Vedovato, Mishra Tanmaya, Sergey Klimenko 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import io
import sys
import argparse
import ctypes
import ROOT
import numpy as np
import pickle
import xgboost as xgb
import pandas as pd
#from ROOT import *
from ROOT import gSystem
from ctypes import *
import string
import time
import copy
from sklearn.metrics import log_loss, make_scorer
from sklearn.model_selection import GridSearchCV, StratifiedKFold
from sklearn.model_selection import train_test_split
from sklearn.metrics import fbeta_score
from sklearn.metrics import brier_score_loss
import cwb_xgboost as cwb

# load wavelet library
if(os.getenv('_USE_CMAKE')==None):
  gSystem.Load("wavelet.so")
else:
  gSystem.Load("%s/lib64/cwb.so"%os.environ["HOME_WAT_INSTALL"])

# python3 cwb_xgboost_tuning.py nfile=bkg_wave.root sfile=sim_wave.root

def getopts():

   how2use="\n how to use -> python3 cwb_xgboost_tuning.py nfile=wave.root\n"

   parser = argparse.ArgumentParser()
   parser.add_argument('--learning-rate',    required=True, help='learning rate for XGB')
   parser.add_argument('--max-depth',        required=True, help='max depth for XGB')
   parser.add_argument('--min-child-weight', required=True, help='min child weight for XGB')
   parser.add_argument('--gamma',            required=True, help='gamma for XGB')
   parser.add_argument('--colsample-bytree', required=True, help='colsample bytree for XGB')
   parser.add_argument('--subsample',        required=True, help='subsample for XGB')
   parser.add_argument('--scale_pos_weight', required=True, help='scale_pos_weight for XGB')
   parser.add_argument('--balance-slope',    required=True, help='scaling for bulk balance')
   parser.add_argument('--balance-balance',  required=True, help='scaling for bulk balance')
   parser.add_argument('--caps-rho0',        required=True, help='rho0 cap')
 
   parser.add_argument('--nfile',            required=True, help='input wave bkg root file')
   parser.add_argument('--sfile',            required=True, help='input wave sim root file')
   parser.add_argument('--ofile',            required=True, help='output parameter file')
   parser.add_argument('--config',           required=False,help='input XGB config file')
   parser.add_argument('--fgetrhor',         required=False,help='input fgetrhor file')
   parser.add_argument('--search',           required=True, help='input search type: bbh/imbhb/blf/blh/bld')
   parser.add_argument('--nfrac',            required=True, help='select nfrac value')
   parser.add_argument('--sfrac',            required=True, help='select sfrac value')
   parser.add_argument('--verbose',          required=False,help='verbose option')
 
   args = parser.parse_args()

   learning_rate    = args.learning_rate
   max_depth        = args.max_depth
   min_child_weight = args.min_child_weight
   gamma            = args.gamma
   colsample_bytree = args.colsample_bytree
   subsample        = args.subsample
   scale_pos_weight = args.scale_pos_weight
   balance_slope    = args.balance_slope
   balance_balance  = args.balance_balance
   caps_rho0        = args.caps_rho0

   nfile            = args.nfile 
   sfile            = args.sfile 
   ofile            = args.ofile 
   config           = args.config 
   fgetrhor         = args.fgetrhor 
   search           = args.search 
   nfrac            = float(args.nfrac) 
   sfrac            = float(args.sfrac) 

   if(args.verbose=='true'):
     verbose=True
   else:
     verbose=False

   if (len(nfile)==0):
     print("Error, no input BKG file specified")
     print(how2use)
     exit()
   if (len(sfile)==0):
     print("Error, no input SIM file specified")
     print(how2use)
     exit()
   if (len(ofile)==0):
     print("Error, no output file specified")
     print(how2use)
     exit()
   if (search!='bbh' and search!='imbhb' and search!='blf' and search!='bhf' and search!='bld'):
     print("Error, no input search specified, available options are: bbh/imbhb/blf/bhf/bld")
     print(how2use)
     exit()
   if ((nfile.find(".root")==-1) and (nfile.find(".py")==-1)):
     print("Error, wrong wave file extension, must be .root/.py")
     print(how2use)
     exit()
   if ((sfile.find(".root")==-1) and (sfile.find(".py")==-1)):
     print("Error, wrong mdc file extension, must be .root/.py")
     print(how2use)
     exit()
   if (ofile.find(".out")==-1):
     print("Error, wrong output extension, must be .out")
     print(how2use)
     exit()
   if (config!=None):
     if (config.find(".py")==-1):
       print("Error, wrong input config extension, must be .py")
       print(how2use)
       exit()
   if (fgetrhor!=None):
     if (fgetrhor.find(".py")==-1):
       print("Error, wrong input fgetrhor extension, must be .py")
       print(how2use)
       exit()
   if nfrac <= 0 or nfrac > 1:
     print("Error, wrong input bkg fraction selection (nfrac), must be ]0, 1]")
     print(how2use)
     exit()
   if sfrac <= 0 or sfrac > 1:
     print("Error, wrong input sim fraction selection (sfrac), must be ]0, 1]")
     print(how2use)
     exit()

   print("\ninput cwb_xgboost parameters\n")
   print(" input BKG wave root file               = ", nfile)
   print(" input SIM wave root file               = ", sfile)
   print(" output file                            = ", ofile)
   print(" xgb config                             = ", config)
   print(" fgetrhor                               = ", fgetrhor)
   print(" search type                            = ", search)
   print(" bkg fraction used for tuning           = ", nfrac)
   print(" sim fraction used for tuning           = ", sfrac)
   print(" verbose                                = ", verbose)
   print("\n")

   return learning_rate,max_depth,min_child_weight,gamma,colsample_bytree,subsample,scale_pos_weight,balance_slope,balance_balance,caps_rho0,nfile,sfile,ofile,config,fgetrhor,search,nfrac,sfrac,verbose

def dump_output(ofile, XGB_clf, xgb_params, ML_caps, ML_balance, ML_options, aucpr_avg, auc_avg,
                fbeta_avg, brier_avg, logloss_avg, mae_avg, aucelf_rhor_avg, aucpr_rhor_avg, auc_rhor_avg, f1_rhor_avg):

   print('\nXGB Hyper-parameters -> write output score file: '+ofile+'\n')
   out = open(ofile, "w")

   old_stdout = sys.stdout
   new_stdout = io.StringIO()
   sys.stdout = new_stdout

   ML_list = XGB_clf.get_booster().feature_names

   # dump ML configuration
   print("\n\n---> ML CONFIGURATION\n")
   print("\nxgb_params="+str(xgb_params))
   print("\nML_list="+str(ML_list))
   print("\nML_caps="+str(ML_caps))
   print("\nML_balance="+str(ML_balance))
   print("\nML_options="+str(ML_options))

   # dump model tree infos
   print("\n\n---> MODEL TREE INFOS\n\n")
   dump_list = XGB_clf.get_booster().get_dump()
   n_tree_limit = XGB_clf.best_ntree_limit
   print("Number_of_trees="+str(n_tree_limit))
   print("Number_of_leaves_in_tree_1="+str(dump_list[0].count('leaf')))
   print("Number_of_leaves_in_the_best_tree="+str(dump_list[n_tree_limit - 1].count('leaf')))
   n_leaves = 0
   dump_list_limit = dump_list[:n_tree_limit]
   for tree in dump_list_limit:
        n_leaves += tree.count('leaf')
   print("Total_number_of_leaves_in_the_trained_model="+str(n_leaves))

   # dump feature importances
   print("\n\n--> FEATURE IMPORTANCES\n")
   print("\nfeature_importances="+str(list(zip(ML_list,XGB_clf.feature_importances_))))
   print("\nimportance_gain="+str(XGB_clf.get_booster().get_score(importance_type="gain")))
   print("\nimportance_weight="+str(XGB_clf.get_booster().get_score(importance_type="weight")))
   print("\nimportance_cover="+str(XGB_clf.get_booster().get_score(importance_type="cover")))
   print("\nimportance_total_gain="+str(XGB_clf.get_booster().get_score(importance_type="total_gain")))
   print("\nimportance_total_cover="+str(XGB_clf.get_booster().get_score(importance_type="total_cover")))

   # dump metric scores
   print("\n\n---> METRIC SCORES\n")
   print('\nAUCPR_score='+str(aucpr_avg))
   print('\nAUC_score='+str(auc_avg))
   print('\nfbeta_score='+str(fbeta_avg))
   print('\nbrier_score='+str(brier_avg))
   print('\nlogloss_score='+str(logloss_avg))
   print('\nmae_score='+str(mae_avg))
   print('\nAUCELF_rhor_score='+str(aucelf_rhor_avg))
   print('\nAUCPR_rhor_score='+str(aucpr_rhor_avg))
   print('\nAUC_rhor_score='+str(auc_rhor_avg))
   print('\nf1_rhor_score='+str(f1_rhor_avg))
   print('')
 
   output = new_stdout.getvalue()
   sys.stdout = old_stdout
   print('\n'+output+'\n')

   out.write(output)
   out.close()


def exec_tuning_with_splits(tpd, ML_list, ML_list_weight, ML_caps, ML_balance, nsplits, seed, search):

   # Sampling high rho0 SIM and BKG events to achieve required class balancing for (rho0 >= rho0_capvalue) events
   # if ML_balance['eval(tuning)']<=0 than tail balance is applied both to train and eval sets (tail with num(sim)=num(bkg))
   if(ML_balance['tail(tuning)'] and ML_balance['eval(tuning)']<=0): tpd = cwb.get_balanced_tail(tpd,ML_caps,seed)

   skf = StratifiedKFold(n_splits = nsplits, shuffle = True, random_state = seed)

   aucelf_rhor_avg  = 0
   aucpr_rhor_avg   = 0
   auc_rhor_avg     = 0
   f1_rhor_avg      = 0
   aucpr_avg        = 0
   auc_avg          = 0
   deteff01_avg     = 0
   logloss_avg      = 0
   mae_avg          = 0
   fbeta_avg        = 0
   brier_avg        = 0
   split_start = time.time()
   split = 1
   for train_index, test_index in skf.split(tpd[ML_list], tpd[['classifier']]):
       X_train, X_eval = tpd[ML_list_weight].iloc[train_index], tpd[ML_list_weight].iloc[test_index]
       y_train, y_eval = tpd[['classifier']].iloc[train_index], tpd[['classifier']].iloc[test_index]
       print("\n -> start split = "+str(split)+' / '+str(nsplits)+' ...\n')
       print("splitted(1) selected events -> size(X_train)/size(X_eval): " + str(X_train.shape[0]) + "/" + str(X_eval.shape[0]))
       print("splitted(1) selected events -> size(y_train)/size(y_eval): " + str(y_train.shape[0]) + "/" + str(y_eval.shape[0]))

       # if ML_balance['eval(tuning)']>0 than tail balance is applied only to train set (tail with num(sim)=num(bkg))
       # this option is mandatory to evaluate correctly the aucelf score when the caps_rho8 parameter is tuned
       # -> every trial is performed with the same number of sim events
       if(ML_balance['tail(tuning)'] and ML_balance['eval(tuning)']>0):
         # apply tail balance to X_train data set
         X_train=cwb.get_balanced_tail(X_train,ML_caps,seed)
         y_train=X_train['classifier']

         # retreive rho0 value from X_eval bkg with percentile=ML_balance['eval(tuning)']
         X_eval = tpd.iloc[test_index]
         rho0 = X_eval.loc[X_eval.classifier==0, 'rho0']
         rho0 = np.sort(rho0)
         eval_perc=ML_balance['eval(tuning)']
         if (eval_perc>100): eval_perc=100
         rho0_perc = np.percentile(rho0,eval_perc)
         print('\n rho0_perc(eval_perc='+str(eval_perc)+') = '+str(round(rho0_perc,4)))

         # select X_eval,y_eval with rho0<rho0_perc
         X_eval = X_eval[ML_list_weight].loc[X_eval['rho0']<rho0_perc]
         y_eval=X_eval['classifier']


       if(ML_balance['bulk(tuning)']):
         # Sampling low rho0 SIM and BKG events to achieve required class balancing for (rho0 < rho0_capvalue) events
         X_train,weight = cwb.get_balanced_bulk(X_train,ML_caps,ML_balance,'tuning',False,'')
       else:
         # BKG binned sample weight
         X_train['weight1'] = 1
         # Storing sample weight
         weight = X_train['weight1']

       # R_eval used to compute the rhor metric scores
       R_eval  = X_eval
       ssample_eval = R_eval.loc[R_eval.classifier == 1]
       nsample_eval = R_eval.loc[R_eval.classifier == 0]
       print("\nR_eval events -> size(sim)/size(bkg): " + str(ssample_eval.shape[0]) + "/" + str(nsample_eval.shape[0]),'\n')

       # Using ML_list for training and validation
       X_train = X_train[ML_list]
       X_eval  = X_eval[ML_list]

       XGB_clf = xgb.XGBClassifier(**xgb_params)

       XGB_clf.fit(X_train, y_train, eval_set=[(X_eval, y_eval)], eval_metric=['mae','logloss','auc','aucpr'], 
                                    verbose=verbose, early_stopping_rounds=50, sample_weight = weight)

       # compute rhor metric scores (see description in the rhor_metric_scores function definition)
       _getrhor = getrhor if (fgetrhor!=None) else cwb.getrhor
       aucpr_rhor_score,auc_rhor_score,f1_rhor_score,aucelf_rhor_score = cwb.rhor_metric_scores(XGB_clf,R_eval,_getrhor,search)

       # compute metric scores
       results = XGB_clf.evals_result()
       eval_df = pd.concat([X_eval, y_eval], axis=1, sort=False)
       eval_df['xgb_eta'] = XGB_clf.predict_proba(eval_df[ML_list])[:,1]
       eval_df['xgb_eta_1'] = XGB_clf.predict(eval_df[ML_list])
       #print(results)

       aucpr_score   = max(results['validation_0']['aucpr'])
       auc_score     = max(results['validation_0']['auc'])
       logloss_score = max(results['validation_0']['logloss'])
       mae_score     = max(results['validation_0']['mae'])
       _fbeta_score  = fbeta_score(y_eval, eval_df['xgb_eta_1'], beta=0.5)
       brier_score   = brier_score_loss(y_eval, eval_df['xgb_eta'])

       print('\n---> metric scores\n')
       print('  -> aucpr_score       = ',round(aucpr_score,4))
       print('  -> auc_score         = ',round(auc_score,4))
       print('  -> logloss_score     = ',round(logloss_score,4))
       print('  -> fbeta_score       = ',round(_fbeta_score,4))
       print('  -> brier_score       = ',round(brier_score,4))
       print('  -> mae_score         = ',round(mae_score,4))
       print('')
       print('  -> aucelf_rhor_score = ',round(aucelf_rhor_score,4))
       print('  -> aucpr_rhor_score  = ',round(aucpr_rhor_score,4))
       print('  -> auc_rhor_score    = ',round(auc_rhor_score,4))
       print('  -> f1_rhor_score     = ',round(f1_rhor_score,4))
       print('\n')

       aucelf_rhor_avg  += aucelf_rhor_score
       aucpr_rhor_avg   += aucpr_rhor_score
       auc_rhor_avg     += auc_rhor_score
       f1_rhor_avg      += f1_rhor_score
       aucpr_avg        += aucpr_score
       auc_avg          += auc_score
       logloss_avg      += logloss_score
       mae_avg          += mae_score
       fbeta_avg        += _fbeta_score
       brier_avg        += brier_score

       split_end = time.time()
       print(" <- end split = "+str(split)+' / '+str(nsplits)," -> split elapsed time = ", split_end-split_start, " (sec)")
       split_start = time.time()
       split += 1

   aucelf_rhor_avg  /= nsplits
   aucpr_rhor_avg   /= nsplits
   auc_rhor_avg     /= nsplits
   f1_rhor_avg      /= nsplits
   aucpr_avg        /= nsplits
   auc_avg          /= nsplits
   logloss_avg      /= nsplits
   mae_avg          /= nsplits
   fbeta_avg        /= nsplits
   brier_avg        /= nsplits

   print('\n')

   dump_output(ofile, XGB_clf, xgb_params, ML_caps, ML_balance, ML_options, aucpr_avg, auc_avg,
               fbeta_avg, brier_avg, logloss_avg, mae_avg, aucelf_rhor_avg, aucpr_rhor_avg, auc_rhor_avg, f1_rhor_avg)


def exec_tuning_no_splits(tpd, ML_list, ML_list_weight, ML_caps, ML_balance, nsplits, seed, search):

   # Sampling high rho0 SIM and BKG events to achieve required class balancing for (rho0 >= rho0_capvalue) events
   # -> num(sim) = num(bkg)
   if(ML_balance['tail(tuning)']): tpd = cwb.get_balanced_tail(tpd,ML_caps,seed)

   # Splitting dataset into tuning (90%) and validation (10%) set
   # X_train, X_eval are the input  (train/test) data -> contains the ML_list features
   # y_train, y_eval are the output (train/test) data -> contains the classifier feature
   X_train, X_eval, y_train, y_eval = train_test_split(tpd[ML_list_weight], tpd[['classifier']], test_size=0.10, random_state=seed)
   print("\nsplitted(1) selected events -> size(X_train)/size(X_eval): " + str(X_train.shape[0]) + "/" + str(X_eval.shape[0]))
   print("\nsplitted(1) selected events -> size(y_train)/size(y_eval): " + str(y_train.shape[0]) + "/" + str(y_eval.shape[0]))

   if(ML_balance['bulk(tuning)']):
     # Sampling low rho0 SIM and BKG events to achieve required class balancing for (rho0 < rho0_capvalue) events
     X_train,weight = cwb.get_balanced_bulk(X_train,ML_caps,ML_balance,'tuning',False,'')
   else:
     # BKG binned sample weight
     X_train['weight1'] = 1
     # Storing sample weight
     weight = X_train['weight1']

   # R_eval used to compute the rhor metric scores
   R_eval  = X_eval
   # Using ML_list for tuning and validation
   X_train = X_train[ML_list]
   X_eval  = X_eval[ML_list]

   # Calling XGBoost Classifier
   XGB_clf = xgb.XGBClassifier(**xgb_params)

   # Tuning XGBoost model
   print("\nStart Tuning with no splits ...\n")
   start = time.time()
   XGB_clf.fit(X_train, y_train, sample_weight = weight, eval_set=[(X_eval, y_eval)],
               eval_metric=['logloss','auc','aucpr'], verbose=verbose, early_stopping_rounds=50)
   end = time.time()
   print("\nEnd Training. Elapsed time = ", end-start, " (sec)\n")

   ML_list = XGB_clf.get_booster().feature_names
   print("\nML_list")
   print(ML_list)
   dump_list = XGB_clf.get_booster().get_dump()
   best_score = XGB_clf.best_score
   best_iteration = XGB_clf.best_iteration
   n_tree_limit = XGB_clf.best_ntree_limit
   print("\n")
   print("Best score                        = ", best_score)
   print("Best iteration                    = ", best_iteration)
   print("Number of trees                   = ", n_tree_limit)
   print("Number of leaves in tree 1        = ", dump_list[0].count('leaf'))
   print("Number of leaves in the best tree = ", dump_list[n_tree_limit - 1].count('leaf'))
   n_leaves = 0
   dump_list_limit = dump_list[:n_tree_limit]
   for tree in dump_list_limit:
        n_leaves += tree.count('leaf')
   print("Total number of leaves in the trained model = ", n_leaves)
   print('\n')

   # compute rhor metric scores (see description in the rhor_metric_scores function definition)
   _getrhor = getrhor if (fgetrhor!=None) else cwb.getrhor
   aucpr_rhor_score,auc_rhor_score,f1_rhor_score,aucelf_rhor_score = cwb.rhor_metric_scores(XGB_clf,R_eval,_getrhor,search)

   dump_output(ofile, XGB_clf, xgb_params, ML_caps, ML_balance, ML_options, aucpr_avg=best_score, auc_avg=0,
               fbeta_avg=0, brier_avg=0, logloss_avg=0, mae_avg=0, aucelf_rhor_avg=aucelf_rhor_score, aucpr_rhor_avg=aucpr_rhor_score, auc_rhor_avg=auc_rhor_score, f1_rhor_avg=f1_rhor_score)


if __name__ == "__main__":

   print("\nStart Tuning ...\n")
   start = time.time()

   #ROOT.ROOT.EnableImplicitMT()         #### BEWARE -> reshuffle entries -> the results are not reproducible !!!

   # get input user parameters
   learning_rate,max_depth,min_child_weight,gamma,colsample_bytree,subsample,scale_pos_weight,balance_slope,balance_balance,caps_rho0,nfile,sfile,ofile,config,fgetrhor,search,nfrac,sfrac,verbose=getopts()

   # load default xgb_config function
   cwb.execfile(os.environ['CWB_SCRIPTS']+'/cwb_xgboost_config.py',globals(),locals())

   # user getrhor function
   if(fgetrhor!=None):
     cwb.execfile(fgetrhor,globals(),locals())

   # get number of detectors
   nifo = cwb.getnifo(nfile)

   # get xgb params
   xgb_params,ML_list,ML_caps,ML_balance,ML_options=xgb_config(search,nifo)
   if (config!=None):
     # load user xgb_config function, overwrite the default configuration
     ML_defcaps = copy.deepcopy(ML_caps)
     cwb.execfile(config,globals(),locals())
     ML_caps['rho0'] = float(caps_rho0)
     cwb.update_ML_list(ML_list,ML_defcaps,ML_caps)
   cwb.print_config(xgb_params,ML_list,ML_caps,ML_balance,ML_options)

   # get ML_list_weight
   ML_list_weight = list()
   ML_list_weight.extend(ML_list)
   ML_list_weight.append('classifier')
   if 'penalty' not in ML_list_weight: ML_list_weight.append('penalty')
   if 'ecor' not in ML_list_weight:    ML_list_weight.append('ecor')
   if 'Qa' not in ML_list_weight:      ML_list_weight.append('Qa')
   if 'Qp' not in ML_list_weight:      ML_list_weight.append('Qp')
   print(ML_list_weight,"\n")

   seed = xgb_params['seed'];

   # set xgb params
   xgb_params['learning_rate']    = float(learning_rate)
   xgb_params['max_depth']        = int(max_depth)
   xgb_params['min_child_weight'] = float(min_child_weight)
   xgb_params['gamma']            = float(gamma)
   xgb_params['colsample_bytree'] = float(colsample_bytree)
   xgb_params['subsample']        = float(subsample)
   xgb_params['scale_pos_weight'] = float(scale_pos_weight)
   print(xgb_params,"\n")

   ML_balance['slope(tuning)']    = balance_slope
   ML_balance['balance(tuning)']  = balance_balance
   print(ML_balance,"\n")

   print(ML_caps,"\n")

   # read input root bkg wave file
   npd=cwb.readfile(nfile,0,nfrac,ML_caps,ML_options,verbose,True,False,ML_balance['cuts(tuning)'])

   # read input root sim wave file
   spd=cwb.readfile(sfile,1,sfrac,ML_caps,ML_options,verbose,False,False,ML_balance['cuts(tuning)'])

   # merge tuning set BKG+SIM
   tpd = pd.concat([spd, npd], ignore_index = True)
   ncount = tpd[tpd['classifier']==0].shape[0]
   scount = tpd[tpd['classifier']==1].shape[0]
   print("\nrandom(1)  selected events -> size(bkg)/size(sim): " + str(ncount) + "/" + str(scount) + " = " + str(float(ncount)/float(scount)))

   if 'nsplits(tuning)' in ML_options: nsplits = ML_options['nsplits(tuning)']
   else:                               nsplits = 10

   if(nsplits>1):
     # exec tuning with multiple data splits
     exec_tuning_with_splits(tpd, ML_list, ML_list_weight, ML_caps, ML_balance, nsplits, seed, search)
   else:
     # exec tuning with no data splits using the the training procedure
     exec_tuning_no_splits(tpd, ML_list, ML_list_weight, ML_caps, ML_balance, nsplits, seed, search)

   end = time.time()
   print("\nEnd Tuning. Elapsed time = ", end-start, " (sec)\n")

