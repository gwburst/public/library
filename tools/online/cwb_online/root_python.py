# Copyright (C) 2019 Marco Drago
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from array import array
import ROOT
import cwb_online.run_utils as run_utils
import os
import subprocess
ROOT.gSystem.Load("%s/lib64/cwb.so"%os.environ["HOME_WAT_INSTALL"])

def getxgbinfo(cWB_conf,option=True):
   lines=open("%s/xgb.txt"%cWB_conf.config_dir).readlines()
   sourcing=lines[0].replace("\n","")
   labels=[]
   freqs=[]
   for l in lines[1:]:
      s=l.split(" ")
      labels.append(s[0])
      cuts=[]
      if (option==True): 
         if (s[1]!="-"):
            cuts.append("(frequency[0]>%s)"%s[1])
         if (s[2]!="-"):
            cuts.append("(frequency[0]<=%s)"%s[2])
         freqs.append("(%s)"%"&&".join(cuts))
      else:
         if (s[1]!="-"):
            cuts.append(run_utils.getnumberfromstring(s[1]))
         else:
            cuts.append(0)
         if (s[2]!="-"):
            cuts.append(run_utils.getnumberfromstring(s[2]))
         else:
            cuts.append(0)
         freqs.append(cuts)
   return sourcing,labels, freqs

def extractlive(root_file,live_file,job_start,job_end):
    iroot = ROOT.TFile(root_file)
    itree = iroot.Get("liveTime")
    otree=itree.CloneTree(0)
    otree.SetDirectory(0)
    live=array('d', [0.])
    otree.SetBranchAddress("live",live)
    itree.GetEntry(0)
    live[0]=job_end-job_start
    otree.Fill()
    oroot=ROOT.TFile(live_file,"RECREATE")
    otree.Write()
    oroot.Close()
    iroot.Close()

def adjustslag(ifile,ofile,s_lag,s_shift):
    shifts=list(map(lambda x: float(x), s_shift.split(",")))
    lags=list(map(lambda x: float(x), s_lag.split(",")))
    iroot = ROOT.TFile(ifile)
    itree = iroot.Get("waveburst")
    otree=itree.CloneTree(0)
    otree.SetDirectory(0)
    slag=array('f', lags)
    time=array('d', [0.]*len(shifts))
    otree.SetBranchAddress("slag",slag)
    itree.SetBranchAddress("time",time)
    otree.SetBranchAddress("time",time)
    for e in range(itree.GetEntries()):
      itree.GetEntry(e)
      for i in range(len(shifts)):
        time[i]-=shifts[i]
      otree.Fill()
    itree_l = iroot.Get("liveTime")
    otree_l=itree_l.CloneTree(0)
    otree_l.SetDirectory(0)
    otree_l.SetBranchAddress("slag",slag)
    for e in range(itree_l.GetEntries()):
      itree_l.GetEntry(e)
      otree_l.Fill()
    #hist=iroot.Get("history");
    oroot=ROOT.TFile(ofile,"RECREATE")
    otree.Write()
    otree_l.Write()
    #hist.Write("history");
    oroot.Close()
    iroot.Close()

def AdjustSlag(ifile,ofile,ss_lag,sshift):
  ROOT.gInterpreter.Declare("""
#ifndef ADJUST_SLAG

#define ADJUST_SLAG
void AdjustSlag(char* ifile, char* ofile, char* ss_lag, char* sshift)
{

   TObjArray* token = TString(ss_lag).Tokenize(TString(","));
   TObjArray* token2 = TString(sshift).Tokenize(TString(","));
   int const nifo=token2->GetEntries();
   int shift[nifo];
   float slag[nifo+1];
   for (int i=0; i<nifo; i++)
     {
        TString s=((TObjString*)token->At(i))->GetString();
        slag[i]=s.Atoi();
       TString s2=((TObjString*)token2->At(i))->GetString();
        shift[i]=s2.Atoi();
     }
   TString s=((TObjString*)token->At(nifo))->GetString();
   slag[nifo]=s.Atoi();
   double time[2*nifo];

   TFile *f_ifile = TFile::Open(ifile);

   TTree* witree = (TTree *) f_ifile->Get("waveburst");
   witree->SetBranchAddress("time",time);
   TTree* wotree = witree->CloneTree(0);
   TFile f_ofile(ofile,"RECREATE");
   wotree->SetDirectory(&f_ofile);
   wotree->SetBranchAddress("slag",slag);
   wotree->SetBranchAddress("time",time);

   for (int i=0; i<witree->GetEntries(); i++)
    {
     witree->GetEvent(i);
     for (int j=0; j<nifo; j++) time[j]-=shift[j];
     wotree->Fill();
    }
   cout << wotree->GetEntries() << endl;

   TTree* itree = (TTree *) f_ifile->Get("liveTime");
   TTree* otree = itree->CloneTree(0);
   otree->SetDirectory(&f_ofile);
   otree->SetBranchAddress("slag",slag);

   for (int i=0; i<itree->GetEntries(); i++)
    {
     itree->GetEvent(i);
     otree->Fill();
    }
   cout << otree->GetEntries() << endl;

   //CWB::History* hist = (CWB::History*) f_ifile->Get("history");

   //hist->Write("history");
   wotree->Write();
   otree->Write();
   f_ofile.Close();

   f_ifile->Close();
   return;
}
#endif""")

  ROOT.AdjustSlag(ifile,ofile,ss_lag,sshift)


def addhistory(ifile,ofile):
    f_ifile=ROOT.TFile(ifile);
    hist=f_ifile.Get("history");
    f_ofile=ROOT.TFile(ofile,"UPDATE");
    hist.Write("history");
    f_ofile.Close();
    f_ifile.Close();

def merge_trees(files,nametree,ofile):
   tree=ROOT.TChain(nametree)
   for f in files:
     tree.Add(f)
   tree.Merge(ofile)

def getpar(parameter,par_file=""):
   ROOT.gROOT.LoadMacro(os.environ['CWB_PARAMETERS_FILE'])
   if (len(par_file)>0):
      if (os.path.exists(par_file)):
         ROOT.gROOT.LoadMacro(par_file)
      else:
         ofile_temp="/tmp/tmp_file.C"
         f=open(ofile_temp,"w")
         print("{%s}"%par_file,file=f)
         f.close()
         ROOT.gROOT.LoadMacro(ofile_temp)
         subprocess.getstatusoutput("rm %s"%ofile_temp)
   if (hasattr(ROOT,parameter)):
       return getattr(ROOT,parameter)
   else:
       return None
